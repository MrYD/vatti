//
//  XLDishWasherDeviceDao.m
//  vatti
//
//  Created by dongxiao on 2018/9/25.
//  Copyright © 2018年 xlink. All rights reserved.
//

#import "XLDishWasherDeviceDao.h"

@implementation XLDishWasherDeviceDao
/**
 查找数据库里面的设备
 
 @param xDevice xDevice
 @param callBack 设备查询回调
 */
+ (void)queryWithXDevice:(XDevice *)xDevice callBack:(XLDishWasherDeviceDaoCallBack)callBack
{
    if (xDevice == nil) {
        if (callBack) {
            callBack(nil);
        }
    }
    
    __block DishWasherDeviceModel *dishWasherDeviceModel;
    
    NSString *productID = xDevice.productID;
    NSString *deviceID = [NSString stringWithFormat:@"%d",xDevice.deviceID];
    NSString *where = [NSString stringWithFormat:@"where %@=%@ and %@=%@",bg_sqlKey(@"productID"),bg_sqlValue(productID),bg_sqlKey(@"deviceID"),bg_sqlValue(deviceID)];
    
    [DishWasherDeviceModel bg_findAsync:nil where:where complete:^(NSArray * _Nullable array) {
        if (array.count > 0) {
            NSArray <DishWasherDeviceModel*> *deviceList = array;
            dishWasherDeviceModel = [deviceList firstObject];
            dishWasherDeviceModel.device = xDevice;
        }else{
            dishWasherDeviceModel = [[DishWasherDeviceModel alloc] initWithXDevice:xDevice];
        }
        
        [[NSOperationQueue mainQueue] addOperationWithBlock:^{
            if (callBack) {
                callBack(dishWasherDeviceModel);
            }
        }];
        
    }];
}

/**
 异步插入或更新设备对象
 
 @param dishWasherDeviceModel 设备模型
 @param callBack 回调
 */
+ (void)saveOrUpdateAsyncWithWaterHeaterDevice:(DishWasherDeviceModel *)dishWasherDeviceModel callBack:(bg_complete_B)callBack
{
    if (dishWasherDeviceModel == nil) {
        return;
    }
    
    [dishWasherDeviceModel bg_saveOrUpdateAsync:^(BOOL isSuccess) {
        [[NSOperationQueue mainQueue] addOperationWithBlock:^{
            if (callBack) {
                callBack(isSuccess);
            }
        }];
    }];
}

/**
 异步删除数据库里面的设备
 
 @param xDevice xDevice
 @param callBack 删除结果回调，如果删除失败，自动回滚
 */
+ (void)deleteAsyncWithXDevice:(XDevice *)xDevice callBack:(bg_complete_B)callBack
{
    if (xDevice == nil) {
        return;
    }
    
    NSString *productID = xDevice.productID;
    NSString *deviceID = [NSString stringWithFormat:@"%d",xDevice.deviceID];
    
    NSString *where = [NSString stringWithFormat:@"where %@=%@ and %@=%@",bg_sqlKey(@"productID"),bg_sqlValue(productID),bg_sqlKey(@"deviceID"),bg_sqlValue(deviceID)];
    
    NSOperationQueue *queue = [[NSOperationQueue alloc] init];
    [queue addOperationWithBlock:^{
        //开启事务
        bg_inTransaction(^BOOL{
            BOOL isSuccess = [DishWasherDeviceModel bg_delete:nil where:where];
            
            [[NSOperationQueue mainQueue] addOperationWithBlock:^{
                if (callBack) {
                    callBack(isSuccess);
                }
            }];
            
            return isSuccess;
        });
    }];
    
}
@end
