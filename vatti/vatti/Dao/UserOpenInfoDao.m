//
//  UserOpenInfoDao.m
//  vatti
//
//  Created by Eric on 2018/8/16.
//  Copyright © 2018年 xlink. All rights reserved.
//

#import "UserOpenInfoDao.h"

@implementation UserOpenInfoDao

+ (void)queryUserOpenInfoModelWithUserID:(NSNumber *)userID withCallBack:(UserOpenInfoDaoCallBack)callBack
{
    if (userID == nil) {
        if (callBack) {
            callBack(nil);
        }
    }
    
    __block UserOpenInfoModel *model;

    NSString *where = [NSString stringWithFormat:@"where %@=%@",bg_sqlKey(@"user_id"),bg_sqlValue(userID)];
    
    [UserOpenInfoModel bg_findAsync:nil where:where complete:^(NSArray * _Nullable array) {
        if (array.count > 0) {
            model = [array firstObject];
        }else{
            model = [[UserOpenInfoModel alloc] init];
        }
        
        [[NSOperationQueue mainQueue] addOperationWithBlock:^{
            if (callBack) {
                callBack(model);
            }
        }];
        
    }];
}

+ (void)saveOrUpdateAsyncWithUserOpenInfoModel:(UserOpenInfoModel *)userOpenInfoModel withCallBack:(bg_complete_B)callBack
{
    if (userOpenInfoModel == nil) {
        return;
    }
    
    [userOpenInfoModel bg_saveOrUpdateAsync:^(BOOL isSuccess) {
        [[NSOperationQueue mainQueue] addOperationWithBlock:^{
            if (callBack) {
                callBack(isSuccess);
            }
        }];
    }];
}

@end
