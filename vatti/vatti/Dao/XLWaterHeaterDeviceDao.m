//
//  XLWaterHeaterDeviceDao.m
//  VattiIOS
//
//  Created by MrYD on 2018/12/24.
//  Copyright © 2018年 Eric. All rights reserved.
//

#import "XLWaterHeaterDeviceDao.h"

@implementation XLWaterHeaterDeviceDao

/**
 查找数据库里面的设备
 
 @param xDevice xDevice
 @param callBack 设备查询回调
 */
+ (void)queryWithXDevice:(XDevice *)xDevice callBack:(XLWaterHeaterDeviceDaoCallBack)callBack
{
    if (xDevice == nil) {
        if (callBack) {
            callBack(nil);
        }
    }
    
    __block WaterHeaterDeviceModel *waterHeartrDeviceModel;

    NSString *productID = xDevice.productID;
    NSString *deviceID = [NSString stringWithFormat:@"%d",xDevice.deviceID];
    NSString *where = [NSString stringWithFormat:@"where %@=%@ and %@=%@",bg_sqlKey(@"productID"),bg_sqlValue(productID),bg_sqlKey(@"deviceID"),bg_sqlValue(deviceID)];
    
    [WaterHeaterDeviceModel bg_findAsync:nil where:where complete:^(NSArray * _Nullable array) {
        if (array.count > 0) {
            NSArray <WaterHeaterDeviceModel*> *deviceList = array;
            waterHeartrDeviceModel = [deviceList firstObject];
            waterHeartrDeviceModel.device = xDevice;
        }else{
            waterHeartrDeviceModel = [[WaterHeaterDeviceModel alloc] initWithXDevice:xDevice];
        }
        
        [[NSOperationQueue mainQueue] addOperationWithBlock:^{
            if (callBack) {
                callBack(waterHeartrDeviceModel);
            }
        }];
        
    }];
}

/**
 异步插入或更新设备对象
 
 @param waterHeaterDeviceModel 设备模型
 @param callBack 回调
 */
+ (void)saveOrUpdateAsyncWithWaterHeaterDevice:(WaterHeaterDeviceModel *)waterHeaterDeviceModel callBack:(bg_complete_B)callBack
{
    if (waterHeaterDeviceModel == nil) {
        return;
    }

    [waterHeaterDeviceModel bg_saveOrUpdateAsync:^(BOOL isSuccess) {
        [[NSOperationQueue mainQueue] addOperationWithBlock:^{
            if (callBack) {
                callBack(isSuccess);
            }
        }];
    }];
}

/**
 异步删除数据库里面的设备
 
 @param xDevice xDevice
 @param callBack 删除结果回调，如果删除失败，自动回滚
 */
+ (void)deleteAsyncWithXDevice:(XDevice *)xDevice callBack:(bg_complete_B)callBack
{
    if (xDevice == nil) {
        return;
    }
    
    NSString *productID = xDevice.productID;
    NSString *deviceID = [NSString stringWithFormat:@"%d",xDevice.deviceID];
    
    NSString *where = [NSString stringWithFormat:@"where %@=%@ and %@=%@",bg_sqlKey(@"productID"),bg_sqlValue(productID),bg_sqlKey(@"deviceID"),bg_sqlValue(deviceID)];
    
    NSOperationQueue *queue = [[NSOperationQueue alloc] init];
    [queue addOperationWithBlock:^{
        //开启事务
        bg_inTransaction(^BOOL{
            BOOL isSuccess = [WaterHeaterDeviceModel bg_delete:nil where:where];
            
            [[NSOperationQueue mainQueue] addOperationWithBlock:^{
                if (callBack) {
                    callBack(isSuccess);
                }
            }];

            return isSuccess;
        });
    }];

}

@end
