//
//  UserOpenInfoDao.h
//  vatti
//
//  Created by Eric on 2018/8/16.
//  Copyright © 2018年 xlink. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "UserOpenInfoModel.h"

NS_ASSUME_NONNULL_BEGIN

@class UserOpenInfoModel;

typedef void(^UserOpenInfoDaoCallBack)(UserOpenInfoModel* _Nullable userOpenInfoModel);

@interface UserOpenInfoDao : NSObject

+ (void)queryUserOpenInfoModelWithUserID:(NSNumber*)userID withCallBack:(UserOpenInfoDaoCallBack)callBack;

+ (void)saveOrUpdateAsyncWithUserOpenInfoModel:(UserOpenInfoModel *)userOpenInfoModel withCallBack:(bg_complete_B)callBack;

@end

NS_ASSUME_NONNULL_END
