//
//  XLDishWasherDeviceDao.h
//  vatti
//
//  Created by dongxiao on 2018/9/25.
//  Copyright © 2018年 xlink. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "DishWasherDeviceModel.h"
#import "XDevice.h"

NS_ASSUME_NONNULL_BEGIN

/**
 设备查询回调
 
 @param xlDishWasherDevice 洗碗机设备
 */
typedef void(^XLDishWasherDeviceDaoCallBack)(DishWasherDeviceModel* _Nullable xlDishWasherDevice);


@interface XLDishWasherDeviceDao : NSObject

/**
 查找数据库里面的设备
 
 @param xDevice xDevice
 @param callBack 设备查询回调
 */
+ (void)queryWithXDevice:(XDevice *)xDevice callBack:(XLDishWasherDeviceDaoCallBack)callBack;

/**
 异步插入或更新设备对象
 
 @param dishWasherDeviceModel 设备模型
 @param callBack 回调
 */
+ (void)saveOrUpdateAsyncWithWaterHeaterDevice:(DishWasherDeviceModel *)dishWasherDeviceModel callBack:(bg_complete_B)callBack;

/**
 异步删除数据库里面的设备
 
 @param xDevice xDevice
 @param callBack 删除结果回调，如果删除失败，自动回滚
 */
+ (void)deleteAsyncWithXDevice:(XDevice *)xDevice callBack:(bg_complete_B)callBack;

@end

NS_ASSUME_NONNULL_END
