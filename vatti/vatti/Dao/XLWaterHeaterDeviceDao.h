//
//  XLWaterHeaterDeviceDao.h
//  VattiIOS
//
//  Created by MrYD on 2018/12/24.
//  Copyright © 2018年 Eric. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "WaterHeaterDeviceModel.h"
#import "XDevice.h"

NS_ASSUME_NONNULL_BEGIN

/**
 设备查询回调

 @param xlWaterHeaterDevice 燃热设备
 */
typedef void(^XLWaterHeaterDeviceDaoCallBack)(WaterHeaterDeviceModel* _Nullable xlWaterHeaterDevice);

@interface XLWaterHeaterDeviceDao : NSObject

/**
 查找数据库里面的设备

 @param xDevice xDevice
 @param callBack 设备查询回调
 */
+ (void)queryWithXDevice:(XDevice *)xDevice callBack:(XLWaterHeaterDeviceDaoCallBack)callBack;

/**
 异步插入或更新设备对象

 @param waterHeaterDeviceModel 设备模型
 @param callBack 回调
 */
+ (void)saveOrUpdateAsyncWithWaterHeaterDevice:(WaterHeaterDeviceModel *)waterHeaterDeviceModel callBack:(bg_complete_B)callBack;

/**
 异步删除数据库里面的设备

 @param xDevice xDevice
 @param callBack 删除结果回调，如果删除失败，自动回滚
 */
+ (void)deleteAsyncWithXDevice:(XDevice *)xDevice callBack:(bg_complete_B)callBack;

@end

NS_ASSUME_NONNULL_END
