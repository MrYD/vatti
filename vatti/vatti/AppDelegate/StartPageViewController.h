//
//  StartPageViewController.h
//  vatti
//
//  Created by BZH on 2018/6/16.
//  Copyright © 2018年 xlink. All rights reserved.
//

/**
 *  启动页
 *
 *  @author BZH
 */

#import "BaseViewController.h"

@interface StartPageViewController : BaseViewController

@end
