//
//  AppDelegate.m
//  vatti
//
//  Created by 许瑞邦 on 2018/4/23.
//  Copyright © 2018年 xlink. All rights reserved.
//

#import "AppDelegate.h"
#import "AppDelegate+ThirdPart.h"
#import "StartPageViewController.h"
#import "XLModuleManager.h"
#import <UMShare/UMShare.h>

#define APPLE_URL               @"www.apple.com"

@interface AppDelegate ()

@end

@implementation AppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    //设置数据库组件写入之后不关闭
    bg_setDisableCloseDB(YES);
    
    //开启网络监控
    [self setupNetNotifier];

    //setup with plist
    [self setupWithPlistAndApplication:application andOptions:launchOptions];
    
    //Bugly
    [self setupBugly];
    
    //设置友盟
    [self setupUM]; 

    //设置根控制器
    [self setRootVC];
    
    return YES;
}

- (BOOL)application:(UIApplication *)app openURL:(NSURL *)url options:(NSDictionary<UIApplicationOpenURLOptionsKey, id> *)options
{
    //6.3的新的API调用，是为了兼容国外平台(例如:新版facebookSDK,VK等)的调用[如果用6.2的api调用会没有回调],对国内平台没有影响。
    BOOL result = [[UMSocialManager defaultManager] handleOpenURL:url options:options];
    if (!result) {
        // 其他如支付等SDK的回调
    }
    return result;
}

- (BOOL)application:(UIApplication *)application openURL:(NSURL *)url sourceApplication:(NSString *)sourceApplication annotation:(id)annotation
{
    //6.3的新的API调用，是为了兼容国外平台(例如:新版facebookSDK,VK等)的调用[如果用6.2的api调用会没有回调],对国内平台没有影响
    BOOL result = [[UMSocialManager defaultManager] handleOpenURL:url sourceApplication:sourceApplication annotation:annotation];
    if (!result) {
        // 其他如支付等SDK的回调
    }
    return result;
}

- (BOOL)application:(UIApplication *)application handleOpenURL:(NSURL *)url
{
    BOOL result = [[UMSocialManager defaultManager] handleOpenURL:url];
    if (!result) {
        // 其他如支付等SDK的回调
    }
    return result;
}

- (void)applicationWillResignActive:(UIApplication *)application {
    
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    [XL_DATA_SOURCE updateUser];
}

- (void)applicationWillEnterForeground:(UIApplication *)application {

}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    
}

- (void)applicationWillTerminate:(UIApplication *)application {

}

/**
 根据pist文件设置
 */
- (void)setupWithPlistAndApplication:(UIApplication *)application andOptions:(NSDictionary *)launchOptions
{
    NSString *modulePlist = [[NSBundle mainBundle] pathForResource:@"XLModules" ofType:@"plist"];
    [XL_MODULE_MANAGER loadModulesWithPlistFile:modulePlist];
    [XL_MODULE_MANAGER application:application didFinishLaunchingWithOptions:launchOptions];
}

/**
 设置根控制器
 */
- (void)setRootVC
{
    StartPageViewController *startVC = [[StartPageViewController alloc] init];
    
    self.window = [[UIWindow alloc] initWithFrame:[UIScreen mainScreen].bounds];
    self.window.rootViewController = startVC;
    [self.window makeKeyAndVisible];
}

/**
 网络监控
 */
- (void)setupNetNotifier {
    //设置网络地址
    NSString *remoteHostName = APPLE_URL;
    
    //加入地址
    self.hostReachability = [Reachability reachabilityWithHostName:remoteHostName];
    NetworkStatus Status = [self.hostReachability currentReachabilityStatus];
    
    //启动
    [self.hostReachability startNotifier];
}

@end
