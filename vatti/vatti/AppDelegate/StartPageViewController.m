//
//  StartPageViewController.m
//  vatti
//
//  Created by BZH on 2018/6/16.
//  Copyright © 2018年 xlink. All rights reserved.
//

#import "StartPageViewController.h"
#import "MainTabViewController.h"
#import "XLUtilTool.h"

#ifdef DEBUG
#define Time 0
#else
#define Time 2
#endif

@interface StartPageViewController ()

///计时器
@property (strong, nonatomic) NSTimer *timer;

@end

@implementation StartPageViewController

- (void)viewDidLoad {
    [super viewDidLoad];
}

- (void)buildData
{
    [super buildData];
    
    self.timer = [NSTimer scheduledTimerWithTimeInterval:Time target:self selector:@selector(handleTimer:) userInfo:nil repeats:NO];
}

#pragma mark - Action
- (void)handleTimer:(NSTimer *)sender
{
    [self skipStartPageAndSetRootMainTabVC];
}

- (IBAction)skipAction:(UIButton *)sender {
    [self skipStartPageAndSetRootMainTabVC];
}

#pragma mark - UI
- (void)skipStartPageAndSetRootMainTabVC
{
    [self destoryTimer];
    
    MainTabViewController *tabbar = [[MainTabViewController alloc] init];
    [XLUtilTool setRootViewController:tabbar animationOptions:UIViewAnimationOptionTransitionCrossDissolve duration:0.3];
}

- (void)destoryTimer
{
    if (self.timer != nil){
        [self.timer invalidate];
        self.timer = nil;
    }
}

@end
