//
//  AppDelegate+ThirdPart.h
//  vatti
//
//  Created by Eric on 2018/7/2.
//  Copyright © 2018年 xlink. All rights reserved.
//

#import "AppDelegate.h"

@interface AppDelegate (ThirdPart)

/**
 设置sdk登录信息并登录
 */
- (void)setupXLSDK;

/**
 设置友盟
 */
- (void)setupUM;

/**
 Bugly
 */
- (void)setupBugly;

@end
