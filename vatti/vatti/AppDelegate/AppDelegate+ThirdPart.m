//
//  AppDelegate+ThirdPart.m
//  vatti
//
//  Created by Eric on 2018/7/2.
//  Copyright © 2018年 xlink. All rights reserved.
//

#import "AppDelegate+ThirdPart.h"
#import "StartPageViewController.h"
#import "XLUtilTool.h"
#import <UMCommon/UMCommon.h>
#import <UMShare/UMShare.h>
#import <Bugly/Bugly.h>

@implementation AppDelegate (ThirdPart)

/**
 设置友盟appkey
 */
- (void)setupUM
{
    [UMConfigure initWithAppkey:@"5b3dfc1ef29d9806180000a9" channel:@"App Store"];
    
    //配置微信
    [self setupWeChat];
    
    //配置qq
    [self setupQQ];
}

/**
 设置sdk登录信息并登录
 */
- (void)setupXLSDK
{
    NSDictionary *dic = [XLUtilTool lastLoginAuthorizationInfo];
    [XL_DATA_SOURCE startSDKWithAuthInfo:dic];
}

/**
 设置微信的appKey和appSecret
 */
- (void)setupWeChat
{
    BOOL success = [[UMSocialManager defaultManager] setPlaform:UMSocialPlatformType_WechatSession appKey:@"wx5d52fd2b3c77af8c" appSecret:@"fc47ee2e84fef0cc45c5d87b2a0b1ad1" redirectURL:nil];
    if (success == YES) {
        NSLog(@"%@",@"注册wx成功");
    }
}

/**
 配置QQ
 */
- (void)setupQQ
{
    BOOL success = [[UMSocialManager defaultManager] setPlaform:UMSocialPlatformType_QQ appKey:@"101499589" appSecret:@"5b3dfc1ef29d9806180000a9" redirectURL:nil];
    if (success == YES) {
        NSLog(@"%@",@"注册QQ成功");
    }
}

/**
 Bugly
 */
- (void)setupBugly
{
    BuglyConfig *buglyConfig = [[BuglyConfig alloc] init];
    
    //卡顿监控
    buglyConfig.blockMonitorEnable = YES;
    //上报规则
    buglyConfig.reportLogLevel = BuglyLogLevelWarn;
    
    [Bugly startWithAppId:@"1e062fd68e" config:buglyConfig];
    
#ifdef DEBUG
    NSLog(@"不重定向日志");
#else
    [self redirectLogToDocumentFolder];
#endif
}

/* 将NSlog打印信息保存到Document目录下的文件中 */
- (void)redirectLogToDocumentFolder
{
    // 获取沙盒路径
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,NSUserDomainMask,YES);
    NSString *documentDirectory = [paths objectAtIndex:0];
    
    // 获取打印输出文件路径
    NSDate *date = [NSDate date];
    //日期
    NSDateFormatter *fmt = [[NSDateFormatter alloc] init];
    [fmt setDateFormat:@"yyyy-MM-dd"];
    NSString *dateString = [fmt stringFromDate:date];
    //时间
    NSDateFormatter *fmt2 = [[NSDateFormatter alloc] init];
    [fmt2 setDateFormat:@"HH:mm:ss"];
    NSString *timeString = [fmt2 stringFromDate:date];
    
    //创建日期文件夹
    NSFileManager *fileManager = [[NSFileManager alloc] init];
    NSString *createPath = [NSString stringWithFormat:@"%@/%@",documentDirectory,dateString];
    if (![[NSFileManager defaultManager]fileExistsAtPath:createPath]) {
        [fileManager createDirectoryAtPath:createPath withIntermediateDirectories:YES attributes:nil error:nil];
    }else{
        NSLog(@"有这个文件夹了");
    }
    
    NSString *fileName = [NSString stringWithFormat:@"%@/%@.log",dateString,timeString];
    NSString *logFilePath = [documentDirectory stringByAppendingPathComponent:fileName];
    NSLog(@"重定向日志到%@",logFilePath);
    // 先删除已经存在的文件
    NSFileManager *defaultManager = [NSFileManager defaultManager];
    [defaultManager removeItemAtPath:logFilePath error:nil];
    // 将NSLog的输出重定向到文件，因为C语言的printf打印是往stdout打印的，这里也把它重定向到文件
    freopen([logFilePath cStringUsingEncoding:NSASCIIStringEncoding],"a+", stdout);
    freopen([logFilePath cStringUsingEncoding:NSASCIIStringEncoding],"a+", stderr);
}
@end
