//
//  AppDelegate.h
//  vatti
//
//  Created by 许瑞邦 on 2018/4/23.
//  Copyright © 2018年 xlink. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Reachability.h"

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

///网络监控对象
@property (strong, nonatomic) Reachability *hostReachability;

@end

