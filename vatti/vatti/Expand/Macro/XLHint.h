//
//  XLHint.h
//  vatti
//
//  Created by Eric on 2018/9/19.
//  Copyright © 2018年 xlink. All rights reserved.
//

#ifndef XLHint_h
#define XLHint_h

#define XLHint_SetDataPontFailed @"操作失败，请查看网络是否异常！"

#define XLHint_NoSSID            @"未检测到无线网络"

#endif /* XLHint_h */
