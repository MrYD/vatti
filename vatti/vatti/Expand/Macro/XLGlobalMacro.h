//
//  XLGlobalMacro.h
//  vatti
//
//  Created by BZH on 2018/5/22.
//  Copyright © 2018年 xlink. All rights reserved.
//

/**
 *  全局常量
 *
 *  @author BZH
 */

#ifndef XLGlobalMacro_h
#define XLGlobalMacro_h

#pragma mark - 网络请求常量

/**
 网络请求超时时间
 */
#define TimeOutSeconds      10.f

/**
 设置数据端点超时时间
 */
#define TimeOutSetDataPoint 10.0f

/**
 获取验证码倒计时时间
 */
#define TimeOutGetVerfyCode 60.0f

#endif /* XLGlobalMacro_h */
