//
//  XLUtilsMacro.h
//  vatti
//
//  Created by 李叶 on 2018/5/22.
//  Copyright © 2018年 xlink. All rights reserved.
//

#ifndef XLUtilsMacro_h
#define XLUtilsMacro_h

#pragma mark - 系统对象
#define kXLNotificationCenter   [NSNotificationCenter defaultCenter]
#define kXLUIApplication        [UIApplication sharedApplication]
#define kXLUIApplicationWindow  [UIApplication sharedApplication].delegate.window
#define kXLAppDelegate          [AppDelegate shareAppDelegate]
#define kXLRootViewController   [UIApplication sharedApplication].delegate.window.rootViewController
#define kXLNSUserDefaults       [NSUserDefaults standardUserDefaults]
#define kXLScreenBounds         [UIScreen mainScreen].bounds
#define kXLScreenWidth          [UIScreen mainScreen].bounds.size.width
#define kXLScreenHeight         [UIScreen mainScreen].bounds.size.height

#pragma mark -颜色获取宏定义

//0x开头的16进制颜色宏
#define HexRGB(rgbValue)    [UIColor colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 green:((float)((rgbValue & 0xFF00) >> 8))/255.0 blue:((float)(rgbValue & 0xFF))/255.0 alpha:1.0]
#define HexRGBA(rgbValue,a) [UIColor colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 green:((float)((rgbValue & 0xFF00) >> 8))/255.0 blue:((float)(rgbValue & 0xFF))/255.0 alpha:(a)]
#define HexRGBClear         [UIColor clearColor]

#define kXL_IS_IPHONE (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone ? YES : NO)

#define kXL_IS_IPHONEX (kXL_IS_IPHONE && kXLScreenWidth == 375. && kXLScreenHeight == 812. ? YES : NO)
//检测是否是刘海屏
#define IsiPhoneNotchInScreen ([UIScreen instancesRespondToSelector:@selector(currentMode)] ? \
(CGSizeEqualToSize(CGSizeMake(1125, 2436), [[UIScreen mainScreen] currentMode].size) || \
CGSizeEqualToSize(CGSizeMake(828, 1792), [[UIScreen mainScreen] currentMode].size) ||  \
CGSizeEqualToSize(CGSizeMake(1242, 2688), [[UIScreen mainScreen] currentMode].size)) : NO)

#pragma mark - BarHeight
/// 状态栏高度
#define kStatusBarHeight (IsiPhoneNotchInScreen ? 44.0 : 20.0)
/// 导航栏高度
#define kNavigationBarHeight 44.0f
/// 导航栏+状态栏高度
#define kTopBarHeight (kStatusBarHeight + kNavigationBarHeight)
/// TabBar高度
#define kTabBarHeight (IsiPhoneNotchInScreen ? 83.0f : 49.0f)
/// 底部偏移高度，适配
#define kBottomOffset (IsiPhoneNotchInScreen ? 34.0f : 0.f)
///是否为空
static inline BOOL IsEmpty(id thing) {
    return (thing == nil) || [thing isEqual:[NSNull null]] ||
    ([thing isKindOfClass:[NSString class]] && [[(NSString *)thing stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]] length] == 0) ||
    ([thing respondsToSelector:@selector(length)] && [(NSData *)thing length] == 0) ||
    ([thing respondsToSelector:@selector(count)] && [(NSArray *)thing count] == 0);
}

#pragma mark - 日志
////正常日志
//#ifdef DEBUG
//#define NSLog(fmt, ...) NSLog((@"[App Log] --> " fmt),##__VA_ARGS__)
//#else
//#define NSLog(...)
//#endif

//正常日志
#ifdef DEBUG
#define XLLog(fmt, ...) NSLog((@"[App Log] --> " fmt),##__VA_ARGS__)
#else
#define XLLog(...)
#endif

//需要检查的日志
#ifdef DEBUG
#define XLErrorLog(fmt, ...) NSLog((@"[App Error Log] --> " fmt),##__VA_ARGS__)
#else
#define XLErrorLog(...)
#endif

#define kXLLogFunc XLLog(@"%s", __func__)

#endif /* XLUtilsMacro_h */

