//
//  XLClassReference.h
//  vatti
//
//  Created by Eric on 2018/7/12.
//  Copyright © 2018年 xlink. All rights reserved.
//

#ifndef XLClassReference_h
#define XLClassReference_h

#import "XLinkSDK.h"
#import "XLUtilsMacro.h"
#import "XLServiceManager.h"
#import "XLDataSource.h"
#import "XLLanguageTool.h"
#import "XLSkinTool.h"
#import "XLDatabaseManager.h"
#import "VirtualDeviceModel.h"
#import "XLCategoryHeader.h"
#import "XLUserDefault.h"
#import "XLConstant.h"
#import "XLinkHttpRequest.h"
#import "XLinkEventNotify.h"
#import "XLDataPointTool.h"

#endif /* XLClassReference_h */
