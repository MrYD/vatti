//
//  XLUtilTool.m
//  vatti
//
//  Created by 李叶 on 2018/5/22.
//  Copyright © 2018年 xlink. All rights reserved.
//

#import "XLUtilTool.h"
#import <AVFoundation/AVCaptureDevice.h>
#import <SystemConfiguration/CaptiveNetwork.h>
#import "AppDelegate.h"

static NSString *XLAppFirstStart = @"XLAppFirstStart_V1.0";
static NSString *XLAppAccountAutoLogin = @"XLAppAccountAutoLogin";
static NSString *XLAppLastAccount = @"XLAppLastAccount";
static NSString *XLAppLoginAuthorizationInfo = @"XLAppLoginAuthorizationInfo";

@implementation XLUtilTool

/**
 验证密码是否有非法符号
 
 @param password 字符串
 @return bool YES/NO
 */
+ (BOOL)validatePassowrd:(NSString *)password {
    NSString * regex = @"^[A-Za-z0-9]+$";
    NSPredicate *pred = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", regex];
    return [pred evaluateWithObject:password];
}

/**
 验证是否只有中文数字英文空格
 
 @param string 需要验证的字符串
 @return bool YES/NO
 */
+ (BOOL)validateString:(NSString *)string {
    NSString * regex = @"^[\u4E00-\u9FA5A-Za-z0-9 ]+$";
    NSPredicate *pred = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", regex];
    return [pred evaluateWithObject:string];
}
/**
 验证是否全为空格
 
 @param str 需要验证的字符串
 @return bool YES/NO
 */
+ (BOOL)isEmpty:(NSString *) str {
    
    if (!str) {
        return true;
    } else {
        //A character set containing only the whitespace characters space (U+0020) and tab (U+0009) and the newline and nextline characters (U+000A–U+000D, U+0085).
        NSCharacterSet *set = [NSCharacterSet whitespaceAndNewlineCharacterSet];
        
        //Returns a new string made by removing from both ends of the receiver characters contained in a given character set.
        NSString *trimedString = [str stringByTrimmingCharactersInSet:set];
        
        if ([trimedString length] == 0) {
            return true;
        } else {
            return false;
        }
    }
}

/**
 验证是否只有中文数字英文
 
 @param string 需要验证的字符串
 @return bool YES/NO
 */
+ (BOOL)validateBlankSpaceString:(NSString *)string {
    NSString * regex = @"^[\u4E00-\u9FA5A-Za-z0-9]+$";
    NSPredicate *pred = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", regex];
    return [pred evaluateWithObject:string];
}

/**
 验证是否只有数字
 
 @param string 需要验证的字符串
 @return bool YES/NO
 */
+ (BOOL)validateNSNumber:(NSString *)string {
    NSString * regex = @"^[0-9]+$";
    NSPredicate *pred = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", regex];
    return [pred evaluateWithObject:string];
}

/**
 验证手机号码是否合法
 
 @param phone 手机号码
 @return boo YES/NO
 */
+ (BOOL)validatePhone:(NSString *)phone {
    NSString *regex = @"^1(([3,4,6-9][0-9])|(5[^4,\\D]))\\d{8}$";
    NSPredicate *pred = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", regex];
    return [pred evaluateWithObject:phone];
}

/**
 验证邮箱是否合法
 
 @param email 邮箱
 @return bool YES/NO
 */
+ (BOOL)validateEmail:(NSString *)email {
    NSString *regex = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    NSPredicate *pred = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", regex];
    return [pred evaluateWithObject:email];
}

+ (UIViewController *)topViewController {
    UIViewController *rootCtrl = [UIApplication sharedApplication].keyWindow.rootViewController;
    return rootCtrl;
}

+ (UIViewController *)currentViewController:(UIViewController *)rootViewController {
    if ([rootViewController isKindOfClass:[UITabBarController class]]) {
        UITabBarController *tabBarCtrl = (UITabBarController *)rootViewController;
        return [XLUtilTool currentViewController:tabBarCtrl.selectedViewController];
    }else if ([rootViewController isKindOfClass:[UINavigationController class]]) {
        UINavigationController *nav = (UINavigationController *)rootViewController;
        return [XLUtilTool currentViewController:nav.viewControllers.lastObject];
    }else if (rootViewController.presentedViewController) {
        
    }
    return nil;
}

+ (BOOL)isValidateFirstStart {
    BOOL isFristStart = [kXLNSUserDefaults boolForKey:XLAppFirstStart];
    if (isFristStart == NO) {
        [kXLNSUserDefaults setBool:YES forKey:XLAppFirstStart];
        [kXLNSUserDefaults synchronize];
    }
    return !isFristStart;
}

+ (BOOL)isValidateAutoLogin {
    BOOL isValidateAutoLogin = [kXLNSUserDefaults boolForKey:XLAppAccountAutoLogin];
    return isValidateAutoLogin;
}

+ (void)setAutoLogin:(BOOL)autoLogin {
    [kXLNSUserDefaults setBool:autoLogin forKey:XLAppAccountAutoLogin];
    [kXLNSUserDefaults synchronize];
}

/**
 设置最后一次登录账号
 */
+ (void)setLastAccount:(NSString *)account {
    [kXLNSUserDefaults setObject:account forKey:XLAppLastAccount];
    [kXLNSUserDefaults synchronize];
}

/**
 获取最后一次登录账号
 */
+ (NSString *)lastAccount {
    return [kXLNSUserDefaults objectForKey:XLAppLastAccount];
}

+ (NSDictionary *)lastLoginAuthorizationInfo {
    return [kXLNSUserDefaults objectForKey:XLAppLoginAuthorizationInfo];
}

+ (void)setlastLoginAuthorizationInfo:(NSDictionary *)info {
    [kXLNSUserDefaults setObject:info forKey:XLAppLoginAuthorizationInfo];
    [kXLNSUserDefaults synchronize];
}

+ (void)clearLastLoginAuthorization{
    [kXLNSUserDefaults removeObjectForKey:XLAppLoginAuthorizationInfo];
    [kXLNSUserDefaults synchronize];
}

+ (void)setRootViewController:(UIViewController *)viewController animationOptions:(UIViewAnimationOptions)option duration:(CGFloat)time {

    viewController.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
    
    UIWindow *window = [UIApplication sharedApplication].delegate.window;
    AppDelegate *delegate = (AppDelegate*)[UIApplication sharedApplication].delegate;
    [UIView transitionWithView:window duration:time options:option animations:^{
        BOOL oldState = [UIView areAnimationsEnabled];
        [UIView setAnimationsEnabled:NO];
        delegate.window.rootViewController = viewController;
        [UIView setAnimationsEnabled:oldState];
    } completion:^(BOOL finished) {
        
    }];
}

/**
 *  判断是否获取相机权限
 *
 *  @return 是否获取相册权限
 */
+ (BOOL)isGetCameraAuthorized{
    AVAuthorizationStatus authStatus = [AVCaptureDevice authorizationStatusForMediaType:AVMediaTypeVideo];
    if (authStatus == AVAuthorizationStatusRestricted || authStatus == AVAuthorizationStatusDenied){
        return NO;
    }
    return YES;
}

/**
 获取当前连接的wifi名称
 */
+ (NSString *)getWifiSSID
{
    NSArray *ifs = (__bridge_transfer id)CNCopySupportedInterfaces();
    id info = nil;
    for (NSString *ifnam in ifs) {
        info = (__bridge_transfer id)CNCopyCurrentNetworkInfo((__bridge CFStringRef)ifnam);
    }
    NSString *ssid = [info objectForKey:@"SSID"];
    return ssid;
}

@end

