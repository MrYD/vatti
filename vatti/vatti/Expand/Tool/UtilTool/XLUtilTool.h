//
//  XLUtilTool.h
//  vatti
//
//  Created by 李叶 on 2018/5/22.
//  Copyright © 2018年 xlink. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface XLUtilTool : NSObject

/**
 验证密码是否有非法符号
 
 @param password 字符串
 @return bool YES/NO
 */
+ (BOOL)validatePassowrd:(NSString *)password;

/**
 验证是否只有中文数字英文空格
 
 @param string 需要验证的字符串
 @return bool YES/NO
 */
+ (BOOL)validateString:(NSString *)string;
/**
 验证是否只有中文数字英文
 
 @param string 需要验证的字符串
 @return bool YES/NO
 */
+ (BOOL)validateBlankSpaceString:(NSString *)string;

/**
 验证是否全为空格
 
 @param string 需要验证的字符串
 @return bool YES/NO
 */
+ (BOOL)isEmpty:(NSString *)string;
/**
 验证是否只有数字
 
 @param string 需要验证的字符串
 @return bool YES/NO
 */
+ (BOOL)validateNSNumber:(NSString *)string;

/**
 验证手机号码是否合法
 
 @param phone 手机号码
 @return boo YES/NO
 */
+ (BOOL)validatePhone:(NSString *)phone;

/**
 验证邮箱是否合法
 
 @param email 邮箱
 @return bool YES/NO
 */
+ (BOOL)validateEmail:(NSString *)email;

/**
 获取根视图
 */
+ (UIViewController *)topViewController;

/**
 是否第一次启动
 */
+ (BOOL)isValidateFirstStart;

/**
 是否自动登录
 */
+ (BOOL)isValidateAutoLogin;

/**
 设置是否自动登录
 */
+ (void)setAutoLogin:(BOOL)autoLogin;

/**
 设置最后一次登录账号
 */
+ (void)setLastAccount:(NSString *)account;

/**
 获取最后一次登录账号
 */
+ (NSString *)lastAccount;

/**
 获取上次登录的授权信息
 */
+ (NSDictionary *)lastLoginAuthorizationInfo;

/**
 设置登录授权信息
 */
+ (void)setlastLoginAuthorizationInfo:(NSDictionary *)info;

/**
 清除登录授权信息
 */
+ (void)clearLastLoginAuthorization;

/**
 设置根视图
 */
+ (void)setRootViewController:(UIViewController *)viewController animationOptions:(UIViewAnimationOptions)option duration:(CGFloat)time;

/**
 *  判断是否获取相机权限
 *
 *  @return 是否获取相册权限
 */
+ (BOOL)isGetCameraAuthorized;

/**
 获取当前连接的wifi名称
 */
+ (NSString *)getWifiSSID;

@end
