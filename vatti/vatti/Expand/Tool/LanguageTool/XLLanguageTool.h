//
//  XLLanguageTool.h
//  vatti
//
//  Created by 李叶 on 2018/5/22.
//  Copyright © 2018年 xlink. All rights reserved.
//

#import <Foundation/Foundation.h>

#define XLLocalizeString(string) [XLLanguageTool getLocalizedLanguageForKey:string]

UIKIT_EXTERN NSString *const XLAppLangauge;

@interface XLLanguageTool : NSObject

/**
 传入自定义的key值 获取本地存储的国际化value
 
 @param key key 国际化key值
 @return  国际化不同语言返回不同
 */
+ (NSString *)getLocalizedLanguageForKey:(NSString *)key;

/**
 *设置手机App的语言
 @param lang en:英文  zh-Hans:简体中文   zh-Hant:繁体中文（没有的话就用系统语言）
 */
+ (void)setLanguage:(NSString *)lang;

/**
 获取当前的语言环境
 
 @return en:英文  zh-Hans:简体中文   zh-Hant:繁体中文（没有的话就用系统语言）
 */
+ (NSString *)getCurrentLanguage;

@end

