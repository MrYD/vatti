//
//  XLLanguageTool.m
//  vatti
//
//  Created by 李叶 on 2018/5/22.
//  Copyright © 2018年 xlink. All rights reserved.
//

#import "XLLanguageTool.h"

NSString *const XLAppLangauge = @"XL_XLINK_APP_LANGUAGE";

@implementation XLLanguageTool

+ (NSString *)getLocalizedLanguageForKey:(NSString *)key {
    if (!key.length) return @"";
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *language = [defaults objectForKey:XLAppLangauge];
    NSString *returnStr = NSLocalizedString(key, @"");
    if (language.length) {
        returnStr=  [[NSBundle bundleWithPath:[[NSBundle mainBundle] pathForResource:[NSString stringWithFormat:@"%@",language] ofType:@"lproj"]] localizedStringForKey:(key) value:nil table:@"Localizable"];
    }
    return returnStr;
}

+ (void)setLanguage:(NSString *)lang{
    //     @param lang en:英文  zh-Hans:简体中文   zh-Hant:繁体中文（没有的话就用系统语言）
    NSArray *arr = @[@"en", @"zh-Hans", @"zh-Hant"];
    if (!lang.length || ![arr containsObject:lang]) return;
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setValue:lang forKey:XLAppLangauge];
}

+ (NSString *)getCurrentLanguage {
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *language = [defaults objectForKey:XLAppLangauge] ? : [[NSLocale preferredLanguages] objectAtIndex:0];
    return language;
}

@end

