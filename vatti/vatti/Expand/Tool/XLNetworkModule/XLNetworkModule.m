//
//  XLNetworkModule.m
//  vatti
//
//  Created by 李叶 on 2018/5/22.
//  Copyright © 2018年 xlink. All rights reserved.
//

#import "XLNetworkModule.h"
#import "XLServiceManager.h"
#import "XLDatabaseManager.h"
#import <MJExtension/MJExtension.h>

@interface XLNetworkModule ()

@end

@implementation XLNetworkModule

+ (instancetype)shareInstance {
    static XLNetworkModule *instance;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        instance = [[XLNetworkModule alloc] init];
    });
    return instance;
}

- (void)getAccountInfomationWithLUserModel:(XLUserModel *)userModel handler:(void(^)(id result, NSError *error))handler {
    [[XLServiceManager shareInstance].userService getAccountInfomationWithUserId:userModel.userId timeout:10. handler:^(id result, NSError *error) {
        if (!error) {
            userModel.nickname = [result objectForKey:@"nickname"];
            userModel.avatar = [result objectForKey:@"avatar"];
            userModel.phone = [result objectForKey:@"phone"];
            userModel.email = [result objectForKey:@"email"];
        }
        if (handler) {
            handler(result, error);
        }
    }];
}

- (void)getAccountSubscribeDeviceListWithXLUserModel:(XLUserModel *)userModel handler:(void (^)(BOOL))handler{
    
    @weakify(self)
    
    if (userModel == nil) {
        if (handler) {
            handler(NO);
        }
        return;
    }
    
    [[XLServiceManager shareInstance].userService getSubscribeDeviceListAndConnectDeviceWithVersion:0 userId:userModel.userId localConnect:YES timeout:30. handler:^(NSArray<XDevice *> *devices, XLinkErrorCode code) {

        @strongify(self)
        
        if (code == XLinkErrorCodeNoError) {
            [userModel updateDeviceModelsWithDevices:devices];
        }
        
        if (handler) {
            handler(code == XLinkErrorCodeNoError);
        }
    }];
}

@end

