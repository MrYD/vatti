//
//  XLNetworkModule.h
//  vatti
//
//  Created by 李叶 on 2018/5/22.
//  Copyright © 2018年 xlink. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "XLServiceManager.h"

#define XL_NETWORK_MODULE [XLNetworkModule shareInstance]

@interface XLNetworkModule : NSObject

+ (instancetype)shareInstance;

/**
 获取个人信息
 */
- (void)getAccountInfomationWithLUserModel:(XLUserModel *)userModel handler:(void(^)(id result, NSError *error))handler;


/**
 获取当前用户下的设备列表
 */
- (void)getAccountSubscribeDeviceListWithXLUserModel:(XLUserModel *)userModel handler:(void (^)(BOOL))handler;


@end
