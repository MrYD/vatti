//
//  XLModuleManager.h
//  XLSmartHome
//
//  Created by Chris on 2018/1/24.
//  Copyright © 2018年 xlink. All rights reserved.
//
#import <Foundation/Foundation.h>
#import "XLModule.h"

#define XL_MODULE_MANAGER [XLModuleManager instance]

@interface XLModuleManager : NSObject<XLModule>

+ (instancetype)instance;

- (void)loadModulesWithPlistFile:(NSString *)plistFile;

- (NSArray<id<XLModule>> *)allModules;


@end
