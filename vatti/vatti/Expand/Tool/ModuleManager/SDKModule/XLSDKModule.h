//
//  XLSDKModule.h
//  XLSmartHome
//
//  Created by Chris on 2018/3/13.
//  Copyright © 2018年 xlink. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "XLModule.h"

@interface XLSDKModule : NSObject<XLModule>

+ (instancetype)shareInstance;

/**
 启动SDK最初配置
 */
- (void)configXLinkSDKDelegate;

/**
 清除SDK最初配置
 */
- (void)claerXLinkSDKDelegate;

@end
