//
//  XLSDKModule.m
//  XLSmartHome
//
//  Created by Chris on 2018/3/13.
//  Copyright © 2018年 xlink. All rights reserved.
//

#import "XLSDKModule.h"
#import "XLinkSDK.h"
#import "XLDataSource.h"
#import "WaterHeaterDeviceModel.h"
#import "XLUtilTool.h"
#import "XLNetworkModule.h"

@interface XLSDKModule ()<XLinkCloudDelegate, XLinkUserDelegate, XLinkDataDelegate, XLinkDeviceStateDelegate>

@end

@implementation XLSDKModule

+ (instancetype)shareInstance
{
    static dispatch_once_t once;
    static XLSDKModule *xlLSDKModule = nil;
    
    dispatch_once(&once, ^{
        xlLSDKModule = [[XLSDKModule alloc] init];
    });
    
    return xlLSDKModule;
}

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    
    [self configXLinkSDKDelegate];
    
    return YES;
}

#pragma mark - XLinkCloudDelegate 云端回调
/**
 *  当SDK与云平台连上/断开的时候，会回调这个方法
 *
 *  @param state 连接状态
 */
- (void)onCloudStateChangedWithCloudConnectionState:(CloudConnectionState)state {
    
    XL_DATA_SOURCE.state = state;
    
    if (state == CloudConnectionStateConnectSuccess) {
        //每次连接云端成功后刷新下设备列表
        [XL_NETWORK_MODULE getAccountSubscribeDeviceListWithXLUserModel:XL_DATA_SOURCE.user handler:nil];
        //        [[XLBackgroundTool shareInstance] connectCloud:XL_DATA_SOURCE.user];
    }else if (state == CloudConnectionStateConnectFail){
        //        [[XLBackgroundTool shareInstance] disconnectCloud];
    }
}

/**
 *  当SDK连接上云端，并接收到云端推送时，会回调这个方法。
 *
 *  @param eventNotify 收到的云端推送信息
 */
- (void)onReceiveEventNotify:(XLinkEventNotify *)eventNotify {
    
    NSDictionary *eventDict = [self analysisEventNotify:eventNotify];
    
    if (eventDict == nil) {
        return;
    }
    
    uint32_t fromId = eventNotify.fromID;
    
    switch (eventNotify.messageType) {
        case EventNotifyTypeDataPointChanged: {
            //            [self dataPointChanged:eventNotifyData];
            break;
        }
        case EventNotifyTypeDataPointAlert: {
            [self configureDataPointAlertWithEventDict:eventDict];
            break;
        }
        case EventNotifyTypeDeviceShare: {
            [self configureDeviceShareWithEventDict:eventDict andFromID:fromId];
            break;
        }
        case EventNotifyTypeSubscribeChanged: {
            [self configureSubscribeChanged];
            break;
        }
        default:
            break;
    }
    
}

#pragma mark - XLinkUserDelegate 用户授权结果回调
/**
 用户被下线（被踢或者手动退出）/授权信息过期 的时候，会回调这个方法。
 请根据LogoutReason进行处理
 
 @param reason 下线原因
 */
- (void)onUserLogout:(LogoutReason)reason {
    XLLog(@"SDK推送退出信息 ----> %lu", (unsigned long)reason);
    
    switch (reason) {
        case LogoutReasonSingleSignKickOff:
            //用户被踢下线
            XL_DATA_SOURCE.userState = XLUserStateKickout;
            break;
        case LogoutReasonTokenExpired:
            //授权过期
            XL_DATA_SOURCE.userState = XLUserStateTokenExpired;
            break;
        case LogoutReasonUserAuthInfoInvaild:
            //授权过期
            XL_DATA_SOURCE.userState = XLUserStateTokenExpired;
            break;
        case LogoutReasonUserLogout:
            //用户离线
            XL_DATA_SOURCE.userState = XLUserStateOffline;
            break;
            
        default:
            break;
    }
}

#pragma mark - XLinkDataDelegate 数据回调

/**
 *  当SDK收到设备上报的所以的数据端点时，会回调这个方法。
 *
 *  @param xDevice 接收到数据的设备
 *  @param list    数据端点列表
 */
- (void)onDataPointUpdateWithDevice:(XDevice *)xDevice withList:(NSArray<XLinkDataPoint *> *)list {
    
}

#pragma mark - deviceStateDelegate 设备状态回调
/**
 *   设备属性或者设备状态发生改变时，会回调这个方法。
 *
 *  @param xDevice 发生改变的设备
 */
- (void)onDeviceChanged:(XDevice *)xDevice withEvent:(XDeviceEvent)event {
    DeviceModel *deviceModel = [[DeviceModel alloc] initWithXDevice:xDevice];
    deviceModel.device = xDevice;
    
    switch (event) {
        case XDeviceEventUnSubscribe://订阅关系取消
            [kXLNotificationCenter postNotificationName:XLNotificationDeviceUnSubscribe object:deviceModel];
            break;
            
        default:
            break;
    }
}

/**
 *  受SDK管理的设备（用户未调用`XLinkSDK.getDeviceManager().removeDevice(xDevice)`）状态发生改变时。
 *
 *  @param xDevice 设备状态的设备
 *  @param state   设备状态
 */
- (void)onDeviceStateChanged:(XDevice *)xDevice withDeviceState:(XDeviceConnectionState)state {
    NSLog(@"onDeviceStateChanged:%@ local:%d cloud:%d",xDevice.macAddress,xDevice.cloudConnectionState,xDevice.localConnectionState);
}

#pragma mark - Other
- (void)onReceivePassthroughDataWithDevice:(XDevice *)xDevice withPayloadType:(int)payloadType withPayload:(NSString *)payload withChannel:(int)channel {
    
}

- (void)onDataPointUpdateWithDevice:(XDevice *)xDevice withIndex:(int)index withDataBuff:(NSData *)data withChannel:(int)channel {
    
}

#pragma mark - Data

- (NSDictionary *)analysisEventNotify:(XLinkEventNotify *)eventNotify
{
    if (eventNotify.notifyData.length < 2) {
        XLErrorLog(@"XLinkEventNotify.notifyData推送长度 < 2");
        return nil;
    }
    
    NSDictionary *dic = [NSJSONSerialization JSONObjectWithData:[eventNotify.notifyData subdataWithRange:NSMakeRange(2, eventNotify.notifyData.length - 2)] options:NSJSONReadingMutableLeaves error:nil];
    return dic;
}


/**
 处理设备数据端点变换引起的报警
 
 @param eventDict 数据字典
 */
- (void)configureDataPointAlertWithEventDict:(NSDictionary *)eventDict
{
    XLLog(@"设备数据端点变换引起的报警-->%@", eventDict);
    
    NSInteger alertCount = [kXLNSUserDefaults integerForKey:UDDataPointAlertCount];
    [kXLNSUserDefaults setInteger:(alertCount + 1) forKey:UDDataPointAlertCount];
    [kXLNSUserDefaults synchronize];
    
    //发送告警变更
    [kXLNotificationCenter postNotificationName:XLNotificationDataPointAlert object:eventDict];
}

/**
 处理设备管理员推送的分享消息
 
 @param eventDict 数据字典
 @param fromID 发送邀请者的id
 */
- (void)configureDeviceShareWithEventDict:(NSDictionary *)eventDict andFromID:(uint32_t)fromID;
{
    XLLog(@"设备管理员推送的分享消息-->%@", eventDict);
    
    NSString *type = [eventDict objectForKey:@"type"];
    if([type isEqualToString:@"0"] == NO){
        return;
    }
    
    NSNumber *numFromID = [NSNumber numberWithInt:fromID];
    
    [kXLNotificationCenter postNotificationName:XLNotificationDeviceShare object:numFromID userInfo:eventDict];
    
}

/**
 用户和设备订阅关系发生变化通知
 */
- (void)configureSubscribeChanged
{
    [XL_NETWORK_MODULE getAccountSubscribeDeviceListWithXLUserModel:XL_DATA_SOURCE.user handler:nil];
}

#pragma mark - XLinkSDK  相关处理

/**
 启动SDK最初配置
 */
- (void)configXLinkSDKDelegate {
    [XLinkSDK share].cloudDelegate = self;
    [XLinkSDK share].deviceStateDelegate = self;
    [XLinkSDK share].dataDelegate = self;
    [XLinkSDK share].userDelegate = self;
}

/**
 清除SDK最初配置
 */
- (void)claerXLinkSDKDelegate {
    [XLinkSDK share].cloudDelegate = nil;
    [XLinkSDK share].deviceStateDelegate = nil;
    [XLinkSDK share].dataDelegate = nil;
    [XLinkSDK share].userDelegate = nil;
}

@end
