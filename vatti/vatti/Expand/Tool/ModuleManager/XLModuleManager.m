//
//  XLModuleManager.m
//  XLSmartHome
//
//  Created by Chris on 2018/1/24.
//  Copyright © 2018年 xlink. All rights reserved.
//

#import "XLModuleManager.h"

typedef void (^XLModuleEnumeration)(id<XLModule> module, BOOL *stop);

@interface XLModuleManager ()

@property (nonatomic, strong) NSMutableArray<id<XLModule>> *modules;

@end

@implementation XLModuleManager

+ (instancetype)instance {
    static XLModuleManager *manager;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        manager = [[XLModuleManager alloc] init];
    });
    return manager;
}

- (void)loadModulesWithPlistFile:(NSString *)plistFile {
    NSArray<NSString *> *modules = [NSArray arrayWithContentsOfFile:plistFile];
    [modules enumerateObjectsUsingBlock:^(NSString * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        id<XLModule> module = [[NSClassFromString(obj) alloc] init];
        [self addModule:module];
    }];
}

- (void)addModule:(id<XLModule>)module {
    if (![self.modules containsObject:module]) {
        [self.modules addObject:module];
    }
}

- (void)respondsToSelector:(SEL)selector enumerateModule:(XLModuleEnumeration)enumeration {
    [self.modules enumerateObjectsUsingBlock:^(id<XLModule>  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        if ([obj respondsToSelector:selector]) {
            enumeration(obj, stop);
        }
    }];
}

#pragma mark ******************* XLModule *******************

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    [self respondsToSelector:_cmd enumerateModule:^(id<XLModule> module, BOOL *stop) {
        [module application:application didFinishLaunchingWithOptions:launchOptions];
    }];
    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application {
    [self respondsToSelector:_cmd enumerateModule:^(id<XLModule> module, BOOL *stop) {
        [module applicationWillResignActive:application];
    }];
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    [self respondsToSelector:_cmd enumerateModule:^(id<XLModule> module, BOOL *stop) {
        [module applicationDidEnterBackground:application];
    }];
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    [self respondsToSelector:_cmd enumerateModule:^(id<XLModule> module, BOOL *stop) {
        [module applicationWillEnterForeground:application];
    }];
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    [self respondsToSelector:_cmd enumerateModule:^(id<XLModule> module, BOOL *stop) {
        [module applicationDidBecomeActive:application];
    }];
}

- (void)applicationWillTerminate:(UIApplication *)application {
    [self respondsToSelector:_cmd enumerateModule:^(id<XLModule> module, BOOL *stop) {
        [module applicationWillTerminate:application];
    }];
}


- (NSArray<id<XLModule>> *)allModules {
    return [self.modules copy];
}


- (NSMutableArray<id<XLModule>> *)modules {
    if (!_modules) {
        _modules = [NSMutableArray array];
    }
    return _modules;
}
@end
