//
//  XLSingleton.h
//  vatti
//
//  Created by 李叶 on 2018/6/1.
//  Copyright © 2018年 xlink. All rights reserved.
//

#ifndef XLSingleton_h
#define XLSingleton_h


#pragma mark ******************* 单例 *******************

#define kXLSingleton_h(name)\
+ (instancetype)shared##name;\
+ (void)destroyInstance;

#define kXLSingleton_m(name)\
static id _instance;\
+ (instancetype)shared##name{ \
static dispatch_once_t onceToken;\
dispatch_once(&onceToken, ^{\
_instance = [[self alloc] init];\
});\
return _instance;\
}\
\
+ (instancetype)allocWithZone:(struct _NSZone *)zone {\
static dispatch_once_t onceToken;\
dispatch_once(&onceToken, ^{\
_instance = [super allocWithZone:zone];\
});\
return _instance;\
}\
\
- (id)copyWithZone:(nullable NSZone *)zone{\
return _instance;\
}\
\
- (id)mutableCopyWithZone:(nullable NSZone *)zone {\
return _instance;\
}\
\
+ (void)destroyInstance {\
_instance=nil;\
}\


#endif /* XLSingleton_h */
