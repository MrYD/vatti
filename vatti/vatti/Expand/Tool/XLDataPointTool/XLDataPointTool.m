//
//  XLDataPointTool.m
//  vatti
//
//  Created by Eric on 2018/7/19.
//  Copyright © 2018年 xlink. All rights reserved.
//

#import "XLDataPointTool.h"

@implementation XLDataPointTool

/**
 判断端点开关
 
 @param value 数据端点的值
 @param shift 位移
 @return YES:开启 NO:关闭
 */
+ (BOOL)isOpenWithValue:(NSInteger)value andShift:(NSInteger)shift
{
    BOOL isOpen = ((value >> shift) & 0x01) == 1;

    return isOpen;
}

/**
 生成开关值
 
 @param value 当前端点值
 @param shift 位移
 @param isOpen YES:开启 NO:关闭
 @return 生成好的开关值
 */
+ (NSInteger)getDataPointValueWithValue:(NSInteger)value andShift:(NSInteger)shift andIsOpen:(BOOL)isOpen
{
    if (isOpen == YES) {
        value = (0x01 << shift) | value;    //开启
    }else{
        value = (~(0x01 << shift)) & value; //关闭
    }
    
    return value;
}

@end
