//
//  XLDataPointTool.h
//  vatti
//
//  Created by Eric on 2018/7/19.
//  Copyright © 2018年 xlink. All rights reserved.
//

/**
 *  数据端点判断工具类
 *
 *  @author BZH
 */

#import <Foundation/Foundation.h>

@interface XLDataPointTool : NSObject

/**
 判断端点开关

 @param value 数据端点的值
 @param shift 位移
 @return YES:开启 NO:关闭
 */
+ (BOOL)isOpenWithValue:(NSInteger)value andShift:(NSInteger)shift;


/**
 生成开关值

 @param value 当前端点值
 @param shift 位移
 @param isOpen YES:开启 NO:关闭
 @return 生成好的开关值
 */
+ (NSInteger)getDataPointValueWithValue:(NSInteger)value andShift:(NSInteger)shift andIsOpen:(BOOL)isOpen;

@end
