//
//  XLSkinTool.h
//  vatti
//
//  Created by 李叶 on 2018/5/22.
//  Copyright © 2018年 xlink. All rights reserved.
//

#import <Foundation/Foundation.h>

#define XLImage(name) [XLSkinTool getSkinImageWithName:name]

UIKIT_EXTERN NSString *const XLAppSkinType;

@interface XLSkinTool : NSObject

/**
 获取换肤图片
 
 @param name 图片名称
 @return 图片
 */
+ (UIImage *)getSkinImageWithName:(NSString *)name;

/**
 设置换肤类型
 
 @param type 换肤类型
 */
+ (void)setSkinType:(NSString *)type;

/**
 获取当前换肤类型
 
 @return 换肤类型
 */
+ (NSString *)getCurrentSkinType;

@end
