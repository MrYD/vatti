//
//  XLSkinTool.m
//  vatti
//
//  Created by 李叶 on 2018/5/22.
//  Copyright © 2018年 xlink. All rights reserved.
//

#import "XLSkinTool.h"

NSString *const XLAppSkinType = @"XL_APP_SKIN_TYPE";

@implementation XLSkinTool

+ (UIImage *)getSkinImageWithName:(NSString *)name {
    return [UIImage imageNamed:name];
}

+ (void)setSkinType:(NSString *)type {
    NSArray *arr = @[@"red", @"block", @"white"];
    if (!type.length || ![arr containsObject:type])  {
        return;
    }
    [kXLNSUserDefaults setValue:type forKey:XLAppSkinType];
}

+ (NSString *)getCurrentSkinType {
    return  [kXLNSUserDefaults objectForKey:XLAppSkinType];
}

@end

