//
//  XLGCDTimerManager.h
//  vatti
//
//  Created by 李叶 on 2018/6/1.
//  Copyright © 2018年 xlink. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "XLSingleton.h"

typedef NS_ENUM(NSUInteger, GCDTimerOption) {
    GCDTimerOptionAbandon,//废弃之前timer(根据timer名称)
    GCDTimerOptionMerge //合并之前timer
};

#define XL_GCD_TIMER_MANAGER [XLGCDTimerManager sharedXLGCDTimerManager]

@interface XLGCDTimerManager : NSObject

kXLSingleton_h(XLGCDTimerManager);

/**
 启动timer (queque 和task 可为空)
 */
- (void)scheduledGCDTimerWithTimerName:(NSString *)name timeInterval:(NSTimeInterval)interval queque:(dispatch_queue_t)queque repeats:(BOOL)repeats removeCancel:(BOOL)cancel task:(dispatch_block_t)task;

- (void)cancelGCDTimerWithTimerName:(NSString *)name;

- (BOOL)verifyGCDTimerWithTimerName:(NSString *)name;


@end
