//
//  XLGCDTimerManager.m
//  vatti
//
//  Created by 李叶 on 2018/6/1.
//  Copyright © 2018年 xlink. All rights reserved.
//

#import "XLGCDTimerManager.h"

@interface XLGCDTimerManager ()

@property (nonatomic, strong) NSMutableDictionary *timers;
@property (nonatomic, strong) NSMutableDictionary *timertasks;

@end

@implementation XLGCDTimerManager

kXLSingleton_m(XLGCDTimerManager);

- (void)scheduledGCDTimerWithTimerName:(NSString *)name timeInterval:(NSTimeInterval)interval queque:(dispatch_queue_t)queque repeats:(BOOL)repeats removeCancel:(BOOL)cancel task:(dispatch_block_t)task {
    if (!name.length) {
        return;
    }
    if (!queque) {
        queque = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
    }
    dispatch_source_t timer = [self.timers objectForKey:name];
    if (!timer) {
        timer = dispatch_source_create(DISPATCH_SOURCE_TYPE_TIMER, 0, 0, queque);
        dispatch_resume(timer);
        [self.timers setObject:timer forKey:name];
    }
    /* timer精度为0.01秒 */
    dispatch_source_set_timer(timer, dispatch_time(DISPATCH_TIME_NOW, interval * NSEC_PER_SEC), interval * NSEC_PER_SEC, 0.01 * NSEC_PER_SEC);
    if (cancel) {
        /* 移除之前的task */
        [self _removeTaskWithTimerName:name];
    }
    [self _cacheTask:task forTimerName:name];
    dispatch_source_set_event_handler(timer, ^{
        NSMutableArray *tasks = [self.timertasks objectForKey:name];
        [tasks enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
            dispatch_block_t task = obj;
            dispatch_async(dispatch_get_main_queue(), ^{
                if (task) {
                    task();
                }
            });
        }];
        if (!repeats) {
            [self _cancelTimerWithTimerName:name];
        }
    });
}

- (void)cancelGCDTimerWithTimerName:(NSString *)name {
    [self _cancelTimerWithTimerName:name];
}

- (BOOL)verifyGCDTimerWithTimerName:(NSString *)name {
    return [self.timers.allKeys containsObject:name];
}

- (void)_removeTaskWithTimerName:(NSString *)name {
    if (![self.timertasks objectForKey:name]) {
        return;
    }
    [self.timertasks removeObjectForKey:name];
}

- (void)_cancelTimerWithTimerName:(NSString *)name {
    dispatch_source_t timer = [self.timers objectForKey:name];
    if (!timer) {
        return;
    }
    [self.timers removeObjectForKey:name];
    dispatch_source_cancel(timer);
    [self.timertasks removeObjectForKey:name];
}

- (void)_cacheTask:(dispatch_block_t)task forTimerName:(NSString *)name {
    NSMutableArray *tasks = [self.timertasks objectForKey:name];
    if (tasks) {
        [tasks addObject:task];
    } else {
        NSMutableArray *array = [NSMutableArray arrayWithObject:task];
        [self.timertasks setObject:array forKey:name];
    }
}


#pragma mark ******************* setter or getter *******************

- (NSMutableDictionary *)timers {
    if (!_timers) {
        _timers = [NSMutableDictionary dictionary];
    }
    return _timers;
}

- (NSMutableDictionary *)timertasks {
    if (!_timertasks) {
        _timertasks = [NSMutableDictionary dictionary];
    }
    return _timertasks;
}

@end
