//
//  XLConstantsMacro.h
//  XLSmartHome
//
//  Created by Chris on 2017/9/25.
//  Copyright © 2017年 Xlink. All rights reserved.
//

#import <Foundation/Foundation.h>

#pragma mark - 公司信息

///公司id
FOUNDATION_EXTERN NSString *const XLCorpID;

///服务器地址
FOUNDATION_EXTERN NSString *const XLXlinkApiServer;

///云端服务器地址
FOUNDATION_EXTERN NSString *const XLXlinkCloudServer;

///云端服务器端口
FOUNDATION_EXTERN NSString *const XLXlinkCloudPort;

///燃热产品ID
FOUNDATION_EXTERN NSString *const XLProductGas;

///洗碗机产品ID
FOUNDATION_EXTERN NSString *const XLProductDish;

///电热产品ID
FOUNDATION_EXTERN NSString *const XLProductEle;

///设备下成员发生
FOUNDATION_EXTERN NSString *const XLDeviceMemberListChangeNofication;

#pragma mark - 推送消息

///设备分享
FOUNDATION_EXTERN NSString *const XLNotificationDeviceShare;
///告警消息变更
FOUNDATION_EXTERN NSString *const XLNotificationDataPointAlert;
///设备订阅
FOUNDATION_EXTERN NSString *const XLNotificationDeviceSubscribe;
///设备取消订阅
FOUNDATION_EXTERN NSString *const XLNotificationDeviceUnSubscribe;

