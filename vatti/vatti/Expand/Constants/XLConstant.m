//
//  XLConstantsMacro.m
//  XLSmartHome
//
//  Created by Chris on 2017/9/25.
//  Copyright © 2017年 Xlink. All rights reserved.
//

#import <UIKit/UIKit.h>

#pragma mark - 公司信息

#define PRIVATE

#ifdef PRIVATE
//私有云(上架版本)
NSString *const XLCorpID                     = @"109c56b61fb1a600";
NSString *const XLXlinkApiServer             = @"http://iot.befish.com.cn";
NSString *const XLXlinkCloudServer           = @"cm.befish.com.cn";
NSString *const XLXlinkCloudPort             = @"1883";
NSString *const XLProductGas                 = @"169c56b63ecb07d1169c56b63ecb7601";
NSString *const XLProductDish                = @"1607d2b688d41f411607d2b688d47a01";
NSString *const XLProductEle                 = @"";
#else
//公有云
NSString *const XLCorpID                     = @"100fa8b412965a00";
NSString *const XLXlinkApiServer             = @"https://api2.xlink.cn";
NSString *const XLXlinkCloudServer           = @"mqtt.xlink.cn";
NSString *const XLXlinkCloudPort             = @"1883";
NSString *const XLProductGas                 = @"160fa8b68e3b03e9160fa8b68e3b3801";
NSString *const XLProductDish                = @"1607d2b688d41f411607d2b688d47a01";
NSString *const XLProductEle                 = @"160fa2b4dda303e9160fa2b4dda39001";
#endif

NSString *const XLDeviceMemberListChangeNofication  = @"XLDeviceMemberListChangeNofication";

#pragma mark - 推送消息

///设备分享
NSString *const XLNotificationDeviceShare        = @"XLNotificationDeviceShare";
///告警消息变更
NSString *const XLNotificationDataPointAlert     = @"XLNotificationDataPointAlert";
///设备订阅
NSString *const XLNotificationDeviceSubscribe    = @"XLNotificationDeviceSubscribe";
///设备取消订阅
NSString *const XLNotificationDeviceUnSubscribe  = @"XLNotificationDeviceUnSubscribe";


