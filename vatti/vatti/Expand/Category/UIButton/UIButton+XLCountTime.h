//
//  UIButton+XLCountTime.h
//  vatti
//
//  Created by 李叶 on 2018/5/24.
//  Copyright © 2018年 xlink. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIButton (XLCountTime)

- (void)xl_startTime:(NSInteger )timeout title:(NSString *)title waitTittle:(NSString *)waitTittle;

@end
