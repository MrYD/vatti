//
//  UIImage+XLCategory.m
//  vatti
//
//  Created by 李叶 on 2018/5/22.
//  Copyright © 2018年 xlink. All rights reserved.
//

#import "UIImage+XLCategory.h"

@implementation UIImage (XLCategory)
- (UIImage *)transAlpha:(CGFloat)alpha {
    UIGraphicsBeginImageContextWithOptions(self.size, NO, 0.0f);
    CGContextRef ctx = UIGraphicsGetCurrentContext();
    CGRect area = CGRectMake(0, 0, self.size.width, self.size.height);
    CGContextScaleCTM(ctx, 1, -1);
    CGContextTranslateCTM(ctx, 0, -area.size.height);
    CGContextSetBlendMode(ctx, kCGBlendModeMultiply);
    CGContextSetAlpha(ctx, alpha);
    CGContextDrawImage(ctx, area, self.CGImage);
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return newImage;
}

void ProviderReleaseData (void *info, const void *data, size_t size)
{
    free((void*)data);
}

+ (UIImage *)xl_circleImageWithString:(NSString *)string bgColor:(UIColor *)bgColor {
    UIImage *bgImg = [UIImage xl_imageForColor:bgColor size:CGSizeMake(40, 40)];
    if (string.length < 1) {
        return bgImg;
    }
    
    NSString *str = [string substringToIndex:1];
    UIImage *circleImg = [[self xl_imageWithString:str bgImg:bgImg] xl_circleImage];
    return circleImg;
}

+ (UIImage *)xl_imageWithString:(NSString *)string bgImg:(UIImage *)image {
    UIGraphicsBeginImageContextWithOptions(image.size, NO, 0.0);
    [image drawAtPoint:CGPointMake(0, 0)];
    CGContextRef context=UIGraphicsGetCurrentContext();
    CGContextDrawPath(context, kCGPathStroke);
    NSDictionary *attributes = @{NSFontAttributeName:[UIFont systemFontOfSize:24],NSForegroundColorAttributeName:[UIColor whiteColor]};
    CGSize size = [string boundingRectWithSize:image.size options:NSStringDrawingUsesLineFragmentOrigin attributes:attributes context:nil].size;
    CGPoint point = CGPointMake((image.size.width - size.width) / 2, (image.size.height - size.height) / 2);
    [string drawAtPoint:point withAttributes:attributes];
    UIImage *watermarkImg = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return watermarkImg;
}

+ (UIImage *)xl_imageWithHexString:(NSString *)hexString {
    const char *hexChar = [hexString cStringUsingEncoding:NSUTF8StringEncoding];
    int hex;
    sscanf(hexChar, "%x", &hex);
    UIColor *color = [UIColor colorWithRed:((hex>>16)&0xFF)/255.0f green:((hex>>8)&0xFF)/255.0f blue:(hex&0xFF)/255.0f alpha:1.0f];
    return [UIImage xl_imageForColor:color size:CGSizeMake(40., 40.)];
}

- (UIImage *)xl_circleImage {
    // NO代表透明
    UIGraphicsBeginImageContextWithOptions(self.size, NO, 0.0);
    // 获得上下文
    CGContextRef ctx = UIGraphicsGetCurrentContext();
    // 添加一个圆
    CGRect rect = CGRectMake(0, 0, self.size.width, self.size.height);
    CGContextAddEllipseInRect(ctx, rect);
    // 裁剪
    CGContextClip(ctx);
    // 将图片画上去
    [self drawInRect:rect];
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    UIColor *color;
    return image;
}

+ (UIImage *)a:(UIColor *)beginColor b:(UIColor *)endColor imageSize:(CGSize)size {
    UIGraphicsBeginImageContextWithOptions(size, YES, 1);
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextSaveGState(context);
    CGColorSpaceRef colorSpaceRef = CGColorSpaceCreateDeviceRGB();
    CFArrayRef colors = CFArrayCreate(kCFAllocatorDefault, (const void*[]){beginColor.CGColor, endColor.CGColor}, 2, nil);
    CGGradientRef gradientRef = CGGradientCreateWithColors(colorSpaceRef, colors, (CGFloat[]){
        0.0f,       // 对应起点颜色位置
        1.0f        // 对应终点颜色位置
    });
    CGContextDrawLinearGradient(context, gradientRef, CGPointMake(0, 0), CGPointMake(size.width, 0), kCGGradientDrawsBeforeStartLocation | kCGGradientDrawsAfterEndLocation);
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    CGContextRestoreGState(context);
    CFRelease(colors);
    CGColorSpaceRelease(colorSpaceRef);
    CGGradientRelease(gradientRef);
    UIGraphicsEndImageContext();
    return image;
}

/**
 *  @brief  根据颜色生成纯色图片
 *
 *  @param color 颜色
 *
 *  @return 纯色图片
 */
+ (UIImage *)xl_imageForColor:(UIColor *)color {
    return [UIImage xl_imageForColor:color size:CGSizeMake(1., 1.)];
}

+ (UIImage *)xl_imageForColor:(UIColor *)color size:(CGSize)size {
    CGRect rect = CGRectMake(0.0f, 0.0f, size.width, size.height);
    UIGraphicsBeginImageContext(rect.size);
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    CGContextSetFillColorWithColor(context, [color CGColor]);
    CGContextFillRect(context, rect);
    
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return image;
}
- (NSData *)xl_imageToData {
    NSData *data;
    if (UIImagePNGRepresentation(self) == nil) {
        data = UIImageJPEGRepresentation(self, 1);
    } else {
        data = UIImagePNGRepresentation(self);
    }
    return data;
}

@end

