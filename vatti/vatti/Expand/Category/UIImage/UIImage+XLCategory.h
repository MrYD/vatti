//
//  UIImage+XLCategory.h
//  vatti
//
//  Created by 李叶 on 2018/5/22.
//  Copyright © 2018年 xlink. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImage (XLCategory)
- (UIImage *)transAlpha:(CGFloat)alpha;

+ (UIImage *)a:(UIColor *)beginColor b:(UIColor *)endColor imageSize:(CGSize)size;

+ (UIImage *)xl_circleImageWithString:(NSString *)string bgColor:(UIColor *)bgColor;

+ (UIImage *)xl_imageWithString:(NSString *)string bgImg:(UIImage *)image;

/**
 十六进制字符串转图片
 */
+ (UIImage *)xl_imageWithHexString:(NSString *)hexString;
/**
 * 圆形图片
 */
- (UIImage *)xl_circleImage;


/**
 *  @brief  根据颜色生成纯色图片
 *
 *  @param color 颜色
 *
 *  @return 纯色图片
 */
+ (UIImage *)xl_imageForColor:(UIColor *)color;

+ (UIImage *)xl_imageForColor:(UIColor *)color size:(CGSize)size;

/**
 图片转成NSData类型
 
 @return 二进制数据
 */
- (NSData *)xl_imageToData;

@end

