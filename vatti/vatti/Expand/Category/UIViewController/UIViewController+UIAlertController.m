//
//  UIViewController+UIAlertController.m
//  vatti
//
//  Created by 李叶 on 2018/5/23.
//  Copyright © 2018年 xlink. All rights reserved.
//

#import "UIViewController+UIAlertController.h"

@implementation UIViewController (UIAlertController)

- (void)alertHintWithMessage:(NSString *)message {
    [self alertHintWithMessage:message handler:nil];
}

- (void)alertHintWithMessage:(NSString *)message
                     handler:(void (^)(UIAlertAction *, NSUInteger))handler {
    [self hintWithTitle:@"提示" message:message style:UIAlertControllerStyleAlert cancelTitle:@"好的" otherTitles:nil handler:handler];
}

- (void)alertHintWithMessage:(NSString *)message
                 cancelTitle:(NSString *)cancelTitle
                 otherTitles:(NSArray<NSString *> *)otherTitles
                     handler:(void (^)(UIAlertAction *, NSUInteger))handler {
    [self alertHintWithTitle:@"提示" message:message cancelTitle:cancelTitle otherTitles:otherTitles handler:handler];
}

- (void)alertHintWithTitle:(NSString *)title
                   message:(NSString *)message
               cancelTitle:(NSString *)cancelTitle
               otherTitles:(NSArray<NSString *> *)otherTitles
                   handler:(void (^)(UIAlertAction *, NSUInteger))handler {
    [self hintWithTitle:title message:message style:UIAlertControllerStyleAlert cancelTitle:cancelTitle otherTitles:otherTitles handler:handler];
}

- (void)hintWithTitle:(NSString *)title
              message:(NSString *)message
                style:(UIAlertControllerStyle)style
          cancelTitle:(NSString *)cancelTitle
          otherTitles:(NSArray<NSString *> *)otherTitles
              handler:(void (^)(UIAlertAction *, NSUInteger))handler {
    UIAlertController *ctrl = [UIAlertController alertControllerWithTitle:title message:message preferredStyle:style];
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:cancelTitle style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        if (handler) {
            handler(action, 0);
        }
    }];
    [ctrl addAction:cancelAction];
    [otherTitles enumerateObjectsUsingBlock:^(NSString * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        UIAlertAction *action = [UIAlertAction actionWithTitle:obj style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            if (handler) {
                handler(action, idx + 1);
            }
        }];
        [ctrl addAction:action];
    }];
    
    [self presentAlertController:ctrl];
}

- (void)showNetworkError {
    [self alertHintWithMessage:@"网络连接失败，请检查网络是否正常"];
}

- (void)presentAlertController:(UIAlertController *)alertVC{
//    UIViewController *topRootViewController = [[UIApplication sharedApplication] keyWindow].rootViewController;
//    // 在这里加一个这个样式的循环
//    while (topRootViewController.presentedViewController) { // 这里固定写法
//        topRootViewController = topRootViewController.presentedViewController;
//    }
//    // 然后再进行present操作
//    [topRootViewController presentViewController:alertVC animated:YES completion:nil];

    if (self.navigationController != nil && ![self isEqual: self.navigationController.topViewController]) {
        return;
    }
    [self presentViewController:alertVC animated:YES completion:nil];
}

@end
