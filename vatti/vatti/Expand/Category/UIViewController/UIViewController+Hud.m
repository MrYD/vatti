//
//  UIViewController+Hud.m
//  vatti
//
//  Created by Eric on 2018/9/18.
//  Copyright © 2018年 xlink. All rights reserved.
//

#import "UIViewController+Hud.h"
#import "MBProgressHUD.h"
#import <objc/runtime.h>

@interface UIViewController ()

///
@property (strong, nonatomic) MBProgressHUD *hud;

@end

@implementation UIViewController (Hud)

- (MBProgressHUD *)hud
{
    return objc_getAssociatedObject(self, _cmd);
}

- (void)setHud:(MBProgressHUD *)hud
{
    objc_setAssociatedObject(self, @selector(hud), hud, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

#pragma mark - HUD

/**
 弹出菊花，无文字提示
 */
- (void)showHUD {
    [self showHUDWithMessage:nil];
}

/**
 提示错误，不自动消失
 
 @param message 错误提示
 */
- (void)showHUDWithMessage:(NSString *)message {
    @weakify(self)
    
    [[NSOperationQueue mainQueue] addOperationWithBlock:^{
        @strongify(self)
        
        [self showHUDAutoWithMessage:message withMode:MBProgressHUDModeIndeterminate];
        
    }];
}

/**
 提示错误，自动消失，默认消失时间为1.5s(出现在屏幕中间)
 
 @param message 错误提示
 */
- (void)showHUDErrorWithMessage:(NSString *)message {
    @weakify(self)
    
    [[NSOperationQueue mainQueue] addOperationWithBlock:^{
        @strongify(self)
        
        [self showHUDAutoWithMessage:message withMode:MBProgressHUDModeIndeterminate];
        
        CGFloat showTime = 1.5f;
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(showTime * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [self hideHUD];
        });
        
    }];
}

/**
 弹出错误提示，自动消失，默认消失时间为1.5s(出现在屏幕下方)
 
 @param message 错误提示
 */
- (void)showErrorWithMessage:(NSString *)message
{
    @weakify(self)
    
    [[NSOperationQueue mainQueue] addOperationWithBlock:^{
        @strongify(self)
        
        [self showHUDAutoWithMessage:message withMode:MBProgressHUDModeText];
        
        CGFloat showTime = 1.5f;
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(showTime * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [self hideHUD];
        });
        
    }];
}

/**
 隐藏HUD
 */
- (void)hideHUD {
    @weakify(self)
    [[NSOperationQueue mainQueue] addOperationWithBlock:^{
        @strongify(self)
        [self resetHud];
    }];
}

/**
 置空HUD
 */
- (void)resetHud
{
    if (self.hud) {
        [self.hud hideAnimated:YES];
        self.hud = nil;
    }
}

/**
 根据当前控制器自动选择弹出位置
 */
- (void)showHUDAutoWithMessage:(NSString *)message withMode:(MBProgressHUDMode)mode
{
    [self resetHud];
    
    if (self.navigationController.view != nil) {
        self.hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
    }else {
        self.hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    }
    
    self.hud.mode = mode;
    
    if (mode == MBProgressHUDModeText) {
        CGFloat bili = 200.f / 554.f;
        self.hud.offset = CGPointMake(0.f, self.view.frame.size.height * bili);
    }
    
    self.hud.detailsLabel.text = message;
}

@end
