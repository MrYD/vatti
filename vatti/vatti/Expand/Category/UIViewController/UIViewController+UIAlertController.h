//
//  UIViewController+UIAlertController.h
//  vatti
//
//  Created by 李叶 on 2018/5/23.
//  Copyright © 2018年 xlink. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIViewController (UIAlertController)

- (void)alertHintWithMessage:(NSString *)message;

- (void)alertHintWithMessage:(NSString *)message
                     handler:(void(^)(UIAlertAction *action, NSUInteger tag))handler;

- (void)alertHintWithMessage:(NSString *)message
                 cancelTitle:(NSString *)cancelTitle
                 otherTitles:(NSArray <NSString *>*)otherTitles
                     handler:(void(^)(UIAlertAction *action, NSUInteger tag))handler;

- (void)alertHintWithTitle:(NSString *)title
                   message:(NSString *)message
               cancelTitle:(NSString *)cancelTitle
               otherTitles:(NSArray <NSString *>*)otherTitles
                   handler:(void(^)(UIAlertAction *action, NSUInteger tag))handler;

- (void)hintWithTitle:(NSString *)title
              message:(NSString *)message
                style:(UIAlertControllerStyle)style
          cancelTitle:(NSString *)cancelTitle
          otherTitles:(NSArray <NSString *>*)otherTitles
              handler:(void(^)(UIAlertAction *action, NSUInteger tag))handler;

#pragma mark - Add
- (void)showNetworkError;

@end
