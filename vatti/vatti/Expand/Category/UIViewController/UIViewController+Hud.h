//
//  UIViewController+Hud.h
//  vatti
//
//  Created by Eric on 2018/9/18.
//  Copyright © 2018年 xlink. All rights reserved.
//

#import <UIKit/UIKit.h>

#define KShowLoading                                 [self showHUD]
#define KShowLoadingWithMsg(msg)                     [self showHUDWithMessage:msg]
#define KShowErrorLoadingWithMsg(msg)                [self showHUDErrorWithMessage:msg]
#define KHideLoading                                 [self hideHUD]

@interface UIViewController (Hud)

#pragma mark - HUD

/**
 弹出菊花，无文字提示
 */
- (void)showHUD;

/**
 提示错误，不自动消失
 
 @param message 错误提示
 */
- (void)showHUDWithMessage:(NSString *)message;

/**
 提示错误，自动消失，默认消失时间为1.5s(出现在屏幕中间)
 
 @param message 错误提示
 */
- (void)showHUDErrorWithMessage:(NSString *)message;

/**
 弹出错误提示，自动消失，默认消失时间为1.5s(出现在屏幕下方)
 
 @param message 错误提示
 */
- (void)showErrorWithMessage:(NSString *)message;

/**
 隐藏HUD
 */
- (void)hideHUD;

@end
