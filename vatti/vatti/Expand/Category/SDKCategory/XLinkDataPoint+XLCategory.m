//
//  XLinkDataPoint+XLCategory.m
//  vatti
//
//  Created by 李叶 on 2018/6/8.
//  Copyright © 2018年 xlink. All rights reserved.
//

#import "XLinkDataPoint+XLCategory.h"

@implementation XLinkDataPoint (XLCategory)

+ (instancetype)dataPointWithType:(XLinkDataType)type withIndex:(NSInteger)index withValue:(id)value
{
    XLinkDataPoint *dataPoint = [[XLinkDataPoint alloc] init];
    dataPoint.type = type;
    dataPoint.index = index;
    dataPoint.value = value;
    return dataPoint;
}

@end
