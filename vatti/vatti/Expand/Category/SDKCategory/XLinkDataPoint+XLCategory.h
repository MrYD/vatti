//
//  XLinkDataPoint+XLCategory.h
//  vatti
//
//  Created by 李叶 on 2018/6/8.
//  Copyright © 2018年 xlink. All rights reserved.
//

#import "XLinkDataPoint.h"

@interface XLinkDataPoint (XLCategory)

+ (instancetype)dataPointWithType:(XLinkDataType)type withIndex:(NSInteger)index withValue:(id)value;

@end
