//
//  UITextField+XLCategory.h
//  vatti
//
//  Created by 李叶 on 2018/5/23.
//  Copyright © 2018年 xlink. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UITextField (XLCategory)

/** 清除输入框的首尾空格和换行符*/
- (NSString *)clearForeAndAftSpace;

/**
 去除输入框所有的空格
 
 @return 去除输入的空格按钮
 */
- (NSString *)clearAllSpace;

/**
 *  添加显示隐藏密码按钮
 */
-(void)addShowPasswordButton;

/**
 添加显示隐藏密码按钮
 
 @param showName 显示密码图片名称
 @param hideName 隐藏密码图片名称
 */
- (void)addShowPasswordButtonWithShowName:(NSString *)showName hideName:(NSString *)hideName;

- (void)addShowPasswordButtonWithShowImage:(UIImage *)showImage hideImage:(UIImage *)hideImage;

/**
 设置 placeholder 颜色
 */
- (void)setPlaceholderColor:(UIColor *)color;

/**
 下划线
 */
- (void)addUnderline;

/**
 下划线指定颜色
 */
- (void)addUnderlineWithColor:(UIColor *)color;

/**
 指定左视图
 */
- (void)addleftImageViewWithImage:(UIImage *)image;

@end
