//
//  UITextField+XLCategory.m
//  vatti
//
//  Created by 李叶 on 2018/5/23.
//  Copyright © 2018年 xlink. All rights reserved.
//

#import "UITextField+XLCategory.h"

@implementation UITextField (XLCategory)
/** 清除输入框的首尾空格和换行符*/
- (NSString *)clearForeAndAftSpace{
    self.text = [self.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    return self.text;
}
- (NSString *)clearAllSpace {
    return [self.text stringByReplacingOccurrencesOfString:@" " withString:@""];
}

- (void)addShowPasswordButton {
    [self addShowPasswordButtonWithShowName:@"eye" hideName:@"eye_off"];
}

-(void)addShowPasswordButtonWithShowName:(NSString *)showName hideName:(NSString *)hideName{
    //密码明文
    [self addShowPasswordButtonWithShowImage:[UIImage imageNamed:showName] hideImage:[UIImage imageNamed:hideName]];
}

- (void)addShowPasswordButtonWithShowImage:(UIImage *)showImage hideImage:(UIImage *)hideImage {
    //密码明文
    UIButton *checkPwdButton = [UIButton buttonWithType:UIButtonTypeCustom];
    checkPwdButton.frame = CGRectMake(0, 0, 23, 23);
    [checkPwdButton addTarget:self action:@selector(checkPwdButtonClick:) forControlEvents:UIControlEventTouchUpInside];
    [checkPwdButton setImage:showImage forState:UIControlStateNormal];
    [checkPwdButton setImage:hideImage forState:UIControlStateSelected];
    checkPwdButton.selected = self.secureTextEntry;
    self.rightView = checkPwdButton;
    self.rightViewMode = UITextFieldViewModeAlways;
}
- (void)checkPwdButtonClick:(UIButton *)button{
    NSString *tempString = self.text;
    
    button.selected = !button.selected;
    self.secureTextEntry = button.selected;
    self.text = @"";
    self.text = tempString;
}

- (void)setPlaceholderColor:(UIColor *)color{
    [self setValue:color forKeyPath:@"_placeholderLabel.textColor"];
}

- (void)addUnderline {
    [self addUnderlineWithColor:[UIColor lightGrayColor]];
}

- (void)addUnderlineWithColor:(UIColor *)color {
    UIView *line = [[UIView alloc] initWithFrame:CGRectMake(0 ,CGRectGetHeight(self.frame) - 0.5, CGRectGetWidth(self.frame), 0.5)];
    line.backgroundColor = color;
    [self addSubview:line];
}

- (void)addleftImageViewWithImage:(UIImage *)image {
    UIImageView *imageView = [[UIImageView alloc] initWithImage:image];
    imageView.frame = CGRectMake(0, 0, 23, 23);
    imageView.contentMode = UIViewContentModeCenter;
    self.leftView = imageView;
    self.leftViewMode = UITextFieldViewModeAlways;
}
@end
