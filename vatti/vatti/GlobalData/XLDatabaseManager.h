//
//  XLDatabaseManager.h
//  vatti
//
//  Created by 李叶 on 2018/5/22.
//  Copyright © 2018年 xlink. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface XLDatabaseManager : NSObject

///Database path.
@property (nonatomic, copy, readonly) NSString *path;

- (BOOL)setCache:(NSDictionary *)cache forUserId:(NSNumber *)userId;

/**
 删除缓存
 */
- (BOOL)removeCache:(NSNumber *)userId;

- (NSDictionary *)cacheWithUserId:(NSNumber *)userId;

+ (instancetype)instance;

@end
