//
//  XLDataSource.m
//  vatti
//
//  Created by 李叶 on 2018/5/22.
//  Copyright © 2018年 xlink. All rights reserved.
//

#import "XLDataSource.h"
#import "XLUtilTool.h"
#import "XLNetworkModule.h"
#import "XLSDKModule.h"

@interface XLDataSource ()

@end

@implementation XLDataSource

+ (instancetype)shareInstance {
    static XLDataSource *xlDataSource;
    static dispatch_once_t onceToken;
    
    dispatch_once(&onceToken, ^{
        xlDataSource = [[XLDataSource alloc] init];
    });
    return xlDataSource;
}

#pragma mark - SDK

- (void)startSDK {
    [self startSDKWithAuthInfo:nil];
}

- (void)startSDKWithAuthInfo:(NSDictionary *)info {
    
    XLinkConfig *config = [[XLinkConfig alloc] init];
    
    config.corpId = XLCorpID;
    config.apiServer = XLXlinkApiServer;
    config.cloudServer = XLXlinkCloudServer;
    config.cloudServerPort = XLXlinkCloudPort;
    config.enableSSL = NO;
    config.debug = YES;

    if (info != nil && info.allKeys.count > 0) {
        XLinkUserModel *userModel = [[XLinkUserModel alloc] initWithDict:info];
        config.userModel = userModel;
        self.userState = XLUserStateConnecting;
        //读取本地缓存
        [self loadUserFromCache:userModel.user_id];
    }
    
    //设置sdk代理
    XLSDKModule *xlSDKModule = [XLSDKModule shareInstance];
    [xlSDKModule configXLinkSDKDelegate];

    [[XLinkSDK share] startWithConfig:config];
}

- (void)loadUserFromCache:(NSNumber *)userId {
    XLUserModel *userModel = [[XLUserModel alloc] init];
    userModel.userId = userId;
    NSDictionary *cacheKeyValues = [[XLDatabaseManager instance] cacheWithUserId:userModel.userId];
    if (cacheKeyValues) {
        [userModel setCacheKeyValues:cacheKeyValues];
    }
    self.user = userModel;
}

- (void)updateUser {
    if (self.user) {
        [self.user updateCache];
    }
}

- (void)appLogout {
    
    //清空本地授权信息
    [XLUtilTool clearLastLoginAuthorization];
    
    if (self.user) {
        [self.user removeCache];
    }
    
    //移除自动登录标记
    [XLUtilTool setAutoLogin:NO];
    
    //停止SDK, 断开云端连接，回收资源，清除授权信息。
    [[XLinkSDK share] logoutAndStop];
    
    //释放用户
    self.user = nil;
    
    //移除UserDefault消息计数
    [kXLNSUserDefaults setInteger:0 forKey:UDDataPointAlertCount];
    [kXLNSUserDefaults synchronize];
}

@end



