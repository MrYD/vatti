//
//  XLUserModel.h
//  vatti
//
//  Created by 李叶 on 2018/5/22.
//  Copyright © 2018年 xlink. All rights reserved.
//

#import "XLModel.h"

@class DeviceModel;

@interface XLUserModel : XLModel

///用户id
@property (copy,   nonatomic) NSNumber *userId;
///登录账号(登录页面数据的账号信息)
@property (copy,   nonatomic) NSString *account;
///账号昵称
@property (copy,   nonatomic) NSString *nickname;
///手机号码
@property (copy,   nonatomic) NSString *phone;
///邮箱
@property (copy,   nonatomic) NSString *email;
///头像
@property (copy,   nonatomic) NSString *avatar;
///所有的设备
@property (strong, nonatomic, readonly) NSArray <DeviceModel *> *deviceModels;

- (DeviceModel *)deviceModelWithXDevice:(XDevice *)device;
- (DeviceModel *)deviceModelWithDeviceId:(UInt32)deviceId;

- (void)setCacheKeyValues:(NSDictionary *)cacheKeyValues;

- (void)updateDeviceModelsWithDevices:(NSArray <XDevice *> *)devices;

- (void)addDevice:(XDevice *)device;

- (void)removeDeviceModel:(DeviceModel *)deviceModel;


/**
 存入数据库信息
 */
- (NSMutableDictionary *)cacheKeyValues;
/**
 更新缓存
 */
- (void)updateCache;
/**
 删除缓存
 */
- (void)removeCache;

@end
