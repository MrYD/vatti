//
//  XLUserModel.m
//  vatti
//
//  Created by 李叶 on 2018/5/22.
//  Copyright © 2018年 xlink. All rights reserved.
//

#import "XLUserModel.h"
#import <MJExtension/MJExtension.h>
#import "XLDatabaseManager.h"
#import "WaterHeaterDeviceModel.h"
#import "DishWasherDeviceModel.h"

@interface XLUserModel ()

@property (strong, nonatomic, readwrite) NSArray <DeviceModel *> *deviceModels;

@end

@implementation XLUserModel

+ (NSArray *)mj_allowedPropertyNames {
    return @[@"userId", @"account", @"nickname", @"phone", @"email", @"avatar"];
}

+ (NSDictionary *)mj_replacedKeyFromPropertyName {
    return @{@"userId" : @"id"};
}

- (void)setCacheKeyValues:(NSDictionary *)cacheKeyValues {
    [self mj_setKeyValues:cacheKeyValues];
}

- (void)updateDeviceModelsWithDevices:(NSArray<XDevice *> *)devices {
    NSMutableArray <DeviceModel *> *deviceModelArray = [NSMutableArray array];
    
    [devices enumerateObjectsUsingBlock:^(XDevice * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        DeviceModel *deviceModel = [self deviceModelWithXDevice:obj];
        if (deviceModel == nil)  {
            if ([obj.productID isEqualToString:XLProductGas] == YES) {
                deviceModel =  [[WaterHeaterDeviceModel alloc] initWithXDevice:obj];
            }else if ([obj.productID isEqualToString:@"1607d2b688d41f411607d2b688d47a01"]){
                deviceModel = [[DishWasherDeviceModel alloc] initWithXDevice:obj];
            }
            
            [deviceModel addObserver];
        }
        
        deviceModel.device.deviceName = obj.deviceName;
        deviceModel.name = obj.deviceName;
        
        if (deviceModel != nil) {
            [deviceModelArray addObject:deviceModel];
        }
        
    }];
    
    [self.deviceModels enumerateObjectsUsingBlock:^(DeviceModel * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        if ([deviceModelArray containsObject:obj] == NO) {
            [obj removeObserver];
        }
    }];
    
    self.deviceModels = [NSArray arrayWithArray:deviceModelArray];
}

- (DeviceModel *)deviceModelWithXDevice:(XDevice *)device {
    __block DeviceModel *deviceModel = nil;
    [self.deviceModels enumerateObjectsUsingBlock:^(DeviceModel * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        if ([obj.device isEqualToDevice:device]) {
            deviceModel = obj;
            *stop = YES;
        }
    }];
    return deviceModel;
}

- (DeviceModel *)deviceModelWithDeviceId:(UInt32)deviceId {
    __block DeviceModel *deviceModel = nil;
    [self.deviceModels enumerateObjectsUsingBlock:^(DeviceModel * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        if (obj.device.deviceID == deviceId) {
            deviceModel = obj;
            *stop = YES;
        }
    }];
    return deviceModel;
}

- (void)addDevice:(XDevice *)xDevice {
    NSMutableArray <DeviceModel *> *deviceModelArray = [NSMutableArray arrayWithArray:self.deviceModels];
    
    DeviceModel *deviceModel = [self deviceModelWithXDevice:xDevice];
    
    if (deviceModel == nil) {
        if ([xDevice.productID isEqualToString:XLProductGas] == YES) {
             deviceModel =  [[WaterHeaterDeviceModel alloc] initWithXDevice:xDevice];
        }else if ([xDevice.productID isEqualToString:@"1607d2b688d41f411607d2b688d47a01"]){
            deviceModel = [[DishWasherDeviceModel alloc] initWithXDevice:xDevice];
        }
       
        [deviceModel addObserver];
        
        if (deviceModel != nil) {
            [deviceModelArray addObject:deviceModel];
        }
    }
    
    deviceModel.device.deviceName = xDevice.deviceName;
    deviceModel.name              = xDevice.deviceName;
    
    self.deviceModels = [NSArray arrayWithArray:deviceModelArray];
}

- (void)removeDeviceModel:(DeviceModel *)deviceModel {
    NSMutableArray <DeviceModel *> *deviceModelArray = [NSMutableArray arrayWithArray:self.deviceModels];
    
    if ([deviceModelArray containsObject:deviceModel]) {
        [deviceModel removeObserver];
        [deviceModelArray removeObject:deviceModel];
    }
    self.deviceModels = [NSArray arrayWithArray:deviceModelArray];
}

- (NSMutableDictionary *)cacheKeyValues {
    NSMutableDictionary *cacheKeyValues = self.mj_keyValues;
    //现在只缓存登录信息，其他信息后期在做
    return cacheKeyValues;
}

- (void)updateCache {
    [[XLDatabaseManager instance] setCache:self.cacheKeyValues forUserId:_userId];
}

- (void)removeCache {
    [[XLDatabaseManager instance] removeCache:_userId];
}

#pragma mark - OverWrite

- (NSArray<DeviceModel *> *)deviceModels
{
    if (!_deviceModels) {
        _deviceModels = [[NSArray alloc] init];
    }
    return _deviceModels;
}

@end
