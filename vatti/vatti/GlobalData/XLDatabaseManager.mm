//
//  XLDatabaseManager.m
//  vatti
//
//  Created by 李叶 on 2018/5/22.
//  Copyright © 2018年 xlink. All rights reserved.
//

#import "XLDatabaseManager.h"


@interface XLDatabaseManager ()


@end

@implementation XLDatabaseManager

+ (instancetype)instance {
    static XLDatabaseManager *manager;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        manager = [[XLDatabaseManager alloc] init];
    });
    return manager;
}

- (BOOL)setCache:(NSDictionary *)cache forUserId:(NSNumber *)userId {
    NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
    [ud setObject:cache forKey:userId.stringValue];
    [ud synchronize];
    return YES;
}

/**
 删除缓存
 */
- (BOOL)removeCache:(NSNumber *)userId {
    NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
    [ud removeObjectForKey:userId.stringValue];
    [ud synchronize];
    return YES;
}

- (NSDictionary *)cacheWithUserId:(NSNumber *)userId {
    return [[NSUserDefaults standardUserDefaults] objectForKey:userId.stringValue];
}


@end
