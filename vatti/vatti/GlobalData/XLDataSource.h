//
//  XLDataSource.h
//  vatti
//
//  Created by 李叶 on 2018/5/22.
//  Copyright © 2018年 xlink. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "XLUserModel.h"

#define XL_DATA_SOURCE [XLDataSource shareInstance]

typedef NS_ENUM(NSUInteger, XLUserState) {
    XLUserStateInit,//初始化状态
    XLUserStateOffline,//用户离线
    XLUserStateKickout,//用户被踢下线
    XLUserStateTokenExpired,//授权过期
    XLUserStateOnline,//用户在线
    XLUserStateConnecting,//用户自动登录中
};
@interface XLDataSource : NSObject

/**
 初始化单例方法
 */
+ (instancetype)shareInstance;

///登录状态
@property (nonatomic, assign) XLUserState userState;

///云端登录状态
@property (nonatomic, assign) CloudConnectionState state;

///登录用户
@property (nonatomic, strong) XLUserModel *user;

#pragma mark - SDK

- (void)startSDK;

- (void)startSDKWithAuthInfo:(NSDictionary *)info;

- (void)updateUser;

- (void)appLogout;

@end
