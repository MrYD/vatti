//
//  XLTextField.h
//  vatti
//
//  Created by 李叶 on 2018/5/23.
//  Copyright © 2018年 xlink. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface XLTextField : UITextField

@property (nonatomic, assign) BOOL selectedState;

@property (nonatomic, assign) IBInspectable BOOL isNoNeedLine;
@property (nonatomic, strong)UIImageView *leftImageView;
@property (nonatomic, strong)UIImageView *rightImageView;

- (void)observeSelectedState;

- (void)addLeftImagePanding:(CGFloat)panding withImage:(UIImage *)image;
@end
