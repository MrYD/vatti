 //
//  XLTextField.m
//  vatti
//
//  Created by 李叶 on 2018/5/23.
//  Copyright © 2018年 xlink. All rights reserved.
//

#import "XLTextField.h"

@implementation XLTextField

- (void)awakeFromNib {
    [super awakeFromNib];
    [self addLeftImagePanding:10 withImage:nil];
}

- (void)observeSelectedState {
    [[self rac_signalForControlEvents:UIControlEventEditingDidBegin] subscribeNext:^(__kindof XLTextField * _Nullable x) {
        x.selectedState = YES;
    }];
    [[self rac_signalForControlEvents:UIControlEventEditingDidEnd] subscribeNext:^(__kindof XLTextField * _Nullable x) {
        x.selectedState = NO;
    }];
}

- (void)setSelectedState:(BOOL)selectedState {
    _selectedState = selectedState;
    [self setNeedsDisplay];
}

- (void)drawRect:(CGRect)rect {
    [super drawRect:rect];
    CGContextRef context = UIGraphicsGetCurrentContext();
    UIColor *color;
    if (self.isNoNeedLine) {
        color = [UIColor clearColor];
    }else {
        color = self.selectedState ? self.selectedColor : self.normalColor;
    }
    CGContextSetFillColorWithColor(context, color.CGColor);
    CGContextFillRect(context, CGRectMake(0, CGRectGetHeight(rect) - 1, CGRectGetWidth(rect), 1));
}

- (UIColor *)normalColor {
    static UIColor *normalColor = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        normalColor = [UIColor colorWithRed:189/255. green:189/255. blue:189/255. alpha:1];
    });
    return normalColor;
}

- (UIColor *)selectedColor {
    static UIColor *selectedColor = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        selectedColor = [UIColor colorWithRed:55/255. green:158/255. blue:231/255. alpha:1];
    });
    return selectedColor;
}

- (void)addLeftImagePanding:(CGFloat)panding withImage:(UIImage *)image
{
    UIView *pandingView = [[UIView alloc] init];
    self.leftImageView = [[UIImageView alloc] initWithImage:image];
    
    pandingView.frame = CGRectMake(0, -10, image.size.width+panding + 5, 45);
    [pandingView addSubview:self.leftImageView];
    [self.leftImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(pandingView.mas_centerX).offset(-5);
        make.centerY.mas_equalTo(pandingView.mas_centerY);
    }];
    self.leftView = pandingView;
    self.leftViewMode = UITextFieldViewModeAlways;
}



-(void)setIsNoNeedLine:(BOOL)isNoNeedLine {
    _isNoNeedLine = isNoNeedLine;
    [self setNeedsLayout];
    [self layoutIfNeeded];
}
@end
