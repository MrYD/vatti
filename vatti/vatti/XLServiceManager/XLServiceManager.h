//
//  XLServiceManager.h
//  vatti
//
//  Created by BZH on 2018/5/21.
//  Copyright © 2018年 xlink. All rights reserved.
//

/**
 *  网络请求服务对象
 *
 *  @author BZH
 */

#import <Foundation/Foundation.h>
#import "XLUserService.h"
#import "XLDeviceService.h"

@interface XLServiceManager : NSObject

+ (instancetype)shareInstance;

///用户服务
@property (strong, nonatomic) XLUserService *userService;

///设备服务
@property (strong, nonatomic) XLDeviceService *deviceService;

@end
