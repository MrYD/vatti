//
//  XLService.h
//  vatti
//
//  Created by 李叶 on 2018/5/22.
//  Copyright © 2018年 xlink. All rights reserved.
//

#import <Foundation/Foundation.h>

/**
 请求方式
 */
typedef NS_ENUM(NSUInteger, XLServiceRequestType) {
    XLServiceRequestTypeGET,
    XLServiceRequestTypePUT,
    XLServiceRequestTypePOST,
    XLServiceRequestTypeDELETE
};


@interface XLService : NSObject

#pragma mark - 需要使用AccessToken

- (void)GET:(NSString *)url content:(id)content timeout:(NSInteger)timeout handler:(void(^)(id result, NSError *error))handler;

- (void)PUT:(NSString *)url content:(id)content timeout:(NSInteger)timeout handler:(void(^)(id result, NSError *error))handler;

- (void)POST:(NSString *)url content:(id)content timeout:(NSInteger)timeout handler:(void(^)(id result, NSError *error))handler;

- (void)DELETE:(NSString *)url content:(id)content timeout:(NSInteger)timeout handler:(void(^)(id result, NSError *error))handler;

- (void)serviceRequestType:(XLServiceRequestType)type url:(NSString *)url content:(id)content timeout:(NSInteger)timeout handler:(void(^)(id, NSError *error))handler;

#pragma mark - 基础Request

- (void)baseServiceRequestType:(XLServiceRequestType)type url:(NSString *)url header:(NSDictionary *)header content:(id)content timeout:(NSInteger)timeout handler:(void(^)(id, NSError *error))handler;

#pragma mark - 插件请求

- (void)plugRequestType:(XLServiceRequestType)type url:(NSString *)url header:(NSDictionary *)header content:(id)content timeout:(NSInteger)timeout handler:(void(^)(id, NSError *error))handler;


@end
