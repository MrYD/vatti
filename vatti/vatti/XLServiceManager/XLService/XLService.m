//
//  XLService.m
//  vatti
//
//  Created by 李叶 on 2018/5/22.
//  Copyright © 2018年 xlink. All rights reserved.
//

#import "XLService.h"
#import "XLinkSDK.h"
#import "XLinkHttpRequest.h"

@implementation XLService

#pragma mark - 需要使用AccessToken
- (void)GET:(NSString *)url content:(id)content timeout:(NSInteger)timeout handler:(void (^)(id, NSError *))handler {
    [self serviceRequestType:XLServiceRequestTypeGET url:url content:content timeout:timeout handler:handler];
}

- (void)PUT:(NSString *)url content:(id)content timeout:(NSInteger)timeout handler:(void (^)(id, NSError *))handler {
    [self serviceRequestType:XLServiceRequestTypePUT url:url content:content timeout:timeout handler:handler];
}

- (void)POST:(NSString *)url content:(id)content timeout:(NSInteger)timeout handler:(void (^)(id, NSError *))handler {
    [self serviceRequestType:XLServiceRequestTypePOST url:url content:content timeout:timeout handler:handler];
}

- (void)DELETE:(NSString *)url content:(id)content timeout:(NSInteger)timeout handler:(void (^)(id, NSError *))handler {
    [self serviceRequestType:XLServiceRequestTypeDELETE url:url content:content timeout:timeout handler:handler];
}

- (void)serviceRequestType:(XLServiceRequestType)type url:(NSString *)url content:(id)content timeout:(NSInteger)timeout handler:(void (^)(id, NSError *))handler {
    if (![XLinkSDK share].userModel.access_token.length) {
        if (handler) {
            dispatch_async(dispatch_get_main_queue(), ^{
                handler(nil, [NSError errorWithDomain:@"[XLinkSDK share].userModel.access_token is not exist" code:-2 userInfo:@{NSLocalizedDescriptionKey : @"developer check"}]);
            });
        }
        NSLog(@"[XLinkSDK share].userModel.access_token is not exist");
        return;
    }
    NSDictionary *header = @{@"Content-Type" : @"application/json", @"Access-Token" : [XLinkSDK share].userModel.access_token};
    [self baseServiceRequestType:type url:url header:header content:content timeout:timeout handler:handler];
}

#pragma mark - 基础Request

- (void)baseServiceRequestType:(XLServiceRequestType)type url:(NSString *)url header:(NSDictionary *)header content:(id)content timeout:(NSInteger)timeout  handler:(void (^)(id, NSError *))handler {
    if (![XLinkSDK share].xlinkConfig.apiServer.length) {
        if (handler) {
            dispatch_async(dispatch_get_main_queue(), ^{
                handler(nil, [NSError errorWithDomain:@"[XLinkSDK share].xlinkConfig.apiServer is nil" code:-2 userInfo:@{NSLocalizedDescriptionKey : @"developer check"}]);
            });
        }
        NSLog(@"[XLinkSDK share].xlinkConfig.apiServer is nil");
        return;
    }
    NSString *httpType;
    switch (type) {
            case XLServiceRequestTypeGET:
            httpType = @"GET";
            break;
            case XLServiceRequestTypePUT:
            httpType = @"PUT";
            break;
            case XLServiceRequestTypePOST:
            httpType = @"POST";
            break;
            case XLServiceRequestTypeDELETE:
            httpType = @"DELETE";
            break;
        default:
            break;
    }
    if (!httpType.length || !url.length) {
        if (handler) {
            dispatch_async(dispatch_get_main_queue(), ^{
                handler(nil, [NSError errorWithDomain:@"type is wrong or url is nil" code:-2 userInfo:@{NSLocalizedDescriptionKey : @"developer check"}]);
            });
        }
        NSLog(@"type is wrong or url is nil");
        return;
    }
    [XLinkHttpRequest requestWithRequestType:httpType withUrl:url withHeader:header withContent:content withTimeout:timeout withDidLoadData:^(id result, NSError *err) {
        if (handler) {
            handler(result, err);
        }
    }];
}

- (void)plugRequestType:(XLServiceRequestType)type url:(NSString *)url header:(NSDictionary *)header content:(id)content timeout:(NSInteger)timeout handler:(void (^)(id, NSError *))handler {
    NSString *httpType;
    switch (type) {
            case XLServiceRequestTypeGET:
            httpType = @"GET";
            break;
            case XLServiceRequestTypePUT:
            httpType = @"PUT";
            break;
            case XLServiceRequestTypePOST:
            httpType = @"POST";
            break;
            case XLServiceRequestTypeDELETE:
            httpType = @"DELETE";
            break;
        default:
            break;
    }
    if (!httpType.length || !url.length) {
        if (handler) {
            dispatch_async(dispatch_get_main_queue(), ^{
                handler(nil, [NSError errorWithDomain:@"type is wrong or url is nil" code:-2 userInfo:@{NSLocalizedDescriptionKey : @"developer check"}]);
            });
        }
        NSLog(@"type is wrong or url is nil");
        return;
    }
    [XLinkHttpRequest pluginRequestWithRequestType:httpType withUrl:url withHeader:header withContent:content withTimeout:timeout withDidLoadData:handler];
}

@end
