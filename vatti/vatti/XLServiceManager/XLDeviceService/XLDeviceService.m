//
//  XLDeviceService.m
//  vatti
//
//  Created by 李叶 on 2018/5/22.
//  Copyright © 2018年 xlink. All rights reserved.
//

#import "XLDeviceService.h"
#import "XLinkSDK.h"
#import "XDeviceManager.h"
#import "XLinkDataPoint.h"
#import "XLinkSetDataPointTask.h"
#import "XLinkGetDataPointTask.h"
#import "XLinkAddDeviceTask.h"
#import "XLinkRemoveDeviceTask.h"
#import "XLinkProbeDataPointTask.h"

@implementation XLDeviceService

#pragma mark - 修改设备信息
/**
 修改设备名称
 
 @param deviceId        设备id
 @param productId       productId
 @param deviceName      设备名称
 @param timeout         超时时间
 @param handler         完成回调
 */
- (void)modifyDeviceNameWithDeviceId:(NSNumber *)deviceId
                           productId:(NSString *)productId
                          deviceName:(NSString *)deviceName
                             timeout:(int)timeout
                             handler:(void (^)(id, NSError *))handler {
    NSString *url = [NSString stringWithFormat:@"/v2/product/%@/device/%@", productId, deviceId];
    NSDictionary *header = @{@"Access-Token" : [XLinkSDK share].userModel.access_token};
    NSDictionary *content = @{@"name" : deviceName};
    [self PUT:url content:content timeout:timeout handler:handler];
}

#pragma mark - 添加/删除设备

/**
 添加设备
 */
- (XLinkTask *)addDevice:(XDevice *)device pinCode:(NSData *)pinCode timeout:(NSUInteger)timeout handler:(void(^)(XLinkErrorCode code))handler {
    XLinkAddDeviceTask *task = [XLinkAddDeviceTask addDeviceTaskWithDevice:device pinCode:pinCode timeout:timeout completionHandler:^(id  _Nullable result, NSError * _Nullable error) {
        if (handler) {
            handler(error.code);
        }
    }];
    [task start];
    return task;
}

/**
 删除设备
 */
- (XLinkRemoveDeviceTask *)removeDevice:(XDevice *)device
                                timeout:(NSUInteger)timeout
                      completionHandler:(XLinkTaskDidCompletionHandler)completionHandler{
    XLinkRemoveDeviceTask *task = [XLinkRemoveDeviceTask removeDeviceTaskWithDevice:device timeout:timeout completionHandler:completionHandler];
    [task start];
    return task;
}

#pragma mark - 控制设备

/**
 断开连接设备
 */
- (void)disconnectDevice:(XDevice *)device {
    [[XLinkSDK share].deviceManager disconnectDevice:device];
}

/**
 连接设备
 */
- (XLinkConnectDeviceTask *)connectDevice:(XDevice *)device
                                  timeout:(NSUInteger)timeout
                                  handler:(void(^)(XLinkErrorCode code))handler
{
    XLinkConnectDeviceTask *task = [XLinkConnectDeviceTask connectDeviceTaskWithDevice:device connectionPolicy:XDeviceConnectionPolicyAuto timeout:timeout completionHandler:^(id result, NSError *error) {
        if (handler) {
            handler(error.code);
        }
    }];
    [task start];
    return task;
}

#pragma mark - 获取设备数据端点

/**
 获取设备数据端点
 */
- (XLinkGetDataPointTask *)getDeviceDataPoints:(XDevice *)device
                                       timeout:(NSUInteger)timeout
                                       handler:(void(^)(NSArray<XLinkDataPoint *> *result, XLinkErrorCode code))handler
{
    XLinkGetDataPointTask *task = [XLinkGetDataPointTask getDataPointTaskWithDevice:device timeout:timeout completionHandler:^(id  _Nullable result, NSError * _Nullable error) {
        if ([result isKindOfClass:[NSArray class]]) {
            NSMutableArray *array = [NSMutableArray arrayWithCapacity:0];
            for (XLinkDataPoint *point in result) {
                NSString *str = [NSString stringWithFormat:@"name:%@,index:%u,value:%@",point.name,point.index,point.value];
                [array addObject:str];
            }
            NSLog(@"接收数据端数据：%@",array);
        }

        if (handler) {
            handler(result,error.code);
        }
    }];
    task.sendDataPolicy = XLinkSendDataPolicyCloudFirst;
    
    [task start];
    return task;
}

/**
 同步数据
 */
- (XLinkProbeDataPointTask*)probeDataPointWithDevice:(XDevice*)xDevice
                                       andIndexArray:(NSArray*)indexArray
                                          andTimeOut:(NSUInteger)timeOut
                                         andCallBack:(XLinkTaskDidCompletionHandler)callBack
{
    XLinkProbeDataPointTask *task = [XLinkProbeDataPointTask probeDataPointTaskWithDevice:xDevice indexArray:indexArray timeout:timeOut completionHandler:callBack];
    [task start];
    return task;
}

#pragma mark - 设置设备数据端点

/**
 设置设备数据端点(发送策略全局[XLinkSDK share].xlinkConfig.sendDataPolicy)
 
 @param device 设备模型
 @param dataPoints 数据端点
 @param timeout 超时时间
 @param handler 完成回调
 @return XLinkSetDataPointTask
 */
- (XLinkSetDataPointTask *)setDeviceDataPoints:(XDevice *)device
                                    dataPoints:(NSArray <XLinkDataPoint *>*)dataPoints
                                       timeout:(NSUInteger)timeout
                                       handler:(XLinkTaskDidCompletionHandler)handler
{
    NSMutableArray *array = [NSMutableArray arrayWithCapacity:0];
    for (XLinkDataPoint *point in dataPoints) {
        NSString *str = [NSString stringWithFormat:@"name:%@,index:%u,value:%@",point.name,point.index,point.value];
        [array addObject:str];
    }
    NSLog(@"发送数据端数据：%@",array);

    self.dataPointArray = dataPoints;
    
    XLinkSetDataPointTask *task = [XLinkSetDataPointTask setDataPointTaskWithDevice:device dataPoints:dataPoints timeout:timeout completionHandler:handler];
    [task start];
    
    return task;
}

@end
