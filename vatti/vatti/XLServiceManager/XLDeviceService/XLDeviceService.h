//
//  XLDeviceService.h
//  vatti
//
//  Created by 李叶 on 2018/5/22.
//  Copyright © 2018年 xlink. All rights reserved.
//

#import "XLService.h"

@class XDevice;
@class XLinkDataPoint;
@class XLinkAddDeviceTask;
@class XLinkRemoveDeviceTask;
@class XLinkSetDataPointTask;
@class XLinkProbeDataPointTask;
@class XLinkConnectDeviceTask;
@class XLinkGetDataPointTask;

@interface XLDeviceService : XLService

#pragma mark - @property

///dataPoint
@property (strong, nonatomic) NSArray <XLinkDataPoint*> *dataPointArray;

#pragma mark - 修改设备信息
/**
 修改设备名称
 
 @param deviceId        设备id
 @param productId       productId
 @param deviceName      设备名称
 @param timeout         超时时间
 @param handler         完成回调
 */
- (void)modifyDeviceNameWithDeviceId:(NSNumber *)deviceId
                           productId:(NSString *)productId
                          deviceName:(NSString *)deviceName
                             timeout:(int)timeout
                             handler:(void(^)(id result, NSError *error))handler;

#pragma mark - 添加/删除设备
/**
 添加设备
 
 @param device 设备模型
 @param pinCode 安全码，一般为设备会显示出来，需要设备支持。没有的时候传nil即可
 @param timeout 超时时间
 @param handler 完成回调
 @return XLinkAddDeviceTask
 */
- (XLinkAddDeviceTask *)addDevice:(XDevice *)device
                          pinCode:(NSData *)pinCode
                          timeout:(NSUInteger)timeout
                          handler:(void(^)(XLinkErrorCode code))handler;

/**
 删除设备
 
 @param device 设备模型
 @param timeout 超时时间
 @param completionHandler 完成回调
 @return XLinkRemoveDeviceTask
 */
- (XLinkRemoveDeviceTask *)removeDevice:(XDevice *)device
                                timeout:(NSUInteger)timeout
                      completionHandler:(XLinkTaskDidCompletionHandler)completionHandler;

#pragma mark - 控制设备

/**
 断开连接设备
 
 @param device 设备模型
 */
- (void)disconnectDevice:(XDevice *)device;

/**
 连接设备
 
 @param device 设备模型
 @param timeout 超时时间
 @param handler 完成回调
 @return XLinkConnectDeviceTask
 */
- (XLinkConnectDeviceTask *)connectDevice:(XDevice *)device
                                  timeout:(NSUInteger)timeout
                                  handler:(void(^)(XLinkErrorCode code))handler;

#pragma mark - 获取设备数据端点
/**
 获取设备数据端点(获取策略全局[XLinkSDK share].xlinkConfig.sendDataPolicy)
 
 @param device 设备模型
 @param timeout 超时
 @param handler 完成超时
 @return XLinkGetDataPointTask
 */
- (XLinkGetDataPointTask *)getDeviceDataPoints:(XDevice *)device
                                       timeout:(NSUInteger)timeout
                                       handler:(void(^)(NSArray<XLinkDataPoint *> *result, XLinkErrorCode code))handler;

/**
 同步数据
 */
- (XLinkProbeDataPointTask*)probeDataPointWithDevice:(XDevice*)xDevice
                                       andIndexArray:(NSArray*)indexArray
                                          andTimeOut:(NSUInteger)timeOut
                                         andCallBack:(XLinkTaskDidCompletionHandler)callBack;

#pragma mark - 设置设备数据端点

/**
 设置设备数据端点(发送策略全局[XLinkSDK share].xlinkConfig.sendDataPolicy)
 
 @param device 设备模型
 @param dataPoints 数据端点
 @param timeout 超时时间
 @param handler 完成回调
 @return XLinkSetDataPointTask
 */
- (XLinkSetDataPointTask *)setDeviceDataPoints:(XDevice *)device
                                    dataPoints:(NSArray <XLinkDataPoint *>*)dataPoints
                                       timeout:(NSUInteger)timeout
                                       handler:(XLinkTaskDidCompletionHandler)handler;

@end
