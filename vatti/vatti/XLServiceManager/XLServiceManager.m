//
//  XLServiceManager.m
//  vatti
//
//  Created by BZH on 2018/5/21.
//  Copyright © 2018年 xlink. All rights reserved.
//

#import "XLServiceManager.h"

@implementation XLServiceManager

+ (instancetype)shareInstance {
    static XLServiceManager *instance;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        instance = [[XLServiceManager alloc] init];
    });
    return instance;
}

- (XLUserService *)userService {
    if (!_userService) {
        _userService = [[XLUserService alloc] init];
    }
    return _userService;
}

- (XLDeviceService *)deviceService {
    if(!_deviceService) {
        _deviceService = [[XLDeviceService alloc]init];
    }
    return _deviceService;
}

@end
