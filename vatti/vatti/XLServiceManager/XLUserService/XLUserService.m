//
//  XLUserService.m
//  vatti
//
//  Created by 李叶 on 2018/5/22.
//  Copyright © 2018年 xlink. All rights reserved.
//

#import "XLUserService.h"
#import "XLinkSDK.h"
#import "XLinkUserAuthorizeTask.h"
#import "XLinkGetDeviceListTask.h"
#import "XLinkSyncDeviceListTask.h"
#import "XLServiceManager.h"

@interface XLUserService()

@property (strong, nonatomic) XLinkHttpRequest *request;

@end

@implementation XLUserService
/**
 1. 发送激活邮件注册账号
 */
- (void)registerAccountSendEmailWithEmail:(NSString *)email nickname:(NSString *)nickname corpId:(NSString *)corpId password:(NSString *)password localLang:(NSString *)localLang pluginId:(NSString *)pluginId timeout:(NSInteger)timeout handler:(void (^)(id, NSError *))handler {
    
}

/**
 3 发送注册手机验证码
 */
- (void)sendRegisterVerifyCodeWithPhone:(NSString *)phone phoneZone:(NSString *)phoneZone captcha:(NSString *)captcha corpId:(NSString *)corpId timeout:(NSInteger)timeout handler:(void (^)(id, NSError *))handler {
    NSMutableDictionary *content = [NSMutableDictionary dictionaryWithObject:corpId forKey:@"corp_id"];
    [content setValue:phone forKey:@"phone"];
    if (phoneZone.length) {
        [content setObject:phoneZone forKey:@"phone_zone"];
    }
    if (captcha.length > 0) {
        [content setObject:captcha forKey:@"captcha"];
    }
    NSString *url =  @"/v2/user_register/verifycode";
    [self baseServiceRequestType:XLServiceRequestTypePOST url:url header:@{@"Content-Type" : @"application/json"} content:content timeout:timeout handler:handler];
}

/**
 3.1 发送注册邮箱验证码
 */
- (void)sendRegisterVerifyCodeWithEmail:(NSString *)email corpId:(NSString *)corpId localLang:(NSString *)localLang timeout:(NSInteger)timeout handler:(void (^)(id, NSError *))handler {
    NSMutableDictionary *content = [NSMutableDictionary dictionaryWithObject:corpId forKey:@"corp_id"];
    [content setObject:email forKey:@"email"];
    if (localLang.length) {
        [content setObject:localLang forKey:@"local_lang"];
    }
    NSString *url =  @"/v2/user_register/email/verifycode";
    [self baseServiceRequestType:XLServiceRequestTypePOST url:url header:@{@"Content-Type" : @"application/json"} content:content timeout:timeout handler:handler];
}

/**
 4 请求注册手机图形验证码
 */
- (void)sendRegisterCaptchaWithPhone:(NSString *)phone phoneZone:(NSString *)phoneZone corpId:(NSString *)corpId timeout:(NSInteger)timeout handler:(void (^)(id, NSError *))handler {
    NSMutableDictionary *content = [NSMutableDictionary dictionaryWithObject:corpId forKey:@"corp_id"];
    [content setValue:phone forKey:@"phone"];
    if (phoneZone.length) {
        [content setObject:phoneZone forKey:@"phone_zone"];
    }
    NSString *url =  @"/v2/user_register/captcha";
    [self baseServiceRequestType:XLServiceRequestTypePOST url:url header:@{@"Content-Type" : @"application/json"} content:content timeout:timeout handler:handler];
}

/**
 5 通过手机验证码注册新账号
 */
- (void)registerAccountWithPhone:(NSString *)phone phoneZone:(NSString *)phoneZone nickname:(NSString *)nickname verifyCode:(NSString *)verifyCode password:(NSString *)password corpId:(NSString *)corpId localLang:(NSString *)localLang pluginId:(NSString *)pluginId timeout:(NSInteger)timeout handler:(void (^)(id, NSError *))handler {
    NSMutableDictionary *content = [NSMutableDictionary dictionaryWithObject:corpId forKey:@"corp_id"];
    if (phoneZone.length) {
        [content setObject:phoneZone forKey:@"phone_zone"];
    }
    if (nickname.length > 0) {
        [content setObject:nickname forKey:@"nickname"];
    }
    [content setObject:phone forKey:@"phone"];
    [content setObject:password forKey:@"password"];
    [content setObject:@"3" forKey:@"source"];
    [content setObject:verifyCode forKey:@"verifycode"];
    if (localLang.length) {
        [content setObject:localLang forKey:@"local_lang"];
    }
    if (pluginId.length) {
        [content setObject:pluginId forKey:@"plugin_id"];
    }
    NSString *url =  @"/v2/user_register";
    [self baseServiceRequestType:XLServiceRequestTypePOST url:url header:@{@"Content-Type" : @"application/json"} content:content timeout:timeout handler:handler];
}

/**
 5.1 通过邮箱验证码注册新账号
 */
- (void)registerAccountWithEmail:(NSString *)email nickname:(NSString *)nickname verifyCode:(NSString *)verifyCode password:(NSString *)password corpId:(NSString *)corpId localLang:(NSString *)localLang timeout:(NSInteger)timeout handler:(void (^)(id, NSError *))handler {
    NSMutableDictionary *content = [NSMutableDictionary dictionaryWithObject:corpId forKey:@"corp_id"];
    if (nickname.length > 0) {
        [content setObject:nickname forKey:@"nickname"];
    }
    [content setObject:email forKey:@"email"];
    [content setObject:password forKey:@"password"];
    [content setObject:@"3" forKey:@"source"];
    [content setObject:verifyCode forKey:@"verifycode"];
    if (localLang.length) {
        [content setObject:localLang forKey:@"local_lang"];
    }
    NSString *url =  @"/v2/user_register/email";
    [self baseServiceRequestType:XLServiceRequestTypePOST url:url header:@{@"Content-Type" : @"application/json"} content:content timeout:timeout handler:handler];
}

/**
 8 获取账号的详细信息
 */
- (void)getAccountInfomationWithUserId:(NSNumber *)userId timeout:(NSInteger)timeout handler:(void (^)(id, NSError *))handler {
    NSString *url = [NSString stringWithFormat:@"/v2/user/%@?filter=setting", userId];
    [self GET:url content:nil timeout:timeout handler:handler];
}

/**
 10 修改密码
 */
- (void)modifyAccountPasswordWithOldPassowrd:(NSString *)oldPassword newPassword:(NSString *)newPassword timeout:(NSInteger)timeout handler:(void (^)(id, NSError *))handler {
    NSMutableDictionary *content = [NSMutableDictionary dictionary];
    [content setObject:newPassword forKey:@"new_password"];
    [content setObject:oldPassword forKey:@"old_password"];
    NSString *url = @"/v2/user/password/reset";
    [self PUT:url content:content timeout:timeout handler:handler];
}

/**
 11.1-A 获取手机重置密码验证码
 */
- (void)sendResetPasswordVerifyCodeWithPhone:(NSString *)phone captcha:(NSString *)captcha phoneZone:(NSString *)phoneZone corpId:(NSString *)corpId timeout:(NSInteger)timeout handler:(void (^)(id, NSError *))handler {
    NSMutableDictionary *content = [NSMutableDictionary dictionaryWithObject:corpId forKey:@"corp_id"];
    [content setObject:phone forKey:@"phone"];
    if (phoneZone.length) {
        [content setObject:phoneZone forKey:@"phone_zone"];
    }
    if (captcha.length > 0) {
        [content setObject:captcha forKey:@"captcha"];
    }
    NSString *url =  @"/v2/user/password/forgot";
    [self baseServiceRequestType:XLServiceRequestTypePOST url:url header:@{@"Content-Type" : @"application/json"} content:content timeout:timeout handler:handler];
}

/**
 11.1-B 发送手机重置密码图形验证码
 */
- (void)sendResetPasswordCaptchaWithPhone:(NSString *)phone corpId:(NSString *)corpId phoneZone:(NSString *)phoneZone timeout:(NSInteger)timeout handler:(void (^)(id, NSError *))handler {
    NSMutableDictionary *content = [NSMutableDictionary dictionaryWithObject:corpId forKey:@"corp_id"];
    [content setObject:phone forKey:@"phone"];
    if (phoneZone.length) {
        [content setObject:phoneZone forKey:@"phone_zone"];
    }
    NSString *url = @"/v2/user/password/captcha";
    [self baseServiceRequestType:XLServiceRequestTypePOST url:url header:@{@"Content-Type" : @"application/json"} content:content timeout:timeout handler:handler];
}

/**
 11.1-C 重置手机账号密码
 */
- (void)resetAccountPasswordWithPhone:(NSString *)phone verifyCode:(NSString *)verifyCode password:(NSString *)password corpId:(NSString *)corpId phoneZone:(NSString *)phoneZone timeout:(NSInteger)timeout handler:(void (^)(id, NSError *))handler {
    NSMutableDictionary *content = [NSMutableDictionary dictionaryWithObject:corpId forKey:@"corp_id"];
    [content setObject:phone forKey:@"phone"];
    [content setObject:verifyCode forKey:@"verifycode"];
    [content setObject:password forKey:@"new_password"];
    if (phoneZone.length) {
        [content setObject:phoneZone forKey:@"phone_zone"];
    }
    NSString *url = @"/v2/user/password/foundback";
    [self baseServiceRequestType:XLServiceRequestTypePOST url:url header:@{@"Content-Type" : @"application/json"} content:content timeout:timeout handler:handler];
}

/**
 11.2-A 发送重置密码邮箱(邮箱收到的是验证码/重置密码连接)(具体看云平台设置)
 */
- (void)sendResetPasswordEmailWithEmail:(NSString *)email captcha:(NSString *)captcha localLang:(NSString *)localLang corpId:(NSString *)corpId timeout:(NSInteger)timeout handler:(void (^)(id, NSError *))handler {
    NSMutableDictionary *content = [NSMutableDictionary dictionaryWithObject:corpId forKey:@"corp_id"];
    [content setObject:email forKey:@"email"];
    if (localLang.length) {
        [content setObject:localLang forKey:@"local_lang"];
    }
    if (captcha.length > 0) {
        [content setObject:captcha forKey:@"captcha"];
    }
    NSString *url =  @"/v2/user/password/forgot";
    [self baseServiceRequestType:XLServiceRequestTypePOST url:url header:@{@"Content-Type" : @"application/json"} content:content timeout:timeout handler:handler];
}

/**
 11.2-A-2 发送找回密码的邮件激活码
 */
- (void)sendResetPasswordVerifyCodeWithEmail:(NSString *)email localLang:(NSString *)localLang corpId:(NSString *)corpId timeout:(NSInteger)timeout handler:(void (^)(id, NSError *))handler {
    NSMutableDictionary *content = [NSMutableDictionary dictionaryWithObject:corpId forKey:@"corp_id"];
    [content setObject:email forKey:@"email"];
    if (localLang.length) {
        [content setObject:localLang forKey:@"local_lang"];
    }
    NSString *url =  @"/v2/user/password/forgot/email_verifycode";
    [self baseServiceRequestType:XLServiceRequestTypePOST url:url header:@{@"Content-Type" : @"application/json"} content:content timeout:timeout handler:handler];
}

/**
 11.2-B 发送邮件重置密码图形验证码
 */
- (void)sendResetPasswordCaptchaWithEmail:(NSString *)email corpId:(NSString *)corpId timeout:(NSInteger)timeout handler:(void (^)(id, NSError *))handler {
    NSMutableDictionary *content = [NSMutableDictionary dictionaryWithObject:corpId forKey:@"corp_id"];
    [content setObject:email forKey:@"email"];
    NSString *url = @"/v2/user/password/captcha";
    [self baseServiceRequestType:XLServiceRequestTypePOST url:url header:@{@"Content-Type" : @"application/json"} content:content timeout:timeout handler:handler];
}

/**
 11.2-C 重置邮箱账号密码
 */
- (void)resetAccountPasswordWithEmail:(NSString *)email verifyCode:(NSString *)verifyCode password:(NSString *)password corpId:(NSString *)corpId timeout:(NSInteger)timeout handler:(void (^)(id, NSError *))handler {
    NSMutableDictionary *content = [NSMutableDictionary dictionaryWithObject:corpId forKey:@"corp_id"];
    [content setObject:email forKey:@"email"];
    [content setObject:verifyCode forKey:@"verifycode"];
    [content setObject:password forKey:@"new_password"];
    NSString *url = @"/v2/user/password/foundback";
    [self baseServiceRequestType:XLServiceRequestTypePOST url:url header:@{@"Content-Type" : @"application/json"} content:content timeout:timeout handler:handler];
}

/**
 12 校验手机验证码(无论校验结果成功与失败，手机验证码被校验一次立即失效。返回一个新的验证码用于完成后续的流程)
 */
- (void)verifyPhoneVerifyCodeWithPhone:(NSString *)phone corpId:(NSString *)corpId phoneZone:(NSString *)phoneZone verifyCode:(NSString *)verifyCode timeout:(NSInteger)timeout handler:(void (^)(id, NSError *))handler {
    NSMutableDictionary *content = [NSMutableDictionary dictionaryWithObject:corpId forKey:@"corp_id"];
    [content setObject:phone forKey:@"phone"];
    [content setObject:verifyCode forKey:@"verifycode"];
    if (phoneZone.length) {
        [content setObject:phoneZone forKey:@"phoneZone"];
    }
    NSString *url = @"/v2/user/verifycode/verify";
    [self baseServiceRequestType:XLServiceRequestTypePOST url:url header:@{@"Content-Type" : @"application/json"} content:content timeout:timeout handler:handler];
}

/**
 7.1 登录账号
 */
- (XLinkTask *)loginWithAccount:(NSString *)account password:(NSString *)password corpId:(NSString *)corpId timeout:(NSInteger)timeout handler:(void (^)(id, NSError *))handler {
    
    XLinkUserAuthorizeTask *task = [XLinkUserAuthorizeTask userAuthorizeTaskWithAccount:account password:password cropId:corpId timeout:timeout completionHandler:handler];
    [task start];
    
    return task;
}

/**
 9.1 获取用户订阅设备列表
 */
- (XLinkTask *)getSubscribeDeviceListWithVersion:(int)version userId:(NSNumber *)userId timeout:(NSInteger)timeout handler:(void (^)(NSArray<XDevice *> *, XLinkErrorCode))handler {
    
    XLinkGetDeviceListTask *task = [XLinkGetDeviceListTask getDeviceListTaskWithVersion:version timeout:timeout completionHandler:^(id result, NSError *error) {
        NSArray <XDevice*> *deviceArray = result;
        if (handler) {
            handler(deviceArray,error.code);
        }
    }];
    
    [task start];
    
    return task;
    
}

/**
 9.2 获取用户订阅设备列表并连接
 */
- (XLinkTask *)getSubscribeDeviceListAndConnectDeviceWithVersion:(int)version userId:(NSNumber *)userId localConnect:(BOOL)localConnect timeout:(NSInteger)timeout handler:(void (^)(NSArray<XDevice *> *, XLinkErrorCode))handler {
    
    XLinkSyncDeviceListTask *task = [XLinkSyncDeviceListTask syncDeviceListTaskWithVersion:version timeout:timeout connectLocal:localConnect completionHandler:^(id result, NSError *error) {
        NSArray <XDevice*> *deviceArray = result;
        if (handler) {
            handler(deviceArray,error.code);
        }
    }];
    
    [task start];
    
    return task;
    
    
}

/*
 *  用户上传头像，并且用上传头像的url设置用户的头像属性avatar
 */
- (void)uploadUserIconWithImage:(NSData *)imageData handler:(void(^)(id result, NSError *error))handler { 
    [self POST:@"/v2/user/avatar/upload?avatarType=jpg" content:imageData timeout:60. handler:handler];
}

/*
 *  修改昵称
 *  @param nickName 昵称
 *  @return 是否修改成功
 */
- (void)modifyNickName:(NSString *)nickName Callback:(void (^)(BOOL, NSString *, NSString *))callback{
    
    for (int i = 0; i<nickName.length; i++) {
        char commitChar = [nickName characterAtIndex:i];
        NSString *temp = [nickName substringWithRange:NSMakeRange(i,1)];
        const char *u8Temp = [temp UTF8String];
        if (3==strlen(u8Temp)){
            
            NSLog(@"字符串中含有中文");
        }else if((commitChar>64)&&(commitChar<91)){
            
            NSLog(@"字符串中含有大写英文字母");
        }else if((commitChar>96)&&(commitChar<123)){
            
            NSLog(@"字符串中含有小写英文字母");
        }else if((commitChar>47)&&(commitChar<58)){
            
            NSLog(@"字符串中含有数字");
        }else{
            if (callback) callback(NO, nil,@"昵称格式仅支持汉字、字母、数字");
            return;
        }
    }
    
    NSString *token = [XLinkSDK share].userModel.access_token;
    if (token.length == 0) {
        if (callback) callback(NO,nil,@"丢失参数。");
        return;
    }
    NSNumber *userID = [XLinkSDK share].userModel.user_id;
    if (userID.integerValue == 0) {
        if (callback) callback(NO,nil,@"丢失参数。");
        return;
    }
    NSString *url = [NSString stringWithFormat:@"/v2/user/%@",userID];
    [self PUT:url content:@{@"nickname": nickName} timeout:TimeOutSeconds handler:^(id result, NSError *error) {
        if (error == nil) {
            if (callback) callback(YES, nickName,nil);
        }else{
            if (callback) callback(NO, nil,@"网络连接失败，请检查网络是否正常");
        }
    }];
}

/**
 *  修改密码
 *
 *  @param oldPassword      旧密码
 *  @param newPassword      新密码
 *  @param handler       完成后的回调
 */
- (void)modifyPasswordWithOldPassword:(NSString *)oldPassword NewPassword:(NSString *)newPassword handler:(void (^)(id, NSError *))handler{
    NSString *url = @"/v2/user/password/reset";
    [self PUT:url content:@{@"old_password": oldPassword, @"new_password": newPassword} timeout:TimeOutSeconds handler:handler];
}

/**
 获取消息列表
 */
- (void)getMessageListWithUserID:(NSNumber *)userID withQueryDictionary:(NSDictionary *)dic andCallBack:(DictCallBack)callback{
    NSString *url = [NSString stringWithFormat:@"/v2/user/%@/messages",userID];
    [self POST:url content:dic timeout:TimeOutSeconds handler:callback];
}

/**
 获取用户收到的邀请记录列表
 */
- (void)getShareDeviceListWithCallBack:(ArrCallBack)callBack
{
    NSString *url = [NSString stringWithFormat:@"/v2/share/device/list"];
    [self GET:url content:nil timeout:TimeOutSeconds handler:callBack];
}

/**
 接受邀请
 */
- (void)acceptInvitationInviteID:(NSDictionary *)dic CallBack:(DictCallBack)callback{
    NSString *url = [NSString stringWithFormat:@"/v2/share/device/accept"];
    [self POST:url content:dic timeout:TimeOutSeconds handler:callback];
}

/**
 拒绝邀请
 */
- (void)refusedInvitationInviteID:(NSDictionary *)dic CallBack:(void (^)(NSDictionary *result, NSError *error))callback{
    NSString *url = [NSString stringWithFormat:@"/v2/share/device/deny"];
    [self POST:url content:dic timeout:TimeOutSeconds handler:callback];
}

/**
 删除设备分享记录
 */
- (void)deleteDeviceShareRecordWithInviteCode:(NSString*)inviteCode withCallBack:(DictCallBack)callback
{
    NSString *url = [NSString stringWithFormat:@"/v2/share/device/delete/%@",inviteCode];
    [self DELETE:url content:nil timeout:TimeOutSeconds handler:callback];
}

/**
 获取用户公开信息
 */
- (void)deleteMessagesWithMessageIDArray:(NSArray*)messageIDArray andCallBack:(DictCallBack)callback
{
    NSString *url = [NSString stringWithFormat:@"/v2/user/%@/message_delete",XL_DATA_SOURCE.user.userId];
    [self POST:url content:messageIDArray timeout:TimeOutSeconds handler:callback];
}

/**
 获取用户公开信息
 */
- (void)getOpenInfoUserID:(NSNumber *)userID CallBack:(void (^)(NSDictionary *, NSError *))callback{
    [XLinkHttpRequest getUserOpenInfoWithUserID:userID didLoadData:callback];
}

/**
 第三方登录
 
 @param sourceType 登录平台
 @param openId 790760921287143424
 @param accessToken 第三方用户Token
 @param name 第三方用户昵称
 @param resource 登录源
 @param pluginId 应用插件ID
 @param callBack 回调
 */
- (void)getUserInfoWithThirdSourceType:(XLinkUserSourceType)sourceType openId:(NSString*)openId andAccessToken:(NSString*)accessToken andName:(NSString*)name andResource:(NSString*)resource andPluginId:(NSString*)pluginId withCallBack:(DictCallBack)callBack{
    
    XLinkThirdPartyAuthorizeTask *task = [XLinkThirdPartyAuthorizeTask thirdPartyUserAuthorizeTaskWithSource:sourceType openId:openId accessToken:accessToken nickName:name cropId:XLCorpID timeout:TimeOutSeconds completionHandler:callBack];
    [task start];
}

/**
 获取用户详细信息
 */
- (void)getUserInfoDetailWithUserID:(NSNumber*)userID withCallBack:(DictCallBack)callBack
{
    [XLinkHttpRequest getUserInfoWithUserID:userID withAccessToken:[XLinkSDK share].userModel.access_token didLoadData:callBack];
}

/**
 获取修改手机号码验证码
 */
- (void)getModifyPhoneSMSVerifycodeWithUserID:(NSNumber*)userID andPhone:(NSString*)phone andCaptcha:(NSString*)captcha withCallBack:(DictCallBack)callBack
{
    NSString *url = [NSString stringWithFormat:@"/v2/user/%@/phone/sms_verifycode",userID];
    
    NSMutableDictionary *parameterDict = [NSMutableDictionary dictionary];
    
    if (phone.length > 0) {
        [parameterDict setObject:phone forKey:@"phone"];
    }
    
    if (captcha.length > 0) {
        [parameterDict setObject:captcha forKey:@"captcha"];
    }
    
    [self POST:url content:parameterDict timeout:TimeOutSeconds handler:callBack];
}

/**
 通过验证码修改手机号码
 */
- (void)modifyPhoneWithSMSVerifyCode:(NSString*)verifyCode andPhone:(NSString*)phone andPassWord:(NSString*)passWord andUserID:(NSNumber*)userID withCallBack:(DictCallBack)callBack
{
    NSString *url = [NSString stringWithFormat:@"/v2/user/%@/phone",userID];
    
    NSMutableDictionary *parameterDict = [NSMutableDictionary dictionary];
    
    if (phone.length > 0) {
        [parameterDict setObject:phone forKey:@"phone"];
    }
    
    if (verifyCode.length > 0) {
        [parameterDict setObject:verifyCode forKey:@"verifycode"];
    }
    
    if (passWord.length > 0) {
        [parameterDict setObject:passWord forKey:@"password"];
    }
    
    [self PUT:url content:parameterDict timeout:TimeOutSeconds handler:callBack];
}

/**
 获取图片验证码
 */
- (void)getPicVerifyCodeWithUserID:(NSNumber*)userID andPhone:(NSString*)phone withCallBack:(DictCallBack)callBack
{
    NSString *url = [NSString stringWithFormat:@"/v2/user/%@/phone/sms_captcha",userID];
    
    NSMutableDictionary *paramDict = [NSMutableDictionary dictionary];
    [paramDict setObject:phone forKey:@"phone"];
    
    [self POST:url content:paramDict timeout:TimeOutSeconds handler:callBack];
}

@end
