//
//  XLUserService.h
//  vatti
//
//  Created by 李叶 on 2018/5/22.
//  Copyright © 2018年 xlink. All rights reserved.
//

#import "XLService.h"
#import "XLinkErrorCode.h"
#import "XDevice.h"
#import "XLinkTask.h"
#import "XLinkThirdPartyAuthorizeTask.h"

typedef void(^DictCallBack)(NSDictionary* dic, NSError* error);
typedef void(^ArrCallBack)(NSArray* arr, NSError* error);

@interface XLUserService : XLService

/**
 发送激活邮件注册账号
 
 @param email 邮箱地址
 @param nickname 昵称(非)
 @param corpId 企业Id
 @param password 密码
 @param localLang 本地语言(非)
 @param pluginId 应用插件Id(非)
 @param timeout 超时时间
 @param handler 完成后回调
 */
- (void)registerAccountSendEmailWithEmail:(NSString *)email
                                 nickname:(NSString *)nickname
                                   corpId:(NSString *)corpId
                                 password:(NSString *)password
                                localLang:(NSString *)localLang
                                 pluginId:(NSString *)pluginId
                                  timeout:(NSInteger)timeout
                                  handler:(void(^)(id result, NSError *error))handler;

/**
 发送注册手机验证码
 
 @param phone   手机号码
 @param phoneZone 手机区号(非,默认+86)
 @param captcha 图形验证码（超过6次才需要,没有传nil）
 @param corpId 企业Id
 @param timeout 超时时间
 @param handler 完成后回调
 */
- (void)sendRegisterVerifyCodeWithPhone:(NSString *)phone
                              phoneZone:(NSString *)phoneZone
                                captcha:(NSString *)captcha
                                 corpId:(NSString *)corpId
                                timeout:(NSInteger)timeout
                                handler:(void(^)(id result, NSError *error))handler;

/**
 发送注册邮箱验证码
 
 @param email 邮箱账号
 @param corpId 企业ID
 @param localLang 本地语言
 @param timeout 超时时间
 @param handler 完成回调
 */
- (void)sendRegisterVerifyCodeWithEmail:(NSString *)email
                                 corpId:(NSString *)corpId
                              localLang:(NSString *)localLang
                                timeout:(NSInteger)timeout
                                handler:(void(^)(id result, NSError *error))handler;

/**
 请求注册手机图形验证码
 
 @param phone   手机号码
 @param phoneZone 手机区号(非,默认+86)
 @param corpId  企业Id
 @param handler 完成后回调
 */
- (void)sendRegisterCaptchaWithPhone:(NSString *)phone
                           phoneZone:(NSString *)phoneZone
                              corpId:(NSString *)corpId
                             timeout:(NSInteger)timeout
                             handler:(void(^)(id result, NSError *error))handler;

/**
 通过手机验证码注册新账号
 
 @param phone 手机号码
 @param phoneZone 手机区号(非,默认+86)
 @param nickname 昵称(非)
 @param verifyCode 验证码
 @param password 密码
 @param corpId 企业Id
 @param localLang 本地语言(非)
 @param pluginId 应用插件Id(非)
 @param timeout 超时时间
 @param handler 完成后回调
 */
- (void)registerAccountWithPhone:(NSString *)phone
                       phoneZone:(NSString *)phoneZone
                        nickname:(NSString *)nickname
                      verifyCode:(NSString *)verifyCode
                        password:(NSString *)password
                          corpId:(NSString *)corpId
                       localLang:(NSString *)localLang
                        pluginId:(NSString *)pluginId
                         timeout:(NSInteger)timeout
                         handler:(void (^)(id result, NSError *error))handler;

/**
 通过邮箱验证码注册新账号
 
 @param email 邮箱
 @param nickname 昵称(非)
 @param verifyCode 验证码
 @param password 密码
 @param corpId 企业Id
 @param localLang 本地语言(非)
 @param timeout 超时时间
 @param handler 完成后回调
 */
- (void)registerAccountWithEmail:(NSString *)email
                        nickname:(NSString *)nickname
                      verifyCode:(NSString *)verifyCode
                        password:(NSString *)password
                          corpId:(NSString *)corpId
                       localLang:(NSString *)localLang
                         timeout:(NSInteger)timeout
                         handler:(void (^)(id result, NSError *error))handler;

/**
 获取账号的详细信息
 
 @param userId  用户ID
 @param timeout 超时时间
 @param handler 完成后回调
 */
- (void)getAccountInfomationWithUserId:(NSNumber *)userId
                               timeout:(NSInteger)timeout
                               handler:(void(^)(id result, NSError *error))handler;

/**
 修改密码
 
 @param oldPassword 旧密码
 @param newPassword 新密码
 @param timeout 超时时间
 @param handler 完成后回调
 */
- (void)modifyAccountPasswordWithOldPassowrd:(NSString *)oldPassword
                                 newPassword:(NSString *)newPassword
                                     timeout:(NSInteger)timeout
                                     handler:(void(^)(id result, NSError *error))handler;

#pragma mark - 找回密码

/**
 获取手机重置密码验证码
 
 @param phone   手机号码
 @param captcha 图形验证码（超过6次才需要,没有传nil）
 @param phoneZone 手机区号(非,默认+86)
 @param corpId  企业Id
 @param timeout 超时时间
 @param handler 完成后回调
 */
- (void)sendResetPasswordVerifyCodeWithPhone:(NSString *)phone
                                     captcha:(NSString *)captcha
                                   phoneZone:(NSString *)phoneZone
                                      corpId:(NSString *)corpId
                                     timeout:(NSInteger)timeout
                                     handler:(void(^)(id result, NSError *error))handler;


/**
 11.1-B 发送手机重置密码图形验证码
 
 @param phone   手机号码
 @param corpId  企业Id
 @param phoneZone 手机区号(非,默认+86)
 @param timeout 超时时间
 @param handler 完成后回调
 */
- (void)sendResetPasswordCaptchaWithPhone:(NSString *)phone
                                   corpId:(NSString *)corpId
                                phoneZone:(NSString *)phoneZone
                                  timeout:(NSInteger)timeout
                                  handler:(void(^)(id result, NSError *error))handler;

/**
 11.1-C 重置手机账号密码
 
 @param phone       手机号码
 @param verifyCode  验证码
 @param password    新密码
 @param corpId      企业Id
 @param phoneZone   手机区号(非,默认+86)
 @param timeout     超时时间
 @param handler     完成后回调
 */
- (void)resetAccountPasswordWithPhone:(NSString *)phone
                           verifyCode:(NSString *)verifyCode
                             password:(NSString *)password
                               corpId:(NSString *)corpId
                            phoneZone:(NSString *)phoneZone
                              timeout:(NSInteger)timeout
                              handler:(void(^)(id result, NSError *error))handler;

/**
 11.2-A 发送重置密码邮箱(邮箱收到的是验证码/重置密码连接)(具体看云平台设置)
 
 @param email   邮箱
 @param captcha 图形验证码（超过6次才需要,没有传nil）
 @param localLang 邮箱语言(非)
 @param corpId  企业Id
 @param timeout 超时时间
 @param handler 完成后回调
 */
- (void)sendResetPasswordEmailWithEmail:(NSString *)email
                                captcha:(NSString *)captcha
                              localLang:(NSString *)localLang
                                 corpId:(NSString *)corpId
                                timeout:(NSInteger)timeout
                                handler:(void(^)(id result, NSError *error))handler;

/**
 11.2-A-2 发送找回密码的邮件激活码
 
 @param email   邮箱
 @param localLang 邮箱语言(非)
 @param corpId  企业Id
 @param timeout 超时时间
 @param handler 完成后回调
 */
- (void)sendResetPasswordVerifyCodeWithEmail:(NSString *)email
                                   localLang:(NSString *)localLang
                                      corpId:(NSString *)corpId
                                     timeout:(NSInteger)timeout
                                     handler:(void(^)(id result, NSError *error))handler;

/**
 11.2-B 发送邮件重置密码图形验证码
 
 @param email   邮箱
 @param corpId  企业Id
 @param timeout 超时时间
 @param handler 完成后回调
 */
- (void)sendResetPasswordCaptchaWithEmail:(NSString *)email
                                   corpId:(NSString *)corpId
                                  timeout:(NSInteger)timeout
                                  handler:(void(^)(id result, NSError *error))handler;

/**
 11.2-C 重置邮箱账号密码
 
 @param email       邮箱
 @param verifyCode  验证码
 @param password    新密码
 @param corpId      企业Id
 @param timeout     超时时间
 @param handler     完成后回调
 */
- (void)resetAccountPasswordWithEmail:(NSString *)email
                           verifyCode:(NSString *)verifyCode
                             password:(NSString *)password
                               corpId:(NSString *)corpId
                              timeout:(NSInteger)timeout
                              handler:(void(^)(id result, NSError *error))handler;

/**
 12 校验手机验证码(无论校验结果成功与失败，手机验证码被校验一次立即失效。返回一个新的验证码用于完成后续的流程)
 
 @param phone 手机号码
 @param corpId 企业Id
 @param phoneZone 手机时区(非,默认+86)
 @param verifyCode 验证码
 @param timeout 超时时间
 @param handler 完成后回调
 */
- (void)verifyPhoneVerifyCodeWithPhone:(NSString *)phone
                                corpId:(NSString *)corpId
                             phoneZone:(NSString *)phoneZone
                            verifyCode:(NSString *)verifyCode
                               timeout:(NSInteger)timeout
                               handler:(void(^)(id result, NSError *error))handler;

#pragma mark - user

/**
 7.1 登录账号
 
 @param account     账号
 @param password    密码
 @param corpId      企业Id
 @param timeout 超时时间
 @param handler     完成后回调
 */
- (XLinkTask *)loginWithAccount:(NSString *)account
                       password:(NSString *)password
                         corpId:(NSString *)corpId
                        timeout:(NSInteger)timeout
                        handler:(void(^)(id result, NSError *error))handler;

/**
 9.1 获取用户订阅设备列表
 
 @param version 版本号
 @param userId 用户Id
 @param timeout 超时时间
 @param handler 完成回调
 @return XLinkGetDeviceListTask
 */
- (XLinkTask *)getSubscribeDeviceListWithVersion:(int)version
                                          userId:(NSNumber *)userId
                                         timeout:(NSInteger)timeout
                                         handler:(void (^) (NSArray<XDevice *> *devices, XLinkErrorCode code))handler;

/**
 9.2 获取用户订阅设备列表并连接
 
 @param version 版本号
 @param userId 用户Id
 @param timeout 超时时间
 @param handler 完成回调
 @return XLinkSyncDeviceListTask
 */
- (XLinkTask *)getSubscribeDeviceListAndConnectDeviceWithVersion:(int)version
                                                          userId:(NSNumber *)userId
                                                    localConnect:(BOOL)localConnect
                                                         timeout:(NSInteger)timeout
                                                         handler:(void (^) (NSArray<XDevice *> *devices, XLinkErrorCode code))handler;

#pragma mark - 用户身份
/*
 *  用户上传头像，并且用上传头像的url设置用户的头像属性avatar
 */
- (void)uploadUserIconWithImage:(NSData *)imageData handler:(void(^)(id result, NSError *error))handler;

/*
 *  修改昵称
 *  @param nickName 昵称
 *  @return 是否修改成功
 */
- (void)modifyNickName:(NSString *)nickName Callback:(void (^)(BOOL success,NSString *name,NSString *errMsg))callback;

/**
 *  修改密码
 *
 *  @param oldPassword      旧密码
 *  @param newPassword      新密码
 *  @param handler       完成后的回调
 */
- (void)modifyPasswordWithOldPassword:(NSString *)oldPassword NewPassword:(NSString *)newPassword handler:(void(^)(id result, NSError *error))handler;

/**
 获取消息列表
 */
- (void)getMessageListWithUserID:(NSNumber *)userID withQueryDictionary:(NSDictionary *)dic andCallBack:(DictCallBack)callback;

/**
 获取用户收到的邀请记录列表
 */
- (void)getShareDeviceListWithCallBack:(ArrCallBack)callBack;

/**
 接受邀请
 */
- (void)acceptInvitationInviteID:(NSDictionary *)dic CallBack:(DictCallBack)callback;

/**
 拒绝邀请
 */
- (void)refusedInvitationInviteID:(NSDictionary *)dic  CallBack:(void (^)(NSDictionary *result,NSError *error))callback;

/**
 删除设备分享记录
 */
- (void)deleteDeviceShareRecordWithInviteCode:(NSString*)inviteCode withCallBack:(DictCallBack)callback;

/**
 删除设备报警信息
 */
- (void)deleteMessagesWithMessageIDArray:(NSArray*)messageIDArray andCallBack:(DictCallBack)callback;

/**
 获取用户公开信息
 */
- (void)getOpenInfoUserID:(NSNumber *)userID  CallBack:(void (^)(NSDictionary *result,NSError *error))callback;

/**
 第三方登录
 
 @param sourceType 登录平台
 @param openId 790760921287143424
 @param accessToken 第三方用户Token
 @param name 第三方用户昵称
 @param resource 登录源
 @param pluginId 应用插件ID
 @param callBack 回调
 */
- (void)getUserInfoWithThirdSourceType:(XLinkUserSourceType)sourceType openId:(NSString*)openId andAccessToken:(NSString*)accessToken andName:(NSString*)name andResource:(NSString*)resource andPluginId:(NSString*)pluginId withCallBack:(DictCallBack)callBack;

/**
 获取用户详细信息
 */
- (void)getUserInfoDetailWithUserID:(NSNumber*)userID withCallBack:(DictCallBack)callBack;

/**
 获取修改手机号码验证码
 */
- (void)getModifyPhoneSMSVerifycodeWithUserID:(NSNumber*)userID andPhone:(NSString*)phone andCaptcha:(NSString*)captcha withCallBack:(DictCallBack)callBack;

/**
 通过验证码修改手机号码
 */
- (void)modifyPhoneWithSMSVerifyCode:(NSString*)verifyCode andPhone:(NSString*)phone andPassWord:(NSString*)passWord andUserID:(NSNumber*)userID withCallBack:(DictCallBack)callBack;

/**
 获取图片验证码
 */
- (void)getPicVerifyCodeWithUserID:(NSNumber*)userID andPhone:(NSString*)phone withCallBack:(DictCallBack)callBack;

@end

