//
//  ServiceViewController.m
//  vatti
//
//  Created by Eric on 2018/7/5.
//  Copyright © 2018年 xlink. All rights reserved.
//

#import "ServiceViewController.h"
#import "BaseWebViewController.h"

//// 在线报装Url
//#define URL_INSTALLATION  @"http://wechat.vatti.com.cn/wxportal/vatti/serviceSupport/installOnline.jsp"
//
//// 在线报修Url
//#define URL_REPAIR  @"http://wechat.vatti.com.cn/wxportal/vatti/serviceSupport/repairOnline.jsp"

// 在线报装Url
#define URL_INSTALLATION  @"http://www.vatti.com.cn/"
// 在线报修Url
#define URL_REPAIR  @"http://www.vatti.com.cn/"

@interface ServiceViewController ()

@end

@implementation ServiceViewController

#pragma mark - LifeCycle
- (void)viewDidLoad {
    [super viewDidLoad];

}

- (void)initConfigure
{
    [super initConfigure];
    
    self.isNeedBackBtn = NO;
}

- (void)initSubViews
{
    [super initSubViews];
    
    //navi
    self.navigationItem.title = @"服务";

}

#pragma mark - Action

/**
 预约安装
 */
- (IBAction)btnScheduledInstallaClicked:(UIButton *)sender {
    
    BaseWebViewController *vc = [[BaseWebViewController alloc] init];
    vc.urlString = URL_INSTALLATION;
    vc.titleString = @"我要安装";

    [self.navigationController pushViewController:vc animated:YES];
}

/**
 在线报修
 */
- (IBAction)BtnOnlinewarrantyClicked:(UIButton *)sender {
    
    BaseWebViewController *vc = [[BaseWebViewController alloc] init];
    vc.urlString = URL_REPAIR;
    vc.titleString = @"我要维修";
    
    [self.navigationController pushViewController:vc animated:YES];
    
}


@end
