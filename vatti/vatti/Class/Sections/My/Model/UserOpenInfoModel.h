//
//  UserOpenInfoModel.h
//  vatti
//
//  Created by Eric on 2018/8/16.
//  Copyright © 2018年 xlink. All rights reserved.
//

/**
 *  用户公开信息模型
 *
 *  @author BZH
 */

#import <Foundation/Foundation.h>
#import "BGFMDB.h"

@interface UserOpenInfoModel : NSObject

///头像
@property (copy,   nonatomic) NSString *avatar;

///昵称
@property (copy,   nonatomic) NSString *nickname;

///用户ID
@property (strong, nonatomic) NSNumber *user_id;

@end
