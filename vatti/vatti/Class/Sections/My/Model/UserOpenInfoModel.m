//
//  UserOpenInfoModel.m
//  vatti
//
//  Created by Eric on 2018/8/16.
//  Copyright © 2018年 xlink. All rights reserved.
//

#import "UserOpenInfoModel.h"

@implementation UserOpenInfoModel

+ (NSArray *)bg_unionPrimaryKeys
{
    return @[@"user_id"];
}

- (BOOL)isEqual:(id)object
{
    NSDictionary *json1 = [self yy_modelToJSONObject];
    NSDictionary *json2 = [object yy_modelToJSONObject];
    
    return [json1 isEqual:json2];
}

@end
