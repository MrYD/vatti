//
//  XLNotificationModel.h
//  vatti
//
//  Created by 李叶 on 2018/6/16.
//  Copyright © 2018年 xlink. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface XLNotificationModel : NSObject


///<#desc#>
@property (copy,   nonatomic) NSString *alert_name;
///<#desc#>
@property (copy,   nonatomic) NSString *alert_value;
///<#desc#>
@property (copy,   nonatomic) NSString *content;
///<#desc#>
@property (copy,   nonatomic) NSString *create_date;
///<#desc#>
@property (copy,   nonatomic) NSString *from;
///<#desc#>
@property (copy,   nonatomic) NSString *m_id;
///<#desc#>
@property (copy,   nonatomic) NSString *index;
///<#desc#>
@property (copy,   nonatomic) NSString *is_push;
///<#desc#>
@property (copy,   nonatomic) NSString *is_read;
///<#desc#>
@property (copy,   nonatomic) NSString *notify_type;
///<#desc#>
@property (copy,   nonatomic) NSString *type;

-(id)initWithDict:(NSDictionary *)dict;
@end
