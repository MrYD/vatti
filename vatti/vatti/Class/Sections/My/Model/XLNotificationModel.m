//
//  XLNotificationModel.m
//  vatti
//
//  Created by 李叶 on 2018/6/16.
//  Copyright © 2018年 xlink. All rights reserved.
//

#import "XLNotificationModel.h"

@implementation XLNotificationModel
-(id)initWithDict:(NSDictionary *)dict{
    if (self = [super init]) {
        
        _alert_name = [dict objectForKey:@"alert_name"];
        _alert_value = [dict objectForKey:@"alert_value"];
        
        NSString *content = [dict objectForKey:@"content"];
        if ([content rangeOfString:@":"].location != NSNotFound) {
            NSArray *array = [content componentsSeparatedByString:@":"];
            _content = [array lastObject];
        }else{
            _content = content;
        }
        
        _create_date = [dict objectForKey:@"create_date"];;
        
        _from = [dict objectForKey:@"from"];
        _m_id = [dict objectForKey:@"id"];
        _index = [dict objectForKey:@"index"];
        _is_push = [dict objectForKey:@"is_push"];
        _is_read = [dict objectForKey:@"is_read"];
        _notify_type = [dict objectForKey:@"notify_type"];
        _type = [dict objectForKey:@"type"];
    }
    return self;
}
@end
