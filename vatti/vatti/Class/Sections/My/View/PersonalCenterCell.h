//
//  PersonalCenterCell.h
//  vatti
//
//  Created by 李叶 on 2018/5/23.
//  Copyright © 2018年 xlink. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PersonalCenterCell : UITableViewCell

@property (weak,   nonatomic) IBOutlet UIImageView *image;
@property (weak,   nonatomic) IBOutlet UILabel *labTitle;
@property (weak,   nonatomic) IBOutlet UIView *readView;
@property (weak,   nonatomic) IBOutlet UIView *lineView;

- (void)setDataWithDict:(NSDictionary*)dict;

@end
