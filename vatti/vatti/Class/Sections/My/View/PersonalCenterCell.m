//
//  PersonalCenterCell.m
//  vatti
//
//  Created by 李叶 on 2018/5/23.
//  Copyright © 2018年 xlink. All rights reserved.
//

#import "PersonalCenterCell.h"

@interface PersonalCenterCell()

@end

@implementation PersonalCenterCell

- (void)awakeFromNib {
    [super awakeFromNib];
    
    self.readView.layer.cornerRadius = 2;
    self.readView.layer.masksToBounds = YES;
}

- (void)setDataWithDict:(NSDictionary*)dict
{
    NSString *imageName = [dict objectForKey:@"image"];
    self.image.image = [UIImage imageNamed:imageName];
    
    self.labTitle.text = [dict objectForKey:@"title"];
    
    BOOL hide = [[dict objectForKey:@"isRed"] boolValue];
    self.readView.hidden = !hide;
}

@end
