//
//  MyFootView.h
//  vatti
//
//  Created by 李叶 on 2018/5/23.
//  Copyright © 2018年 xlink. All rights reserved.
//

#import <UIKit/UIKit.h>

@class MyFootView;

@protocol MyFootViewDelegate <NSObject>

@optional

- (void)myFootView:(MyFootView*)footView didClickLogoutBtn:(UIButton*)sender;

@end

@interface MyFootView : UIView

+ (instancetype)view;

@property (weak, nonatomic) id<MyFootViewDelegate> delegate;

@end


