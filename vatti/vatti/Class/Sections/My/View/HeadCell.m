//
//  HeadCell.m
//  vatti
//
//  Created by 李叶 on 2018/5/28.
//  Copyright © 2018年 xlink. All rights reserved.
//

#import "HeadCell.h"

@implementation HeadCell

- (void)awakeFromNib {
    [super awakeFromNib];
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    
    self.headPortrait.layer.cornerRadius  = self.headPortrait.frame.size.width/2;
    self.headPortrait.layer.masksToBounds = YES;
}

@end
