//
//  PersonInfoCell.h
//  vatti
//
//  Created by 李叶 on 2018/5/25.
//  Copyright © 2018年 xlink. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PersonInfoCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *title;
@property (weak, nonatomic) IBOutlet UILabel *subTitle;
@property (weak, nonatomic) IBOutlet UIImageView *headPortrait;

@property (weak, nonatomic) IBOutlet UILabel *line;

@property (strong,nonatomic) NSString *titleName;
@property (strong,nonatomic) NSString *subTitleName;
@property (strong,nonatomic) NSString *imageName;
@property (assign,nonatomic) BOOL isLine;
@property (assign,nonatomic) BOOL isimage;


@end
