//
//  InvitationMessageView.m
//  vatti
//
//  Created by BZH on 2018/6/16.
//  Copyright © 2018年 xlink. All rights reserved.
//

#import "InvitationMessageView.h"
#import "UIColor+XLCategory.h"
#import "MessageCell.h"
#import "MemberModel.h"
#import "XLUtilTool.h"
#import "XLServiceManager.h"

@interface InvitationMessageView() <UITableViewDelegate,UITableViewDataSource>

@end

static NSString *const XLInvitationMessageCellID = @"XLInvitationMessageCellID";

@implementation InvitationMessageView

- (void)awakeFromNib{
    [super awakeFromNib];
    [self.tableView registerNib:[UINib nibWithNibName:NSStringFromClass([MessageCell class]) bundle:nil] forCellReuseIdentifier:XLInvitationMessageCellID];
}

-(void)showEmptyViewWithText: (NSString *) text {
    self.tipsLab.text = text;
    self.emptyView.hidden = NO;
}

- (void)hideEmptyView{
    self.emptyView.hidden = YES;
}

#pragma mark - UITableViewDelegate & UITableViewDataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 1;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return self.dataSourceArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    NSInteger secNum = indexPath.section;
    
    MessageCell *cell = [tableView dequeueReusableCellWithIdentifier:XLInvitationMessageCellID];
    cell.memberModel = [self.dataSourceArray objectAtIndex:secNum];
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return UITableViewAutomaticDimension;
}

- (CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 60.0f;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return CGFLOAT_MIN;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return CGFLOAT_MIN;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    return [UIView new];
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    return [UIView new];
}

/**
 左滑cell时出现按钮代理方法
 */
- (NSArray *)tableView:(UITableView *)tableView editActionsForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    @weakify(self)
    
    if ([XLUtilTool isValidateAutoLogin] == YES){
        
        NSInteger secNum = indexPath.section;
        
        UITableViewRowAction *actionDelete = [UITableViewRowAction rowActionWithStyle:UITableViewRowActionStyleDefault title:@"删除" handler:^(UITableViewRowAction *action, NSIndexPath *indexPath) {
            @strongify(self)
            
            MemberModel *model = [self.dataSourceArray objectAtIndex:secNum];
            
            if (self.delegate && [self.delegate respondsToSelector:@selector(invitationMessageView:deleteDeviceShareWithInviteCode:)]) {
                [self.delegate invitationMessageView:self deleteDeviceShareWithInviteCode:model.invite_code];
            }
            
        }];
        actionDelete.backgroundColor = [UIColor redColor];
        
        return @[actionDelete];
    }else{
        return @[];
    }
    
}

@end
