//
//  DeviceAlarmView.h
//  vatti
//
//  Created by BZH on 2018/6/16.
//  Copyright © 2018年 xlink. All rights reserved.
//

/**
 *  设备告警
 *
 *  @author BZH
 */

#import <UIKit/UIKit.h>
#import "MJRefresh.h"
#import "XLNotificationModel.h"

@class DeviceAlarmView;

@protocol DeviceAlarmViewDelegate <NSObject>

@optional

@end

@interface DeviceAlarmView : UIView

///列表
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UIView *emptyView;

@property (weak, nonatomic) IBOutlet UILabel *tipsLab;

///数据源
@property (strong, nonatomic) NSArray <XLNotificationModel*> *dataSourceArray;

-(void)showEmptyViewWithText: (NSString *) text;

-(void)hideEmptyView;

@end
