//
//  InvitationMessageView.h
//  vatti
//
//  Created by BZH on 2018/6/16.
//  Copyright © 2018年 xlink. All rights reserved.
//

/**
 *  邀请信息
 *
 *  @author BZH
 */

@class InvitationMessageView;
@class MemberModel;

@protocol InvitationMessageViewDelegate <NSObject>

@optional

/**
 删除邀请信息
 */
- (void)invitationMessageView:(InvitationMessageView*)invitationMessageView deleteDeviceShareWithInviteCode:(NSString*)inviteCode;

@end

#import <UIKit/UIKit.h>

@interface InvitationMessageView : UIView

@property (weak,   nonatomic) IBOutlet UITableView *tableView;
@property (weak,   nonatomic) IBOutlet UIView *emptyView;
@property (weak,   nonatomic) IBOutlet UILabel *tipsLab;

@property (weak,   nonatomic) id <InvitationMessageViewDelegate> delegate;
@property (strong, nonatomic) NSArray <MemberModel*> *dataSourceArray;

-(void)showEmptyViewWithText: (NSString *) text;

-(void)hideEmptyView;

@end
