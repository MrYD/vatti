//
//  MessageCell.m
//  vatti
//
//  Created by 李叶 on 2018/6/16.
//  Copyright © 2018年 xlink. All rights reserved.
//

#import "MessageCell.h"
#import "UIColor+XLCategory.h"

@interface MessageCell()

@end

@implementation MessageCell

- (void)awakeFromNib
{
    [super awakeFromNib];
    
    self.labLeft.textColor = [UIColor colorWithHexString:@"323345"];
    self.labRight.textColor = [UIColor colorWithHexString:@"818286"];
}

- (void)setErrorNum:(NSString *)errorNum{
    _errorNum = errorNum;
    
    self.labLeft.text = errorNum;
    self.labRight.text = @"";
}

#pragma mark - OverWrite

- (void)setNotifacationModel:(XLNotificationModel *)notifacationModel{
    _notifacationModel = notifacationModel;
    
    self.labLeft.text = notifacationModel.content;
    
    NSString *dateString = [notifacationModel.create_date substringToIndex:10];
    dateString = [dateString stringByReplacingOccurrencesOfString:@"-" withString:@"/"];
    self.labRight.text = dateString;
}

- (void)setMemberModel:(MemberModel *)memberModel{
    _memberModel = memberModel;
    
    NSString *acceptStr = @"accept";
    NSString *denyStr = @"deny";
    NSString *cancelStr = @"cancel";
    NSString *unsubscribedStr = @"unsubscribed";
    
    BOOL b1 = [memberModel.state isEqualToString:acceptStr];
    BOOL b2 = [memberModel.state isEqualToString:denyStr];
    BOOL b3 = [memberModel.state isEqualToString:cancelStr];
    BOOL b4 = [memberModel.state isEqualToString:unsubscribedStr];
    
    NSString *leftString = nil;
    if (b1 == YES) {
        leftString = [NSString stringWithFormat:@"您已接收%@的设备分享邀请",memberModel.from_name];
    }
    
    if (b2 == YES) {
        leftString = [NSString stringWithFormat:@"您已拒绝%@的设备分享邀请",memberModel.from_name];
    }
    
    if (b3 || b4) {
        leftString = @"您的设备管理权限已移除";
    }
    
    self.labLeft.text = leftString;

    self.labRight.text = [self formateTime:memberModel.gen_date.floatValue];
    
}

/**
 字符串转时间戳
 */
- (NSString *)formateTime:(long long)time
{
    NSDate *date = [NSDate dateWithTimeIntervalSince1970:time/1000];//取到秒，此处time单位是毫秒
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyy/MM/dd"];
    
    return [formatter stringFromDate:date];
}



@end
