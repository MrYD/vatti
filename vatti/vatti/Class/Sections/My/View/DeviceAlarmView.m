//
//  DeviceAlarmView.m
//  vatti
//
//  Created by BZH on 2018/6/16.
//  Copyright © 2018年 xlink. All rights reserved.
//

#import "DeviceAlarmView.h"
#import "UIColor+XLCategory.h"
#import "MessageCell.h"
#import "XLNotificationModel.h"

@interface DeviceAlarmView() <UITableViewDelegate,UITableViewDataSource>

@end

static NSString *const XLMessageCellID = @"XLMessageCellID";

@implementation DeviceAlarmView

- (void)awakeFromNib{
    [super awakeFromNib];
    [self.tableView registerNib:[UINib nibWithNibName:NSStringFromClass([MessageCell class]) bundle:nil] forCellReuseIdentifier:XLMessageCellID];
}

-(void)showEmptyViewWithText: (NSString *) text {
    self.tipsLab.text = text;
    self.emptyView.hidden = NO;
}

- (void)hideEmptyView{
    self.emptyView.hidden = YES;
}

#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 1;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return self.dataSourceArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    NSInteger secNum = indexPath.section;
    
    MessageCell *cell = [tableView dequeueReusableCellWithIdentifier:XLMessageCellID];
    cell.notifacationModel = [self.dataSourceArray objectAtIndex:secNum];
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return UITableViewAutomaticDimension;
}

- (CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 60.0f;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return CGFLOAT_MIN;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return CGFLOAT_MIN;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    return [UIView new];
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    return [UIView new];
}

@end
