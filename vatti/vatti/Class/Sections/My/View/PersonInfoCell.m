//
//  PersonInfoCell.m
//  vatti
//
//  Created by 李叶 on 2018/5/25.
//  Copyright © 2018年 xlink. All rights reserved.
//

#import "PersonInfoCell.h"

@implementation PersonInfoCell

- (void)awakeFromNib {
    [super awakeFromNib];

    self.headPortrait.layer.cornerRadius = 16;
    self.headPortrait.clipsToBounds =YES;
}

- (void)setIsLine:(BOOL)isLine{
    self.line.hidden = !isLine;
}

- (void)setIsimage:(BOOL)isimage{
    self.headPortrait.hidden = !isimage;
    self.subTitle.hidden = isimage;
}

- (void)setTitleName:(NSString *)titleName{
    self.title.text = titleName;
}

- (void)setSubTitleName:(NSString *)subTitleName{
    self.subTitle.text = subTitleName;
}


@end
