//
//  MessageCell.h
//  vatti
//
//  Created by 李叶 on 2018/6/16.
//  Copyright © 2018年 xlink. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "XLNotificationModel.h"
#import "MemberModel.h"

@interface MessageCell : UITableViewCell

@property (weak,   nonatomic) IBOutlet UILabel *labLeft;
@property (weak,   nonatomic) IBOutlet UILabel *labRight;

@property (strong, nonatomic) XLNotificationModel *notifacationModel;
@property (strong, nonatomic) MemberModel *memberModel;
@property (copy,   nonatomic) NSString *errorNum;

@end
