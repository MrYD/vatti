//
//  MyFootView.m
//  vatti
//
//  Created by 李叶 on 2018/5/23.
//  Copyright © 2018年 xlink. All rights reserved.
//

#import "MyFootView.h"
#import "XLUtilTool.h"

@interface MyFootView()

@property (weak, nonatomic) IBOutlet UIButton *loginOut;

@end

@implementation MyFootView

+ (instancetype)view
{
    return [[NSBundle mainBundle] loadNibNamed:NSStringFromClass([self class]) owner:nil options:nil].firstObject;
}

- (void)awakeFromNib
{
    [super awakeFromNib];
    
    self.loginOut.layer.cornerRadius = 5;
}

- (IBAction)loginOut:(UIButton *)sender {
    if (self.delegate && [self.delegate respondsToSelector:@selector(myFootView:didClickLogoutBtn:)]){
        [self.delegate myFootView:self didClickLogoutBtn:sender];
    }
    
}
@end
