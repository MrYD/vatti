//
//  HeadCell.h
//  vatti
//
//  Created by 李叶 on 2018/5/28.
//  Copyright © 2018年 xlink. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HeadCell : UITableViewCell

@property (weak,   nonatomic) IBOutlet UIImageView *headPortrait;
@property (weak,   nonatomic) IBOutlet UILabel *name;

@end
