//
//  VersionViewController.h
//  vatti
//
//  Created by BZH on 2018/5/11.
//  Copyright © 2018年 xlink. All rights reserved.
//

/**
 *  版本信息
 *
 *  @author BZH
 */

#import "BaseViewController.h"

@interface VersionViewController : BaseViewController

@end
