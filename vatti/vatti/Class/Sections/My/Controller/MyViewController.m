//
//  MyViewController.m
//  vatti
//
//  Created by Eric on 2018/8/16.
//  Copyright © 2018年 xlink. All rights reserved.
//

#import "MyViewController.h"
#import "XLUtilTool.h"
#import "UIImage+XLCategory.h"
#import "MJRefresh.h"
#import <UIImageView+WebCache.h>

#import "HeadCell.h"
#import "PersonalCenterCell.h"
#import "MyFootView.h"

#import "ChangePasswordViewController.h"
#import "InformationViewController.h"
#import "VersionViewController.h"
#import "LoginViewController.h"
#import "BaseNavigationViewController.h"
#import "NotificationCenterController.h"
#import "RegisterCompleteViewController.h"

#import "UserOpenInfoModel.h"
#import "UserOpenInfoDao.h"

#define Title @"title"
#define Image @"image"
#define IsRed @"isRed"

@interface MyViewController ()<UITableViewDataSource,UITableViewDelegate,MyFootViewDelegate, UserInfoUpdateDelegate>
///列表
@property (weak,   nonatomic) IBOutlet UITableView *tableView;

///头像
@property (strong, nonatomic) UIImageView *headPortrait;

///数据源
@property (strong, nonatomic) NSArray *dataSourceArray;

///用户公开信息
@property (strong, nonatomic) UserOpenInfoModel *userOpenInfoModel;

@end

static NSString *const HeadCellID = @"HeadCellID";
static NSString *const PersonalCenterCellID = @"PersonalCenterCell";

@implementation MyViewController

#pragma mark - LifeCycle

- (void)viewDidLoad {
    [super viewDidLoad];
}

- (void)initConfigure
{
    [super initConfigure];
    
    self.isNeedBackBtn = NO;
}

- (void)initSubViews
{
    [super initSubViews];
    
    //navi
    self.navigationItem.title = @"我的";
    
    //table view
    [self.tableView registerNib:[UINib nibWithNibName:NSStringFromClass([PersonalCenterCell class]) bundle:nil] forCellReuseIdentifier:PersonalCenterCellID];
    [self.tableView registerNib:[UINib nibWithNibName:NSStringFromClass([HeadCell class]) bundle:nil] forCellReuseIdentifier:HeadCellID];
    
    if ([XLUtilTool isValidateAutoLogin] == YES) {
        self.tableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
            [self requestOpenUserInfo];
        }];
        self.tableView.scrollEnabled = YES;
    }else{
        self.tableView.mj_header = nil;
        self.tableView.scrollEnabled = NO;
    }
    
}

- (void)buildData
{
    [super buildData];
    
    //监听告警变更
    [kXLNotificationCenter addObserver:self.tableView selector:@selector(reloadData) name:XLNotificationDataPointAlert object:nil];
    
    if ([XLUtilTool isValidateAutoLogin] == YES) {
        
        //从数据库查询用户公开信息模型
        [UserOpenInfoDao queryUserOpenInfoModelWithUserID:XL_DATA_SOURCE.user.userId withCallBack:^(UserOpenInfoModel * _Nullable userOpenInfoModel) {
            self.userOpenInfoModel = userOpenInfoModel;
            [self.tableView reloadData];
            
            //获取用户公开信息
            [self requestOpenUserInfo];
        }];
        
    }
    
}

- (void)viewWillLayoutSubviews
{
    [super viewWillLayoutSubviews];
}

#pragma mark - LazyLoad

- (NSArray *)dataSourceArray
{
    NSMutableDictionary *passwordDic = [NSMutableDictionary dictionary];
    [passwordDic setObject:@"修改密码" forKey:Title];
    [passwordDic setObject:@"ic_changepassword.png" forKey:Image];
    [passwordDic setObject:@"0" forKey:IsRed];
    
    NSMutableDictionary *loginDic = [NSMutableDictionary dictionary];
    [loginDic setObject:@"点击登录Vatti账号" forKey:Title];
    [loginDic setObject:@"my.png" forKey:Image];
    [loginDic setObject:@"0" forKey:IsRed];
    
    NSMutableDictionary *infoDic = [NSMutableDictionary dictionary];
    [infoDic setObject:@"消息中心" forKey:Title];
    [infoDic setObject:@"ic_infocenter.png" forKey:Image];
    
    NSInteger alertCount = [kXLNSUserDefaults integerForKey:UDDataPointAlertCount];
    if (alertCount == 0) {
        [infoDic setObject:@"0" forKey:IsRed];
    }else{
        [infoDic setObject:@"1" forKey:IsRed];
    }
    
    NSMutableDictionary *versionDic = [NSMutableDictionary dictionary];
    [versionDic setObject:@"版本信息" forKey:Title];
    [versionDic setObject:@"ic_info.png" forKey:Image];
    [versionDic setObject:@"0" forKey:IsRed];
    
    if ([XLUtilTool isValidateAutoLogin] == YES) {
        _dataSourceArray  = @[passwordDic,infoDic,versionDic];
    }else{
        _dataSourceArray  = @[loginDic,versionDic];
    }
    
    return _dataSourceArray;
}

#pragma mark - Action

- (void)goLogin{
    LoginViewController *login = [[LoginViewController alloc] init];

    BaseNavigationViewController *nvc = [[BaseNavigationViewController alloc] initWithRootViewController:login];
    [nvc.navigationBar setBackgroundImage:[UIImage xl_imageForColor:[UIColor colorWithRed:255/255. green:255/255. blue:255/255. alpha:1]] forBarMetrics:UIBarMetricsDefault];
    
    [XLUtilTool setRootViewController:nvc animationOptions:UIViewAnimationOptionTransitionCrossDissolve duration:0.3];
}

#pragma mark - Data

#pragma mark - UI

#pragma mark - Request

/**
 获取用户公开信息
 */
- (void)requestOpenUserInfo
{
    @weakify(self)
    
    [[XLServiceManager shareInstance].userService getOpenInfoUserID:XL_DATA_SOURCE.user.userId CallBack:^(NSDictionary *result, NSError *error) {
        @strongify(self)
        
        if (error == nil) {
            UserOpenInfoModel *model = [UserOpenInfoModel yy_modelWithJSON:result];
            if ([model isEqual:self.userOpenInfoModel] == NO) {
                
                [UserOpenInfoDao saveOrUpdateAsyncWithUserOpenInfoModel:model withCallBack:^(BOOL isSuccess) {
                    NSLog(@"本地完成");
                }];
                self.userOpenInfoModel = model;
                [self.tableView reloadData];
                
            }
        }
    }];
}

#pragma mark - MyFootViewDelegate

- (void)myFootView:(MyFootView *)footView didClickLogoutBtn:(UIButton *)sender{
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"提示" message:@"您确定退出登录?" preferredStyle:(UIAlertControllerStyleAlert)];
    UIAlertAction *sure = [UIAlertAction actionWithTitle:@"确定" style:(UIAlertActionStyleDefault) handler:^(UIAlertAction * _Nonnull action) {
        XL_DATA_SOURCE.userState = XLUserStateOffline;
    }];
    
    UIAlertAction *cancle = [UIAlertAction actionWithTitle:@"取消" style:(UIAlertActionStyleCancel) handler:nil];
    [alert addAction:cancle];
    [alert addAction:sure];
    [self.navigationController presentViewController:alert animated:YES completion:nil];
}

#pragma mark - UITableViewDelegate & UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    if ([XLUtilTool isValidateAutoLogin] == YES) {
        if (self.userOpenInfoModel.user_id == nil) {
            return 0;
        }else{
            return self.dataSourceArray.count + 1;
        }
        
    }else{
        return self.dataSourceArray.count;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    NSInteger rowNum = indexPath.row;
    
    if ([XLUtilTool isValidateAutoLogin] == YES) {
        if (indexPath.row == 0) {
            HeadCell *cell = [tableView dequeueReusableCellWithIdentifier:HeadCellID forIndexPath:indexPath];
            
            cell.headPortrait.hidden = ![XLUtilTool isValidateAutoLogin];
            
            cell.name.hidden = ![XLUtilTool isValidateAutoLogin];
            
            [cell.headPortrait sd_setImageWithURL:[NSURL URLWithString:self.userOpenInfoModel.avatar] placeholderImage:[UIImage imageNamed:@"Setting_head_none"]];
            
            cell.name.text = self.userOpenInfoModel.nickname;
            
            return cell;
        }else{
            PersonalCenterCell *cell = [tableView dequeueReusableCellWithIdentifier:PersonalCenterCellID forIndexPath:indexPath];
            NSDictionary *dict = [self.dataSourceArray objectAtIndex:(rowNum - 1)];
            
            [cell setDataWithDict:dict];
            
            return cell;
        }
    }else{
        PersonalCenterCell *cell = [tableView dequeueReusableCellWithIdentifier:PersonalCenterCellID forIndexPath:indexPath];
        NSDictionary *dict = [self.dataSourceArray objectAtIndex:rowNum];
        
        [cell setDataWithDict:dict];
        
        return cell;
    }
    
    return [[UITableViewCell alloc] init];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if ([XLUtilTool isValidateAutoLogin] == YES) {
        if (indexPath.row == 0) {
            return 180.;
        }else{
            return 50.;
        }
    }else{
        return 50.;
    }
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(nonnull NSIndexPath *)indexPath{
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    UIStoryboard *sb = [UIStoryboard storyboardWithName:@"My" bundle:nil];
    NSInteger rowNum = indexPath.row;
    
    if ([XLUtilTool isValidateAutoLogin] == YES) {
        if (rowNum == 0) {
            InformationViewController *vc = [[InformationViewController alloc] init];
            vc.delegate = self;
            [self.navigationController pushViewController:vc animated:YES];
        }else if (rowNum == 1) {
            ChangePasswordViewController *vc = [sb instantiateViewControllerWithIdentifier:@"ChangePasswordViewController"];
            [self.navigationController pushViewController:vc animated:YES];
        }else if (rowNum == 2) {
            NotificationCenterController *vc = [[NotificationCenterController alloc] init];
            [self.navigationController pushViewController:vc animated:YES];
        }else if (rowNum == 3) {
            VersionViewController *vc = [sb instantiateViewControllerWithIdentifier:@"VersionViewController"];
            [self.navigationController pushViewController:vc animated:YES];
        }
        
    }else{
        if (indexPath.row == 0) {
            [self goLogin];
        }else if (indexPath.row == 1) {
            VersionViewController *vc = [sb instantiateViewControllerWithIdentifier:@"VersionViewController"];
            [self.navigationController pushViewController:vc animated:YES];
        }
    }
    
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *v = [UIView new];
    v.backgroundColor = [UIColor clearColor];
    return v;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    UIView *footerView;
    
    BOOL b1 = [XLUtilTool isValidateAutoLogin];
    BOOL b2 = self.userOpenInfoModel.user_id != nil;
    if (b1 && b2) {
        MyFootView *myFootView = [MyFootView view];
        myFootView.delegate = self;
        
        footerView = myFootView;
    }else{
        footerView = [[UIView alloc] init];
    }

    return footerView;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if ([XLUtilTool isValidateAutoLogin] == YES) {
        return CGFLOAT_MIN;
    }else{
        return 44.0f;
    }
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 120.0f;
}

#pragma mark - UserInfoUpdateDelegate

- (void)onUserInfoUpdate
{
    [self requestOpenUserInfo];
}
@end
