//
//  VersionViewController.m
//  vatti
//
//  Created by BZH on 2018/5/11.
//  Copyright © 2018年 xlink. All rights reserved.
//

#import "VersionViewController.h"

@interface VersionViewController ()

@property (weak, nonatomic) IBOutlet UIImageView *infoImage;
@property (weak, nonatomic) IBOutlet UILabel *labVersion;

@end

@implementation VersionViewController

- (void)viewDidLoad {
    [super viewDidLoad];
}

- (void)initSubViews{
    [super initSubViews];
    
    self.navigationItem.title = @"版本信息";
    
    self.infoImage.layer.cornerRadius = 15;
    self.infoImage.layer.masksToBounds = YES;
    
}

- (void)buildData
{
    [super buildData];
    
    NSDictionary *infoDic = [[NSBundle mainBundle] infoDictionary];
    
    // 获取App的版本号
    NSString *appVersion = [infoDic objectForKey:@"CFBundleShortVersionString"];
    self.labVersion.text = [NSString stringWithFormat:@"当前版本：V%@",appVersion];
}




@end
