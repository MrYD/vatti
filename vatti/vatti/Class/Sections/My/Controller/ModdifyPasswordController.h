//
//  ModdifyPasswordController.h
//  vatti
//
//  Created by BZH on 2018/5/25.
//  Copyright © 2018年 xlink. All rights reserved.
//

/**
 *  修改密码成功
 *
 *  @author BZH
 */

#import "BaseViewController.h"

@interface ModdifyPasswordController : BaseViewController

@end
