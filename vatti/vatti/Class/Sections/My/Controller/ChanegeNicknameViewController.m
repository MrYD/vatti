//
//  ChanegeNicknameViewController.m
//  vatti
//
//  Created by 李叶 on 2018/5/28.
//  Copyright © 2018年 xlink. All rights reserved.
//

#import "ChanegeNicknameViewController.h"
#import "UIColor+XLCategory.h"
#import "XLUserService.h"
#import "XLUtilTool.h"
#import "XLTextField.h"
#import "UserInfoUpdateDelegate.h"

@interface ChanegeNicknameViewController ()

@property (weak, nonatomic) IBOutlet XLTextField *tfNickName;

@end

@implementation ChanegeNicknameViewController

- (void)viewDidLoad {
    [super viewDidLoad];
}

- (void)initSubViews{
    [super initSubViews];
    
    //naiv
    self.navigationItem.title = XLLocalizeString(@"修改昵称");
    
    UIBarButtonItem *leftItem = [[UIBarButtonItem alloc]initWithTitle:@"取消" style:(UIBarButtonItemStyleDone) target:self action:@selector(cancle)];
    [leftItem setTitleTextAttributes:@{NSForegroundColorAttributeName:[UIColor blackColor],NSFontAttributeName : [UIFont systemFontOfSize:15]} forState:UIControlStateNormal];
    [leftItem setTitleTextAttributes:@{NSForegroundColorAttributeName:[UIColor grayColor],NSFontAttributeName : [UIFont systemFontOfSize:15]} forState:UIControlStateDisabled];
    
    UIBarButtonItem *rightItem = [[UIBarButtonItem alloc] initWithTitle:@"确认" style:UIBarButtonItemStyleDone target:self action:@selector(done)];
    [rightItem setTitleTextAttributes:@{NSForegroundColorAttributeName:[UIColor colorWithHexString:@"00C355"],NSFontAttributeName : [UIFont systemFontOfSize:15]} forState:UIControlStateNormal];
    [rightItem setTitleTextAttributes:@{NSForegroundColorAttributeName:[UIColor grayColor],NSFontAttributeName : [UIFont systemFontOfSize:15]} forState:UIControlStateDisabled];
    rightItem.enabled = NO;
    
    self.navigationItem.leftBarButtonItem = leftItem;
    self.navigationItem.rightBarButtonItem = rightItem;
}

- (void)buildData
{
    [super buildData];
    
    self.tfNickName.text = XL_DATA_SOURCE.user.nickname;
}

- (void)cancle{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)done{
    
    @weakify(self)
    
    NSString *name = self.tfNickName.text;
    
    if(!name.length){
        [self alertHintWithMessage:@"请输入昵称"];
        return;
    }
    if ([XLUtilTool isEmpty:name] == YES) {
        [self alertHintWithMessage:@"昵称不能带有特殊字符"];
        return;
    }
    if (name.length > 10) {
        [self alertHintWithMessage:@"昵称不得超过10个字符"];
        return;
    }
    
    KShowLoading;
    [[XLServiceManager shareInstance].userService modifyNickName:self.tfNickName.text Callback:^(BOOL success, NSString *name, NSString *errMsg) {
        @strongify(self)
        KHideLoading;
        
        if (success == YES) {
            XL_DATA_SOURCE.user.nickname = name;
            [XL_DATA_SOURCE.user updateCache];
            
            if (self.delegate && [self.delegate respondsToSelector:@selector(onUserInfoUpdate)]) {
                [self.delegate onUserInfoUpdate];
            }
            
            [self.navigationController popViewControllerAnimated:YES];
        }else{
            [self alertHintWithMessage:errMsg];
        }
    }];
}

#pragma mark - Action

- (IBAction)tfValueChanged:(XLTextField *)sender {
    BOOL hadName = sender.text.length > 0;
    self.navigationItem.rightBarButtonItem.enabled = hadName;
    
    if (sender.text.length > 10) {
        NSRange r = NSMakeRange(0, 10);
        NSString *tempString = [sender.text substringWithRange:r];
        sender.text = tempString;
    }
}



@end
