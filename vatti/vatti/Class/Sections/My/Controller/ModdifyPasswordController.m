//
//  ModdifyPasswordController.m
//  vatti
//
//  Created by BZH on 2018/5/25.
//  Copyright © 2018年 xlink. All rights reserved.
//

#import "ModdifyPasswordController.h"
#import "LoginViewController.h"
#import "BaseNavigationViewController.h"
#import "XLUtilTool.h"
#import "UIImage+XLCategory.h"

@interface ModdifyPasswordController ()

@end

@implementation ModdifyPasswordController

- (void)viewDidLoad {
    [super viewDidLoad];
}

- (void)initConfigure
{
    [super initConfigure];
    
    self.isNeedBackBtn = NO;
}

- (void)initSubViews
{
    [super initSubViews];
    
    self.navigationItem.title = @"修改密码";
}

#pragma mark - Action
- (IBAction)complete:(UIButton *)sender {
    //退出登录
    [XL_DATA_SOURCE appLogout];
    
    //跳转登录页面
    [self goLogin];
}

- (void)goLogin{
    LoginViewController *login = [[LoginViewController alloc] init];
    
    BaseNavigationViewController *nvc = [[BaseNavigationViewController alloc] initWithRootViewController:login];
    [nvc.navigationBar setBackgroundImage:[UIImage xl_imageForColor:[UIColor colorWithRed:255/255. green:255/255. blue:255/255. alpha:1]] forBarMetrics:UIBarMetricsDefault];
    
    [XLUtilTool setRootViewController:nvc animationOptions:UIViewAnimationOptionTransitionCrossDissolve duration:0.3];
}

@end
