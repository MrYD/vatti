//
//  InformationViewController.m
//  vatti
//
//  Created by 李叶 on 2018/5/25.
//  Copyright © 2018年 xlink. All rights reserved.
//

#import "InformationViewController.h"
#import <TZImagePickerController.h>
#import <UIImageView+WebCache.h>
#import "UIImage+XLCategory.h"
#import "XLUserService.h"
#import "PersonInfoCell.h"
#import "XLServiceManager.h"
#import "ChanegeNicknameViewController.h"

@interface InformationViewController ()<UITableViewDelegate,UITableViewDataSource,UIImagePickerControllerDelegate,UINavigationControllerDelegate,UserInfoUpdateDelegate>

@property (strong, nonatomic) UITableView *tableView;
@property (strong, nonatomic) NSArray *dataSourceArray;

@property (strong, nonatomic) XLUserService *userService;


@end

static NSString *const PersonInfoCellID = @"PersonInfoCellID";

@implementation InformationViewController

- (void)viewDidLoad {
    [super viewDidLoad];
}

- (void)initConfigure
{
    [super initConfigure];
    
    self.userService = [XLServiceManager shareInstance].userService;
}


- (void)initSubViews{
    [super initSubViews];
    
    //naiv
    self.navigationItem.title = XLLocalizeString(@"个人资料");
    
    //tableView
    [self setupTableView];
}

- (void)buildData
{
    [super buildData];
    
    //dataSource
    self.dataSourceArray = @[@"昵称:",@"账号:"];
}

- (void)viewWillLayoutSubviews
{
    [super viewWillLayoutSubviews];
    
    self.tableView.frame = self.view.bounds;
}

- (UIStatusBarStyle)preferredStatusBarStyle{
    return UIStatusBarStyleDefault;
}

#pragma mark - UITableViewDataSource && UITableViewDelegate

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if (section == 0){
        return 1;
    }else{
        return self.dataSourceArray.count;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    NSInteger rowNum = indexPath.row;
    
    PersonInfoCell *cell = [tableView dequeueReusableCellWithIdentifier:PersonInfoCellID];
    
    if (indexPath.section == 0){
        cell.isimage = YES;
        cell.titleName = @"设置头像";
        cell.isLine = NO;
        [cell.headPortrait sd_setImageWithURL:[NSURL URLWithString:XL_DATA_SOURCE.user.avatar] placeholderImage:[UIImage imageNamed:@"Setting_head_none"] completed:nil];
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        return cell;
    }else{
        cell.isimage = NO;
        cell.titleName = self.dataSourceArray[rowNum];
        
        if (rowNum == 0){
            cell.subTitle.text = XL_DATA_SOURCE.user.nickname;
            cell.isLine = YES;
        }else if (rowNum == 1){
            NSString *accountString = [self getFormatAccountString];
            cell.subTitle.text = accountString;
            cell.isLine = YES;
        }
        
        if (rowNum == (self.dataSourceArray.count - 1)){
            cell.isLine = NO;
        }else{
            cell.isLine = YES;
        }
        
        if (rowNum == 1) {
            cell.accessoryType = UITableViewCellAccessoryNone;
        }else{
            cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        }
        
        return cell;
    }
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    if (indexPath.section == 0){
        [self showActionAlert];
    }else if (indexPath.section == 1){
        if (indexPath.row == 0){
            [self showChanegeNicknameViewController];
        }
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 30;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    CGRect frameRect = CGRectMake(26, 0, 100, 30);
    
    UILabel *label = [[UILabel alloc] init];
    label.textAlignment = NSTextAlignmentLeft;
    label.frame = frameRect;
    label.font = [UIFont systemFontOfSize:14];
    
    label.textColor = [UIColor grayColor];
    if (section == 1){
        label.text = @"基本信息";
    }
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, kXLScreenWidth, 40)];
    [view addSubview:label];
    return view;
}

#pragma mark - Action
- (void)showActionAlert
{
    @weakify(self)
    
    UIAlertController *alert = [[UIAlertController alloc] init];
    
    UIAlertAction *takePhoto = [UIAlertAction actionWithTitle:@"拍照" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        @strongify(self)
        [self showPhotoVC];
    }];
    
    UIAlertAction *choosePhoto = [UIAlertAction actionWithTitle:@"从手机相册选择" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        @strongify(self)
        
        TZImagePickerController *imagePickerVC = [[TZImagePickerController alloc] initWithMaxImagesCount:1 delegate:nil];
        imagePickerVC.showSelectBtn = NO;
        imagePickerVC.allowCrop = YES;
        
        [imagePickerVC setDidFinishPickingPhotosHandle:^(NSArray<UIImage *> *photos, NSArray *assets,BOOL isSelectOriginalPhoto) {
            UIImage *originalImage = photos.firstObject;
            NSData *imageData = UIImageJPEGRepresentation(originalImage, 0.5f);
            [self uploadUserIconWithData:imageData andIcon:originalImage];
        }];
        [self presentViewController:imagePickerVC animated:YES completion:nil];
    }];
    
    UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:nil];
    
    [alert addAction:takePhoto];
    [alert addAction:choosePhoto];
    [alert addAction:cancel];
    
    [self presentViewController:alert animated:YES completion:nil];
}

- (void)showChanegeNicknameViewController
{
    ChanegeNicknameViewController *nickName = [[ChanegeNicknameViewController alloc]init];
    nickName.delegate = self;
    [self.navigationController pushViewController:nickName animated:YES];
}

#pragma mark - UI

- (void)showPhotoVC
{
    UIImagePickerController *pickerVC = [[UIImagePickerController alloc] init];
    pickerVC.delegate = self;
    pickerVC.sourceType = UIImagePickerControllerSourceTypeCamera;
    pickerVC.allowsEditing = YES;
    [self presentViewController:pickerVC animated:YES completion:nil];
}

- (void)setupTableView
{
    self.tableView = [[UITableView alloc]initWithFrame:CGRectZero style:UITableViewStyleGrouped];
    [self.tableView registerNib:[UINib nibWithNibName:NSStringFromClass([PersonInfoCell class]) bundle:nil] forCellReuseIdentifier:PersonInfoCellID];
    self.tableView.rowHeight = 50;
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    [self.view addSubview:self.tableView];
}

#pragma mark - Data

/**
 保持原来的长宽比，生成一个缩略图
 */
- (UIImage *)thumbnailWithImageWithoutScale:(UIImage *)image size:(CGSize)asize
{
    UIImage *newimage;
    if (nil == image) {
        newimage = nil;
    }
    else{
        CGSize oldsize = image.size;
        CGRect rect;
        if (asize.width/asize.height > oldsize.width/oldsize.height) {
            rect.size.width = asize.height*oldsize.width/oldsize.height;
            rect.size.height = asize.height;
            rect.origin.x = (asize.width - rect.size.width)/2;
            rect.origin.y = 0;
        }
        else{
            rect.size.width = asize.width;
            rect.size.height = asize.width*oldsize.height/oldsize.width;
            rect.origin.x = 0;
            rect.origin.y = (asize.height - rect.size.height)/2;
        }
        UIGraphicsBeginImageContext(asize);
        CGContextRef context = UIGraphicsGetCurrentContext();
        CGContextSetFillColorWithColor(context, [[UIColor clearColor] CGColor]);
        UIRectFill(CGRectMake(0, 0, asize.width, asize.height));//clear background
        [image drawInRect:rect];
        newimage = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
    }
    return newimage;
}

- (NSString*)getFormatAccountString
{
    NSRange cutRang = NSMakeRange(3, 4);
    NSString *tempString = [XL_DATA_SOURCE.user.account substringWithRange:cutRang];
    NSString *accountString = [XL_DATA_SOURCE.user.account stringByReplacingOccurrencesOfString:tempString withString:@"****"];
    return accountString;
}

#pragma mark - Request

- (void)uploadUserIconWithData:(NSData*)imageData andIcon:(UIImage*)icon
{
    @weakify(self)
    
    KShowLoading;
    [self.userService uploadUserIconWithImage:imageData handler:^(id result, NSError *error) {
        @strongify(self)
        KHideLoading;
        
        if (error == nil) {
            NSString *imageUrl = [result objectForKey:@"url"];
            
            XL_DATA_SOURCE.user.avatar = imageUrl;
            [XL_DATA_SOURCE updateUser];
            
            dispatch_async(dispatch_get_main_queue(), ^{
                [self.tableView reloadData];
                
                if (self.delegate && [self.delegate respondsToSelector:@selector(onUserInfoUpdate)]) {
                    [self.delegate onUserInfoUpdate];
                }
            });
            
        }else{
            [self alertHintWithMessage:@"上传头像失败"];
        }
    }];
}

#pragma mark - UIImagePickerControllerDelegate

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker{
    [picker dismissViewControllerAnimated:YES completion:nil];
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary<NSString *,id> *)info{
    
    [picker dismissViewControllerAnimated:YES completion:nil];
    
    UIImage *originalImage = [info valueForKey:UIImagePickerControllerEditedImage];
    NSData *imageData = UIImageJPEGRepresentation(originalImage, 0.5f);
    
    [self uploadUserIconWithData:imageData andIcon:originalImage];
}

#pragma mark - Information

- (void)onUserInfoUpdate {
    [self.tableView reloadData];
    if (self.delegate && [self.delegate respondsToSelector:@selector(onUserInfoUpdate)]) {
        [self.delegate onUserInfoUpdate];
    }
}

@end
