//
//  InformationViewController.h
//  vatti
//
//  Created by 李叶 on 2018/5/25.
//  Copyright © 2018年 xlink. All rights reserved.
//

#import "BaseViewController.h"
#import "UserInfoUpdateDelegate.h"

@class InformationViewController;

@interface InformationViewController : BaseViewController

@property (weak,   nonatomic) id <UserInfoUpdateDelegate> delegate;

@end
