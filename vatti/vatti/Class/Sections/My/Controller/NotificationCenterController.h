//
//  NotificationCenterController.h
//  vatti
//
//  Created by BZH on 2018/6/16.
//  Copyright © 2018年 xlink. All rights reserved.
//

/**
 *  消息中心
 *
 *  @author BZH
 */

#import "BaseViewController.h"

@interface NotificationCenterController : BaseViewController

@end
