//
//  ChanegeNicknameViewController.h
//  vatti
//
//  Created by 李叶 on 2018/5/28.
//  Copyright © 2018年 xlink. All rights reserved.
//

#import "BaseViewController.h"
#import "UserInfoUpdateDelegate.h"

@interface ChanegeNicknameViewController : BaseViewController

@property (weak,   nonatomic) id <UserInfoUpdateDelegate> delegate;

@end
