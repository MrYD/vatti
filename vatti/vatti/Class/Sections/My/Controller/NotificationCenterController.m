//
//  NotificationCenterController.m
//  vatti
//
//  Created by BZH on 2018/6/16.
//  Copyright © 2018年 xlink. All rights reserved.
//

#import "NotificationCenterController.h"
#import "UIColor+XLCategory.h"
#import "InvitationMessageView.h"
#import "DeviceAlarmView.h"
#import "MemberModel.h"

#define Limit 10

@interface NotificationCenterController ()<InvitationMessageViewDelegate>
///设备告警按钮
@property (weak,   nonatomic) IBOutlet UIButton *btnDeviceAlarm;
///邀请信息按钮
@property (weak,   nonatomic) IBOutlet UIButton *btnInvitationMessage;
///分割线
@property (weak,   nonatomic) IBOutlet UILabel *line;
///邀请信息
@property (strong, nonatomic) InvitationMessageView *invitationMessageView;
///设备告警
@property (strong, nonatomic) DeviceAlarmView *deviceAlarmView;
///消息滑动视图
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;

///邀请信息数据源
@property (strong, nonatomic) NSMutableArray <MemberModel*> *invitationDataSourceArray;
///设备告警数据源
@property (strong, nonatomic) NSMutableArray <XLNotificationModel*> *deviceAlarmDataSourceArray;
///分页
@property (assign, nonatomic) NSInteger index;

@end

@implementation NotificationCenterController

- (void)viewDidLoad {
    [super viewDidLoad];
}

- (void)initConfigure
{
    [super initConfigure];
    
    self.invitationDataSourceArray = [NSMutableArray array];
    self.deviceAlarmDataSourceArray = [NSMutableArray array];
}

- (void)initSubViews{
    [super initSubViews];
    
    //navi
    self.navigationItem.title = @"消息中心";

    [self setUpRightBarButton];
    
    //ui
    [self setUpUI];
}

- (void)buildData
{
    [super buildData];
    
    //获取邀请信息列表数据
    [self.invitationMessageView.tableView.mj_header beginRefreshing];
    
    //获取设备告警列表数据
    [self.deviceAlarmView.tableView.mj_header beginRefreshing];
    
    //移除UserDefault消息计数
    [kXLNSUserDefaults setInteger:0 forKey:UDDataPointAlertCount];
    [kXLNSUserDefaults synchronize];
    
    [kXLNotificationCenter postNotificationName:XLNotificationDataPointAlert object:nil];
}

- (void)viewWillLayoutSubviews
{
    [super viewWillLayoutSubviews];
    
    self.scrollView.contentSize = CGSizeMake(self.view.frame.size.width*2, 0);
    
    CGRect alarmRect = CGRectMake(0, 0, self.scrollView.bounds.size.width, self.scrollView.bounds.size.height);
    CGRect invitationRect = CGRectMake(self.scrollView.bounds.size.width, 0, self.scrollView.bounds.size.width, self.scrollView.bounds.size.height);
    self.invitationMessageView.frame = invitationRect;
    self.deviceAlarmView.frame = alarmRect;
}

#pragma mark - UI

- (void)setUpUI
{
    [self.btnDeviceAlarm setSelected:YES];

    
    self.invitationMessageView = [[NSBundle mainBundle] loadNibNamed:@"InvitationMessageView" owner:nil options:nil].lastObject;
    
    self.invitationMessageView.delegate = self;
    self.invitationMessageView.tableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingTarget:self refreshingAction:@selector(refreshInvitationMessage)];

    self.deviceAlarmView = [[NSBundle mainBundle] loadNibNamed:@"DeviceAlarmView" owner:nil options:nil].lastObject;
    
    self.deviceAlarmView.tableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingTarget:self refreshingAction:@selector(headerRefresh)];
    self.deviceAlarmView.tableView.mj_footer = [MJRefreshBackNormalFooter footerWithRefreshingTarget:self refreshingAction:@selector(footerRefresh)];
    [self.scrollView addSubview:self.invitationMessageView];
    [self.scrollView addSubview:self.deviceAlarmView];

}

- (void)setUpRightBarButton {
    UIBarButtonItem *rightBarBtn = [[UIBarButtonItem alloc] initWithTitle:@"清除" style:UIBarButtonItemStylePlain target:self action:@selector(rightBtnClicked:)];
    [rightBarBtn setTintColor:[UIColor blackColor]];
    self.navigationItem.rightBarButtonItem = rightBarBtn;
    [self.navigationItem.rightBarButtonItem setEnabled: self.deviceAlarmDataSourceArray.count > 0];
}

#pragma mark - Aciton

/**
 设备告警
 */
- (IBAction)btnDeviceAlarmClicked:(UIButton *)sender {
    
    [self setUpRightBarButton];

    [UIView animateWithDuration:0.5 animations:^{
        self.line.center = CGPointMake(self.btnDeviceAlarm.center.x, self.line.center.y);
        [self.btnDeviceAlarm setSelected:YES];
        [self.btnInvitationMessage setSelected:NO];
    }];
    [UIView animateWithDuration:0.5 animations:^{
        self.scrollView.contentOffset = CGPointMake(0, 0);
    }];
}

/**
 邀请信息
 */
- (IBAction)btnInvitationMessageClicked:(UIButton *)sender {
    
    self.navigationItem.rightBarButtonItem = nil;
    
    [UIView animateWithDuration:0.5 animations:^{
        self.line.center = CGPointMake(self.btnInvitationMessage.center.x, self.line.center.y);
        [self.btnDeviceAlarm setSelected:NO];
        [self.btnInvitationMessage setSelected:YES];
        
    }];
    [UIView animateWithDuration:0.5 animations:^{
        self.scrollView.contentOffset = CGPointMake(kXLScreenWidth, 0);
    }];
    
}

- (void)rightBtnClicked:(UIBarButtonItem*)btn
{
    //先弹出提示框，是否清除所有告警消息
    [self alertHintWithMessage:@"是否清除当前页面告警消息？" cancelTitle:@"取消" otherTitles:@[@"确定"] handler:^(UIAlertAction *action, NSUInteger tag) {
        if (tag == 1) {
            //data
            NSArray *messageIDArray = [self.deviceAlarmDataSourceArray valueForKey:@"m_id"];
            
            //request
            [self deleteWarningMessagesWithMessageIDArray:messageIDArray];
        }
    }];
    
    [self alertHintWithMessage:nil];

}

- (void)deleteWarningMessagesWithMessageIDArray:(NSArray*)messageIDArray
{
    @weakify(self)
    
    XLUserService *userService = [XLServiceManager shareInstance].userService;
    [userService deleteMessagesWithMessageIDArray:messageIDArray andCallBack:^(NSDictionary *dic, NSError *error) {
        @strongify(self)
        KHideLoading;
        
        if (error == nil) {
            [self requestDeviceAlarmViewDataWithIsNewRequest:YES andIsNeedHud:YES];
        }
    }];
}

/**
 下拉刷新
 */
- (void)headerRefresh{
    self.index = 0;
    [self.deviceAlarmView hideEmptyView];
    [self requestDeviceAlarmViewDataWithIsNewRequest:YES andIsNeedHud:NO];
}

/**
 上拉加载
 */
- (void)footerRefresh{
    [self requestDeviceAlarmViewDataWithIsNewRequest:NO andIsNeedHud:NO];
}

- (void)refreshInvitationMessage {
    [self.invitationMessageView hideEmptyView];
    [self requestInvitationMessageViewDataWithHud:NO];
}

#pragma mark - Request

/**
 获取设备告警列表数据
 */
- (void)requestDeviceAlarmViewDataWithIsNewRequest:(BOOL)isNewRequest andIsNeedHud:(BOOL)isNeedHud{
    
    @weakify(self)
    
    XLUserService *userService = [XLServiceManager shareInstance].userService;
    XLUserModel *user = [XLDataSource shareInstance].user;
    
    NSDictionary *queryDic = @{ @"offset" : @(self.index),@"limit" : @(Limit), @"query" : @{}};
    
    if (isNeedHud == YES) {
        KShowLoading;
    }
    
    [userService getMessageListWithUserID:user.userId withQueryDictionary:queryDic andCallBack:^(NSDictionary *dic, NSError *error) {
        @strongify(self)
        KHideLoading;
        
        [self.deviceAlarmView.tableView.mj_header endRefreshing];
        
        if (error == nil) {
            if (isNewRequest == YES) {
                [self.deviceAlarmDataSourceArray removeAllObjects];
            }
            
            NSArray *array = [dic objectForKey:@"list"];
            for (NSDictionary *dict in array) {
                XLNotificationModel *model = [[XLNotificationModel alloc] initWithDict:dict];
                [self.deviceAlarmDataSourceArray addObject:model];
            }
            
            if (array.count == Limit) {
                [self.deviceAlarmView.tableView.mj_footer endRefreshing];
                self.index = self.index + Limit;
            }else{
                [self.deviceAlarmView.tableView.mj_footer endRefreshingWithNoMoreData];
            }
            
            self.deviceAlarmView.dataSourceArray = self.deviceAlarmDataSourceArray;
            [self.deviceAlarmView.tableView reloadData];

        }
        [self.navigationItem.rightBarButtonItem setEnabled:self.deviceAlarmDataSourceArray.count > 0];
        if (self.deviceAlarmDataSourceArray.count <= 0) {
            if (error) {
                [self.deviceAlarmView showEmptyViewWithText:@"网络连接失败，请检查网络是否正常"];
            }else {
                [self.deviceAlarmView showEmptyViewWithText:@"暂无消息"];
            }
        }
        
    }];
    
}

/**
 获取邀请信息列表数据
 */
- (void)requestInvitationMessageViewDataWithHud:(BOOL)isNeedHud
{
    @weakify(self)
    
    if (isNeedHud == YES) {
        KShowLoading;
    }
    
    XLUserService *userService = [XLServiceManager shareInstance].userService;
    XLUserModel *user = [XLDataSource shareInstance].user;
    
    [userService getShareDeviceListWithCallBack:^(NSArray *arr, NSError *error) {
        @strongify(self)
        KHideLoading;
        
        if ([self.invitationMessageView.tableView.mj_header isRefreshing]) {
            [self.invitationMessageView.tableView.mj_header endRefreshing];
        }
        
        if (error == nil) {
            NSMutableArray *tempArray = [NSMutableArray array];
            for (int i=0; i<arr.count; i++) {
                NSDictionary *dic = [arr objectAtIndex:i];
                MemberModel *model = [MemberModel yy_modelWithDictionary:dic];
                
                NSString *cancelStr = @"cancel";
                NSString *acceptStr = @"accept";
                NSString *denyStr = @"deny";
                NSString *unsubscribedStr = @"unsubscribed";
                
                BOOL b1 = [model.state isEqualToString:cancelStr];
                BOOL b2 = [model.state isEqualToString:acceptStr];
                BOOL b3 = [model.state isEqualToString:denyStr];
                BOOL b4 = [model.state isEqualToString:unsubscribedStr];
                BOOL b5 = b1 || b2 || b3 || b4;
                BOOL b6 = [model.user_id isEqualToString:user.userId.stringValue];
                
                if (b5 && b6) {
                    [tempArray addObject:model];
                }
                
            }
            
            self.invitationDataSourceArray = [NSMutableArray arrayWithArray:[[tempArray reverseObjectEnumerator] allObjects]];
            
            self.invitationMessageView.dataSourceArray = self.invitationDataSourceArray;
            
            [self.invitationMessageView.tableView reloadData];
        }
        
        if (self.invitationDataSourceArray.count <= 0) {
            if (error) {
                [self.invitationMessageView showEmptyViewWithText:@"网络连接失败，请检查网络是否正常"];
            }else {
                [self.invitationMessageView showEmptyViewWithText:@"暂无消息"];
            }
        }
    }];
}

/**
 删除邀请记录
 */
- (void)deleteDeviceShareWithInviteCode:(NSString*)inviteCode
{
    @weakify(self)
    
    KShowLoading;
    XLUserService *userService = [XLServiceManager shareInstance].userService;
    [userService deleteDeviceShareRecordWithInviteCode:inviteCode withCallBack:^(NSDictionary *dic, NSError *error) {
        @strongify(self)
        KHideLoading;
        
        if (error == nil) {
            [self requestInvitationMessageViewDataWithHud:YES];
        }
    }];
}

#pragma mark - InvitationMessageViewDelegate

/**
 删除邀请记录
 */
- (void)invitationMessageView:(InvitationMessageView *)invitationMessageView deleteDeviceShareWithInviteCode:(NSString *)inviteCode
{
    [self deleteDeviceShareWithInviteCode:inviteCode];
}

@end
