//
//  ChangePasswordViewController.m
//  vatti
//
//  Created by 许瑞邦 on 2018/5/11.
//  Copyright © 2018年 xlink. All rights reserved.
//

#import "ChangePasswordViewController.h"
#import "XLTextField.h"
#import "XLUtilTool.h"
#import "XLUserService.h"
#import "ModdifyPasswordController.h"
#import "UITextField+XLCategory.h"

@interface ChangePasswordViewController ()

@property (weak, nonatomic) IBOutlet XLTextField *originalPassword;
@property (weak, nonatomic) IBOutlet XLTextField *password;
@property (weak, nonatomic) IBOutlet XLTextField *againNewPassword;
@property (weak, nonatomic) IBOutlet UIButton *submit;

@end

@implementation ChangePasswordViewController

- (void)viewDidLoad {
    [super viewDidLoad];
}

- (void)initSubViews{
    [super initSubViews];
    
    //naiv
    self.navigationItem.title = XLLocalizeString(@"修改密码");
}

- (void)buildData
{
    [super buildData];
    
    @weakify(self)

    [self.originalPassword addShowPasswordButton];
    [self.password addShowPasswordButton];
    [self.againNewPassword addShowPasswordButton];

    [[self.password  rac_signalForControlEvents:UIControlEventEditingDidEndOnExit] subscribeNext:^(__kindof UIControl * _Nullable x) {
        [x resignFirstResponder];
    }];
    
    [[self.originalPassword rac_signalForControlEvents:UIControlEventEditingDidEndOnExit] subscribeNext:^(__kindof UIControl * _Nullable x){
        @strongify(self)
        [self.password becomeFirstResponder];
    }];
    
    [[self.password rac_signalForControlEvents:UIControlEventEditingDidEndOnExit] subscribeNext:^(__kindof UIControl * _Nullable x){
        @strongify(self)
        [self.againNewPassword becomeFirstResponder];
    }];
    
    [[self.submit rac_signalForControlEvents:UIControlEventTouchUpInside] subscribeNext:^(__kindof UIControl * _Nullable x) {
        @strongify(self)
        [self changePassword];
    }];
    
    RAC(self.submit,enabled) = [RACSignal combineLatest:@[self.originalPassword.rac_textSignal,self.password.rac_textSignal,self.againNewPassword.rac_textSignal]reduce:^(NSString *originalPassword,NSString *newPassword,NSString *againNewPassword){
        return @(originalPassword.length && newPassword.length && againNewPassword.length);
    }];
    
    [self.originalPassword observeSelectedState];
    [self.password observeSelectedState];
    [self.againNewPassword observeSelectedState];
}

- (void)changePassword{
    [self.view endEditing:YES];
    
    @weakify(self)
    
    NSString *originalPassword = self.originalPassword.text;
    NSString *newPassword = self.password.text;
    NSString *againNewPassword = self.againNewPassword.text;
    
    if (originalPassword.length < 6 || originalPassword.length > 16) {
        [self alertHintWithMessage:@"原密码格式错误，请重试"];
        return;
    }
    if (newPassword.length < 6 || newPassword.length > 16 || againNewPassword.length < 6 || againNewPassword.length > 16) {
        [self alertHintWithMessage:@"新密码格式错误，请重试"];
        return;
    }
    if ([newPassword isEqualToString:againNewPassword] == NO) {
        [self alertHintWithMessage:@"两次密码输入不一致"];
        return;
    }

    KShowLoading;
    [[XLServiceManager shareInstance].userService modifyPasswordWithOldPassword:originalPassword NewPassword:newPassword handler:^(id result, NSError *error) {
        @strongify(self)
        KHideLoading;
        
        if (error == nil) {
            ModdifyPasswordController *vc = [[ModdifyPasswordController alloc] init];
            [self.navigationController pushViewController:vc animated:YES];
        }else {
            [self failWithError:error];
        }
    }];
}

- (void)failWithError:(NSError *)error {
    NSString *message = @"网络连接失败，请检查网络是否正常";
    if (error.code == XLinkErrorCodeApiAccountPasswordError) {
        message = @"原密码错误，请重试";
    }
    [self alertHintWithMessage:message];
}

@end
