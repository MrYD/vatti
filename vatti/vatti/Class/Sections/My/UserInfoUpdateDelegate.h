//
//  UserInfoUpdateDelegate.h
//  vatti
//
//  Created by SunWangXia on 2018/8/3.
//  Copyright © 2018年 xlink. All rights reserved.
//

@protocol UserInfoUpdateDelegate <NSObject>

@optional

- (void)onUserInfoUpdate;

@end
