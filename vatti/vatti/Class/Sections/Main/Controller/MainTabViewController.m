//
//  MainTabViewController.m
//  vatti
//
//  Created by 许瑞邦 on 2018/4/28.
//  Copyright © 2018年 xlink. All rights reserved.
//

#import "MainTabViewController.h"
#import "DeviceViewController.h"
#import "ServiceViewController.h"
#import "MyViewController.h"
#import "BaseNavigationViewController.h"
#import "LoginViewController.h"
#import "XLUtilTool.h"
#import "UIImage+XLCategory.h"
#import "AppDelegate+ThirdPart.h"
#import "XLNetworkModule.h"
#import "UIViewController+UIAlertController.h"
#import "UIViewController+Hud.h"

@interface MainTabViewController ()

@end

@implementation MainTabViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    //初始化Tabbar
    [self setupTabbar];

    //显示隐藏红点
    [self showBadgeWithCount];
    
    //监听通知
    [self setupNotification];
    
    //初始化XLSDK
    AppDelegate *delegate = (AppDelegate*)[UIApplication sharedApplication].delegate;
    [delegate setupXLSDK];

    //最后监听RAC
    [self setupRAC];
}

- (void)dealloc
{
    NSLog(@"tabbar销毁了");
    [kXLNotificationCenter removeObserver:self];
}

/**
 根据状态退出登录
 */
- (void)applogoutWithState:(XLUserState)state {
    if (state == XLUserStateOffline) {
        [self appLogout];
    }else if (state == XLUserStateKickout) {
        [self alertHintWithMessage:@"账号已在其他设备登录，请重新登录" handler:nil];
        [self appLogout];
    }else if (state == XLUserStateTokenExpired) {
        [self alertHintWithMessage:@"登录信息已过期，请重新登录" handler:nil];
        [self appLogout];
    }
}

#pragma mark - Data

- (void)setupNotification
{
    @weakify(self)
    
    //监听告警变更
    [[[kXLNotificationCenter rac_addObserverForName:XLNotificationDataPointAlert object:nil] deliverOnMainThread] subscribeNext:^(NSNotification * _Nullable x) {
        @strongify(self)
        [self showBadgeWithCount];
    }];
    
    //监听设备分享
    [[[kXLNotificationCenter rac_addObserverForName:XLNotificationDeviceShare object:nil] deliverOnMainThread] subscribeNext:^(NSNotification * _Nullable x) {
        @strongify(self)
        [self configureXLNotificationDeviceShare:x];
    }];
}

- (void)setupRAC
{
    @weakify(self)
    
    //监听用户登录状态
    [[RACObserve(XL_DATA_SOURCE, userState).distinctUntilChanged deliverOnMainThread] subscribeNext:^(id  _Nullable x) {
        @strongify(self)
        
        XLUserState state = [x integerValue];
        [self applogoutWithState:state];
        [XL_DATA_SOURCE updateUser];
    }];

}

- (void)configureDeviceShareWithEventDict:(NSDictionary*)eventDict andResult:(id)result andError:(NSError*)err
{
    @weakify(self)
    
    if (err == nil){
        
        NSString *title;
        if ([result objectForKey:@"nickname"]){
            title = [result objectForKey:@"nickname"];
        }else{
            title = [result objectForKey:@"user_id"];
        }
        
        NSString *message = [NSString stringWithFormat:@"收到来自 %@的设备分享",title];
        
        [self alertHintWithMessage:message cancelTitle:@"拒绝" otherTitles:@[@"接收"] handler:^(UIAlertAction *action, NSUInteger tag) {
            @strongify(self)
            
            if (tag == 0) {
                [self denyDeviceInviteWithEventDict:eventDict];
            }else if (tag == 1) {
                [self acceptDeviceInviteWithEventDict:eventDict];
            }
        }];
        
    }
}

/**
 接受邀请
 */
- (void)acceptDeviceInviteWithEventDict:(NSDictionary *)eventDict {
    @weakify(self)
    
    NSString *inviteId    = [eventDict objectForKey : @"invite_code"];
    NSString *device_id   = [eventDict objectForKey : @"device_id"];
    NSDictionary *content = @{@"invite_code" : inviteId};
    
    [[XLServiceManager shareInstance].userService acceptInvitationInviteID:content CallBack:^(NSDictionary *dic, NSError *error) {
        @strongify(self)
        
        if (error == nil) {
            [XL_NETWORK_MODULE getAccountSubscribeDeviceListWithXLUserModel:XL_DATA_SOURCE.user handler:nil];
        }else{
            [self showErrorWithMessage:@"接收失败"];
        }
    }];
}

/**
 拒绝邀请
 */
- (void)denyDeviceInviteWithEventDict:(NSDictionary *)eventDict {
    NSString *invite_code = [eventDict objectForKey : @"invite_code"];
    NSDictionary *content = @{@"invite_code" : invite_code};
    
    [[XLServiceManager shareInstance].userService refusedInvitationInviteID:content CallBack:nil];
}

#pragma mark - Action
- (void)appLogout {
    [XL_DATA_SOURCE appLogout];
    
    LoginViewController *login = [[LoginViewController alloc] init];
    
    BaseNavigationViewController *nvc = [[BaseNavigationViewController alloc] initWithRootViewController:login];
    [nvc.navigationBar setBackgroundImage:[UIImage xl_imageForColor:[UIColor colorWithRed:255/255. green:255/255. blue:255/255. alpha:1]] forBarMetrics:UIBarMetricsDefault];
    
    [XLUtilTool setRootViewController:nvc animationOptions:UIViewAnimationOptionTransitionCrossDissolve duration:0.3];
}

- (void)configureXLNotificationDeviceShare:(NSNotification*)notification
{
    @weakify(self)
    
    NSDictionary *eventDict = notification.userInfo;
    NSNumber *numFromID = notification.object;
    
    //数据源不为空
    BOOL b1 = eventDict != nil;
    //邀请者id不为空
    BOOL b2 = numFromID != nil;
    
    //当前页面不是登录页
    UINavigationController *selectNavi = self.selectedViewController;
    BOOL b3 = [selectNavi.topViewController class] != [LoginViewController class];
    
    if (b1 && b2) {
        if (b3) {
            [XLinkHttpRequest getUserOpenInfoWithUserID:numFromID didLoadData:^(id result, NSError *err) {
                @strongify(self)
                [self configureDeviceShareWithEventDict:eventDict andResult:result andError:err];
            }];
        }else{
            //在启动页面接收到通知的时候，延时5秒
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                [kXLNotificationCenter postNotificationName:XLNotificationDeviceShare object:notification.object userInfo:notification.userInfo];
            });
        }
    }
}

#pragma mark - UI
- (void)setupTabbar{
    DeviceViewController *deviceVC = [[DeviceViewController alloc] init];
    BaseNavigationViewController *deviceNav = [[BaseNavigationViewController alloc] initWithRootViewController:deviceVC];
    deviceNav.tabBarItem.title = @"智能";
    
    ServiceViewController *serviceVC = [[ServiceViewController alloc] init];
    BaseNavigationViewController *serviceNav = [[BaseNavigationViewController alloc] initWithRootViewController:serviceVC];
    serviceNav.tabBarItem.title = @"服务";
    
    MyViewController *myVC = [[MyViewController alloc] init];
    BaseNavigationViewController *myNav = [[BaseNavigationViewController alloc] initWithRootViewController:myVC];
    myNav.tabBarItem.title = @"会员";
    
    self.viewControllers = @[deviceNav, serviceNav, myNav];
    
    NSString *deviceNorImgName  = @"tab_bar_ic_home_gray";
    NSString *deviceSelImgName  = @"tab_bar_ic_home";
    NSString *serviceNorImgName = @"tab_bar_ic_serivce_gray";
    NSString *serviceSelImgName = @"tab_bar_ic_serivce";
    NSString *myNorImgName      = @"tab_bar_ic_my";
    NSString *mySelImgName      = @"tab_bar_ic_my_touched";
    
    UIColor *norColor = [UIColor lightGrayColor];;
    UIColor *selColor = [UIColor colorWithRed:0x57/255.0 green:0xbe/255.0 blue:0x62/255.0 alpha:1.0];;
    
    //设置字体颜色
    NSMutableDictionary *norTextArrays = [NSMutableDictionary dictionary];
    norTextArrays[NSForegroundColorAttributeName] = norColor;
    norTextArrays[NSFontAttributeName] = [UIFont fontWithName:@"Montserrat-Regular" size:10];
    
    NSMutableDictionary *selTextArrays = [NSMutableDictionary dictionary];
    selTextArrays[NSForegroundColorAttributeName] = selColor;
    selTextArrays[NSFontAttributeName] = [UIFont fontWithName:@"Montserrat-Regular" size:10];
    
    [deviceNav.tabBarItem  setTitleTextAttributes:norTextArrays forState:UIControlStateNormal];
    [deviceNav.tabBarItem  setTitleTextAttributes:selTextArrays forState:UIControlStateSelected];
    [serviceNav.tabBarItem setTitleTextAttributes:norTextArrays forState:UIControlStateNormal];
    [serviceNav.tabBarItem setTitleTextAttributes:selTextArrays forState:UIControlStateSelected];
    [myNav.tabBarItem      setTitleTextAttributes:norTextArrays forState:UIControlStateNormal];
    [myNav.tabBarItem      setTitleTextAttributes:selTextArrays forState:UIControlStateSelected];
    
    UIImage *devNorImg = [UIImage imageNamed:deviceNorImgName];
    deviceNav.tabBarItem.image = [devNorImg imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    UIImage *devSelImg = [UIImage imageNamed:deviceSelImgName];
    deviceNav.tabBarItem.selectedImage = [devSelImg imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    
    UIImage *serNorImg = [UIImage imageNamed:serviceNorImgName];
    serviceNav.tabBarItem.image = [serNorImg imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    UIImage *serSelImg = [UIImage imageNamed:serviceSelImgName];
    serviceNav.tabBarItem.selectedImage = [serSelImg imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    
    UIImage *myNorImg = [UIImage imageNamed:myNorImgName];
    myNav.tabBarItem.image = [myNorImg imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    UIImage *mySelImg = [UIImage imageNamed:mySelImgName];
    myNav.tabBarItem.selectedImage = [mySelImg imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    
    self.tabBar.translucent = NO;
}

- (void)showBadgeWithCount
{
    NSInteger alertCount = [kXLNSUserDefaults integerForKey:UDDataPointAlertCount];
    if (alertCount > 0) {
        [self showBadgeOnItmIndex:2];
    }else{
        [self hideBadgeOnItemIndex:2];
    }
}

/**
 tabbar显示小红点
 
 @param index 第几个控制器显示，从0开始算起
 */
- (void)showBadgeOnItmIndex:(int)index{
    [self removeBadgeOnItemIndex:index];
    //label为小红点，并设置label属性
    UIView *redPoint = [[UIView alloc] init];
    redPoint.tag = 1000+index;
    redPoint.layer.cornerRadius = 3;
    redPoint.clipsToBounds = YES;
    redPoint.backgroundColor = [UIColor redColor];
    CGRect tabFrame = self.tabBar.frame;
    
    //计算小红点的X值，根据第index控制器，小红点在每个tabbar按钮的中部偏移0.1，即是每个按钮宽度的0.6倍
    CGFloat percentX = (index+0.6);
    CGFloat tabBarButtonW = CGRectGetWidth(tabFrame)/self.tabBar.items.count;
    CGFloat x = percentX*tabBarButtonW;
    CGFloat y = 0.1*CGRectGetHeight(tabFrame);
    //10为小红点的高度和宽度
    redPoint.frame = CGRectMake(x, y, 6, 6);
    
    [self.tabBar addSubview:redPoint];
    //把小红点移到最顶层
    [self.tabBar bringSubviewToFront:redPoint];
}

/**
 隐藏红点
 
 @param index 第几个控制器隐藏，从0开始算起
 */
- (void)hideBadgeOnItemIndex:(int)index{
    [self removeBadgeOnItemIndex:index];
}

/**
 移除控件
 
 @param index 第几个控制器要移除控件，从0开始算起
 */
- (void)removeBadgeOnItemIndex:(int)index{
    for (UIView *subView in self.tabBar.subviews) {
        if (subView.tag == 1000+index) {
            [subView removeFromSuperview];
        }
    }
}


@end
