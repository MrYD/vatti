//
//  MainTabViewController.h
//  vatti
//
//  Created by 许瑞邦 on 2018/4/28.
//  Copyright © 2018年 xlink. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MainTabViewController : UITabBarController

/**
 隐藏红点
 
 @param index 第几个控制器隐藏，从0开始算起
 */
-(void)hideBadgeOnItemIndex:(int)index;

/**
 tabbar显示小红点
 
 @param index 第几个控制器显示，从0开始算起
 */
- (void)showBadgeOnItmIndex:(int)index;

@end
