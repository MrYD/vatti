//
//  XLScanDeviceHelperTool.m
//  vatti
//
//  Created by 李叶 on 2018/6/1.
//  Copyright © 2018年 xlink. All rights reserved.
//

#import "XLScanDeviceHelperTool.h"
#import "XLNetworkModule.h"

#define ScanDeviceTimeOut 30.0f

@implementation XLScanDeviceHelperTool

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.devicesArray = [NSMutableArray array];
    }
    return self;
}

/**
 生成扫描task(任务)
 */
- (void)createScanTaskWithProductIds:(NSArray *)productIds{
    @weakify(self)
    
    //增加判断，预防只cancel 没有置为nil的情况
    BOOL b1 = self.task != nil;
    BOOL b2 = self.task.isTimeout == NO;
    BOOL b3 = self.task.isComplete == NO;
    if (b1 && b2 && b3) {
        NSLog(@"搜索任务已存在，不需重复创建");
        return;
    }
    
    [self.devicesArray removeAllObjects];
    
    XLinkScanDeviceTask *scanTask = [XLinkScanDeviceTask scanDeviceTaskWithProductIdArray:productIds timeout:ScanDeviceTimeOut didDiscoveredDeviceHandler:^(XDevice *device) {
        @strongify(self)
        
        [self deviceToHeavy:device];
        
    } completionHandler:^(id result, NSError *error) {
        @strongify(self)
        
        NSLog(@"搜索结束");
        
        [self.task cancel];
        self.task = nil;
        
        if (self.devicesArray.count > 0) {
            NSLog(@"搜索完成，并且搜索到新的设备");
            if (self.delegate && [self.delegate respondsToSelector:@selector(xlScanDeviceHelperToolScranFinished:)]) {
                [self.delegate xlScanDeviceHelperToolScranFinished:self];
            }
        }else{
            if (self.delegate && [self.delegate respondsToSelector:@selector(xlScanDeviceHelperTool:scanDevicesFailedWithError:)]) {
                NSLog(@"搜索完成，但没有搜索到新的设备，回调失败");
                [self.delegate xlScanDeviceHelperTool:self scanDevicesFailedWithError:error];
            }
        }
        
    }];
    
    self.task = scanTask;
}

- (void)deviceToHeavy:(XDevice *)device {
    
    BOOL b1 = device.isBinded;
    if (b1 == YES) {
        NSLog(@"设备已经被绑定。");
        return;
    }
    
    for (XDevice *d in self.devicesArray) {
        BOOL b2 = [d isEqualToDevice:device];
        if (b2 == YES) {
            NSLog(@"搜索到重复设备。");
            return;
        }
    }
    
    NSLog(@"搜索到新设备。");
    [self.devicesArray addObject:device];
    
    if (self.delegate && [self.delegate respondsToSelector:@selector(xlScanDeviceHelperTool:scanDevicesSuccssfully:)]) {
        NSLog(@"搜索到新设备，并回调。");
        [self.delegate xlScanDeviceHelperTool:self scanDevicesSuccssfully:self.devicesArray];
    }
}

/**
 取消扫描task(任务)
 */
- (void)cancelTask {
    if (self.task != nil) {
        [self.task cancel];
        self.task = nil;
    }
    
    if (self.delegate != nil) {
        self.delegate = nil;
    }
}

- (void)dealloc {
    [self cancelTask];
}

@end
