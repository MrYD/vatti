//
//  ConnectAppointmentCell.m
//  vatti
//
//  Created by 李叶 on 2018/6/7.
//  Copyright © 2018年 xlink. All rights reserved.
//

#import "ConnectAppointmentCell.h"

@implementation ConnectAppointmentCell

-(void)setDeviceModel:(WaterHeaterDeviceModel *)deviceModel{
    _deviceModel = deviceModel;
    
    @weakify(self);
    [[[RACObserve(self.deviceModel, connected).distinctUntilChanged takeUntil:self.rac_prepareForReuseSignal] deliverOnMainThread]subscribeNext:^(id  _Nullable x) {
        @strongify(self);
        if(deviceModel.connected){
            [self.connect setImage:[UIImage imageNamed:@"ic_online"] forState:(UIControlStateNormal)];
        }else{
            [self.connect setImage:[UIImage imageNamed:@"ic_offline"] forState:(UIControlStateNormal)];
        }
    }];
    [[[RACObserve(self.deviceModel, function_flag).distinctUntilChanged takeUntil:self.rac_prepareForReuseSignal] deliverOnMainThread]subscribeNext:^(id  _Nullable x) {
        @strongify(self);
        if((deviceModel.function_flag & (0x01 << 6)) >> 6  == 1){
            self.appointment.hidden = NO;
        }else{
            self.appointment.hidden = YES;
        }
    }];
    if(!XL_DATA_VIRTUAL.isLogin){
        [self.connect setImage:[UIImage imageNamed:@"ic_online"] forState:(UIControlStateNormal)];
        self.appointment.hidden = NO;
    }
   
}

@end
