//
//  DeviceCell.h
//  vatti
//
//  Created by 李叶 on 2018/5/25.
//  Copyright © 2018年 xlink. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "WaterHeaterDeviceModel.h"

@interface DeviceCell : UITableViewCell

@property (weak,   nonatomic) IBOutlet UIImageView *image;
@property (weak,   nonatomic) IBOutlet UIImageView *ivWarn;

@property (weak,   nonatomic) IBOutlet UILabel *deviceName;
@property (weak,   nonatomic) IBOutlet UILabel *deviceState;
@property (weak,   nonatomic) IBOutlet UILabel *deviceOnline;

@property (strong, nonatomic) NSString *imageName;
@property (strong, nonatomic) NSString *state;

@property (strong, nonatomic) WaterHeaterDeviceModel *deviceModel;


/**
 显示告警
 */
- (void)showWarning:(BOOL)isShow;

@end
