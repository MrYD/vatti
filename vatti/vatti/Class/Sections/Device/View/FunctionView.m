//
//  FunctionView.m
//  vatti
//
//  Created by Eric on 2018/7/23.
//  Copyright © 2018年 xlink. All rights reserved.
//

#import "FunctionView.h"
#import "BZButton.h"
#import "UIColor+XLCategory.h"

#define SelectedColor   @"00C355"
#define UNSelectedColor @"AAAAAA"

@interface FunctionView()

@end

@implementation FunctionView

+ (instancetype)view
{
    return [[NSBundle mainBundle] loadNibNamed:NSStringFromClass([self class]) owner:nil options:nil].firstObject;
}

- (void)awakeFromNib
{
    [super awakeFromNib];
    
    [self setupFunctionView];
}

#pragma mark - UI

- (void)setupFunctionView
{
    UIColor *selectedColor   = [UIColor colorWithHexString:SelectedColor];
    UIColor *unSelectedColor = [UIColor colorWithHexString:UNSelectedColor];
    
    [self.btnAppoint setTitleColor:unSelectedColor forState:UIControlStateNormal];
    [self.btnAppoint setTitleColor:selectedColor forState:UIControlStateSelected];
    
    [self.btnHot setTitleColor:unSelectedColor forState:UIControlStateNormal];
    [self.btnHot setTitleColor:selectedColor forState:UIControlStateSelected];
    
    [self.btnComfortableBath setTitleColor:unSelectedColor forState:UIControlStateNormal];
    [self.btnComfortableBath setTitleColor:selectedColor forState:UIControlStateSelected];
    
    [self.btnBathtub setTitleColor:unSelectedColor forState:UIControlStateNormal];
    [self.btnBathtub setTitleColor:selectedColor forState:UIControlStateSelected];
    
    [self.btnWaterfallBath setTitleColor:unSelectedColor forState:UIControlStateNormal];
    [self.btnWaterfallBath setTitleColor:selectedColor forState:UIControlStateSelected];
    
    [self.btnSwitch setTitleColor:unSelectedColor forState:UIControlStateNormal];
    [self.btnSwitch setTitleColor:selectedColor forState:UIControlStateSelected];
    
}

- (void)setAllBtnSelected:(BOOL)selected
{
    self.btnAppoint.selected         = selected;
    self.btnHot.selected             = selected;
    self.btnComfortableBath.selected = selected;
    self.btnBathtub.selected         = selected;
    self.btnWaterfallBath.selected   = selected;
    self.btnSwitch.selected          = selected;
}

- (void)setAllBtnEnable:(BOOL)enable
{
    self.btnAppoint.enabled         = enable;
    self.btnHot.enabled             = enable;
    self.btnComfortableBath.enabled = enable;
    self.btnBathtub.enabled         = enable;
    self.btnWaterfallBath.enabled   = enable;
    self.btnSwitch.enabled          = enable;
}

- (void)setBtnSelectedWithFunctionFlag:(NSInteger)function_flag
{
    self.btnSwitch.selected          = [XLDataPointTool isOpenWithValue:function_flag andShift:0];
    self.btnComfortableBath.selected = [XLDataPointTool isOpenWithValue:function_flag andShift:2];
    self.btnHot.selected             = [XLDataPointTool isOpenWithValue:function_flag andShift:3];
    self.btnBathtub.selected         = [XLDataPointTool isOpenWithValue:function_flag andShift:4];
    self.btnWaterfallBath.selected   = [XLDataPointTool isOpenWithValue:function_flag andShift:5];
    self.btnAppoint.selected         = [XLDataPointTool isOpenWithValue:function_flag andShift:6];
}

#pragma mark - Action

- (IBAction)btnFunctionClicked:(BZButton *)sender {
    
    if (self.delegate && [self.delegate respondsToSelector:@selector(functionView:btnClicked:)]) {
        [self.delegate functionView:self btnClicked:sender];
    }
}

@end
