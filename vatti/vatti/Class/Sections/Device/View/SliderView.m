//
//  SliderView.m
//  testDeviceControl
//
//  Created by Eric on 2018/7/20.
//  Copyright © 2018年 Eric. All rights reserved.
//

#import "SliderView.h"
#import "UIColor+XLCategory.h"

///slider 左、右间距
#define SliderLRPanding 70.0f

#define SliderBGLayerH 6.0f

@interface SliderView()

#pragma mark - Constraint

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *consIVLockLeading;

#pragma mark - UIView

///slider view
@property (weak, nonatomic) IBOutlet UIView *viewSlider;

///slider 渐变块背景 view
@property (weak, nonatomic) IBOutlet UIView *viewSliderLayerBG;

#pragma mark - UIImageView

@property (weak, nonatomic) IBOutlet UIImageView *ivLock;

#pragma mark - UIButton

@property (weak, nonatomic) IBOutlet UIButton *btnCut;
@property (weak, nonatomic) IBOutlet UIButton *btnAdd;

#pragma mark - 滑块相关

///渐变layer底部灰色
@property (strong, nonatomic) CALayer *layerSliderBG;

///渐变layer
@property (strong, nonatomic) CAGradientLayer *layerSliderGradient;

///渐变layer的约束layer
@property (strong, nonatomic) CALayer *layerSliderGradientMask;

///约束layer的宽度
@property (assign, nonatomic) CGFloat layerSliderMaskWidth;


#pragma mark - NSArray

///渐变颜色数组
@property (strong, nonatomic) NSArray *gradientColorsArray;

///离线颜色数组
@property (strong, nonatomic) NSArray *offlineColorsArray;

#pragma mark - Data

///连接
@property (assign, nonatomic) BOOL isConnected;

///开关
@property (assign, nonatomic) BOOL isOpen;

@end

@implementation SliderView

#pragma mark - LifeCycle

+ (instancetype)view
{
    return [[NSBundle mainBundle] loadNibNamed:NSStringFromClass([self class]) owner:nil options:nil].firstObject;
}


- (void)awakeFromNib
{
    [super awakeFromNib];
    
    //渐变颜色数组
    self.gradientColorsArray = @[(id)[UIColor colorWithHexString:@"F3C7A1"].CGColor,(id)[UIColor colorWithHexString:@"EC544F"].CGColor];
    //离线颜色数组
    self.offlineColorsArray = @[(id)[UIColor colorWithHexString:@"C0BEBE"].CGColor,(id)[UIColor colorWithHexString:@"C0BEBE"].CGColor];
    
    [self setupSlider];
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    
    //slider宽度
    CGFloat sliderWidth = self.frame.size.width - (SliderLRPanding * 2);
    
    //滑块
    self.layerSliderBG.frame        = CGRectMake(0, 0, sliderWidth, SliderBGLayerH);
    
    self.layerSliderGradient.frame  = CGRectMake(0, 0, sliderWidth, SliderBGLayerH);
    
    [self setupLayerSliderGradientMask];
    self.layerSliderGradientMask.frame = CGRectMake(0, 0, self.layerSliderMaskWidth, SliderBGLayerH);
    
    //锁图
    CGFloat bili = 1.0f * (50 - 35) / (60 - 35);
    self.consIVLockLeading.constant = (sliderWidth - (2*2)) * bili - (16 / 2);
    
}

#pragma mark - UI

- (void)setupSlider
{
    self.slider.continuous = NO;
    
    [self.slider setThumbImage:[UIImage imageNamed:@"ic_slider_thumb"] forState:UIControlStateNormal];
    
    [self setupLayerSliderBG];
    
    [self setupLayerSliderGradient];
    
    [self setupLayerSliderGradientMask];
    
    [self caculateLayerSliderMaskWidth];
}

- (void)setupLayerSliderGradient
{
    self.layerSliderGradient              = [CAGradientLayer layer];
    self.layerSliderGradient.startPoint   = CGPointMake(0, 0);
    self.layerSliderGradient.endPoint     = CGPointMake(1, 0);
    self.layerSliderGradient.cornerRadius = 5;

    [self.viewSliderLayerBG.layer addSublayer:self.layerSliderGradient];
}

- (void)setupLayerSliderBG
{
    self.layerSliderBG                    = [CALayer layer];
    self.layerSliderBG.backgroundColor    = [UIColor colorWithHexString:@"F1F4F7"].CGColor;
    self.layerSliderBG.cornerRadius       = 5;
    
    [self.viewSliderLayerBG.layer addSublayer:self.layerSliderBG];
}

- (void)setupLayerSliderGradientMask
{
    self.layerSliderGradientMask                 = [CALayer layer];
    self.layerSliderGradientMask.backgroundColor = [UIColor redColor].CGColor;//这里一定要给定颜色，否则可变渐变layer不能正常显示
    
    self.layerSliderGradient.mask                = self.layerSliderGradientMask;
}

/**
 更新UI
 */
- (void)updateUI
{
    [self setNeedsLayout];
    [self layoutIfNeeded];
}

/**
 根据温度更新UI
 */
- (void)updateUIWithTemp:(NSInteger)temp andIsConnected:(BOOL)isConnected andIsOpen:(BOOL)isOpen
{
    self.slider.value = temp;
    self.isConnected = isConnected;
    self.isOpen = isOpen;
    
    BOOL b1 = self.isConnected == YES;
    BOOL b2 = self.isOpen == YES;
    if (b1 && b2) {
        self.layerSliderGradient.colors       = self.gradientColorsArray;
    }else{
        self.layerSliderGradient.colors       = self.offlineColorsArray;
    }
    
    [self caculateLayerSliderMaskWidth];
    [self updateUI];
}

/**
 更新高温锁图
 */
- (void)updateIVLockState:(BOOL)isLock
{
    UIImage *lockImage   = [UIImage imageNamed:@"ic_locked"];
    UIImage *unLockImage = [UIImage imageNamed:@"ic_unlock"];
    
    if (isLock == YES) {
        [self.ivLock setImage:lockImage];
    }else{
        [self.ivLock setImage:unLockImage];
    }
}

/**
 更新控制可用性
 */
- (void)updateControlWithEnable:(BOOL)isEnable
{
    self.btnCut.enabled = isEnable;
    self.btnAdd.enabled = isEnable;
    self.slider.userInteractionEnabled = isEnable;
}

#pragma mark - Action

/**
 滑块值改变
 */
- (IBAction)sliderValueChanged:(UISlider *)sender {
    
    if (self.delegate && [self.delegate respondsToSelector:@selector(sliderView:sliderValueChanged:)]) {
        [self.delegate sliderView:self sliderValueChanged:sender];
    }
}

- (IBAction)sliderDragInside:(UISlider *)sender {
    if (self.delegate && [self.delegate respondsToSelector:@selector(sliderView:sliderDragInside:)]) {
        [self.delegate sliderView:self sliderDragInside:sender];
    }
}

- (IBAction)btnCutClicked:(UIButton *)sender {
    if (self.delegate && [self.delegate respondsToSelector:@selector(sliderView:btnCutClicked:)]) {
        [self.delegate sliderView:self btnCutClicked:sender];
    }
}

- (IBAction)btnAddClicked:(UIButton *)sender {
    if (self.delegate && [self.delegate respondsToSelector:@selector(sliderView:btnAddClicked:)]) {
        [self.delegate sliderView:self btnAddClicked:sender];
    }
}

#pragma mark - Data

- (void)caculateLayerSliderMaskWidth
{
    CGFloat sliderW = self.frame.size.width - SliderLRPanding * 2;
    self.layerSliderMaskWidth = 1.0f * sliderW * (self.slider.value - 35) / (60 - 35);
}

@end
