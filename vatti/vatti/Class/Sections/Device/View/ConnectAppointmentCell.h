//
//  ConnectAppointmentCell.h
//  vatti
//
//  Created by 李叶 on 2018/6/7.
//  Copyright © 2018年 xlink. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "WaterHeaterDeviceModel.h"

@interface ConnectAppointmentCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIButton *connect;
@property (weak, nonatomic) IBOutlet UIButton *appointment;

@property (strong, nonatomic) WaterHeaterDeviceModel *deviceModel;

@end
