//
//  SliderView.h
//  testDeviceControl
//
//  Created by Eric on 2018/7/20.
//  Copyright © 2018年 Eric. All rights reserved.
//

/**
 *  滑块面板
 *
 *  @author BZH
 */

@class SliderView;

@protocol SliderViewDelegate <NSObject>

@optional

/**
 滑块值将要发生改变
 */
- (void)sliderView:(SliderView*)sliderView sliderDragInside:(UISlider *)slider;

/**
 滑块值发生改变
 */
- (void)sliderView:(SliderView*)sliderView sliderValueChanged:(UISlider *)slider;

/**
 减少按钮点击
 */
- (void)sliderView:(SliderView*)sliderView btnCutClicked:(UIButton*)btnCut;

/**
 增加按钮点击
 */
- (void)sliderView:(SliderView*)sliderView btnAddClicked:(UIButton*)btnAdd;

@end

#import <UIKit/UIKit.h>

@interface SliderView : UIView

#pragma mark - Function

/**
 初始化
 */
+ (instancetype)view;

/**
 更新UI
 */
- (void)updateUI;

/**
 根据温度更新UI
 */
- (void)updateUIWithTemp:(NSInteger)temp andIsConnected:(BOOL)isConnected andIsOpen:(BOOL)isOpen;

/**
 更新高温锁图
 */
- (void)updateIVLockState:(BOOL)isLock;

/**
 更新控制可用性
 */
- (void)updateControlWithEnable:(BOOL)isEnable;

#pragma mark - Slider

@property (weak, nonatomic) IBOutlet UISlider *slider;

@property (weak,   nonatomic) id <SliderViewDelegate> delegate;


@end
