//
//  DeviceCell.m
//  vatti
//
//  Created by 李叶 on 2018/5/25.
//  Copyright © 2018年 xlink. All rights reserved.
//

#import "DeviceCell.h"
#import "XLUtilTool.h"

@implementation DeviceCell

- (void)awakeFromNib
{
    [super awakeFromNib];

    self.layer.borderWidth = 1.0f;
}

- (void)setImageName:(NSString *)imageName{
    self.image.image = [UIImage imageNamed:imageName];
}

- (void)setDeviceModel:(WaterHeaterDeviceModel *)deviceModel{
    _deviceModel = deviceModel;
    @weakify(self);
    [[RACObserve(deviceModel, name) takeUntil:self.rac_prepareForReuseSignal].distinctUntilChanged.deliverOnMainThread subscribeNext:^(NSString * _Nullable x) {
        @strongify(self);
        if (x.length > 0){
            self.deviceName.text = x;
        }else{
            self.deviceName.text = XLDeviceGasWaterHeaterName;
        }
    }];
    [[[RACObserve(self.deviceModel, connected).distinctUntilChanged takeUntil:self.rac_prepareForReuseSignal]
      deliverOnMainThread] subscribeNext:^(id  _Nullable x) {
        @strongify(self)
        BOOL isOnline = [x boolValue];
        if (isOnline == YES) {
            self.deviceOnline.text = @"在线";
        }else{
            self.deviceOnline.text = @"离线";
        }
    }];

    [[[RACObserve(self.deviceModel, function_flag).distinctUntilChanged takeUntil:self.rac_prepareForReuseSignal] deliverOnMainThread]subscribeNext:^(id  _Nullable x) {
        @strongify(self)

        //游客模式判断
        BOOL b1 = XL_DATA_VIRTUAL.isLogin;
        
        if (b1 == NO) {
            return;
        }
        
        BOOL isAppointmentOpen = [XLDataPointTool isOpenWithValue:deviceModel.function_flag andShift:6];
        
        if (isAppointmentOpen == YES){
            self.deviceState.text = @"预约已开启";
        }else{
            self.deviceState.text = @"预约已关闭";
        }

    }];
    
    [[[RACObserve(self.deviceModel, master_err).distinctUntilChanged takeUntil:self.rac_prepareForReuseSignal] deliverOnMainThread]subscribeNext:^(id  _Nullable x) {
        @strongify(self)
        
        if (deviceModel.master_err < 1 || deviceModel.master_err > 18){
            [self showWarning:NO];
        }else{
            [self showWarning:YES];
        }
        
    }];

    //游客模式判断
    if ([XLUtilTool isValidateAutoLogin] == NO){
        self.deviceName.text = XLDeviceGasWaterHeaterName;
        self.deviceOnline.hidden = YES;
        self.deviceState.text = @"虚拟体验";
    }else{
        self.deviceOnline.hidden = NO;
    }
}

#pragma mark - Action

/**
 显示告警
 */
- (void)showWarning:(BOOL)isShow
{
    if (isShow == YES) {
        self.layer.borderColor = [UIColor redColor].CGColor;
        self.accessoryType = UITableViewCellAccessoryNone;
        self.ivWarn.hidden = NO;
    }else{
        self.layer.borderColor = [UIColor clearColor].CGColor;
        self.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        self.ivWarn.hidden = YES;
    }
}

@end
