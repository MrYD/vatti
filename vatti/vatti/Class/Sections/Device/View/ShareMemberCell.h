//
//  ShareMemberCell.h
//  vatti
//
//  Created by 李叶 on 2018/6/9.
//  Copyright © 2018年 xlink. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MemberModel.h"

@interface ShareMemberCell : UITableViewCell

@property (weak,   nonatomic) IBOutlet UILabel *shareState;
@property (weak,   nonatomic) IBOutlet UILabel *userName;

@property (strong, nonatomic) MemberModel *memberModel;

@end
