//
//  AppointmentCell.m
//  vatti
//
//  Created by 李叶 on 2018/6/12.
//  Copyright © 2018年 xlink. All rights reserved.
//

#import "AppointmentCell.h"

@interface AppointmentCell()

@property (weak, nonatomic) IBOutlet UILabel *labDate;
@property (weak, nonatomic) IBOutlet UISwitch *open;

@end

@implementation AppointmentCell

- (void)setAppointmentModel:(AppointmentModel *)appointmentModel{
    _appointmentModel = appointmentModel;
    
    if (appointmentModel.index == 22) {
        self.labDate.text = @"全天候24小时预热";
    }else{
        self.labDate.text = [NSString stringWithFormat:@"%02d:%02d-%02d:%02d",appointmentModel.startHours,appointmentModel.startMinutes,appointmentModel.endHours,appointmentModel.endMinutes];
    }
    
    self.open.on = appointmentModel.isOpen;
}

- (IBAction)switchAction:(UISwitch *)sender {
    if (self.delegate && [self.delegate respondsToSelector:@selector(appointmentCell:dicClickedSwitch:)]) {
        [self.delegate appointmentCell:self dicClickedSwitch:sender];
    }
}
@end
