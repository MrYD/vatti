//
//  AppointmentCell.h
//  vatti
//
//  Created by 李叶 on 2018/6/12.
//  Copyright © 2018年 xlink. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppointmentModel.h"
#import "WaterHeaterDeviceModel.h"

@class AppointmentCell;

@protocol AppointmentCellDelegate <NSObject>

@optional

/**
 点击UISwitch
 */
- (void)appointmentCell:(AppointmentCell*)cell dicClickedSwitch:(UISwitch*)sender;

@end

@interface AppointmentCell : UITableViewCell

///
@property (strong, nonatomic) AppointmentModel *appointmentModel;

///
@property (weak,   nonatomic) id<AppointmentCellDelegate> delegate;

@end
