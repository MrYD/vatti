//
//  ShareMemberCell.m
//  vatti
//
//  Created by 李叶 on 2018/6/9.
//  Copyright © 2018年 xlink. All rights reserved.
//

#import "ShareMemberCell.h"

@implementation ShareMemberCell


- (void)setMemberModel:(MemberModel *)memberModel{
    _memberModel = memberModel;
    
    self.userName.text = memberModel.to_name;

    if([memberModel.state isEqualToString:@"pending"]){
        self.shareState.text = @"等待处理";
    }else if ([memberModel.state isEqualToString:@"accept"]){
        self.shareState.text = @"已接收";
    }else if ([memberModel.state isEqualToString:@"deny"]){
        self.shareState.text = @"已拒绝";
    }else if ([memberModel.state isEqualToString:@"overtime"]){
        self.shareState.text = @"分享失败";
    }else if ([memberModel.state isEqualToString:@"cancel"]){
        self.shareState.text = @"已取消";
    }else if ([memberModel.state isEqualToString:@"invalid"]){
        self.shareState.text = @"分享失败";
    }else if ([memberModel.state isEqualToString:@"unsubscribed"]){
        self.shareState.text = @"已解除";
    }
}

@end
