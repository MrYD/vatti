//
//  FunctionView.h
//  vatti
//
//  Created by Eric on 2018/7/23.
//  Copyright © 2018年 xlink. All rights reserved.
//

/**
 *  功能面板
 *
 *  @author BZH
 */

@class FunctionView;
@class BZButton;

@protocol FunctionViewDelegate <NSObject>

@optional

/**
 所有面板按钮点击
 */
- (void)functionView:(FunctionView*)functionView btnClicked:(BZButton*)button;

@end

#import <UIKit/UIKit.h>

@interface FunctionView : UIView

+ (instancetype)view;

- (void)setAllBtnSelected:(BOOL)selected;

- (void)setAllBtnEnable:(BOOL)enable;

- (void)setBtnSelectedWithFunctionFlag:(NSInteger)function_flag;

@property (weak,   nonatomic) id <FunctionViewDelegate> delegate;

///预约
@property (weak, nonatomic) IBOutlet BZButton *btnAppoint;
///即热
@property (weak, nonatomic) IBOutlet BZButton *btnHot;
///舒适浴
@property (weak, nonatomic) IBOutlet BZButton *btnComfortableBath;
///浴缸
@property (weak, nonatomic) IBOutlet BZButton *btnBathtub;
///瀑布浴
@property (weak, nonatomic) IBOutlet BZButton *btnWaterfallBath;
///开关
@property (weak, nonatomic) IBOutlet BZButton *btnSwitch;

@end
