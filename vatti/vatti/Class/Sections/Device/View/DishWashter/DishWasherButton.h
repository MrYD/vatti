//
//  DishWasherButton.h
//  UITTest
//
//  Created by dongxiao on 2018/8/8.
//  Copyright © 2018年 xiaodongd. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DishWasherButton : UIButton

@property (nonatomic, strong)NSDictionary *tagDic;

- (void)setActiveImage:(UIImage *)activeImage
     activeColor:(UIColor *)activeColor
andUnactiveImage:(UIImage *)unactiveImage
   unactiveColor:(UIColor *)unactiveColor;

- (void)setDishWasherButtonActive:(BOOL)active;

@end
