//
//  DishWasherButton.m
//  UITTest
//
//  Created by dongxiao on 2018/8/8.
//  Copyright © 2018年 xiaodongd. All rights reserved.
//

#import "DishWasherButton.h"

#define NOTIFY_DISHWASHERBTN_ACTIVECHANGE @"notify.dishWasherBtn.activeChange"

@interface DishWasherButton ()

@property (nonatomic, strong) UIColor *activeColor;

@property (nonatomic, strong) UIColor *unactiveColor;

@property (nonatomic, strong) UIImage *activeImage;

@property (nonatomic, strong) UIImage *unactiveImage;

@property (nonatomic, assign) UIControlState *btnState;
@end


@implementation DishWasherButton

//- (instancetype)init {
//    self = [super init];
//    if (self) {
//    }
//    return self;
//}

- (void)setActiveImage:(UIImage *)activeImage
     activeColor:(UIColor *)activeColor
andUnactiveImage:(UIImage *)unactiveImage
   unactiveColor:(UIColor *)unactiveColor
{
    self.activeImage = activeImage;
    
    self.activeColor = activeColor;
    
    self.unactiveImage = unactiveImage;
    
    self.unactiveColor = unactiveColor;
}


- (void)setDishWasherButtonActive:(BOOL)active
{
    self.enabled = active;
    if (self.enabled == YES) {
        [self setBackgroundImage:self.activeImage forState:UIControlStateNormal];
        [self setTitleColor:self.activeColor forState:UIControlStateNormal];
    }
    else{
        [self setBackgroundImage:self.unactiveImage forState:UIControlStateNormal];
        [self setTitleColor:self.unactiveColor forState:UIControlStateNormal];
        [self setSelected:NO];
    }
}

- (void)dealloc
{
//    [[NSNotificationCenter defaultCenter] removeObserver:self name:NOTIFY_DISHWASHERBTN_ACTIVECHANGE object:nil];
}
@end
