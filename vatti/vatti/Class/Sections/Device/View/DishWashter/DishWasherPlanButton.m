//
//  DishWasherPlanButton.m
//  vatti
//
//  Created by dongxiao on 2018/9/26.
//  Copyright © 2018年 xlink. All rights reserved.
//

#import "DishWasherPlanButton.h"
#import "DishWasherConstants.h"
#import "DishWasherTools.h"
@interface DishWasherPlanButton ()
@property(nonatomic,strong)NSDateFormatter *dateFormatter;
@end
@implementation DishWasherPlanButton
-(id)init
{
    self = [super init];
    if (self) {
        [self setupDefualtView];
    }
    return self;
}
-(id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self setupDefualtView];
    }
    return self;
}
-(id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self) {
        [self setupDefualtView];
    }
    return self;
}
-(void)setPlantActSwitch:(BOOL)plantActSwitch
{
    if (_plantActSwitch == plantActSwitch) {
        return;
    }
    _plantActSwitch = plantActSwitch;
    [self refreshView];
}
-(void)setOrderTime:(NSInteger)orderTime
{
    if (_orderTime == orderTime) {
        return;
    }
    _orderTime = orderTime;
    if (orderTime!=0) {
        NSInteger timeHour = ((self.orderTime >> 8) & 0xff);
        NSInteger timeMin = (self.orderTime & 0xff);
        NSInteger timeType = (self.orderTime >> 31);
        NSInteger timeTake = timeType==0?[DishWasherTools getTimeInterValueWithSwitchFunc:self.switchFunc]:0;
        _timeType = timeType;
        _selectedDate = [[NSDate date] dateByAddingTimeInterval:timeHour*60*60+timeMin*60+timeTake*60];
    }
    [self refreshView];
}
-(void)setSwitchFunc:(NSInteger)switchFunc
{
    if (_switchFunc == switchFunc) {
        return;
    }
    _switchFunc = switchFunc;
    [self refreshView];
}
-(void)setSelectedDate:(NSDate *)selectedDate
{
    if (_selectedDate == selectedDate) {
        return;
    }
    _selectedDate = selectedDate;
    [self refreshView];
    
}
- (void)setDishWasherButtonActive:(BOOL)active
{
    self.enabled = active;
    [self refreshView];
}
-(void)refreshView
{
    if (self.enabled == NO) {
        [self setupDefualtView];
        return;
    }
    if (!self.plantActSwitch) {
        [self setupDefualtView];
        return;
    }
    if (self.isCustom&&(self.switchFunc>=9&&self.switchFunc<=19)) {
        [self setupPlanView];
    }else if (!self.isCustom&&(self.switchFunc>=1&&self.switchFunc<9)) {
        [self setupPlanView];
    }else {
        [self setupView];
    }
}
-(void)setupDefualtView
{
    
    [self setTitle:@"预约" forState:UIControlStateNormal];
    self.titleLabel.textAlignment = NSTextAlignmentCenter;
    self.titleLabel.lineBreakMode = NSLineBreakByWordWrapping;
    [self.titleLabel setFont:UIFontAdjust(36.f)];
    [self setBackgroundImage:[[UIImage imageNamed:@"ic_shortbtn_unactive"] resizableImageWithCapInsets:UIEdgeInsetsMake(5, 30, 5, 30) resizingMode:UIImageResizingModeStretch] forState:UIControlStateNormal];
    [self setBackgroundImage:[[UIImage imageNamed:@"ic_shortbtn_unactive"] resizableImageWithCapInsets:UIEdgeInsetsMake(5, 30, 5, 30) resizingMode:UIImageResizingModeStretch] forState:UIControlStateDisabled];
    [self setTitleColor:[UIColor colorWithHexString:@"818286"] forState:UIControlStateNormal];
    [self setTitleColor:[UIColor colorWithHexString:@"818286"] forState:UIControlStateDisabled];
}
-(void)setupView
{
    self.titleLabel.lineBreakMode = NSLineBreakByWordWrapping;
    [self setTitle:@"已预约" forState:UIControlStateNormal];
    self.titleLabel.textAlignment = NSTextAlignmentCenter;
    [self.titleLabel setFont:UIFontAdjust(36.f)];
    [self setBackgroundImage:[[UIImage imageNamed:@"ic_shortbtn_unactive"] resizableImageWithCapInsets:UIEdgeInsetsMake(5, 30, 5, 30) resizingMode:UIImageResizingModeStretch] forState:UIControlStateNormal];
    [self setTitleColor:[UIColor colorWithHexString:@"818286"] forState:UIControlStateNormal];
}
-(void)setupPlanView
{
    if (!self.selectedDate) {
        return;
    }
    NSString *timeString = [self.dateFormatter stringFromDate:self.selectedDate];
    timeString = [timeString stringByReplacingOccurrencesOfString:@" " withString:@""];
    NSString *title = [NSString stringWithFormat:@"预约\n%@%@", (_timeType == 0)?@"结束":@"开始", timeString];
    [self setBackgroundImage:[[UIImage imageNamed:@"ic_shortbtn_active"] resizableImageWithCapInsets:UIEdgeInsetsMake(5, 30, 5, 30) resizingMode:UIImageResizingModeStretch] forState:UIControlStateNormal];
    [self setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [self.titleLabel setFont:UIFontAdjust(24.f)];
    [self setTitle:title forState:UIControlStateNormal];
    
}
-(NSDateFormatter*)dateFormatter
{
    if (!_dateFormatter) {
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:@"K:mm a"];
        dateFormatter.locale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US"];
        _dateFormatter = dateFormatter;
    }
    return _dateFormatter;
}


/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
