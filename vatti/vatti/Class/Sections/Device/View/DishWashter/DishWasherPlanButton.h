//
//  DishWasherPlanButton.h
//  vatti
//
//  Created by dongxiao on 2018/9/26.
//  Copyright © 2018年 xlink. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DishWasherButton.h"
@interface DishWasherPlanButton : DishWasherButton
@property(nonatomic,strong)NSDate *selectedDate;
@property(nonatomic,assign)NSInteger orderTime;
@property(nonatomic,assign)BOOL plantActSwitch;
@property(nonatomic,assign)NSInteger switchFunc;
@property(nonatomic,assign)BOOL isCustom;
@property(nonatomic,assign)NSInteger timeType;
@end
