//
//  CycleScrollViewModel.m
//  vatti
//
//  Created by dongxiao on 2018/8/13.
//  Copyright © 2018年 xlink. All rights reserved.
//

#import "CycleScrollViewModel.h"

@interface CycleScrollViewModel ()

@property (nonnull, strong) NSArray *activeImageNameArr;
@property (nonnull, strong) NSArray *unactiveImageNameArr;
@property (nonnull, strong) NSArray *mainTitleStrArr;
@property (nonnull, strong) NSArray *subTitleStrArr;
@property (nonnull, strong) NSArray *disableBtnNumArr;

@end

@implementation CycleScrollViewModel

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.activeImageNameArr = @[@"ic_zhinengxi_active",
                                    @"ic_qianglixi_active",
                                    @"ic_biaozhunxi_active",
                                    @"ic_kuaisuxi_active",
                                    @"ic_qingrongxi_active",
                                    @"ic_lengchongxi_active",
                                    @"ic_repiaoxi_active",
                                    @"ic_eco_active"];
        
        self.unactiveImageNameArr = @[@"ic_zhinengxi",
                                      @"ic_qianglixi",
                                      @"ic_biaozhunxi",
                                      @"ic_kuaisuxi",
                                      @"ic_qingrongxi",
                                      @"ic_lengchongxi",
                                      @"ic_repiaoxi",
                                      @"ic_eco"];
        
        self.mainTitleStrArr = @[@"智能洗",
                                 @"强力洗",
                                 @"标准洗",
                                 @"快速洗",
                                 @"轻柔洗",
                                 @"冷冲洗",
                                 @"热漂洗",
                                 @"ECO"];
        
        self.subTitleStrArr = @[@"",
                                 @"时间:120min|能耗:1.2kW/h|用水:13.5L",
                                 @"时间:90min|能耗:0.95kW/h|用水:11L",
                                 @"时间:30min|能耗:0.65kW/h|用水:8.5L",
                                 @"时间:80min|能耗:0.7kW/h|用水:11L",
                                 @"时间:20min|能耗:0.015kW/h|用水:8L",
                                 @"时间:40min|能耗:0.55kW/h|用水:3.5L",
                                 @"时间:180min|能耗:0.72kW/h|用水:9.5L"];
        
        self.disableBtnNumArr = @[@[@3, @5],
                                  @[],
                                  @[],
                                  @[@3, @5],
                                  @[@5],
                                  @[@0, @1, @2, @3, @4, @5, @6],
                                  @[@0, @1, @2, @3, @4, @5, @6],
                                  @[@3, @5]];
    }
    return self;
}

+ (instancetype)shareInstance {
    static CycleScrollViewModel *instance;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        instance = [[CycleScrollViewModel alloc] init];
    });
    return instance;
}

- (NSArray *)getActiveArr
{
    NSMutableArray *arr = [NSMutableArray array];
    for(NSInteger i = 0; i < 8; i++)
    {
        [arr addObject:[NSDictionary dictionaryWithObjectsAndKeys:self.activeImageNameArr[i], @"image", self.mainTitleStrArr[i], @"mainTitle", self.subTitleStrArr[i], @"subTitle", nil]];
    }
    return arr;
}

- (NSArray *)getUnactiveArr
{
    NSMutableArray *arr = [NSMutableArray array];
    for(NSInteger i = 0; i < 8; i++)
    {
        [arr addObject:[NSDictionary dictionaryWithObjectsAndKeys:self.unactiveImageNameArr[i], @"image", self.mainTitleStrArr[i], @"mainTitle", self.subTitleStrArr[i], @"subTitle", nil]];
    }
    return arr;
}

- (NSArray *)getDisableArr
{
    return self.disableBtnNumArr;
}
@end
