//
//  SDCollectionViewCell.m
//  SDCycleScrollView
//
//  Created by aier on 15-3-22.
//  Copyright (c) 2015年 GSD. All rights reserved.
//


/*
 
 *********************************************************************************
 *
 * 🌟🌟🌟 新建SDCycleScrollView交流QQ群：185534916 🌟🌟🌟
 *
 * 在您使用此自动轮播库的过程中如果出现bug请及时以以下任意一种方式联系我们，我们会及时修复bug并
 * 帮您解决问题。
 * 新浪微博:GSD_iOS
 * Email : gsdios@126.com
 * GitHub: https://github.com/gsdios
 *
 * 另（我的自动布局库SDAutoLayout）：
 *  一行代码搞定自动布局！支持Cell和Tableview高度自适应，Label和ScrollView内容自适应，致力于
 *  做最简单易用的AutoLayout库。
 * 视频教程：http://www.letv.com/ptv/vplay/24038772.html
 * 用法示例：https://github.com/gsdios/SDAutoLayout/blob/master/README.md
 * GitHub：https://github.com/gsdios/SDAutoLayout
 *********************************************************************************
 
 */


#import "SDCollectionViewCell.h"
#import "UIView+SDExtension.h"


@implementation SDCollectionViewCell
{
    __weak UILabel *_titleLabel;
    __weak UILabel *_mainLabel;
    __weak UILabel *_subLabel;
}


- (instancetype)initWithFrame:(CGRect)frame
{
    if (self = [super initWithFrame:frame]) {
        [self setupImageView];
        [self setupTitleLabel];
        [self setupContentLabel];
    }
    
    return self;
}

- (void)setTitleLabelBackgroundColor:(UIColor *)titleLabelBackgroundColor
{
    _titleLabelBackgroundColor = titleLabelBackgroundColor;
    _titleLabel.backgroundColor = titleLabelBackgroundColor;
}

- (void)setTitleLabelTextColor:(UIColor *)titleLabelTextColor
{
    _titleLabelTextColor = titleLabelTextColor;
    _titleLabel.textColor = titleLabelTextColor;
}

- (void)setTitleLabelTextFont:(UIFont *)titleLabelTextFont
{
    _titleLabelTextFont = titleLabelTextFont;
    _titleLabel.font = titleLabelTextFont;
}

- (void)setupImageView
{
    UIImageView *imageView = [[UIImageView alloc] init];
    imageView.layer.cornerRadius = 5;
    imageView.layer.masksToBounds = YES;
    _imageView = imageView;
    [self.contentView addSubview:imageView];
}

- (void)setupTitleLabel
{
    UILabel *titleLabel = [[UILabel alloc] init];
    _titleLabel = titleLabel;
    _titleLabel.hidden = YES;
    [self.contentView addSubview:titleLabel];

}

- (void)setupContentLabel
{
    UILabel *mainLabel = [[UILabel alloc] initWithFrame:CGRectMake(0.f, CGRectGetHeight(self.contentView.frame) / 4.0, CGRectGetWidth(self.contentView.frame), CGRectGetHeight(self.contentView.frame) / 4.0)];
    _mainLabel = mainLabel;
    [self.contentView addSubview:_mainLabel];
    _mainLabel.textAlignment = NSTextAlignmentCenter;
    [_mainLabel setTextColor:[UIColor whiteColor]];
    [_mainLabel setFont:[UIFont systemFontOfSize:30]];
    _mainLabel.center = CGPointMake(CGRectGetWidth(self.contentView.frame) / 2, _mainLabel.center.y);

    UILabel *subLabel = [[UILabel alloc] initWithFrame:CGRectMake(0.f, 0.f, CGRectGetWidth(self.contentView.frame), CGRectGetHeight(self.contentView.frame) / 4.0)];
    _subLabel = subLabel;
    [self.contentView addSubview:_subLabel];
    _subLabel.textAlignment = NSTextAlignmentCenter;
    [_subLabel setFont:[UIFont systemFontOfSize:11]];
    [_subLabel setTextColor:[UIColor whiteColor]];
    _subLabel.center = CGPointMake(CGRectGetWidth(self.contentView.frame) / 2, CGRectGetHeight(self.contentView.frame) / 3.0 * 2);
}

- (void)setTitle:(NSString *)title
{
    _title = [title copy];
    _titleLabel.text = [NSString stringWithFormat:@"   %@", title];
    if (_titleLabel.hidden) {
        _titleLabel.hidden = NO;
    }
}

- (void)setMainLabelStr:(NSString *)mainLabelStr
{
    _mainLabelStr = [mainLabelStr copy];
    _mainLabel.text = mainLabelStr;
}

- (void)setSubLabelStr:(NSString *)subLabelStr
{
    _subLabelStr = [subLabelStr copy];
    _subLabel.text = subLabelStr;
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    
    if (self.onlyDisplayText) {
        _titleLabel.frame = self.bounds;
    } else {
        if (!self.zoomType) {
            _imageView.frame = self.bounds;
        }

        CGFloat titleLabelW = self.sd_width;
        CGFloat titleLabelH = _titleLabelHeight;
        CGFloat titleLabelX = 0;
        CGFloat titleLabelY = self.sd_height - titleLabelH;
        _titleLabel.frame = CGRectMake(titleLabelX, titleLabelY, titleLabelW, titleLabelH);
    }
}

@end
