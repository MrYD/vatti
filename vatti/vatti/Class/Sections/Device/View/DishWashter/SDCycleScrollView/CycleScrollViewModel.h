//
//  CycleScrollViewModel.h
//  vatti
//
//  Created by dongxiao on 2018/8/13.
//  Copyright © 2018年 xlink. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CycleScrollViewModel : NSObject

+ (instancetype)shareInstance;

- (NSArray *)getActiveArr;
- (NSArray *)getUnactiveArr;
- (NSArray *)getDisableArr;
@end
