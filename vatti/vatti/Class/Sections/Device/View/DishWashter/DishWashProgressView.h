//
//  DishWashProgressView.h
//  vatti
//
//  Created by dongxiao on 2018/9/21.
//  Copyright © 2018年 xlink. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DishWashProgressView : UIView
@property(nonatomic,assign)BOOL disabled;
@property(nonatomic,assign)int func_step;
@property(nonatomic,assign)int switch_func;
@end
