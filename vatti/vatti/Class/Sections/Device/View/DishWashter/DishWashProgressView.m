//
//  DishWashProgressView.m
//  vatti
//
//  Created by dongxiao on 2018/9/21.
//  Copyright © 2018年 xlink. All rights reserved.
//

#import "DishWashProgressView.h"
#import "UIColor+XLCategory.h"
#define kCourseTag  1000
#define kLineTag    2000
#define kTitleTag   3000
typedef NS_OPTIONS(NSUInteger, DishWasherOption) {
    DishWasherPrewash = 1<<0,
    DishWasherMianwash = 1<<1,
    DishWasherRinse = 1<<2,
    DishWasherDry = 1<<3,
};
@interface DishWashProgressView()

@property(nonatomic,assign)DishWasherOption options;

@end
@implementation DishWashProgressView
-(void)setFunc_step:(int)func_step
{
    if (_disabled) {
        func_step = 0;
    }
    if (_func_step==func_step) {
        return;
    }
    _func_step = func_step;
    for (int i = 0; i < 4; i++) {
        DishWasherOption option = DishWasherPrewash<<i;
        if (self.options&option) {
            UIImageView *course = (UIImageView *)[self viewWithTag:kCourseTag+option];
            UIImageView *line = (UIImageView *)[self viewWithTag:kLineTag+option];
            UILabel *title = (UILabel *)[self viewWithTag:kTitleTag+option];
            title.font = [UIFont systemFontOfSize:14];
            title.textColor = [UIColor colorWithHexString:@"818286"];
            
            if (i<self.func_step) {
                [course setImage:[UIImage imageNamed:@"ic_cource_active"]];
                [line setImage:[UIImage imageNamed:@"line_active"]];
                
            }else {
                [course setImage:[UIImage imageNamed:@"course_unactive"]];
                [line setImage:[UIImage imageNamed:@"line_unactive"]];
            }
            if (i+1==func_step) {
                title.font = [UIFont systemFontOfSize:18];
                title.textColor = [UIColor colorWithHexString:@"323345"];
            }
        }
    }
}

-(id)init
{
    self = [super init];
    return self;
}
-(void)setSwitch_func:(int)switch_func
{
    if (_switch_func == switch_func) {
        return;
    }
    //1.智能 2.强力 3.标准 4.快速 5.轻柔 6.冷冲 7.热漂 8.ECO 9.自定义（参考自定义程序）
    _switch_func = switch_func;
    DishWasherOption option;
    switch (switch_func) {
        case 4:
            option = DishWasherMianwash|DishWasherRinse;
        case 6:
            option = DishWasherPrewash|DishWasherRinse;
            break;
        case 7:
            option = DishWasherMianwash|DishWasherRinse|DishWasherDry;
            break;
        case 5:
            option = DishWasherMianwash|DishWasherRinse|DishWasherDry;
            break;
        default:
            option = 0;
            if (switch_func>=1 && switch_func<=8) {
                option = DishWasherPrewash|DishWasherMianwash|DishWasherRinse|DishWasherDry;
            }
            break;
    }
    self.options = option;
}
-(void)setOptions:(DishWasherOption)options
{
    if (_options == options) {
        return;
    }
    _options = options;
    [self setupSubview];
}
-(void)setDisabled:(BOOL)disabled
{
    if (_disabled == disabled) {
        return;
    }
    _disabled = disabled;
    if (disabled) {
        self.func_step = 0;
    }
}
-(void)setupSubview
{
    NSArray *titleArray = @[@"预洗", @"主洗", @"漂洗", @"烘干"];
    for (UIView *view in self.subviews) {
        [view removeFromSuperview];
    }
    NSMutableArray *courseArray =[NSMutableArray arrayWithCapacity:0];
    NSMutableArray *lineArray =[NSMutableArray arrayWithCapacity:0];
    NSMutableArray *titleViewArray =[NSMutableArray arrayWithCapacity:0];
    for (int i = 0; i < 4; i++) {
        DishWasherOption option = DishWasherPrewash<<i;
        if (self.options&option) {
            UIImageView *coure = [[UIImageView alloc] init];
            coure.tag = kCourseTag + option;
            [self addSubview:coure];
            [courseArray addObject:coure];
            UIImageView *line = nil;
            if (courseArray.count >= 2) { //必须有两个以上步骤才能显示连接线
                line = [[UIImageView alloc] init];
                line.tag = kLineTag + option;
                [self addSubview:line];
                [lineArray addObject:line];
            }
            UILabel *title = [[UILabel alloc] init];
            title.tag = kTitleTag + option;
            title.text = [titleArray objectAtIndex:i];
            title.textAlignment = NSTextAlignmentCenter;
            [self addSubview:title];
            [titleViewArray addObject:title];
            if (i<self.func_step) {
                [coure setImage:[UIImage imageNamed:@"ic_cource_active"]];
                [line setImage:[UIImage imageNamed:@"line_active"]];
                title.font = [UIFont systemFontOfSize:18];
                title.textColor = [UIColor colorWithHexString:@"323345"];
                
            }else {
                [coure setImage:[UIImage imageNamed:@"course_unactive"]];
                [line setImage:[UIImage imageNamed:@"line_unactive"]];
                title.font = [UIFont systemFontOfSize:14];
                title.textColor = [UIColor colorWithHexString:@"818286"];
            }
        }
    }
    if (courseArray.count == 1) {
        [courseArray mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerX.top.equalTo(self);
            make.width.height.equalTo(@20);
        }];
        
        UIView *view = courseArray.firstObject;
        [titleViewArray mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(view).offset(30);
            make.width.height.equalTo(@50);
            make.centerX.equalTo(self);
        }];
    }else if (courseArray.count > 1) {
        CGFloat contentLength = ([UIScreen mainScreen].bounds.size.width-68)/4*courseArray.count;
        CGFloat spacing = ([UIScreen mainScreen].bounds.size.width - contentLength)/2;
        [courseArray mas_distributeViewsAlongAxis:MASAxisTypeHorizontal withFixedItemLength:20 leadSpacing:spacing tailSpacing:spacing];
        [courseArray mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(self);
            make.height.equalTo(@20);
        }];
        if (lineArray.count>1) {
            [lineArray mas_distributeViewsAlongAxis:MASAxisTypeHorizontal withFixedSpacing:20 leadSpacing:spacing + 20 tailSpacing:spacing + 20 ];
            [lineArray mas_makeConstraints:^(MASConstraintMaker *make) {
                make.top.equalTo(self).offset(10);
                make.height.equalTo(@1);
            }];
        }else {
            [lineArray mas_makeConstraints:^(MASConstraintMaker *make) {
                make.centerX.equalTo(self);
                make.top.equalTo(self).offset(10);
                make.height.equalTo(@1);
                make.right.equalTo(self).offset(-spacing - 20);
                make.right.equalTo(self).offset(spacing + 20);
            }];
        }
        
        
        [titleViewArray mas_distributeViewsAlongAxis:MASAxisTypeHorizontal withFixedItemLength:50 leadSpacing:(spacing - 15) tailSpacing:(spacing - 15)];
        [titleViewArray mas_makeConstraints:^(MASConstraintMaker *make) {
            UIImageView *view = (UIImageView *)[courseArray objectAtIndex:0];
            make.top.equalTo(view).offset(30);
            make.height.equalTo(@50);
        }];
    }
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
