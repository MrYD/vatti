//
//  DishWasherTools.h
//  vatti
//
//  Created by dongxiao on 2018/9/26.
//  Copyright © 2018年 xlink. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "DishWasherDeviceModel.h"

typedef NS_ENUM(NSUInteger, DishWasherDataPoint) {//端点ID
    /*!电控板厂家生产编码 */
    DishWasherDataPoint_manufacture_id          = 0,//电控板厂家生产编码
    /*!软件版本号 */
    DishWasherDataPoint_sw_ver                  = 1,//软件版本号
    /*!硬件版本号 */
    DishWasherDataPoint_hw_ver                  = 2,//硬件版本号
    /*!水阀 */
    DishWasherDataPoint_control_id              = 3,//水阀
    /*!权限 */
    DishWasherDataPoint_priority                = 4,//权限
    /*!运行命令 */
    DishWasherDataPoint_control_cmd             = 5,//运行命令
    /*!功能选择 */
    DishWasherDataPoint_switch_func             = 6,//功能选择
    /*!暂时不用 */
    DishWasherDataPoint_func_time               = 7,//暂时不用
    /*!功能模式 */
    DishWasherDataPoint_func_mode               = 8,//功能模式
    /*!软水档位 */
    DishWasherDataPoint_water_gears             = 9,//软水档位
    /*!设置 */
    DishWasherDataPoint_act_switch              = 10,//设置
    /*!预约时间 */
    DishWasherDataPoint_order_time              = 11,//预约时间
    /*!设备状态 */
    DishWasherDataPoint_dev_statu               = 12,//设备状态
    /*!BIT0:门状态：门开/门关 值0：门开 值1：门关 BIT1:专用盐状态：（备用） 值0：缺失 值1：充足 BIT2:亮碟剂状态：（备用） 值0：缺失 值1：充足 */
    DishWasherDataPoint_running_flag            = 13,//
    /*!程序运行当前步骤 */
    DishWasherDataPoint_func_step               = 14,//程序运行当前步骤
    /*!程序剩余时间 */
    DishWasherDataPoint_Remaining_Time          = 15,//程序剩余时间
    /*!当前温度 */
    DishWasherDataPoint_curr_temp               = 16,//当前温度
    /*!程序设定温度 */
    DishWasherDataPoint_func_temp               = 17,//程序设定温度
    /*!温度故障 */
    DishWasherDataPoint_err_code                = 18,//温度故障
    /*!上一次运行的程序 */
    DishWasherDataPoint_last_func               = 19,//上一次运行的程序
    /*!当前app用户id */
    DishWasherDataPoint_user_id                 = 20,//当前app用户id
};

//Bit 位
typedef NS_ENUM(NSUInteger, DishWasherControlCmdBit) {
    /*!BIT0:运行命令：上电 */
    DishWasherControlCmdBitPowerOn      = 0,//上电
    /*!BIT1:运行命令：关电 */
    DishWasherControlCmdBitPowerOff     = 1,//关电
    /*!BIT2:运行命令：运行 */
    DishWasherControlCmdBitRun          = 2,//运行
    /*!BIT3:运行命令：暂停 */
    DishWasherControlCmdBitPause        = 3,//暂停
    /*!BIT4:运行命令：停止 */
    DishWasherControlCmdBitStop         = 4,//停止
    /*!BIT5:门动作命令：开门 */
    DishWasherControlCmdBitOpenDoor     = 5,//开门
    /*!BIT6:门动作命令：关门 */
    DishWasherControlCmdBitCloseDoor    = 6,//关门
};

typedef NS_ENUM(NSUInteger, DishWasherSwitchFunc) {
    /*!无效 */
    DishWasherSwitchFuncUnknown         = 0,
    /*!1：智能 */
    DishWasherSwitchFuncIntelligence    = 1,//智能
    /*!2：强力 */
    DishWasherSwitchFuncStrength        = 2,//强力
    /*!3：标准 */
    DishWasherSwitchFuncStandard        = 3,//标准
    /*!4：快速 */
    DishWasherSwitchFuncQuick           = 4,//快速
    /*!5：轻柔 */
    DishWasherSwitchFuncSoft            = 5,//轻柔
    /*!6：冷冲 */
    DishWasherSwitchFuncColdRinse       = 6,//冷冲
    /*!7：热漂 */
    DishWasherSwitchFuncHotRinse        = 7,//热漂
    /*!8：ECO */
    DishWasherSwitchFuncECO             = 8,//ECO
    //自定义模式
    /*!9：20分钟程序 */
    DishWasherSwitchFuncTwenty          = 9,//20分钟程序
    /*!10：30分钟程序 */
    DishWasherSwitchFuncThirty          = 10,//30分钟程序
    /*!11：40分钟程序 */
    DishWasherSwitchFuncForty           = 11,//40分钟程序
    /*!12：50分钟程序 */
    DishWasherSwitchFuncFifty           = 12,//50分钟程序
    /*!13：60分钟程序 */
    DishWasherSwitchFuncSixty           = 13,//60分钟程序
    /*!14：70分钟程序 */
    DishWasherSwitchFuncSeventy         = 14,//70分钟程序
    /*!15：80分钟程序 */
    DishWasherSwitchFuncEighty          = 15,//80分钟程序
    /*!16：90分钟程序 */
    DishWasherSwitchFuncNinety          = 16,//90分钟程序
    /*!17：100分钟程序 */
    DishWasherSwitchFuncHundred         = 17,//100分钟程序
    /*!18：110分钟程序 */
    DishWasherSwitchFuncHundredTen      = 18,//110分钟程序
    /*!19：120分钟程序 */
    DishWasherSwitchFuncHundredTwenty   = 19,//120分钟程序
};

//Bit 位
typedef NS_ENUM(NSUInteger, DishWasherFuncModeBit) {
    /*!BIT0:增强消毒 */
    DishWasherFuncModeEnhancedDisinfection  = 0,//增强消毒
    /*!BIT1:加漂洗 */
    DishWasherFuncModeAddRinse              = 1,//加漂洗
    /*!BIT2:无水干燥 */
    DishWasherFuncModeAnhydrousDry          = 2,//无水干燥
    /*!BIT3:夜间洗 */
    DishWasherFuncModeNocturnalWash         = 3,//夜间洗
    /*!BIT4:少量洗 */
    DishWasherFuncModeSmallAmountWash       = 4,//少量洗
    /*!BIT5:加速省时 */
    DishWasherFuncModeTimeSaving            = 5,//加速省时
    /*!BIT6:增压 */
    DishWasherFuncModePressurize            = 6,//增压

};

//Bit 位
typedef NS_ENUM(NSUInteger, DishWasherActSwitchBit) {
    /*!BIT0:自动开门功能关闭/开启 值0：关闭 值1：开启 */
    DishWasherActSwitchAutonOpenDoor        = 0,//自动开门功能
    /*!BIT1:自动关机功能关闭/开启 值0：关闭 值1：开启 */
    DishWasherActSwitchAutoShutDown         = 1,//自动关机功能
    /*!BIT2:灯光关闭/开启 值0：关闭 值1：开启 */
    DishWasherActSwitchLightSetting         = 2,//灯光
    /*!BIT3:结束提示音（滴滴声） 值0：关闭 值1：开启 */
    DishWasherActSwitchWarningTone          = 3,//结束提示音
    /*!BIT4:预约模式关闭/开启 值0：关闭 值1：开启 */
    DishWasherActSwitchPlanMode             = 4,//预约模式
    
};

typedef NS_ENUM(NSUInteger, DishWasherDeviceStatue) {
    /*!值1：休眠(插入电源线，屏幕不亮状态) */
    DishWasherDeviceStatueHibernation       = 1,//休眠
    /*!值2：待机(按下电源键，屏幕点亮状态) */
    DishWasherDeviceStatueStandby           = 2,//待机
    /*!值3：启动(按下运行键，程序运行状态) */
    DishWasherDeviceStatueStart             = 3,//启动
    /*!值4：暂停(程序运行时，按下暂停键，程序暂停状态) */
    DishWasherDeviceStatuePlanPlan          = 4,//暂停
};

typedef NS_ENUM(NSUInteger, DishWasherFuncStep) {
    /*!1.预洗 */
    DishWasherFuncStepPreWashing            = 1,//预洗
    /*!2.主洗 */
    DishWasherFuncStepMainWashing           = 2,//主洗
    /*!3.漂洗 */
    DishWasherFuncStepRinsing               = 3,//漂洗
    /*!4.干燥 */
    DishWasherFuncStepDrying                = 4,//干燥
};
@interface DishWasherTools : NSObject

+ (NSInteger)getTimeInterValueWithSwitchFunc:(NSInteger)switchFunc;

/**
 通过洗碗机switchFunc参数获取

 @param switchFunc 洗碗机洗碗模式
 
 @code
 1：智能 2：强力 3：标准 4：快速 5：轻柔
 6：冷冲 7：热漂 8：ECO 9：自定义 ...ect
 @endcode
 
 @return 返回洗碗机当前洗碗模式名称
 */
+ (NSString *)getStringWithSwitchFunc:(NSInteger)switchFunc;

/**
 通过洗碗机remainingTime参数获取剩余显示时间字符串

 @param remainingTime 洗碗机洗碗程序剩余时间
 
 @code
 remainingTime 32位 高位表示小时 低位表示分钟秒钟
 @endcode
 
 @return 返回用于显示的剩余时间字符串HH:mm
 */
+ (NSString *)getMMSSFromSS:(uint32_t)remainingTime;

/**
 通过洗碗机orderTime参数获取距离洗涤开始剩余显示时间字符串

 @param orderTime 洗碗机洗碗程序预约的时间
 @return 返回用于显示距离洗涤开始的剩余时间字符串HH:mm
 */
+(NSString *)getMMSSFromOrderTime:(uint32_t)orderTime;
/**
 通过洗碗机switch_func参数判断是否是自定义模式

 @param switch_func 洗碗机洗碗模式
 
 @code
 1~8 系统模式 9~19自定义模式
 @endcode
 
 @return 返回是否是自定义模式
 */
+ (BOOL)isCustomFuncWithSwitchFunc:(uint8_t)switch_func;

/**
 通过洗碗机act_switch参数判断预约功能是否开启

 @param act_switch 功能设置
 
 @code
 BIT4:预约模式 值0：关闭 值1：开启
 @endcode

 @return 返回预约是否开启
 */
+ (BOOL)isOpenPlanedWithActSwitch:(uint8_t)act_switch;

/**
 通过洗碗机deviceModel中switch_func和act_switch参数判断预约功能是否开启

 @param deviceModel 洗碗机数据Model
 
 @return 返回是否开启自定义模式
 */
+ (BOOL)isCustomPlanedWithDevice:(DishWasherDeviceModel*)deviceModel;
@end
