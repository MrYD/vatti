//
//  DishWasherTools.m
//  vatti
//
//  Created by dongxiao on 2018/9/26.
//  Copyright © 2018年 xlink. All rights reserved.
//

#import "DishWasherTools.h"
#import "XLDataPointTool.h"

@implementation DishWasherTools
+ (NSInteger)getTimeInterValueWithSwitchFunc:(NSInteger)switchFunc
{
    
    NSInteger minute;
    switch (switchFunc) {
        case 1:
            minute = 60;
            break;
        case 2:
            minute = 120;
            break;
        case 3:
            minute = 90;
            break;
        case 4:
            minute = 30;
            break;
        case 5:
            minute = 80;
            break;
        case 6:
            minute = 20;
            break;
        case 7:
            minute = 40;
            break;
        case 8:
            minute = 180;
            break;
        case 9:
            minute = 20;
            break;
        case 10:
            minute = 30;
            break;
        case 11:
            minute = 40;
            break;
        case 12:
            minute = 50;
            break;
        case 13:
            minute = 60;
            break;
        case 14:
            minute = 70;
            break;
        case 15:
            minute = 80;
            break;
        case 16:
            minute = 90;
            break;
        case 17:
            minute = 100;
            break;
        case 18:
            minute = 110;
            break;
        case 19:
            minute = 120;
            break;
        default:
            minute = 0;
            break;
    }
    
    return minute;
    
}
+ (NSString *)getStringWithSwitchFunc:(NSInteger)switchFunc
{
    
    NSString *string = @"";
    switch (switchFunc) {
        case 1:
            string = @"智能";
            break;
        case 2:
            string = @"强力";
            break;
        case 3:
            string = @"标准";
            break;
        case 4:
            string = @"快速";
            break;
        case 5:
            string = @"轻柔";
            break;
        case 6:
            string = @"冷冲";
            break;
        case 7:
            string = @"热漂";
            break;
        case 8:
            string = @"ECO";
            break;
        default:
            if (switchFunc>=9&&switchFunc<=19) {
                string = @"自定义";
            }
            break;
    }
    return string;
    
}
+ (NSDate *)zeroOfTodayTime
{
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSDateComponents *components = [calendar components:NSUIntegerMax fromDate:[NSDate date]];
    components.hour = 0;
    components.minute = 0;
    components.second = 0;
    
    // components.nanosecond = 0 not available in iOS
    NSTimeInterval ts = [[calendar dateFromComponents:components] timeIntervalSince1970];
    return [NSDate dateWithTimeIntervalSince1970:ts];
}

// 计算今天现在的时间
+ (NSInteger)sinceTodayTime
{
    NSTimeInterval timeValue = [[NSDate date] timeIntervalSinceDate:[self zeroOfTodayTime]];
    return timeValue;
}
+(NSString *)getMMSSFromSS:(uint32_t)remainingTime
{
    /*
    uint8_t hours = remainingTime>>16;
    uint8_t minutes = (remainingTime>>8)&0xFF;
    uint8_t seconds = remainingTime&0xFF;
    if (seconds != 0) {
        minutes+=1;
    }
     */
    //板子新计算方法
    NSInteger totalSeconds = ((remainingTime>>16) & 0xFF)*3600 +((remainingTime>>8)&0xFF)*60+(remainingTime&0xFF);
    totalSeconds = totalSeconds%60==0 ? totalSeconds:(totalSeconds/60+1)*60;
    uint8_t totalMinutes = totalSeconds/60;
    uint8_t hours = totalMinutes/60;
    uint8_t minutes = totalMinutes%60;
    NSString *formatTime = [NSString stringWithFormat:@"%.2u:%.2u",hours,minutes];
    return formatTime;
}
+(NSString *)getMMSSFromOrderTime:(uint32_t)orderTime
{
    NSInteger totalMinutes = ((orderTime >> 8) & 0xff)*60 + (orderTime & 0xff);
    NSInteger hours = totalMinutes/60;
    NSInteger minutes = totalMinutes%60;
    NSString *formatTime = [NSString stringWithFormat:@"%.2zd:%.2zd",hours,minutes];
    return formatTime;
}
+ (BOOL)isCustomFuncWithSwitchFunc:(uint8_t)switch_func
{
    return (switch_func >= DishWasherSwitchFuncTwenty && switch_func <= DishWasherSwitchFuncHundredTwenty);
}
+ (BOOL)isOpenPlanedWithActSwitch:(uint8_t)act_switch
{
    return [XLDataPointTool isOpenWithValue:act_switch andShift:DishWasherActSwitchPlanMode];
}
+ (BOOL)isCustomPlanedWithDevice:(DishWasherDeviceModel*)deviceModel
{
    return [self isCustomFuncWithSwitchFunc:deviceModel.switch_func]&&[self isOpenPlanedWithActSwitch:deviceModel.act_switch];
}
@end
