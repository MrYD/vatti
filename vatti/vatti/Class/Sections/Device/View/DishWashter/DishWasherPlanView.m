//
//  DishWasherPlanView.m
//  vatti
//
//  Created by dongxiao on 2018/8/15.
//  Copyright © 2018年 xlink. All rights reserved.
//

#import "DishWasherPlanView.h"
#import "DishWasherConstants.h"

@interface DishWasherPlanView ()  <UIPickerViewDataSource, UIPickerViewDelegate>

@property (nonatomic, strong) UIPickerView *datePicker;
@property (nonatomic, assign) NSUInteger timeType;
@property (nonatomic, assign) NSUInteger timeHour;
@property (nonatomic, assign) NSUInteger timeMin;
@property (nonatomic, assign) NSUInteger timeAM_PM;

@property (nonatomic, strong) UIButton *confirmBtn;
@property (nonatomic, strong) UIButton *cancelBtn;

@property (nonatomic, copy) EditPlanBlock editPlanBlock;
@property (nonatomic, copy) EditNewPlanBlock editNewPlanBlock;
@property (nonatomic, copy) CancelPlanBlock cancelPlanBlock;
@property (nonatomic, strong)DishWasherDeviceModel *deviceModel;
@end

@implementation DishWasherPlanView
    
- (instancetype)initPlanViewWithDeviceModel:(DishWasherDeviceModel *)model WithEditBlack:(EditPlanBlock)editPlanBlock withCancleBlock:(CancelPlanBlock)cancelPlanBlock WithTimeType:(NSInteger)dishwasherTimeType timeHour:(NSInteger)timeHour timeMin:(NSInteger)timeMin timeAM_PM:(NSInteger)timeAM_PM
{
    self = [super init];
    if (self) {
        _timeType = dishwasherTimeType;
        _timeHour = timeHour;
        _timeMin = timeMin;
        _timeAM_PM = timeAM_PM;
        _editPlanBlock = editPlanBlock;
        _cancelPlanBlock = cancelPlanBlock;
        _deviceModel = model;
    }
    
    return self;
}
- (instancetype)initPlanViewWithDeviceModel:(DishWasherDeviceModel *)model WithEditBlack:(EditNewPlanBlock)editNewPlanBlock withCancleBlock:(CancelPlanBlock)cancelPlanBlock
{
    self = [super init];
    if (self) {
        _editNewPlanBlock = editNewPlanBlock;
        _cancelPlanBlock = cancelPlanBlock;
        _deviceModel = model;
    }
    
    return self;
}
- (void)loadDishWasherPlanView
{
    self.backgroundColor = [UIColor clearColor];
    
    UIView *backgroundView = [[UIView alloc] init];
    backgroundView.backgroundColor = [UIColor whiteColor];
    //设置圆角边框
    backgroundView.layer.cornerRadius = 8;
    backgroundView.layer.masksToBounds = YES;
    //设置边框及边框颜色
    backgroundView.layer.borderWidth = 1;
    backgroundView.layer.borderColor =[ [UIColor lightGrayColor] CGColor];
    [self addSubview:backgroundView];
    [backgroundView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(UIAdjustNum(25.f));
        make.right.mas_equalTo(UIAdjustNum(-25.f));
        make.height.mas_equalTo(self.mas_height).multipliedBy(0.5f);
        make.bottom.mas_equalTo(0.f);
    }];
    
    self.cancelBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.cancelBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [self.cancelBtn.titleLabel setFont:UIFontAdjust(36.f)];
    [self.cancelBtn setBackgroundImage:[UIImage imageNamed:@"ic_shortbtn_active"] forState:UIControlStateNormal];
    [backgroundView addSubview:self.cancelBtn];
    
    self.confirmBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.confirmBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [self.confirmBtn.titleLabel setFont:UIFontAdjust(36.f)];
    [self.confirmBtn setBackgroundImage:[UIImage imageNamed:@"ic_shortbtn_active"] forState:UIControlStateNormal];
    [self.confirmBtn addTarget:self action:@selector(confirmBtnClicked) forControlEvents:UIControlEventTouchUpInside];
    [self.confirmBtn addTarget:self action:@selector(confirmBtnNewClicked) forControlEvents:UIControlEventTouchUpInside];
    [backgroundView addSubview:self.confirmBtn];
    
    [@[self.cancelBtn, self.confirmBtn] mas_distributeViewsAlongAxis:MASAxisTypeHorizontal withFixedSpacing:UIAdjustNum(50.f) leadSpacing:UIAdjustNum(25.f) tailSpacing:UIAdjustNum(25.f)];
    [@[self.cancelBtn, self.confirmBtn] mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.mas_equalTo(UIAdjustNum(-20.f));
        make.height.mas_equalTo(UIAdjustNum(90.f));
    }];
    
    self.datePicker = [[UIPickerView alloc] init];
    self.datePicker.backgroundColor = [UIColor whiteColor];
    self.datePicker.center = CGPointMake(CGRectGetWidth(self.frame) / 2, CGRectGetHeight(self.frame) / 2);
    self.datePicker.delegate = self;
    self.datePicker.dataSource = self;
    [self.datePicker selectRow:(12*25) inComponent:1 animated:NO];
    [self.datePicker selectRow:(60*10) inComponent:2 animated:NO];
    [backgroundView addSubview: self.datePicker];
    [self.datePicker mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(0);
        make.left.mas_equalTo(UIAdjustNum(5.f));
        make.right.mas_equalTo(UIAdjustNum(-5.f));
        make.bottom.mas_equalTo(self.confirmBtn.mas_top).mas_offset(UIAdjustNum(1.f));
    }];
    
    [self hiddenPlanView];
}

- (void)confirmBtnNewClicked
{
    @weakify(self)
//    [[RACObserve(self.deviceModel, running_flag).distinctUntilChanged deliverOnMainThread] subscribeNext:^(id  _Nullable x) {
//        @strongify(self)
        if (((self.deviceModel.running_flag & 1) == 0) && (self.deviceModel.dev_statu != 3)) {
            
            UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"提示" message:@"请关好门!" preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction *action1 = [UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                return ;
            }];
            
            [alert addAction:action1];
            [[UIApplication sharedApplication].keyWindow.rootViewController presentViewController:alert animated:YES completion:nil];
            return ;
            
        } else if ((self.deviceModel.running_flag & 1) == 1) {
            NSUInteger timeType = [self.datePicker selectedRowInComponent:0]==0?1:0;
            NSUInteger timeHour = [self.datePicker selectedRowInComponent:1]%24;
            NSUInteger timeMin = [self.datePicker selectedRowInComponent:2]%60;
//            NSUInteger timeAM_PM = [self.datePicker selectedRowInComponent:3];
            NSUInteger timeAM_PM = 0;
            NSDate *selectedDate = [self selectedDateWithTimeHour:timeHour timeMin:timeMin timeAMPM:timeAM_PM];
            if (_editNewPlanBlock) {
                _editNewPlanBlock(timeType,selectedDate);
            }
            
#if ENV == 1
#elif ENV == 2
#endif
            [self hiddenPlanView];
        }
//    }];

}
- (void)confirmBtnClicked
{
    @weakify(self)
//    [[RACObserve(self.deviceModel, running_flag).distinctUntilChanged deliverOnMainThread] subscribeNext:^(id  _Nullable x) {
//        @strongify(self)
        if (((self.deviceModel.running_flag & 1) == 0) && (self.deviceModel.dev_statu != 3)) {
            
            UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"提示" message:@"请关好门!" preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction *action1 = [UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                [[[UIApplication sharedApplication] keyWindow].rootViewController.navigationController dismissViewControllerAnimated:YES completion:nil];
                return ;
            }];
            
            [alert addAction:action1];
            [[UIApplication sharedApplication].keyWindow.rootViewController.navigationController presentViewController:alert animated:YES completion:nil];
            return;
            
        } else if ((self.deviceModel.running_flag & 1) == 1) {
            
            _timeType = [self.datePicker selectedRowInComponent:0];
            _timeType = ((_timeType == 0) ? 1 : 0);
            _timeHour = [self.datePicker selectedRowInComponent:1];
            _timeHour = _timeHour % 12;
            _timeMin = [self.datePicker selectedRowInComponent:2];
            _timeMin = _timeMin % 60;
            _timeAM_PM = [self.datePicker selectedRowInComponent:3];
            
            if (_editPlanBlock) {
                _editPlanBlock(_timeType, _timeHour, _timeMin, _timeAM_PM, [self sinceTodayTime]);
            }
            
#if ENV == 1
#elif ENV == 2
#endif
            [self hiddenPlanView];
        }
//    }];

}

- (void)cancelBtnClicked
{
    [self hiddenPlanView];
}

- (void)cancelPlanClicked
{
    [self hiddenPlanView];
    if (_cancelPlanBlock) {
        _cancelPlanBlock();
    }
}

- (void)hiddenPlanView
{
    self.hidden = YES;
}

- (void)showPlanView
{
    self.hidden = NO;
    
    [self.datePicker reloadAllComponents];
    [self.cancelBtn setTitle:@"取消" forState:UIControlStateNormal];
    [self.cancelBtn addTarget:self action:@selector(cancelBtnClicked) forControlEvents:UIControlEventTouchUpInside];
    [self.confirmBtn setTitle:@"确认" forState:UIControlStateNormal];
    
}
- (void)showEditPlanViewWithTimeType:(NSInteger)timeType selectedDate:(NSDate*)selectedDate
{
    if (selectedDate) {
        NSCalendar *calendar = [NSCalendar currentCalendar];
        NSDateComponents *components = [calendar components:NSUIntegerMax fromDate:selectedDate];
        _timeAM_PM = components.hour>11?1:0;
        _timeHour = components.hour - 12*_timeAM_PM;
        _timeMin = components.minute;
    }else {
        _timeAM_PM = 0;
        _timeHour = 0;
        _timeMin = 0;
    }
    _timeType = timeType;

    [self.datePicker reloadAllComponents];
    
    [self.cancelBtn setTitle:@"取消预约" forState:UIControlStateNormal];
    [self.cancelBtn addTarget:self action:@selector(cancelPlanClicked) forControlEvents:UIControlEventTouchUpInside];
    [self.confirmBtn setTitle:@"编辑预约" forState:UIControlStateNormal];
    
    self.hidden = NO;
}
- (void)showEditPlanViewWithTimeType:(NSInteger)dishwasherTimeType timeHour:(NSInteger)timeHour timeMin:(NSInteger)timeMin timeAM_PM:(NSInteger)timeAM_PM
{
    _timeType = dishwasherTimeType;
    _timeHour = timeHour;
    _timeMin = timeMin;
    _timeAM_PM = timeAM_PM;
    [self.datePicker reloadAllComponents];
    
    [self.cancelBtn setTitle:@"取消预约" forState:UIControlStateNormal];
    [self.cancelBtn addTarget:self action:@selector(cancelPlanClicked) forControlEvents:UIControlEventTouchUpInside];
    [self.confirmBtn setTitle:@"编辑预约" forState:UIControlStateNormal];
    
    self.hidden = NO;
}

#pragma mark - UIPickerViewDataSource, UIPickerViewDelegate
//选择器分为几块
-(NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView{
    return 4;
}

//选择器有多少行
-(NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component{
    NSInteger row = 0;
    switch (component) {
        case 0:
            row = 2;
            break;
        case 1:
            row = 12 * 50;
            break;
        case 2:
            row = 60 * 20;
            break;
        case 3:
            row = 2;
            break;
            
        default:
            break;
    }
    return row;
}


- (nullable NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    NSString *contentStr = nil;
    switch (component) {
        case 0:
            if (row == 0){
                contentStr = @"开始";
            }
            else{
                contentStr = @"结束";
            }
            break;
        case 1:
            contentStr = [NSString stringWithFormat:@"%ld", row % 24];
            break;
        case 2:
            contentStr = [NSString stringWithFormat:@"%ld", row % 60];
            break;
        case 3:
            if (row == 0){
                contentStr = @"";
            }
            else{
                contentStr = @"";
            }
            break;
        default:
            break;
    }
    
    return contentStr;
}


- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
//    switch (component) {
//        case 0:
//            _timeType = ((row == 0) ? 1 : 0);
//
//            break;
//        case 1:
//            _timeHour = row % 12;
//
//            break;
//        case 2:
//            _timeMin = row % 60;
//
//            break;
//        case 3:
//            _timeAM_PM = row;
//            break;
//
//        default:
//            break;
//    }
}

#pragma mark - dateTime
// 获取当天0点时间
- (NSDate *)zeroOfTodayTime
{
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSDateComponents *components = [calendar components:NSUIntegerMax fromDate:[NSDate date]];
    components.hour = 0;
    components.minute = 0;
    components.second = 0;
    
    // components.nanosecond = 0 not available in iOS
    NSTimeInterval ts = [[calendar dateFromComponents:components] timeIntervalSince1970];
    return [NSDate dateWithTimeIntervalSince1970:ts];
}

// 计算今天现在的时间
- (NSInteger)sinceTodayTime
{
    NSTimeInterval timeValue = [[NSDate date] timeIntervalSinceDate:[self zeroOfTodayTime]];
    return timeValue;
}

- (BOOL)isOutOfDate
{
    NSInteger hourValue = _timeHour + 12 * _timeAM_PM;
    NSInteger minValue = _timeMin;
    
    NSInteger selectTime = hourValue * 60 * 60 + minValue * 60;
    
    return (selectTime < [self sinceTodayTime]);
}
-(NSDate*)selectedDateWithTimeHour:(NSUInteger)timeHour timeMin:(NSUInteger)timeMin timeAMPM:(NSUInteger)timeAMPM
{
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSDateComponents *components = [calendar components:NSUIntegerMax fromDate:[NSDate date]];
    components.hour = timeHour+12*timeAMPM;
    components.minute = timeMin;
    components.second = 0;
    NSDate *selectedDate = [calendar dateFromComponents:components];
    if ([selectedDate earlierDate:[NSDate date]] == selectedDate) {
        selectedDate = [selectedDate dateByAddingTimeInterval:24*60*60];
    }
    return selectedDate;
}
@end
