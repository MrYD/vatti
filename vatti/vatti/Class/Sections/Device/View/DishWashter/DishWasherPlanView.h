//
//  DishWasherPlanView.h
//  vatti
//
//  Created by dongxiao on 2018/8/15.
//  Copyright © 2018年 xlink. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DishWasherDeviceModel.h"
typedef void (^EditPlanBlock)(NSUInteger timeType, NSUInteger timeHour, NSUInteger timeMin, NSUInteger timeAM_PM, NSInteger sinceTodayTime);
typedef void (^EditNewPlanBlock)(NSUInteger timeType, NSDate *selectedDate);
typedef void (^CancelPlanBlock)(void);

@interface DishWasherPlanView : UIView

- (instancetype)initPlanViewWithDeviceModel:(DishWasherDeviceModel *)model WithEditBlack:(EditPlanBlock)editPlanBlock withCancleBlock:(CancelPlanBlock)cancelPlanBlock WithTimeType:(NSInteger)dishwasherTimeType timeHour:(NSInteger)timeHour timeMin:(NSInteger)timeMin timeAM_PM:(NSInteger)timeAM_PM;
- (instancetype)initPlanViewWithDeviceModel:(DishWasherDeviceModel *)model WithEditBlack:(EditNewPlanBlock)editNewPlanBlock withCancleBlock:(CancelPlanBlock)cancelPlanBlock;
- (void)showEditPlanViewWithTimeType:(NSInteger)dishwasherTimeType timeHour:(NSInteger)timeHour timeMin:(NSInteger)timeMin;
- (void)loadDishWasherPlanView;

- (void)showPlanView;

- (void)hiddenPlanView;

- (void)showEditPlanViewWithTimeType:(NSInteger)dishwasherTimeType timeHour:(NSInteger)timeHour timeMin:(NSInteger)timeMin timeAM_PM:(NSInteger)timeAM_PM;

- (void)showEditPlanViewWithTimeType:(NSInteger)timeType selectedDate:(NSDate*)selectedDate;
@end
