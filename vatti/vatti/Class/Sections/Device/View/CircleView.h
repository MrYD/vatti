//
//  CircleView.h
//  testDeviceControl
//
//  Created by Eric on 2018/7/20.
//  Copyright © 2018年 Eric. All rights reserved.
//

/**
 *  圆形面板
 *
 *  @author BZH
 */

@class CircleView;

@protocol CircleViewDelegate <NSObject>

@optional

/**
 点击查看详情
 */
- (void)circleView:(CircleView*)circleView btnShowDetailClicked:(UIButton*)btnShowDetail;

@end

#import <UIKit/UIKit.h>

@interface CircleView : UIView

#pragma mark - IBOutet

#pragma mark - UIView

//自定义控件最外层 view
@property (weak, nonatomic) IBOutlet UIView *viewCircle;
///圆形背景 view
@property (weak, nonatomic) IBOutlet UIView *viewCircleBG;
///正常 view
@property (weak, nonatomic) IBOutlet UIView *viewNormal;
///告警 view
@property (weak, nonatomic) IBOutlet UIView *viewWaring;

#pragma mark - UILabel

///35
@property (weak, nonatomic) IBOutlet UILabel *lab35;
///60
@property (weak, nonatomic) IBOutlet UILabel *lab60;

///正在运行
@property (weak, nonatomic) IBOutlet UILabel *labRuning;
///温度 46℃
@property (weak, nonatomic) IBOutlet UILabel *labTemp;
///单位 ℃
@property (weak, nonatomic) IBOutlet UILabel *labUnit;
///预约状态
@property (weak, nonatomic) IBOutlet UILabel *labAppState;

///设备出现故障无法使用
@property (weak, nonatomic) IBOutlet UILabel *labWaring;

#pragma mark - Function

/**
 初始化
 */
+ (instancetype)view;

/**
 更新UI
 */
- (void)updateUI;

/**
 根据温度更新UI
 */
- (void)updateUIWithTemp:(NSInteger)temp andIsConnected:(BOOL)isConnected andIsOpen:(BOOL)isOpen;

#pragma mark - property

@property (weak,   nonatomic) id <CircleViewDelegate> delegate;

@end
