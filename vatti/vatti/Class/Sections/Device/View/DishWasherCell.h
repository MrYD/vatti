//
//  DishWasherCell.h
//  vatti
//
//  Created by dongxiao on 2018/8/27.
//  Copyright © 2018年 xlink. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DishWasherDeviceModel.h"
@interface DishWasherCell : UITableViewCell

@property (weak,   nonatomic) IBOutlet UIImageView *image;
@property (weak,   nonatomic) IBOutlet UIImageView *ivWarn;

@property (weak,   nonatomic) IBOutlet UILabel *deviceName;
@property (weak,   nonatomic) IBOutlet UILabel *deviceState;
@property (weak,   nonatomic) IBOutlet UILabel *deviceOnline;

@property (strong, nonatomic) NSString *imageName;
@property (strong, nonatomic) NSString *state;

@property (strong, nonatomic) DishWasherDeviceModel *deviceModel;

@end
