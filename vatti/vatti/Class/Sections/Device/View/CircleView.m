//
//  CircleView.m
//  testDeviceControl
//
//  Created by Eric on 2018/7/20.
//  Copyright © 2018年 Eric. All rights reserved.
//

#import "CircleView.h"
#import "UIColor+XLCategory.h"

@interface CircleView()

#pragma mark - 圆形相关

///渐变圆
@property (strong, nonatomic) CAGradientLayer *layerGradientCircle;

///渐变圆约束mask
@property (strong, nonatomic) CAShapeLayer *layerGradientCircleMask;

///渐变圆底部灰色
@property (strong, nonatomic) CAShapeLayer *layerCircleBG;

///贝塞尔曲线，用于渐变圆mask
@property (strong, nonatomic) UIBezierPath *bezierForLayerCircleMask;

///圆心
@property (assign, nonatomic) CGPoint circleCenter;

///圆弧度
@property (assign, nonatomic) CGFloat circleRadius;

///线宽
@property (assign, nonatomic) CGFloat lineWidth;

///开始点
@property (assign, nonatomic) CGFloat circleStartAngle;

///结束点
@property (assign, nonatomic) CGFloat circleEndAngle;

#pragma mark - NSArray

///渐变颜色数组
@property (strong, nonatomic) NSArray *gradientColorsArray;

///离线颜色数组
@property (strong, nonatomic) NSArray *offlineColorsArray;

#pragma mark - UIView
@property (weak, nonatomic) IBOutlet UIView *viewReadLine;


#pragma mark - Data

///连接
@property (assign, nonatomic) BOOL isConnected;

///开关
@property (assign, nonatomic) BOOL isOpen;
@end


@implementation CircleView

#pragma mark - LifeCycle

+ (instancetype)view
{
    return [[NSBundle mainBundle] loadNibNamed:NSStringFromClass([self class]) owner:nil options:nil].firstObject;
}

- (void)awakeFromNib
{
    [super awakeFromNib];
    
    //线宽
    self.lineWidth = 10.0f;
    
    //开始点
    self.circleStartAngle = (0.75f * M_PI);
    
    //渐变颜色数组
    self.gradientColorsArray = @[(id)[UIColor colorWithHexString:@"F3C7A1"].CGColor,(id)[UIColor colorWithHexString:@"EC544F"].CGColor];
    //离线颜色数组
    self.offlineColorsArray = @[(id)[UIColor colorWithHexString:@"C0BEBE"].CGColor,(id)[UIColor colorWithHexString:@"C0BEBE"].CGColor];
    
    //labels
    [self setupLabels];
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    
    //圆
    [self setupCircleView];
}

#pragma mark - UI

- (void)setupLabels
{
    self.labWaring.text = @"设备出现故障\n暂无法使用";
}

- (void)setupCircleView
{
    //渐变圆底部灰色
    [self setupLayerCircleBG];
    
    //渐变圆
    [self setupLayerGradientCircle];
    
    //渐变圆约束mask
    [self setupLayerGradientCircleMask];
    
    //红色线
    self.viewReadLine.hidden = !self.isConnected;
}

/**
 渐变圆底部灰色
 */
- (void)setupLayerCircleBG
{
    //结束点
    CGFloat circleEndAngle   = (0.25f * M_PI);
    
    UIBezierPath *pathForLayerCircleBG = [UIBezierPath bezierPathWithArcCenter:self.circleCenter radius:self.circleRadius startAngle:self.circleStartAngle endAngle:circleEndAngle clockwise:YES];
    
    [self.layerCircleBG removeFromSuperlayer];
    self.layerCircleBG = nil;
    
    self.layerCircleBG              = [CAShapeLayer layer];
    self.layerCircleBG.lineWidth    = self.lineWidth;
    self.layerCircleBG.strokeColor  = [UIColor colorWithHexString:@"F1F4F7"].CGColor;
    self.layerCircleBG.fillColor    = [UIColor clearColor].CGColor;
    self.layerCircleBG.lineCap      = kCALineCapRound;
    self.layerCircleBG.path         = [pathForLayerCircleBG CGPath];
    
    [self.viewCircleBG.layer addSublayer:self.layerCircleBG];
}

/**
 渐变圆
 */
- (void)setupLayerGradientCircle
{
    [self.layerGradientCircle removeFromSuperlayer];
    self.layerGradientCircle = nil;
    
    self.layerGradientCircle              = [CAGradientLayer layer];
    self.layerGradientCircle.frame        = self.viewCircleBG.bounds;
    self.layerGradientCircle.cornerRadius = 5;
    self.layerGradientCircle.startPoint   = CGPointMake(0, 0);
    self.layerGradientCircle.endPoint     = CGPointMake(1, 0);
    
    BOOL b1 = self.isConnected == YES;
    BOOL b2 = self.isOpen == YES;
    if (b1 && b2) {
        self.layerGradientCircle.colors       = self.gradientColorsArray;
    }else{
        self.layerGradientCircle.colors       = self.offlineColorsArray;
    }
    
    [self.viewCircleBG.layer addSublayer:self.layerGradientCircle];
}

/**
 渐变圆约束mas
 */
- (void)setupLayerGradientCircleMask
{
    self.bezierForLayerCircleMask = [UIBezierPath bezierPathWithArcCenter:self.circleCenter radius:self.circleRadius startAngle:self.circleStartAngle endAngle:self.circleEndAngle clockwise:YES];
    
    [self.layerGradientCircleMask removeFromSuperlayer];
    self.layerGradientCircleMask = nil;
    
    self.layerGradientCircleMask                 = [CAShapeLayer layer];
    self.layerGradientCircleMask.lineWidth       = self.lineWidth;
    self.layerGradientCircleMask.strokeColor     = [UIColor lightGrayColor].CGColor;
    self.layerGradientCircleMask.fillColor       = [UIColor clearColor].CGColor;
    self.layerGradientCircleMask.lineCap         = kCALineCapRound;
    self.layerGradientCircleMask.backgroundColor = [UIColor redColor].CGColor;
    self.layerGradientCircleMask.path            = self.bezierForLayerCircleMask.CGPath;
    
    self.layerGradientCircle.mask                = self.layerGradientCircleMask;
}

/**
 更新UI
 */
- (void)updateUI
{
    [self setNeedsLayout];
    [self layoutIfNeeded];
}

/**
 根据温度更新UI
 */
- (void)updateUIWithTemp:(NSInteger)temp andIsConnected:(BOOL)isConnected andIsOpen:(BOOL)isOpen
{
    //data
    CGFloat fanWei = 60 - 35;
    
    if (isConnected == YES) {
        //labTemp
        if (temp < 35) {
            temp = 35;
        }else if (temp > 60){
            temp = 60;
        }
        
        self.labTemp.text = [NSString stringWithFormat:@"%ld",(long)temp];
        self.labUnit.hidden = NO;
        self.circleEndAngle = (temp - 35) * 1.5 * M_PI / fanWei + self.circleStartAngle;
    }else{
        //labTemp
        self.labTemp.text = @"-";
        self.labUnit.hidden = YES;
        self.circleEndAngle = (0.75f * M_PI);
    }
    
    //connected
    self.isConnected = isConnected;
    //open
    self.isOpen = isOpen;
    
    //ui
    [self updateUI];
}

#pragma mark - Action

/**
 点击查看详情
 */
- (IBAction)btnShowDetailClicked:(UIButton *)sender {
    if (self.delegate && [self.delegate respondsToSelector:@selector(circleView:btnShowDetailClicked:)]) {
        [self.delegate circleView:self btnShowDetailClicked:sender];
    }
}

#pragma mark - LazyLoad

/**
 圆心
 */
- (CGPoint)circleCenter
{
    CGFloat circleViewWH = _viewCircleBG.frame.size.height;
    CGFloat circleX = circleViewWH / 2.0f;
    CGFloat circleY = circleX;
    
    _circleCenter = CGPointMake(circleX, circleY);
    
    return _circleCenter;
}

/**
 圆弧度
 */
- (CGFloat)circleRadius
{
    _circleRadius = _viewCircleBG.frame.size.width / 2.0f - (self.lineWidth / 2);
    return _circleRadius;
}

@end
