//
//  AddDeviceModel.h
//  vatti
//
//  Created by 李叶 on 2018/6/1.
//  Copyright © 2018年 xlink. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface AddDeviceModel : NSObject
@property (nonatomic, copy) NSString *name;

@property (nonatomic, copy) NSArray <NSString *>*product_ids;

@property (nonatomic, copy) NSString *image;

@property (nonatomic, copy) NSString *image_guide;

@property (nonatomic, copy) NSString *add_guide;
@end
