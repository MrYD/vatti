//
//  AddDeviceListViewController.m
//  vatti
//
//  Created by 李叶 on 2018/6/1.
//  Copyright © 2018年 xlink. All rights reserved.
//

#import "AddDeviceListViewController.h"
#import "AddDeviceTableViewHeaderView.h"
#import "SearchDeviceCell.h"
#import "AddDeviceFailViewController.h"
#import "AddDeviceSuccessViewController.h"
#import "XDeviceManager.h"
#import "AddDeviceViewController.h"
#import "XLNetworkModule.h"

#define AddDeviceTimeOut 300.0f

@interface AddDeviceListViewController ()<UITableViewDelegate, UITableViewDataSource>

@property (weak,   nonatomic) IBOutlet UITableView *tableView;

@property (weak,   nonatomic) IBOutlet UIButton *btnNextStep;
@property (weak,   nonatomic) IBOutlet UIButton *btnRefresh;

///tableView 头部
@property (strong, nonatomic) AddDeviceTableViewHeaderView *tableViewHeader;

@property (strong, nonatomic) XDevice *chooseDevice;

@property (assign, nonatomic) BOOL isSearching;

@property (assign, nonatomic) NSInteger currentRow;

@end

@implementation AddDeviceListViewController

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.currentRow = -1;
    }
    return self;
}

#pragma mark - LifeCycle
- (void)viewDidLoad {
    [super viewDidLoad];
}

- (void)initConfigure
{
    [super initConfigure];
    
    self.isSearching = YES;
}

- (void)initSubViews{
    [super initSubViews];
    
    //navi
    self.navigationItem.title = XLLocalizeString(@"添加设备");
    
    UIBarButtonItem *cancleItem = [[UIBarButtonItem alloc]initWithTitle:@"取消" style:(UIBarButtonItemStyleDone) target:self action:@selector(cancle)];
    [cancleItem setTitleTextAttributes:@{NSForegroundColorAttributeName:[UIColor blackColor],NSFontAttributeName : [UIFont systemFontOfSize:15]} forState:UIControlStateNormal];
    [cancleItem setTitleTextAttributes:@{NSForegroundColorAttributeName:[UIColor grayColor],NSFontAttributeName : [UIFont systemFontOfSize:15]} forState:UIControlStateDisabled];
    self.navigationItem.leftBarButtonItem = cancleItem;
    
    //tableView
    [self.tableView registerNib:[UINib nibWithNibName:NSStringFromClass([SearchDeviceCell class]) bundle:nil] forCellReuseIdentifier:NSStringFromClass([SearchDeviceCell class])];
}

- (void)buildData
{
    [super buildData];
    
    @weakify(self)
    [[RACObserve(self, chooseDevice).distinctUntilChanged deliverOnMainThread] subscribeNext:^(id  _Nullable x) {
        
        @strongify(self)
        self.btnNextStep.enabled = self.chooseDevice != nil;
    }];
}

- (void)viewWillLayoutSubviews
{
    [super viewWillLayoutSubviews];
    
    self.tableViewHeader.frame = CGRectMake(0, 0, self.view.frame.size.width, 100);
}

#pragma mark - UI

/**
 刷新列表
 */
- (void)refreshData
{
    [self.tableView reloadData];
}

#pragma mark - Action

- (void)cancle{
    [self.helper cancelTask];
    
    NSArray *vcArray = self.navigationController.viewControllers;
    for (id vc in vcArray) {
        BOOL b1 = [[vc class] isEqual:[AddDeviceViewController class]];
        if (b1 == YES) {
            [self.navigationController popToViewController:vc animated:YES];
            break;
        }
    }
}

- (void)addDevice:(XDevice *)device{
    [XL_DATA_SOURCE.user addDevice:device];
    [XL_DATA_SOURCE updateUser];
    [XL_NETWORK_MODULE getAccountSubscribeDeviceListWithXLUserModel:XL_DATA_SOURCE.user handler:nil];
    
    AddDeviceSuccessViewController *success = [[AddDeviceSuccessViewController alloc] init];
    [self.navigationController pushViewController:success animated:YES];
}

- (void)addDeviceFailWithErrorCode:(NSInteger)errCode {
    AddDeviceFailViewController *failCtrl = [[AddDeviceFailViewController alloc] init];
    failCtrl.type = AddDeviceFailedTypeSubscribe;
    [self.navigationController pushViewController:failCtrl animated:YES];
}

/**
 下一步，添加设备
 */
- (IBAction)btnNextStepCliced:(UIButton *)sender {
    
    @weakify(self)
    if (self.chooseDevice == nil) {
        [self alertHintWithMessage:XLLocalizeString(@"请选择需要添加的设备")];
        return;
    }
    
    [self.helper cancelTask];
    
    //断开连接
    [[XDeviceManager shareManager] disconnectDevice:self.chooseDevice];
    
    NSLog(@"开始请求添加设备");
    KShowLoading;
    [[XLServiceManager shareInstance].deviceService addDevice:self.chooseDevice pinCode:nil timeout:AddDeviceTimeOut handler:^(XLinkErrorCode code) {
        @strongify(self)
        KHideLoading;
        
        NSLog(@"请求添加设备结束");
        
        if (code == XLinkErrorCodeNoError) {
            NSLog(@"请求添加设备成功");
            [self addDevice:self.chooseDevice];
        }else{
            NSLog(@"请求添加设备失败");
            [self addDeviceFailWithErrorCode:code];
        }
        
    }];
}


/**
 重新搜索
 */
- (IBAction)btnRefreshClicked:(UIButton *)sender {
    
    //data
    //去掉选中设备
    self.chooseDevice = nil;
    
    //清空搜索设备列表
    [self.helper.devicesArray removeAllObjects];
    
    //更新UI
    [self refreshData];
    [self updateUIWithScranFinished:NO];
    
    if (self.delegate && [self.delegate respondsToSelector:@selector(addDeviceListViewController:didClickedBtnRefresh:)]) {
        [self.delegate addDeviceListViewController:self didClickedBtnRefresh:sender];
    }
}

/**
 显示、隐藏重新搜索按钮
 
 @param isShow 是否显示 YES:显示 NO:不显示
 */
- (void)showRefreshBtn:(BOOL)isShow
{
    self.btnRefresh.hidden = !isShow;
}

#pragma mark - Data

- (void)updateUIWithScranFinished:(BOOL)isFinished
{
    //显示重新搜索按钮
    [self showRefreshBtn:isFinished];
    
    //更新头部状态
    self.isSearching = !isFinished;
    [self.tableViewHeader updateLabStateWithIsSearching:self.isSearching];
}

#pragma mark - UITableViewDelegate & UITableViewDataSource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.helper.devicesArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    NSInteger rowNum = indexPath.row;
    
    SearchDeviceCell *cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([SearchDeviceCell class]) forIndexPath:indexPath];
    
    XDevice *device = [self.helper.devicesArray objectAtIndex:rowNum];
    
    cell.device = device;
    cell.labMac.text = [device getMacAddressSimple];
    

    if (device.deviceName != nil && device.deviceName.length != 0) {
        cell.labName.text = device.deviceName;
    }
    else
    {
        if ([device.productID isEqualToString:@"1607d2b688d41f411607d2b688d47a01"]) {
            
            cell.labName.text = XLDeviceDishWasherName;
            
        } else {
            cell.labName.text = [device.productID isEqualToString:XLProductGas] ? @"大静界TH6i 热水器" : @"健康洗QH01i热水器";
        }
    }
    
    cell.ivSelected.hidden  = ![self.chooseDevice.macAddress isEqualToData:device.macAddress];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
    NSInteger rowNum = indexPath.row;
    self.chooseDevice = [self.helper.devicesArray objectAtIndex:rowNum];
    NSArray *indexList;
    if (self.currentRow != -1) {
        NSIndexPath *lastIndex = [NSIndexPath indexPathForRow:self.currentRow inSection:0];
        indexList = @[lastIndex, indexPath];
    }else {
        indexList = @[indexPath];
    }
    [tableView beginUpdates];
    [tableView reloadRowsAtIndexPaths:indexList withRowAnimation:UITableViewRowAnimationAutomatic];
    [tableView endUpdates];
    self.currentRow = rowNum;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 80.0f;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 60.0f;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    
    self.tableViewHeader = [[NSBundle mainBundle] loadNibNamed:@"AddDeviceTableViewHeaderView" owner:nil options:nil].lastObject;
    
    [self.tableViewHeader updateLabStateWithIsSearching:self.isSearching];
    
    CGRect vFrame = CGRectMake(0, 0, kXLScreenWidth, 100);
    UIView *v = [[UIView alloc] initWithFrame:vFrame];
    [v addSubview:self.tableViewHeader];
    
    return v;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    return [[UIView alloc] init];
}

@end
