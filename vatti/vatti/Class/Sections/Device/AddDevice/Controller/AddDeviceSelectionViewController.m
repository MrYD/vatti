//
//  AddDeviceSelectionViewController.m
//  vatti
//
//  Created by 彭英科 on 2018/8/8.
//  Copyright © 2018年 xlink. All rights reserved.
//

#import "AddDeviceSelectionViewController.h"
#import "SelectionDeviceCell.h"
#import "DeviceGuideViewController.h"

static NSString *const SelectionDeviceCellID = @"SelectionDeviceCell";


@interface AddDeviceSelectionViewController () <UITableViewDelegate,UITableViewDataSource>

///tableView
@property (weak, nonatomic) IBOutlet UITableView *tableView;

///下一步
@property (weak, nonatomic) IBOutlet UIButton *nextBtn;

///productID数组
@property (strong, nonatomic) NSArray <NSDictionary<NSString *, NSString *> *>*productArray;

///选择的行
@property (assign, nonatomic) NSInteger selectedRow;

@end

@implementation AddDeviceSelectionViewController 

- (void)viewDidLoad {
    [super viewDidLoad];
}

- (void)initConfigure
{
    [super initConfigure];
    
    NSDictionary *gasProductDict = @{@"productId": XLProductGas, @"name": XLDeviceGasWaterHeaterName};
    NSDictionary *eleProductDict = @{@"productId": XLProductEle, @"name": XLDeviceEleWaterHeaterName};
    NSDictionary *dishProductDict = @{@"productId": XLProductDish, @"name": XLDeviceDishWasherName};
    self.productArray = @[gasProductDict,eleProductDict,dishProductDict];
    
    self.selectedRow = 0;
}


- (void)initSubViews {
    
    [super initSubViews];
    
    //navi
    self.navigationItem.title = XLLocalizeString(@"添加设备");
    UIBarButtonItem *cancleItem = [[UIBarButtonItem alloc]initWithTitle:@"取消" style:(UIBarButtonItemStyleDone) target:self action:@selector(cancle)];
    [cancleItem setTitleTextAttributes:@{NSForegroundColorAttributeName:[UIColor blackColor],NSFontAttributeName : [UIFont systemFontOfSize:15]} forState:UIControlStateNormal];
    [cancleItem setTitleTextAttributes:@{NSForegroundColorAttributeName:[UIColor grayColor],NSFontAttributeName : [UIFont systemFontOfSize:15]} forState:UIControlStateDisabled];
    self.navigationItem.leftBarButtonItem = cancleItem;
    
    //tableView
    self.tableView.scrollEnabled = NO;
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    UIView *footView = [[UIView alloc] init];
    self.tableView.tableFooterView = footView;
    //注册cell
    [self.tableView registerNib:[UINib nibWithNibName:NSStringFromClass([SelectionDeviceCell class]) bundle:nil] forCellReuseIdentifier:SelectionDeviceCellID];
    
    //btn
    self.nextBtn.layer.cornerRadius = 8;
    self.nextBtn.layer.masksToBounds = YES;
    self.nextBtn.layer.borderColor = [UIColor grayColor].CGColor;
    [self.nextBtn.layer setBorderWidth:1.0];
    
}


#pragma mark -- UITableViewDelegate & UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.productArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    NSInteger rowNum = indexPath.row;
    
    SelectionDeviceCell *cell = [tableView dequeueReusableCellWithIdentifier:SelectionDeviceCellID];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;

    NSDictionary *product = [self.productArray objectAtIndex:rowNum];
    cell.title.text = product[@"name"];
    
    if (rowNum == self.selectedRow) {
        cell.selectionStateImage.hidden = NO;
    }else{
        cell.selectionStateImage.hidden = YES;
    }
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    NSInteger rowNum = indexPath.row;
    
    self.selectedRow = rowNum;
    
    [self.tableView reloadData];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 70.f;
}

//返回
- (void)cancle{
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark -- 下一步
- (IBAction)nextAction:(id)sender {
    NSDictionary *product = [self.productArray objectAtIndex:self.selectedRow];
    
    DeviceGuideViewController *vc = [[DeviceGuideViewController alloc] init];
    vc.productId = product[@"productId"];
    [self.navigationController pushViewController:vc animated:YES];
}

@end
