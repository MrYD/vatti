//
//  AddDeviceListViewController.h
//  vatti
//
//  Created by 李叶 on 2018/6/1.
//  Copyright © 2018年 xlink. All rights reserved.
//

#import "BaseViewController.h"
#import "XLScanDeviceHelperTool.h"

@class AddDeviceListViewController;

@protocol AddDeviceListViewControllerDelegate <NSObject>

@optional

/**
 重新搜索
 */
- (void)addDeviceListViewController:(AddDeviceListViewController*)vc didClickedBtnRefresh:(UIButton*)btnRefresh;

@end

@interface AddDeviceListViewController : BaseViewController

@property (strong, nonatomic) XLScanDeviceHelperTool *helper;

@property (copy,   nonatomic) NSString *productId;

@property (weak,   nonatomic) id <AddDeviceListViewControllerDelegate> delegate;

/**
 刷新列表
 */
- (void)refreshData;

/**
 扫描完成
 */
- (void)updateUIWithScranFinished:(BOOL)isFinished;
@end
