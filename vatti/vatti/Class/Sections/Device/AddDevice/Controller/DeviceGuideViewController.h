//
//  DeviceGuideViewController.h
//  vatti
//
//  Created by 李叶 on 2018/5/28.
//  Copyright © 2018年 xlink. All rights reserved.
//

#import "BaseViewController.h"

@interface DeviceGuideViewController : BaseViewController

///<#desc#>
@property (copy,   nonatomic) NSString *productId;

@end
