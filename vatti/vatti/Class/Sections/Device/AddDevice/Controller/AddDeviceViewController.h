//
//  AddDeviceViewController.h
//  vatti
//
//  Created by 李叶 on 2018/5/28.
//  Copyright © 2018年 xlink. All rights reserved.
//

#import "BaseViewController.h"

@interface AddDeviceViewController : BaseViewController

@property (nonatomic, copy) NSString *wifiName;
@property (nonatomic, copy) NSString *deviceSerizNo;
@property (nonatomic, copy) NSString *deviceVerifyCode;

@property (nonatomic, copy) NSString *productId;

@end
