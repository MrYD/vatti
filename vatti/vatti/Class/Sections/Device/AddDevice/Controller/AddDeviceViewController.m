//
//  AddDeviceViewController.m
//  vatti
//
//  Created by 李叶 on 2018/5/28.
//  Copyright © 2018年 xlink. All rights reserved.
//

#import "AddDeviceViewController.h"
#import "XLTextField.h"
#import "UITextField+XLCategory.h"
#import "ConnectWIFIViewController.h"
#import "XLUtilTool.h"
#import "AppDelegate.h"

@interface AddDeviceViewController ()

@property (weak, nonatomic) IBOutlet XLTextField *tfWIFIName;
@property (weak, nonatomic) IBOutlet XLTextField *tfWIFIPassword;

///是否需要跳转到配网页面
@property (assign, nonatomic) BOOL isPushToConnectWifi;

@end

@implementation AddDeviceViewController

- (void)viewDidLoad {
    [super viewDidLoad];
}

- (void)initConfigure
{
    [super initConfigure];
    
    self.isPushToConnectWifi = NO;
}


- (void)initSubViews{
    [super initSubViews];
    
    //naiv
    self.navigationItem.title = @"输入密码";
    
    UIBarButtonItem *cancleItem = [[UIBarButtonItem alloc]initWithTitle:@"取消" style:(UIBarButtonItemStyleDone) target:self action:@selector(cancle)];
    [cancleItem setTitleTextAttributes:@{NSForegroundColorAttributeName:[UIColor blackColor],NSFontAttributeName : [UIFont systemFontOfSize:15]} forState:UIControlStateNormal];
    [cancleItem setTitleTextAttributes:@{NSForegroundColorAttributeName:[UIColor grayColor],NSFontAttributeName : [UIFont systemFontOfSize:15]} forState:UIControlStateDisabled];
    self.navigationItem.leftBarButtonItem = cancleItem;
    
    UIBarButtonItem *addItem = [[UIBarButtonItem alloc]initWithTitle:@"发送" style:(UIBarButtonItemStyleDone) target:self action:@selector(send)];
    [addItem setTitleTextAttributes:@{NSForegroundColorAttributeName:[UIColor blackColor],NSFontAttributeName : [UIFont systemFontOfSize:15]} forState:UIControlStateNormal];
    [addItem setTitleTextAttributes:@{NSForegroundColorAttributeName:[UIColor grayColor],NSFontAttributeName : [UIFont systemFontOfSize:15]} forState:UIControlStateDisabled];
    self.navigationItem.rightBarButtonItem = addItem;
    

//#define NoPassWord
    
#ifdef NoPassWord
    NSLog(@"暂时屏蔽密码必填");
#else
    RAC(addItem,enabled) = [RACSignal combineLatest:@[self.tfWIFIPassword.rac_textSignal] reduce:^(NSString *password){
        return @(password.length);
    }];
#endif
    
    self.tfWIFIName.text = self.wifiName;
    self.tfWIFIName.enabled = NO;
    
    [self.tfWIFIPassword addShowPasswordButton];
    [self.tfWIFIPassword becomeFirstResponder];
    
}

- (void)buildData
{
    [super buildData];
    
    @weakify(self)
    [[[kXLNotificationCenter rac_addObserverForName:UIApplicationWillEnterForegroundNotification object:nil] deliverOnMainThread] subscribeNext:^(NSNotification * _Nullable x) {
        @strongify(self)

        NSString *ssid = [XLUtilTool getWifiSSID];
        
        if (!ssid.length){
            self.tfWIFIName.text = XLHint_NoSSID;
        }else{
            self.tfWIFIName.text = ssid;
        }
        
    }];
    
    [[[kXLNotificationCenter rac_addObserverForName:UIKeyboardDidHideNotification object:nil] deliverOnMainThread] subscribeNext:^(NSNotification * _Nullable x) {
        @strongify(self)
        
        if (self.isPushToConnectWifi) {
            self.isPushToConnectWifi = NO;
            [self pushToConnectWIFIViewController];
        }
    }];
}

#pragma mark - Action

- (void)send{
    NSString *ssid = [XLUtilTool getWifiSSID];
    
    BOOL b1 = (self.tfWIFIName.text.length > 0) == NO;
    BOOL b2 = (ssid.length > 0) == NO;
    
    if (b1 || b2){
        [self alertHintWithMessage:XLHint_NoSSID];
        return;
    }
    
    if ([self.tfWIFIPassword isFirstResponder]) {
        [self.tfWIFIPassword resignFirstResponder];
        self.isPushToConnectWifi = YES;
    }else {
        [self pushToConnectWIFIViewController];
    }
}

- (void)cancle{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)pushToConnectWIFIViewController
{
    NSString *ssid = self.tfWIFIName.text;
    NSLog(@"跳转到ConnectWIFIViewController, self.wifiName: %@|%p", ssid, ssid);
    ConnectWIFIViewController *connection = [[ConnectWIFIViewController alloc]init];
    connection.wifiName = self.tfWIFIName.text;
    connection.password = self.tfWIFIPassword.text;
    connection.productId = self.productId;
    [self.navigationController pushViewController:connection animated:YES];
}

@end
