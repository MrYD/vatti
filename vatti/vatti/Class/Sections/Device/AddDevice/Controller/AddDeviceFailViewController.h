//
//  AddDeviceFailViewController.h
//  vatti
//
//  Created by 李叶 on 2018/6/4.
//  Copyright © 2018年 xlink. All rights reserved.
//

#import "BaseViewController.h"

typedef NS_ENUM(NSUInteger, AddDeviceFailedType) {
    AddDeviceFailedTypeConfigure,
    AddDeviceFailedTypeScan,
    AddDeviceFailedTypeSubscribe,
};

@interface AddDeviceFailViewController : BaseViewController

@property (assign, nonatomic) AddDeviceFailedType type;

@end
