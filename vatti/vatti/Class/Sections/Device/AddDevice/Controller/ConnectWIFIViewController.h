//
//  ConnectWIFIViewController.h
//  SimpleConfig
//
//  Created by Realsil on 14/11/11.
//  Copyright (c) 2014年 Realtek. All rights reserved.
//

/**
 *  输入wifi密码进行配网
 *
 *  @author BZH
 */

#import "BaseViewController.h"

@interface ConnectWIFIViewController : BaseViewController

///外部传入的wifi名称
@property (nonatomic, copy) NSString *wifiName;
///外部传入的wifi密码
@property (nonatomic, copy) NSString *password;
///外部传入的productId
@property (nonatomic, copy) NSString *productId;

@property (weak, nonatomic) IBOutlet UILabel *labTips;


@end
