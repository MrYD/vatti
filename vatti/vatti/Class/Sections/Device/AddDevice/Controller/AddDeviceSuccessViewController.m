//
//  AddDeviceSuccessViewController.m
//  vatti
//
//  Created by 李叶 on 2018/6/4.
//  Copyright © 2018年 xlink. All rights reserved.
//

#import "AddDeviceSuccessViewController.h"
//#import <UIViewController+JKBackButtonTouched.h>

@interface AddDeviceSuccessViewController ()

@end

@implementation AddDeviceSuccessViewController

- (void)viewDidLoad {
    [super viewDidLoad];
}

- (void)initConfigure
{
    [super initConfigure];
    
    self.isNeedBackBtn = NO;
}

- (void)initSubViews{
    [super initSubViews];
}

- (IBAction)complete:(UIButton *)sender {
    [self.navigationController popToRootViewControllerAnimated:YES];
}

@end
