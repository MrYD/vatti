//
//  DeviceGuideViewController.m
//  vatti
//
//  Created by 李叶 on 2018/5/28.
//  Copyright © 2018年 xlink. All rights reserved.
//

#import "DeviceGuideViewController.h"
#import "AddDeviceViewController.h"
#import "XLUtilTool.h"

@interface DeviceGuideViewController ()

@property (weak, nonatomic) IBOutlet UIButton *nextAction;

@end

@implementation DeviceGuideViewController

- (void)viewDidLoad {
    [super viewDidLoad];

}

- (void)initSubViews{
    [super initSubViews];
    
    self.navigationItem.title = @"添加设备";
    
    UIBarButtonItem *cancleItem = [[UIBarButtonItem alloc]initWithTitle:@"取消" style:(UIBarButtonItemStyleDone) target:self action:@selector(cancle)];
    [cancleItem setTitleTextAttributes:@{NSForegroundColorAttributeName:[UIColor blackColor],NSFontAttributeName : [UIFont systemFontOfSize:15]} forState:UIControlStateNormal];
    [cancleItem setTitleTextAttributes:@{NSForegroundColorAttributeName:[UIColor grayColor],NSFontAttributeName : [UIFont systemFontOfSize:15]} forState:UIControlStateDisabled];
    self.navigationItem.leftBarButtonItem = cancleItem;
    
    [self.nextAction setTitleColor:[UIColor grayColor] forState:0];
    [self.nextAction.layer setBorderWidth:1.0];
    [self.nextAction setTitle:@"下一步" forState:0];
    
    self.nextAction.layer.cornerRadius = 8;
}

- (IBAction)nextAction:(UIButton *)sender {
    NSString *ssid = [XLUtilTool getWifiSSID];
    
    if (ssid.length){
        AddDeviceViewController *addDevice = [[AddDeviceViewController alloc]init];
        addDevice.wifiName = ssid;
        addDevice.productId = self.productId;
        [self.navigationController pushViewController:addDevice animated:YES];
    }else {
        [self alertHintWithMessage:XLHint_NoSSID];
    }
   
}

- (void)cancle{
    [self.navigationController popViewControllerAnimated:YES];
}
@end
