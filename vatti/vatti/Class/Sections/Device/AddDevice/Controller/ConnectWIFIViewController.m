#import "ConnectWIFIViewController.h"
#import "SimpleConfigUtil.h"
#import "XLScanDeviceHelperTool.h"
#import "AddDeviceListViewController.h"
#import "AddDeviceFailViewController.h"
#import "SimpleConfig.h"
#import "XLGCDTimerManager.h"
#import "XDeviceManager.h"
///
typedef struct rtk_sc_context{
    unsigned int    m_mode;
    unsigned int    m_recv_len;
    unsigned char   m_recv_buf[MAX_BUF_LEN];
}SC_CONTEXT;

#define timeout_cfg         120

#ifdef DEBUG
#define timeout_cfg_step1   50
#else
#define timeout_cfg_step1   50
#endif

@interface ConnectWIFIViewController () <XLScanDeviceHelperToolDelegate,AddDeviceListViewControllerDelegate>
///配网所需属性
{
@private
    ///SC Related
    SC_CONTEXT  m_context;
}

///
@property (strong, nonatomic) SimpleConfig *simpleConfig;

@property (strong, nonatomic) NSMutableArray *confirm_list;

///搜索设备工具
@property(strong, nonatomic) XLScanDeviceHelperTool *xlScanDeviceHelperTool;

///nowSSID
@property(copy, nonatomic) NSString *nowSSID;

///添加设备控制器
@property(strong, nonatomic) AddDeviceListViewController *addDeviceListVC;

//目标AP的SSID
@property (strong, nonatomic) NSString *targetAPSSID;

//保存ap和密码
@property (strong, nonatomic) NSMutableDictionary *APInfo_DataFile;

///WIFI配置进度条
@property (assign, nonatomic) uint16_t wifiConfigProgress;

@property(copy,   nonatomic) NSString *homeAP_SSID;

///进度条定时器
@property (strong, nonatomic) NSTimer *progressTimer;

///配网定时器
@property (strong, nonatomic) NSTimer *configTimer;

//soft AP
@property (assign, nonatomic) BOOL isTipSoftAP;
@property (assign, nonatomic) BOOL isSoftAPmode;
@property (assign, nonatomic) BOOL isAlertWiFiSetting;
@property (assign, nonatomic) BOOL isSwitchToWiFiSetting;
@property (assign, nonatomic) BOOL isBackToWiFiSetting;
@property (assign, nonatomic) BOOL isCheckHomeAP;
//检查回到目标AP的次数
@property (assign, nonatomic) int checkBackTargetAPCount;

@property (copy, nonatomic) NSString *softAP_SSID;

@end

@implementation ConnectWIFIViewController

//配网的设备数量
static const int kConfigDeviceCount = 1;
//ap模式切换后的超时时间
static const int kCheckSoftApModeTimeout = 90;
static NSString * const kCheckSoftApModeTimerName = @"CheckSoftApMode";

-(NSMutableDictionary *)APInfo_DataFile{
    if (!_APInfo_DataFile) {
        _APInfo_DataFile = [NSMutableDictionary dictionary];
    }
    return _APInfo_DataFile;
}

- (void)initConfigure {
    [super initConfigure];
    
    self.xlScanDeviceHelperTool = [[XLScanDeviceHelperTool alloc] init];
    
}

- (void)initSubViews {
    [super initSubViews];
    
    //naiv
    self.navigationItem.title = @"添加设备";
    
    UIBarButtonItem *cancleItem = [[UIBarButtonItem alloc] initWithTitle:@"取消" style:(UIBarButtonItemStyleDone) target:self action:@selector(leftBarButtonItemClicked:)];
    [cancleItem setTitleTextAttributes:@{NSForegroundColorAttributeName: [UIColor blackColor], NSFontAttributeName: [UIFont systemFontOfSize:15]} forState:UIControlStateNormal];
    [cancleItem setTitleTextAttributes:@{NSForegroundColorAttributeName: [UIColor grayColor], NSFontAttributeName: [UIFont systemFontOfSize:15]} forState:UIControlStateDisabled];
    self.navigationItem.leftBarButtonItem = cancleItem;
    
}

- (void)buildData {
    [super buildData];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    NSLog(@"config viewDidLoad");
    NSLog(@"self.wifiName: %@|%p", self.wifiName, self.wifiName);
    
    self.confirm_list = nil;
    self.homeAP_SSID = [SimpleConfigUtil fetchCurrSSID];
    
    m_context.m_mode = MODE_INIT;
    
    self.simpleConfig = [[SimpleConfig alloc] init];
    _configTimer = [NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(configTimerHandler:) userInfo:nil repeats:YES];
    
    _isTipSoftAP = NO;
    _isSoftAPmode = NO;
    
    [self rtkStartWifiConfig];
}

- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
    // Must release simpleConfig, so that its asyncUDPSocket delegate won't receive data
    NSLog(@"config viewDidDisappear");
    if (self.simpleConfig != nil) {
        [self.simpleConfig rtk_sc_close_sock];
        self.simpleConfig = nil;
    }
    
    [self stopProgressTimer];
}

- (void)stopProgressTimer {
    if ([_progressTimer isValid]) {
        [_progressTimer invalidate];
    }
    if (_progressTimer) {
        _progressTimer = nil;
    }
}

- (void)configActionDirectly {
    NSString *PIN_cfg = @"";
    int g_config_device_num = 1;
    [self.simpleConfig rtk_sc_config_set_devNum:g_config_device_num];
    [self startWaitingProgress:@"Device Configuring" timeout:120];
    int ret = [self configAction:PIN_cfg];
    if (ret == RTK_FAILED) {
        [self pushToFailedVCWithType:AddDeviceFailedTypeConfigure];
    }
}


/* action responder */
- (void)rtkStartWifiConfig {
    @weakify(self)
    
    _isCheckHomeAP = NO;
    _isAlertWiFiSetting = NO;
    _isSwitchToWiFiSetting = NO;
    _isBackToWiFiSetting = NO;
    _checkBackTargetAPCount = 0;
    
    NSLog(@"SSID/pwd : %@ / %@", self.homeAP_SSID, self.password);
    
    [self setWiFipassword:self.homeAP_SSID :self.password];
    [self setTargetAPSSID:self.homeAP_SSID];
    
    
    if (_isTipSoftAP) {
        _isTipSoftAP = NO;
        [_configTimer invalidate];
        _configTimer = nil;
        
        {
            NSString *nowSSID = [SimpleConfigUtil fetchCurrSSID];
            NSString *softAP_BSSID = @"";
            softAP_BSSID = [SimpleConfigUtil fetchCurrBSSID];
            
            [self setTargetAPSSID:self.homeAP_SSID];
            
            [self.simpleConfig rtk_sc_softAP_init:softAP_BSSID :(int) [softAP_BSSID length]];
            
            self.isSoftAPmode = YES;
            
            self.nowSSID = nowSSID;
        }
        
        [self alertHintWithTitle:@"提示" message:@"请前往WiFi设置连接RSC-XXXX(密码:12345678), 连接成功返回【智尚心居】" cancelTitle:@"取消" otherTitles:@[@"已连接"] handler:^(UIAlertAction *action, NSUInteger tag) {
            @strongify(self)

            [self configureAlertControWithTag:tag];
            
        }];
        
    } else if (_isSoftAPmode == YES) {
        
        NSString *softAP_BSSID = @"";
        _softAP_SSID = [SimpleConfigUtil fetchCurrSSID];
        softAP_BSSID = [SimpleConfigUtil fetchCurrBSSID];
        NSLog(@"isSoftAPmode:%@(%@)", _softAP_SSID, softAP_BSSID);
        
        [self.simpleConfig rtk_sc_set_sc_model:SC_MODEL_1 duration:-1]; //R3
        if ([self.simpleConfig rtk_sc_isMyDevice:_softAP_SSID :(int) [_softAP_SSID length]]) {
            [self configActionDirectly];
        } else {
            NSLog(@"The SSID of Soft AP is not my device");
            m_context.m_mode = MODE_INIT;
            if (_isTipSoftAP){
                _isTipSoftAP = NO;
            }
            
            if (_isSoftAPmode){
                _isSoftAPmode = NO;
            }
            
            [self alertHintWithMessage:@"当前连接的不是设备AP热点。" handler:^(UIAlertAction *action, NSUInteger tag) {
                [self leftBarButtonItemClicked:nil];
            }];
        }
    } else {
        self.wifiConfigProgress = 0;
        _isSoftAPmode = NO;
        _isTipSoftAP = NO;
        [self configActionDirectly];
    }
}

- (int)configAction:(NSString *)m_PIN {
    int ret = RTK_FAILED;
    if (m_context.m_mode == MODE_INIT || m_context.m_mode == MODE_WAIT_FOR_IP) {
        if (_isSoftAPmode) {
            NSLog(@"-----ap mode----");
            [self.simpleConfig rtk_sc_set_sc_model:SC_MODEL_1 duration:-1]; //R3
        } else {
            NSLog(@"-----smartlink mode----");
            [self.simpleConfig rtk_sc_set_sc_model:SC_MODEL_2 duration:-1];//R1
        }
        
        NSString *ssid = self.wifiName;
        NSString *pass = self.password;
        NSString *pin = m_PIN;
        
        NSLog(@"开始向设备发送数据SSID: %@|%p, 密码：%@|%p, self.wifiName: %@|%p", ssid, ssid, pass, pass, self.wifiName, self.wifiName);
        int timerCount = 0;
        int count = 1;
        ret = [self.simpleConfig rtk_sc_config_start:ssid psw:pass pin:pin];

        if (ret == RTK_FAILED) {
            return RTK_FAILED;
        }
        m_context.m_mode = MODE_CONFIG;
        
        return RTK_SUCCEED;
    } else if (m_context.m_mode == MODE_CONFIG) {
        // stop sending profile
        m_context.m_mode = MODE_INIT;
        [self.simpleConfig rtk_sc_config_stop];
        
        return RTK_FAILED;
    }
    return RTK_FAILED;
}


/******* private functions *******/
- (void)configTimerHandler:(NSTimer *)sender {
    if (self.simpleConfig == nil || _configTimer == nil) {
        //NSLog(@"Timer error in config vc");
        return;
    }
    
    unsigned int sc_mode = [self.simpleConfig rtk_sc_get_mode];
    //NSLog(@"sc_mode = %d", sc_mode);
    
    int sc_pattern;
    
    switch (sc_mode) {
        case MODE_INIT:{
            if ([self isWithIPNoName]) {
                [self showControlButton];
            }
            if (_isCheckHomeAP) {
                [self checkHomeAP];
            }
        }
            break;
            
        case MODE_CONFIG:{
            
        }
            break;
            
        case MODE_WAIT_FOR_IP: {
            //NSLog(@"<APP> MODE_WAIT_FOR_IP\n");
            sc_pattern = [self.simpleConfig rtk_sc_get_pattern];
            if (sc_pattern == PATTERN_FOUR) {
                //模式4
                if (_isSoftAPmode) {
                    NSLog(@"apmode----------");
                    NSString *previous_homeAPSSID = [self getTargetAPSSID];
                    NSString *current_SSID = @"";
                    current_SSID = [SimpleConfigUtil fetchCurrSSID];
                    
                    if (current_SSID != nil) {
                        if ([current_SSID length] > 0 && ![current_SSID isEqualToString:_softAP_SSID]) {
                            if (![current_SSID isEqualToString:previous_homeAPSSID]) {
                                NSLog(@"%d to check %@ => %@", __LINE__, current_SSID, previous_homeAPSSID);
                                
                                //根据Realtek意见注释掉configTimerHandler中以下方法
                                //[self stopWaitingProgress];
                                
                                _isSwitchToWiFiSetting = YES;
                                m_context.m_mode = MODE_WAIT_FOR_IP;
                                NSLog(@"!!! alertmsg_connectback !!!");
                                if (_isAlertWiFiSetting == NO){
                                    [self alertShowMsgConnectBack];
                                }
                                _isAlertWiFiSetting = YES;
                            }
                        } else {
                            NSLog(@"%d to check %@ vs %@ (%d)", __LINE__, current_SSID, previous_homeAPSSID, _checkBackTargetAPCount);
                            _checkBackTargetAPCount++;
                            if (_checkBackTargetAPCount >= 20) {
                                
                                //根据Realtek意见注释掉configTimerHandler中以下方法
                                //[self stopWaitingProgress];
                                
                                _isSwitchToWiFiSetting = YES;
                                m_context.m_mode = MODE_WAIT_FOR_IP;
                                NSLog(@"!!! alertmsg_connectback !!!");
                                if (_isAlertWiFiSetting == NO){
                                    [self alertShowMsgConnectBack];
                                }
                                _isAlertWiFiSetting = YES;
                            }
                        }
                    }
                } else {
                    //其他模式
                    if (_isBackToWiFiSetting) {
                        NSLog(@"(%d)isBackToWiFiSetting CHECK DEVICE:%lu", __LINE__, (unsigned long) [self.simpleConfig.config_list count]);
                        if ([self.simpleConfig.config_list count] >= kConfigDeviceCount) {
                            NSLog(@"!!!!! FINISH !!!!!!");
                            m_context.m_mode = MODE_INIT;
                            [self.simpleConfig rtk_sc_config_stop];
                            [self showConfigList];
                            return;
                        }
                    }
                }
            }
        }
            break;
            
        default:
            break;
    }
}


- (void)alertShowMsgConnectBack {
    NSString *previous_SSID = [self getTargetAPSSID];
    NSString *msg = [NSString stringWithFormat:@"请前往WiFi设置连接WiFi(WiFi名称: %@), 连接成功后返回【智尚心居】", previous_SSID];
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"提示" message:msg preferredStyle:UIAlertControllerStyleAlert];
    [alertController addAction:[UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        _isBackToWiFiSetting = NO;
        _isSwitchToWiFiSetting = NO;
        _isSoftAPmode = NO;
        self->m_context.m_mode = MODE_INIT;
    }]];
    [alertController addAction:[UIAlertAction actionWithTitle:@"已连接" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        _isBackToWiFiSetting = YES;
        _isSwitchToWiFiSetting = YES;
        _isSoftAPmode = NO;
        self->m_context.m_mode = MODE_WAIT_FOR_IP;
    }]];
    [self presentAlertController:alertController];
}


- (BOOL)isWithIPNoName {
    BOOL ret = NO;
    struct dev_info dev;
    NSValue *dev_val;
    NSMutableArray *list = self.simpleConfig.config_list;
    
    if (list == nil || [list count] == 0) {
        return NO;
    }
    
    //NSLog(@"current list count=%d", [list count]);
    dev_val = [list objectAtIndex:0];   // the earliest dev_info added
    [dev_val getValue:&dev];
    // check have ip
    //NSLog(@"ip of obj0: %x", dev.ip);
    ret |= (dev.ip == 0) ? NO : YES;
    
    if (ret == NO) {
        return ret;
    }
    // check have no name
    NSString *curr_name = [NSString stringWithUTF8String:(const char *) (dev.extra_info)];
    ret |= ([curr_name isEqualToString:@""] || [curr_name isEqualToString:@"\n"]);
    //NSLog(@"name of obj0: %@", curr_name);
    
    return ret;
}


- (void)checkHomeAP {
    NSString *previous_homeAPSSID = [self getTargetAPSSID];
    NSString *current_SSID = @"";
    current_SSID = [SimpleConfigUtil fetchCurrSSID];
    
    if (current_SSID != nil) {
        if ([current_SSID length] > 0 && ![current_SSID isEqualToString:previous_homeAPSSID]) {
            _isCheckHomeAP = NO;

            self.isSoftAPmode = YES;

            if (![current_SSID isEqualToString:previous_homeAPSSID]) {
                
                NSString *msg = [NSString stringWithFormat:@"当前连接的wifi(%@)不是设备的AP热点，\n 请前往Wi-Fi设置连接RSC-XXXX，并输入密码12345678后，返回此页面",current_SSID];
                
                [self alertHintWithMessage:msg cancelTitle:@"取消" otherTitles:@[@"已连接"] handler:^(UIAlertAction *action, NSUInteger tag) {
                    [self configureAlertControWithTag:tag];
                }];
            }
        }
    }
}

- (void)showConfigList {
    NSLog(@"!!!!! showConfigList !!!!!!");
    
    @weakify(self)
    
    struct dev_info dev;
    NSValue *dev_val;
    
    _isBackToWiFiSetting = NO;
    
    self.confirm_list = [[NSMutableArray alloc] initWithArray:self.simpleConfig.config_list copyItems:YES];
    //NSMutableArray *list = simpleConfig.config_list;
    
    [self stopWaitingProgress];
    
    for (int i = 0; i < [self.confirm_list count]; i++) {
        dev_val = [self.confirm_list objectAtIndex:i];
        [dev_val getValue:&dev];
        
        NSLog(@"======<APP> Dump dev_info %d======", i);
        NSLog(@"MAC: %02x:%02x:%02x:%02x:%02x:%02x", dev.mac[0], dev.mac[1], dev.mac[2], dev.mac[3], dev.mac[4], dev.mac[5]);
        NSLog(@"Status: %d", dev.status);
        NSLog(@"Device type: %d", dev.dev_type);
        NSLog(@"IP:%x", dev.ip);
        NSLog(@"Name:%@", [NSString stringWithUTF8String:(const char *) (dev.extra_info)]);
        
        NSString *mac = [NSString stringWithFormat:@"%02x%02x%02x%02x%02x%02x", dev.mac[0], dev.mac[1], dev.mac[2], dev.mac[3], dev.mac[4], dev.mac[5]];
        XDevice *device = [[XDeviceManager shareManager] getDeviceWithMacAddressString:mac productId:self.productId];
        if (device != nil) {
            [[XDeviceManager shareManager] disconnectDevice:device];
        }
    }
    
    m_context.m_mode = MODE_INIT;
    
    //后续操作
    [_configTimer invalidate];
    _configTimer = nil;
    
    [self.simpleConfig rtk_sc_config_stop];
    
    self.labTips.text = @"配网完成，正在搜索设备...";
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t) (5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [self.simpleConfig rtk_sc_close_sock];
        self.simpleConfig = nil;
        
        //修改不回去连接原来的wifi 直接扫描设备
        [self requestDevice];
        
//        //判断
//        //1.如果当前连接的wifi是@RSC开头，即设备的wifi，需要提示用户，连接回自己的wifi
//        //2.如果当前连接的wifi是用户自己的wifi，则搜索设备
//        if (_isSoftAPmode == YES) {
//            [self alertHintWithTitle:@"配网成功" message:@"请前往WiFi设置页面切换WiFi连接" cancelTitle:@"已连接" otherTitles:nil handler:^(UIAlertAction *action, NSUInteger tag) {
//                @strongify(self)
//
//                if (tag == 0) {
//                    //确定
//                    [self requestDevice];
//                }
//            }];
//        } else {
//            [self requestDevice];
//        }
    });
}

- (void)showControlButton {
    if (m_context.m_mode != MODE_WAIT_FOR_IP && m_context.m_mode != MODE_CONFIG){
        return;
    }
    m_context.m_mode = MODE_INIT;
}


#pragma mark - 假装被点击

- (void)waitAlertViewClickCancel {
    NSLog(@"<APP> wait alert show: Cancel");
    [self stopWaitingProgress];
    
    // stop sending profile
    if (_isSwitchToWiFiSetting){
        NSLog(@"SwithToWiFiSetting");
    }else {
        m_context.m_mode = MODE_INIT;
        [self.simpleConfig rtk_sc_config_stop];
    }
}

- (void)startWaitingProgress:(NSString *)wait_title timeout:(float)timeout {
    if (_progressTimer) {
        [self stopWaitingProgress];
    }
    
    if (timeout > 0) {
        _progressTimer = [NSTimer scheduledTimerWithTimeInterval:1.2 target:self selector:@selector(updateProgress) userInfo:nil repeats:YES];
    }
}

- (void)checkSoftAPmode:(NSString *)arg {
    if (_isSoftAPmode == NO){
        return;
    }
    
    if ([XL_GCD_TIMER_MANAGER verifyGCDTimerWithTimerName:kCheckSoftApModeTimerName]){
        return;
    }
    
    NSLog(@"!!! checkSoftAPmode !!!");
    
    NSString *nowSSID = arg;
    __block int count = 0;
    
    [XL_GCD_TIMER_MANAGER scheduledGCDTimerWithTimerName:kCheckSoftApModeTimerName timeInterval:1 queque:dispatch_get_main_queue() repeats:YES removeCancel:YES task:^{
        count++;
        _softAP_SSID = [SimpleConfigUtil fetchCurrSSID];
        NSLog(@"%d)checkSoftAPmode isSoftAPmode:%@", count, _softAP_SSID);
        //超时或者连到对应的ap模式后，进行下一步的操作
        if(count>=kCheckSoftApModeTimeout || ![nowSSID isEqualToString:_softAP_SSID]){
            
            _configTimer = [NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(configTimerHandler:) userInfo:nil repeats:YES];
            [self startApModeConfigWithCount:count];
            [XL_GCD_TIMER_MANAGER cancelGCDTimerWithTimerName:kCheckSoftApModeTimerName];
        }
    }];
}

- (void)startApModeConfigWithCount:(uint16_t)count{
    //连上之后开始配网
    //NSString *softAP_BSSID = @"";
    NSString *msg = @"";
    
    BOOL b1 = [self.simpleConfig rtk_sc_isMyDevice:_softAP_SSID :(int) [_softAP_SSID length]] == NO;
    BOOL b2 = _softAP_SSID.length > 0;
    if (b1 && b2) {
        _isSoftAPmode = NO;
        _isTipSoftAP = NO;
        self.wifiConfigProgress = 0;
        [self stopWaitingProgress];
        
        msg = [NSString stringWithFormat:@"当前连接的wifi(%@)不是设备的AP热点", _softAP_SSID];
        
        [self alertHintWithMessage:msg handler:^(UIAlertAction *action, NSUInteger tag) {
            [self leftBarButtonItemClicked:nil];
        }];
    } else {
        if (count < kCheckSoftApModeTimeout) {
            // 设备不支持5G频段，以2.4G处理
            [self.simpleConfig rtk_sc_setTargetAPis5GBand:NO];
            [self rtkStartWifiConfig];
        } else {
            [self alertHintWithMessage:@"配网超时" handler:^(UIAlertAction *action, NSUInteger tag) {
                self.isSoftAPmode = NO;
                self.isTipSoftAP = NO;
                self.wifiConfigProgress = 0;
                [self stopWaitingProgress];
            }];
        }
    }
}

- (void)stopWaitingProgress {
    [self stopProgressTimer];
    m_context.m_mode = MODE_INIT;
}

- (void)updateProgress {
    
    
    if ([self.simpleConfig.config_list count] >= kConfigDeviceCount) {
        NSLog(@"!!!!! FINISH !!!!!!");
        [self showConfigList];
        
        return;
    }
    
    self.wifiConfigProgress += 1;
    
    int currentProgress = self.wifiConfigProgress;
    
    NSLog(@"Wait %d %%", currentProgress);
    
    if (currentProgress == timeout_cfg_step1) {//when 50%
        [self waitAlertViewClickCancel];
        _isTipSoftAP = YES;
        [self rtkStartWifiConfig];
    } else if (currentProgress == timeout_cfg_step1 + 1) {
        //[self rtk_start_listener:nil];
    } else if (currentProgress >= 100){//when 100%
        [self stopWaitingProgress];
        self.wifiConfigProgress = 0;
        if ([self.simpleConfig.config_list count] > 0) {
            [self showConfigList];
            return;
        } else {
            [self pushToFailedVCWithType:AddDeviceFailedTypeConfigure];
        }
    }
}

- (void)updateDeviceInfo {
    NSLog(@"start_checkDeviceConfigured: %lu / %d", (unsigned long) [self.simpleConfig.config_list count], kConfigDeviceCount);
    if ([self.simpleConfig.config_list count] >= kConfigDeviceCount) {
        NSLog(@"!!!!! FINISH !!!!!!");
        [self showConfigList];
        
        return;
    }
}

- (NSString *)getTargetAPSSID {
    if (self.targetAPSSID.length) {
        return self.targetAPSSID;
    } else {
        return @"";
    }
}

- (void)setTargetAPSSID:(NSString *)SSID {
    if (SSID.length) {
        _targetAPSSID = [NSString stringWithString:SSID];
    }else {
        _targetAPSSID = @"";
    }
}

- (NSString *)getWiFipassword:(NSString *)APname {
    int ap_num = 0;
    NSString *passwordAllData = @"";
    NSString *password = @"";
    
    if (APname == nil || [APname length] == 0) {
        NSLog(@"getWiFipassword: APname is emply");
        return @"";
    }
    
    passwordAllData = [self.APInfo_DataFile objectForKey:@"APINFO"];
    
    NSLog(@"passwordAllData:%@", passwordAllData);
    
    NSArray *APData = [passwordAllData componentsSeparatedByString:@";"];
    NSArray *APItemInfo = nil;
    
    ap_num = (int) [APData count] - 1;
    for (int i = 0; i < ap_num; i++) {
        NSLog(@"<APP> get AP(%d/%d) :%@\n", i + 1, ap_num, [APData objectAtIndex:i]);
        APItemInfo = [[APData objectAtIndex:i] componentsSeparatedByString:@","];
        if ([APname isEqualToString:[APItemInfo objectAtIndex:0]]) {
            password = [APItemInfo objectAtIndex:1];
            return password;
        }
    }
    
    return password;
}

- (Boolean)setWiFipassword:(NSString *)APname :(NSString *)password {
    Boolean isNewAP = true;
    //check previous data
    NSString *preWiFiAllData = @"";
    NSString *storeWiFiData = @"";
    NSString *storeWiFiAllData = @"";
    
    preWiFiAllData = [self.APInfo_DataFile objectForKey:@"APINFO"];
    
    NSArray *APData = [preWiFiAllData componentsSeparatedByString:@";"];
    NSArray *APItemInfo = nil;
    int APNum = (int) [APData count] - 1;
    for (int i = 0; i < APNum; i++) {
        //NSLog(@"<APP> set AP-%d:%@\n", i+1, [APData objectAtIndex:i]);
        APItemInfo = [[APData objectAtIndex:i] componentsSeparatedByString:@","];
        if ([APname isEqualToString:[APItemInfo objectAtIndex:0]]) {
            isNewAP = false;
            break;
        }
    }
    
    if (isNewAP) {
        //new
        if (preWiFiAllData == nil){
            preWiFiAllData = @"";
        }
        storeWiFiAllData = [preWiFiAllData stringByAppendingString:APname];
        storeWiFiAllData = [storeWiFiAllData stringByAppendingString:@","];
        storeWiFiAllData = [storeWiFiAllData stringByAppendingString:password];
        storeWiFiAllData = [storeWiFiAllData stringByAppendingString:@";"];
    } else {
        //update
        for (int i = 0; i < APNum; i++) {
            APItemInfo = [[APData objectAtIndex:i] componentsSeparatedByString:@","];
            if ([APname isEqualToString:[APItemInfo objectAtIndex:0]]) {
                storeWiFiData = [NSString stringWithFormat:@"%@,%@;", [APItemInfo objectAtIndex:0], password];
            } else {
                storeWiFiData = [NSString stringWithFormat:@"%@,%@;", [APItemInfo objectAtIndex:0], [APItemInfo objectAtIndex:1]];
            }
            storeWiFiAllData = [storeWiFiAllData stringByAppendingString:storeWiFiData];
        }
    }
    
    [self.APInfo_DataFile setValue:storeWiFiAllData forKey:@"APINFO"];
    
    return true;
}

#pragma mark - UI


#pragma mark - Action

- (void)leftBarButtonItemClicked:(UIBarButtonItem *)barBtn {
    [XL_GCD_TIMER_MANAGER cancelGCDTimerWithTimerName:kCheckSoftApModeTimerName];
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)pushToAddDeviceListVC {
    CFAbsoluteTime startTime = CFAbsoluteTimeGetCurrent();
    
    if (self.addDeviceListVC == nil) {
        self.addDeviceListVC = [[AddDeviceListViewController alloc] init];
        self.addDeviceListVC.delegate = self;
        self.addDeviceListVC.helper = self.xlScanDeviceHelperTool;
        self.addDeviceListVC.productId = self.productId;
    }
    [self.navigationController pushViewController:self.addDeviceListVC animated:YES];
    CFAbsoluteTime endTime = (CFAbsoluteTimeGetCurrent() - startTime);
    NSLog(@"pushToAddDeviceListVC耗时: %f ms", endTime * 1000.0);
}

/**
 跳转到失败页面
 */
- (void)pushToFailedVCWithType:(AddDeviceFailedType) type {
    AddDeviceFailViewController *fail = [[AddDeviceFailViewController alloc] init];
    fail.type = type;
    [self.navigationController pushViewController:fail animated:YES];
}

#pragma mark - Request

- (void)requestDevice {
    
    //产品ID
    NSArray *productIds = @[self.productId];
    
    if ([self.xlScanDeviceHelperTool.task isStarted]) {
        NSLog(@"ScanDeviceTask is started!");
        return;
    }
    
    [self.xlScanDeviceHelperTool createScanTaskWithProductIds:productIds];
    
    //每次需要重新指定代理，因为代理有可能在添加设备列表被释放
    self.xlScanDeviceHelperTool.delegate = self;
    
    NSLog(@"ScanDeviceTask start!");
    [self.xlScanDeviceHelperTool.task start];
}

#pragma mark - Data
         
- (void)configureAlertControWithTag:(NSInteger)tag
{
    if (tag == 0) {
        //取消
        _isSoftAPmode = NO;
        [self leftBarButtonItemClicked:nil];
    } else if (tag == 1) {
        //确定
        [self checkSoftAPmode:self.nowSSID];
    }
}

#pragma mark - XLScanDeviceHelperToolDelegate

/**
 扫描到设备
 */
- (void)xlScanDeviceHelperTool:(XLScanDeviceHelperTool *)tool scanDevicesSuccssfully:(NSMutableArray<XDevice *> *)devices {
    
    if (self.addDeviceListVC != nil) {
        [self.addDeviceListVC refreshData];
        return;
    }
    
    [self pushToAddDeviceListVC];
}

/**
 扫描不到设备
 */
- (void)xlScanDeviceHelperTool:(XLScanDeviceHelperTool *)tool scanDevicesFailedWithError:(NSError *)error {
    [self pushToFailedVCWithType:AddDeviceFailedTypeScan];
}

/**
 扫描完成
 */
- (void)xlScanDeviceHelperToolScranFinished:(XLScanDeviceHelperTool *)tool
{
    [self.addDeviceListVC updateUIWithScranFinished:YES];
}

#pragma mark - AddDeviceListViewControllerDelegate

- (void)addDeviceListViewController:(AddDeviceListViewController *)vc didClickedBtnRefresh:(UIButton *)btnRefresh
{
    //扫描设备
    [self requestDevice];
}

#pragma mark - private
- (void)presentAlertController:(UIAlertController *)alertVC{
    UIViewController *topRootViewController = [[UIApplication sharedApplication] keyWindow].rootViewController;
    // 在这里加一个这个样式的循环
    while (topRootViewController.presentedViewController) { // 这里固定写法
        topRootViewController = topRootViewController.presentedViewController;
    }
    // 然后再进行present操作
    [topRootViewController presentViewController:alertVC animated:YES completion:nil];
}


@end
