//
//  AddDeviceFailViewController.m
//  vatti
//
//  Created by 李叶 on 2018/6/4.
//  Copyright © 2018年 xlink. All rights reserved.
//

#import "AddDeviceFailViewController.h"
#import "DeviceGuideViewController.h"

@interface AddDeviceFailViewController ()

@property (weak, nonatomic) IBOutlet UILabel *promptTitle;

@property (weak, nonatomic) IBOutlet UILabel *errorInfo1;
@property (weak, nonatomic) IBOutlet UILabel *eroorInfo2;
@property (weak, nonatomic) IBOutlet UILabel *errorInfo3;
@property (weak, nonatomic) IBOutlet UILabel *errorInfo4;

@property (weak, nonatomic) IBOutlet UIButton *btnRetry;
@end

@implementation AddDeviceFailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
}

- (void)initSubViews{
    [super initSubViews];
    
    self.navigationItem.title = @"添加设备";
    
    switch (self.type) {
        case AddDeviceFailedTypeConfigure:
            self.promptTitle.text = @"配网失败";
            
            self.errorInfo1.text = @"1.请确保设备正在通电状态。";
            self.eroorInfo2.text = @"2.请确保WiFi已连接RSC-XXX。";
            self.errorInfo3.text = @"3.请检查无线网络是否正常。";
            self.errorInfo4.text = @"";
            break;
        case AddDeviceFailedTypeSubscribe:
            self.promptTitle.text = @"添加设备失败";
            
            self.errorInfo1.text = @"1.请确保设备正在通电状态。";
            self.eroorInfo2.text = @"2.请检查无线网络是否正常。";
            self.errorInfo3.text = @"3.长按按钮3秒进行重启，再重新尝试连接。";
            self.errorInfo4.text = @"4.请确保wifi密码输入正确。";
            
            [self.btnRetry setTitle:@"重新连接" forState:UIControlStateNormal];
            break;
        case AddDeviceFailedTypeScan:
            self.promptTitle.text = @"未搜索到设备";
            
            self.errorInfo1.text = @"1.请确保设备正在通电状态。";
            self.eroorInfo2.text = @"2.请检查无线网络是否正常。";
            self.errorInfo3.text = @"";
            self.errorInfo4.text = @"";
            break;
        default:
            self.promptTitle.text = @"未知错误";
            
            self.errorInfo1.text = @"1.请确保设备正在通电状态。";
            self.eroorInfo2.text = @"2.请检查无线网络是否正常。";
            self.errorInfo3.text = @"";
            self.errorInfo4.text = @"";
            break;
    }

    UIBarButtonItem *cancleItem = [[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"btn_left1"] style:(UIBarButtonItemStyleDone) target: self action:@selector(back)];
    cancleItem.tintColor = [UIColor darkGrayColor];
    self.navigationItem.leftBarButtonItem = cancleItem;
}

- (void)back{
    NSArray *vcArray = self.navigationController.viewControllers;
    for (id vc in vcArray) {
        BOOL b1 = [[vc class] isEqual:[DeviceGuideViewController class]];
        if (b1 == YES) {
            [self.navigationController popToViewController:vc animated:YES];
            break;
        }
    }
}

- (IBAction)retryAction:(UIButton *)sender {
    NSArray *vcArray = self.navigationController.viewControllers;
    for (id vc in vcArray) {
        BOOL b1 = [[vc class] isEqual:[DeviceGuideViewController class]];
        if (b1 == YES) {
            [self.navigationController popToViewController:vc animated:YES];
            break;
        }
    }
}



@end
