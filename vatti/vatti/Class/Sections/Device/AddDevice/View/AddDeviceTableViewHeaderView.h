//
//  AddDeviceTableViewHeaderView.h
//  vatti
//
//  Created by 李叶 on 2018/6/1.
//  Copyright © 2018年 xlink. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AddDeviceTableViewHeaderView : UITableViewHeaderFooterView

/**
 根据搜索状态更新lab文本
 */
- (void)updateLabStateWithIsSearching:(BOOL)isSearching;

@end
