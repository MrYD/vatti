//
//  AddDeviceTableViewHeaderView.m
//  vatti
//
//  Created by 李叶 on 2018/6/1.
//  Copyright © 2018年 xlink. All rights reserved.
//

#import "AddDeviceTableViewHeaderView.h"

@interface AddDeviceTableViewHeaderView()

@property (weak, nonatomic) IBOutlet UILabel *labSearchState;

@end

@implementation AddDeviceTableViewHeaderView

- (void)updateLabStateWithIsSearching:(BOOL)isSearching
{
    if (isSearching == YES) {
        self.labSearchState.text = @"正在搜索设备...";
    }else{
        self.labSearchState.text = @"搜索完毕";
    }
}

@end
