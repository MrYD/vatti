//
//  SelectionDeviceCell.h
//  vatti
//
//  Created by 彭英科 on 2018/8/8.
//  Copyright © 2018年 xlink. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SelectionDeviceCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *title;

@property (weak, nonatomic) IBOutlet UIImageView *selectionStateImage;

@end
