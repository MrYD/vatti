//
//  SearchDeviceCell.h
//  vatti
//
//  Created by 李叶 on 2018/6/1.
//  Copyright © 2018年 xlink. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SearchDeviceCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *labName;
@property (weak, nonatomic) IBOutlet UILabel *labMac;

@property (weak, nonatomic) IBOutlet UIImageView *ivSelected;

@property (strong, nonatomic) XDevice *device;

@end
