//
//  BathViewController.m
//  vatti
//
//  Created by BZH on 2018/6/10.
//  Copyright © 2018年 xlink. All rights reserved.
//

#import "BathViewController.h"
#import "BathListCell.h"
#import "BathSetViewController.h"
#import "XLUtilTool.h"
#import "UIColor+XLCategory.h"

@interface BathViewController ()<UITableViewDelegate,UITableViewDataSource,BathSetViewControllerDelegate>

@property (weak,   nonatomic) IBOutlet UITableView *tableview;
@property (weak,   nonatomic) IBOutlet UIButton *addBath;

@property (strong, nonatomic) NSMutableArray *dataSource;

@property (assign, nonatomic) NSInteger indexPath;

@end

static NSString *const XLBathListCell = @"XLBathListCell";

@implementation BathViewController

#pragma mark - LifeCycle
- (void)viewDidLoad {
    [super viewDidLoad];
}

- (void)initSubViews{
    [super initSubViews];
    
    //naiv
    self.navigationItem.title = XLLocalizeString(@"浴缸");
    
    //tableView
    [self.tableview registerNib:[UINib nibWithNibName:NSStringFromClass([BathListCell class]) bundle:nil] forCellReuseIdentifier:XLBathListCell];
}

- (void)buildData
{
    [super buildData];
    
    @weakify(self)
    
    [self reloadTableview];
    
    //浴缸1水量发生变化
    [[RACObserve(self.deviceModel, bath1_water) deliverOnMainThread] subscribeNext:^(id  _Nullable x) {
        @strongify(self)
        [self reloadTableview];
    }];
    
    //浴缸2水量发生变化
    [[RACObserve(self.deviceModel, bath2_water) deliverOnMainThread] subscribeNext:^(id  _Nullable x) {
        @strongify(self)
        [self reloadTableview];
    }];
    
    //浴缸选中状态发生变化
    [[RACObserve(self.deviceModel, bath_switch) deliverOnMainThread] subscribeNext:^(id  _Nullable x) {
        @strongify(self)
        [self reloadTableview];
    }];

}

#pragma mark - UI

- (void)reloadTableview {
    //处理数据
    [self configureData];
    
    //更新UI
    [self updateUI];
    
}

- (void)updateUI
{
    self.addBath.hidden = self.dataSource.count == 2;
    
    [self.tableview reloadData];
}

#pragma mark - Action

- (IBAction)addBathAction:(UIButton *)sender {
    if (self.dataSource.count < 2){
        
        BathSetViewController *bathSet = [[BathSetViewController alloc] init];
        bathSet.index = 1;
        bathSet.delegate = self;
        bathSet.deviceModel = self.deviceModel;
        bathSet.dataList = [NSMutableArray arrayWithArray:self.dataSource];
        [self.navigationController pushViewController:bathSet animated:YES];
        
    }
}

/**
 设置数据到数据端点

 @param select YES:点击 NO:删除
 */
- (void)setDataPoint:(NSInteger)value andIndex:(NSInteger)index andSelect:(BOOL)select{
    @weakify(self)
    
    NSArray *arr = nil;

    //开关端点
    XLinkDataPoint *dataPointSelected = [XLinkDataPoint dataPointWithType:2 withIndex:34 withValue:@(value)];
    int waterIndex = index == 35 ? 35 : 36;
    
    //选中
    if (select == YES){
        arr = @[dataPointSelected];
    }else{
        //删除端点
        XLinkDataPoint *dataPointDeleted = [XLinkDataPoint dataPointWithType:2 withIndex:waterIndex withValue:@(0)];
        arr = @[dataPointSelected,dataPointDeleted];
    }
    
    //游客模式判断
    if (XL_DATA_VIRTUAL.isLogin == NO){
        
        [self.deviceModel addDataPoints:arr];
        [self reloadTableview];
        return;
    }
    
    KShowLoading;
    [[XLServiceManager shareInstance].deviceService setDeviceDataPoints:self.deviceModel.device dataPoints:arr timeout:10. handler:^(id result, NSError *error) {
        @strongify(self)
        KHideLoading;

        if (error == nil) {
            [self.deviceModel addDataPoints:arr];
            [self reloadTableview];
        }else{
            KShowErrorLoadingWithMsg(XLHint_SetDataPontFailed);
        }
    }];
}

- (void)alertWithIndex:(int)index{
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"提示" message:[NSString stringWithFormat:@"是否删除浴缸%d",index + 1] preferredStyle:(UIAlertControllerStyleAlert)];
    UIAlertAction *sure = [UIAlertAction actionWithTitle:@"确定" style:(UIAlertActionStyleDefault) handler:^(UIAlertAction * _Nonnull action) {
        NSInteger value = '\0';

        int selectIndex = [[self.dataSource[index] objectForKey:@"index"] intValue];
        if (selectIndex == 35){
            value = 0x02 ;
        }else if (selectIndex == 36){
            value = 0x01;
        }
        
        [self setDataPoint:value andIndex:selectIndex andSelect:NO];
    }];
    UIAlertAction *cancle = [UIAlertAction actionWithTitle:@"取消" style:(UIAlertActionStyleCancel) handler:nil];
    [alert addAction:cancle];
    [alert addAction:sure];
    [self.navigationController presentViewController:alert animated:YES completion:nil];
}

#pragma mark - Data

- (void)configureData
{
    self.dataSource = [NSMutableArray array];

    //浴缸1
    NSMutableDictionary *mDict1 = [NSMutableDictionary dictionary];
    
    BOOL isOpen = [XLDataPointTool isOpenWithValue:self.deviceModel.bath_switch andShift:0];
    [mDict1 setObject:@(isOpen) forKey:@"switch"];
    
    [mDict1 setObject:@(self.deviceModel.bath1_water) forKey:@"water"];
    [mDict1 setObject:@(35) forKey:@"index"];
    
    [self.dataSource addObject:mDict1];
 
    //浴缸2
    if (self.deviceModel.bath2_water != 0){

        NSMutableDictionary *mDict2 = [NSMutableDictionary dictionary];

        BOOL isOpen = [XLDataPointTool isOpenWithValue:self.deviceModel.bath_switch andShift:1];
        [mDict2 setObject:@(isOpen) forKey:@"switch"];
        
        [mDict2 setObject:@(self.deviceModel.bath2_water) forKey:@"water"];
        [mDict2 setObject:@(36) forKey:@"index"];
        
        [self.dataSource addObject:mDict2];
    }
    
}

/**
 判断是否相同设备
 */
- (BOOL)isEqualDevice:(WaterHeaterDeviceModel *)deviceModel
{
    BOOL isEqual = [deviceModel.device isEqualToDevice:self.deviceModel.device];
    
    return isEqual;
}

#pragma mark - UITableViewDataSource & UITableViewDelegate

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.dataSource.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    NSInteger rowNum = indexPath.row;
    
    BathListCell *cell = [tableView dequeueReusableCellWithIdentifier:XLBathListCell];
    
    //数据源
    NSDictionary *dict = [self.dataSource objectAtIndex:rowNum];
    
    //开关
    BOOL isOpen = [[dict objectForKey:@"switch"] boolValue];
    cell.selectImage.hidden = !isOpen;
    
    //姓名
    cell.name.text = [NSString stringWithFormat:@"浴缸%ld", rowNum + 1];
    
    //水量 100L
    int waterValue = [[dict objectForKey:@"water"] intValue];
    cell.bathWater.text = [NSString stringWithFormat:@"出水量： %d L",waterValue*10];
    
    //分割线
    if (self.dataSource.count == 1) {
        cell.lineView.hidden = NO;
    }else{
        if (rowNum == self.dataSource.count - 1) {
            cell.lineView.hidden = YES;
        }else{
            cell.lineView.hidden = NO;
        }
    }

    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 50.;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];

    NSInteger rowNum = indexPath.row;
    
    NSInteger valueSelecte = '\0';

    int selectIndex = [[self.dataSource[rowNum] objectForKey:@"index"] intValue];
    
    if (selectIndex == 35){
        valueSelecte = 0x01;
        
    }else if (selectIndex == 36){
        valueSelecte = 0x02;
    }

    [self setDataPoint:valueSelecte andIndex:selectIndex andSelect:YES];
    
}

/**
 左滑cell时出现按钮代理
 */
- (NSArray *)tableView:(UITableView *)tableView editActionsForRowAtIndexPath:(NSIndexPath *)indexPath
{
    //编辑
    UITableViewRowAction *actionEidt = [UITableViewRowAction rowActionWithStyle:UITableViewRowActionStyleNormal title:@"编辑" handler:^(UITableViewRowAction *action, NSIndexPath *indexPath) {
        BathSetViewController *bathSet = [[BathSetViewController alloc] init];
        bathSet.index = indexPath.row;
        bathSet.delegate = self;
        bathSet.deviceModel = self.deviceModel;
        bathSet.dataList = self.dataSource;
        [self.navigationController pushViewController:bathSet animated:YES];
    }];
    actionEidt.backgroundColor = [UIColor colorWithHexString:XLGreenColor];
    
    //删除
    UITableViewRowAction *actionDelete = [UITableViewRowAction rowActionWithStyle:UITableViewRowActionStyleNormal title:@"删除" handler:^(UITableViewRowAction *action, NSIndexPath *indexPath) {
        if (indexPath.row == 0) {
            return;
        }
        
        [self alertWithIndex:indexPath.row];
    }];
    
    if (indexPath.row == 0) {
        actionDelete.backgroundColor = [UIColor lightGrayColor];
    }else{
        actionDelete.backgroundColor = [UIColor redColor];
    }
    

    return @[actionDelete,actionEidt];

}

#pragma mark - BathSetViewControllerDelegate

- (void)completeCallBack{
    self.addBath.hidden = YES;
    
    [self reloadTableview];
}

@end
