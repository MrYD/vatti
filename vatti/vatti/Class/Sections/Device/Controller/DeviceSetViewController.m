//
//  DeviceSetViewController.m
//  vatti
//
//  Created by 李叶 on 2018/6/9.
//  Copyright © 2018年 xlink. All rights reserved.
//

#import "DeviceSetViewController.h"
#import "DeviceSetCell.h"
#import "InformationQueryViewController.h"
#import "ShareDeviceViewController.h"
#import "NoSharerViewController.h"
#import "XLinkHttpRequest.h"
#import "HotViewController.h"
#import "BathViewController.h"
#import "AppointmentViewController.h"
#import "XLUtilTool.h"

@interface DeviceSetViewController ()<UITableViewDelegate,UITableViewDataSource>

@property (weak,  nonatomic) IBOutlet UITableView *tabview;

@property (strong, nonatomic) NSArray *dataSourceArray;

@end

static NSString *const XLDeviceSetCell = @"XLDeviceSetCell";

@implementation DeviceSetViewController

#pragma mark - LifeCycle
- (void)viewDidLoad {
    [super viewDidLoad];
}

- (void)initSubViews
{
    [super initSubViews];
    
    //navi
    self.navigationItem.title = XLLocalizeString(@"设置");
    
    //tableView
    [self.tabview registerNib:[UINib nibWithNibName:NSStringFromClass([DeviceSetCell class]) bundle:nil] forCellReuseIdentifier:XLDeviceSetCell];
    
}

- (void)buildData
{
    [super buildData];

    if ([self.deviceModel.device.role intValue] == 1){
        self.dataSourceArray = @[@"信息查询",@"预约设置",@"即热设置",@"浴缸设置"];
    }else{
        self.dataSourceArray = @[@"信息查询",@"预约设置",@"即热设置",@"浴缸设置",@"设备分享"];
    }
}

#pragma mark - UITableViewDataSource & UITableViewDelegate

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.dataSourceArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    DeviceSetCell *cell = [tableView dequeueReusableCellWithIdentifier:XLDeviceSetCell];
    
    cell.name.text = [self.dataSourceArray objectAtIndex:indexPath.row];
    
    if (indexPath.row == self.dataSourceArray.count - 1){
        cell.line.hidden = YES;
    }else{
        cell.line.hidden = NO;
    }
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 50.;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    if (indexPath.row == 0){
        UIStoryboard *sb = [UIStoryboard storyboardWithName:@"Device" bundle:nil];
        InformationQueryViewController *informationQuery = [sb instantiateViewControllerWithIdentifier:@"InformationQueryViewController"];
        informationQuery.deviceModel = self.deviceModel;
        [self.navigationController pushViewController:informationQuery animated:YES];
    }else if (indexPath.row == 1){
        AppointmentViewController *appointment = [[AppointmentViewController alloc] init];
        appointment.deviceModel = self.deviceModel;
        [self.navigationController pushViewController:appointment animated:YES];
    }else if (indexPath.row == 2){
        HotViewController *hot = [[HotViewController alloc] init];
        hot.deviceModel = self.deviceModel;
        [self.navigationController pushViewController:hot animated:YES];
    }else if (indexPath.row == 3){
        BathViewController *bath = [[BathViewController alloc] init];
        bath.deviceModel = self.deviceModel;
        [self.navigationController pushViewController:bath animated:YES];
    }else if (indexPath.row == 4){
        ShareDeviceViewController *share = [[ShareDeviceViewController alloc] init];
        share.deviceModel = self.deviceModel;
        [self.navigationController pushViewController:share animated:YES];
      
    }
}
@end
