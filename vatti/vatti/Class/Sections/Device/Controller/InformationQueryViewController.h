//
//  InformationQueryViewController.h
//  vatti
//
//  Created by BZH on 2018/6/9.
//  Copyright © 2018年 xlink. All rights reserved.
//

/**
 *  信息查询
 *
 *  @author BZH
 */

#import "BaseWaterHeaterDeviceViewController.h"

@interface InformationQueryViewController : BaseWaterHeaterDeviceViewController

@end
