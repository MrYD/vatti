//
//  BathSetViewController.h
//  vatti
//
//  Created by 李叶 on 2018/6/11.
//  Copyright © 2018年 xlink. All rights reserved.
//

#import "BaseViewController.h"
#import "WaterHeaterDeviceModel.h"

@protocol BathSetViewControllerDelegate <NSObject>

@optional

- (void)completeCallBack;

@end


@interface BathSetViewController : BaseViewController

@property (assign, nonatomic) NSInteger index;
@property (strong, nonatomic) WaterHeaterDeviceModel *deviceModel;
@property (strong, nonatomic) NSMutableArray *dataList;

@property (weak,   nonatomic) id<BathSetViewControllerDelegate> delegate;
@end


