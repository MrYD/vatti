//
//  AppointmentViewController.h
//  vatti
//
//  Created by BZH on 2018/6/12.
//  Copyright © 2018年 xlink. All rights reserved.
//

/**
 *  预约设置
 *
 *  @author BZH
 */

#import "BaseWaterHeaterDeviceViewController.h"

@interface AppointmentViewController : BaseWaterHeaterDeviceViewController

@end
