//
//  ShareViewController.m
//  vatti
//
//  Created by 李叶 on 2018/6/10.
//  Copyright © 2018年 xlink. All rights reserved.
//

#import "ShareViewController.h"
#import "XLTextField.h"
#import "XLUtilTool.h"
//#import <UIViewController+JKBackButtonTouched.h>
//#import <UINavigationController+JKStackManager.h>
#import <MJExtension/MJExtension.h>

@interface ShareViewController ()

@property (weak,   nonatomic) IBOutlet XLTextField *sharePhone;
@property (weak,   nonatomic) IBOutlet UIButton *shareBtn;

@end

@implementation ShareViewController

- (void)viewDidLoad {
    [super viewDidLoad];
}

- (void)initSubViews{
    [super initSubViews];
    
    //navi
    self.navigationItem.title = XLLocalizeString(@"设备分享");
}

- (void)initConfigure
{
    [super initConfigure];
    
    @weakify(self)
    
    [self.sharePhone observeSelectedState];
    
    RAC(self.shareBtn, enabled) = [RACSignal combineLatest:@[self.sharePhone.rac_textSignal] reduce:^(NSString *account){
        @strongify(self)
        return @(account.length);
    }];
}

#pragma mark - Action
- (IBAction)shareAction:(UIButton *)sender {
    if ([XLUtilTool validatePhone:self.sharePhone.text] == NO){
        [self alertHintWithMessage:@"请输入正确的账号（手机号）"];
        return;
    }
    
    //游客
    if (XL_DATA_VIRTUAL.isLogin == NO){
        return;
    }
    
    if ([self.sharePhone.text isEqualToString:XL_DATA_SOURCE.user.account]){
        [self alertHintWithMessage:@"不能分享设备给自己"];
        return;
    }
    
    [self requestShareDevice];
}

#pragma mark - Data
- (void)configureWithResult:(id)result andError:(NSError*)err
{
    @weakify(self)
    
    if (err == nil){
        [self alertHintWithMessage:@"邀请已发送，等待对方确认" handler:^(UIAlertAction *action, NSUInteger tag) {
            @strongify(self)
            
            [kXLNotificationCenter postNotificationName:XLDeviceMemberListChangeNofication object:nil];
            [self.navigationController popViewControllerAnimated:YES];
        }];
    }else{
        if (err.code == XLinkErrorCodeApiUserNotExists){
            [self alertHintWithMessage:@"请输入正确的账号（手机号）"];
        }else{
            [self showNetworkError];
        }
    }
}

#pragma mark - Request

/**
 分享设备
 */
- (void)requestShareDevice
{
    @weakify(self)
    
    NSNumber *deviceID = [NSNumber numberWithUnsignedInteger:self.deviceModel.device.deviceID];

    KShowLoading;
    [XLinkHttpRequest shareDeviceWithDeviceID:deviceID withAccessToken:[XLinkSDK share].userModel.access_token withShareUserAccount:self.sharePhone.text withMode:@"app" withExpire:@"3600" withExtend:nil withAuthority:nil didLoadData:^(id result, NSError *err) {
        @strongify(self)
        KHideLoading;
        
        [self configureWithResult:result andError:err];
        
    }];
}


@end
