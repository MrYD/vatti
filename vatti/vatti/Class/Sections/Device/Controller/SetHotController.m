//
//  SetHotController.m
//  vatti
//
//  Created by 李叶 on 2018/6/10.
//  Copyright © 2018年 xlink. All rights reserved.
//

#import "SetHotController.h"
#import "UIColor+XLCategory.h"

#define BeiShu 1000

@interface SetHotController ()<UIPickerViewDelegate,UIPickerViewDataSource>

@property (weak,   nonatomic) IBOutlet UIPickerView *pickerView;

@property (strong, nonatomic) NSArray *selectParticle;
@property (strong, nonatomic) NSArray *unit;

@property (assign, nonatomic) int selectValue;
@property (assign, nonatomic) int selectRow;
@property (assign, nonatomic) int minutes;

@end

@implementation SetHotController

- (void)viewDidLoad {
    [super viewDidLoad];
}

- (void)initSubViews{
    [super initSubViews];
    
    //pickerView
    self.pickerView.showsSelectionIndicator = YES;
    
    //navi
    UIBarButtonItem *addItem = [[UIBarButtonItem alloc]initWithTitle:@"完成" style:(UIBarButtonItemStyleDone) target:self action:@selector(rightAction)];
    [addItem setTitleTextAttributes:@{NSForegroundColorAttributeName:[UIColor colorWithHexString:XLGreenColor],NSFontAttributeName : [UIFont systemFontOfSize:15]} forState:UIControlStateNormal];
    [addItem setTitleTextAttributes:@{NSForegroundColorAttributeName:[UIColor grayColor],NSFontAttributeName : [UIFont systemFontOfSize:15]} forState:UIControlStateDisabled];
    self.navigationItem.rightBarButtonItem = addItem;
    
}

- (void)buildData
{
    [super buildData];
    
    //initDataSouce
    [self initDataSouce];
    
    //type
    if (self.type == 0){
        [self.pickerView selectRow:self.defaultValue - 3 inComponent:0 animated:YES];
    }else if (self.type == 1){
        [self.pickerView selectRow:self.defaultValue - 20 inComponent:0 animated:YES];
    }else if (self.type == 3){
        NSInteger selectRow = self.defaultValue - 1;
        [self.pickerView selectRow:selectRow inComponent:0 animated:YES];
    }else if (self.type == 4 || self.type == 5){
        NSInteger selectRowH = self.startHours   + 24 * (BeiShu/2);
        NSInteger selectRowM = self.startMinutes + 60 * (BeiShu/2);
        
        [self.pickerView selectRow:selectRowH inComponent:0 animated:YES];
        [self.pickerView selectRow:selectRowM inComponent:1 animated:YES];
    }
}

- (void)initDataSouce{
    if (self.type == 0){
        self.navigationItem.title = XLLocalizeString(@"保温温差");
        self.unit = [[NSArray alloc] initWithObjects:@"℃",nil];
        self.selectParticle = [self getArrayWithStartValue:3 endValue:15 isBath:NO];
    }else if (self.type == 1){
        self.navigationItem.title = XLLocalizeString(@"保温时长");
        if (self.defaultValue < 20) {
            self.defaultValue = 20;
        }else if(self.defaultValue > 99) {
            self.defaultValue = 99;
        }
        self.unit = [[NSArray alloc] initWithObjects:@"min",nil];
        self.selectParticle = [self getArrayWithStartValue:20 endValue:99 isBath:NO];
    }else if (self.type == 3){
        self.navigationItem.title = XLLocalizeString(@"出水量");
        self.selectValue = self.defaultValue * 10;
        self.unit = [[NSArray alloc] initWithObjects:@"L",nil];
        self.selectParticle = [self getArrayWithStartValue:1 endValue:60 isBath:YES];
    }else if (self.type == 4){
        self.navigationItem.title = XLLocalizeString(@"开始时间");
        self.unit = [self getArrayWithStartValue:0 endValue:59 isBath:NO];
        self.selectParticle = [self getArrayWithStartValue:0 endValue:23 isBath:NO];
    }else if (self.type == 5){
        self.navigationItem.title = XLLocalizeString(@"结束时间");
        self.unit = [self getArrayWithStartValue:0 endValue:59 isBath:NO];
        self.selectParticle = [self getArrayWithStartValue:0 endValue:23 isBath:NO];
    }else if (self.type == 6){
        self.navigationItem.title = XLLocalizeString(@"预约设置");
        self.unit = [[NSArray alloc] initWithObjects:@"℃",nil];
        self.selectParticle = [self getArrayWithStartValue:35 endValue:50 isBath:NO];
    }
}

- (NSArray *)getArrayWithStartValue:(int)start endValue:(int)end isBath:(BOOL)isBath{
    NSMutableArray *dataArray = [NSMutableArray array];
    
    for (int i=start;i<=end;i++){
        if (isBath == YES){
            [dataArray addObject:[NSString stringWithFormat:@"%d",i*10]];
        }else{
            [dataArray addObject:[NSString stringWithFormat:@"%d",i]];
        }
    }
    
    if (self.type == 4 || self.type == 5) {
        NSMutableArray *copyDataArray = [NSMutableArray array];
        
        for (int i=0; i<BeiShu; i++) {
            [copyDataArray addObjectsFromArray:dataArray];
        }
        
        return copyDataArray;
    }
    
    
    return [NSArray arrayWithArray:dataArray];
}

#pragma mark - UIPickerViewDataSource & UIPickerViewDelegate

/**
 列数
 */
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView {
    return 2;
}

/**
 每列个数
 */
- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
    if (component == 0) {
        return self.selectParticle.count;
    }else{
        return self.unit.count;
    }
}

/**
 每列宽度
 */
- (CGFloat)pickerView:(UIPickerView *)pickerView widthForComponent:(NSInteger)component {
    return 60.0f;
}

/**
 返回选中的行
 */
- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    NSString *str;
    if (component == 0) {
        str = [self.selectParticle objectAtIndex:row];
        self.selectValue = [str intValue];
    }else{
        str = [self.unit objectAtIndex:row];
        self.minutes = [str intValue];
    }
    
}

/**
 返回当前行的内容,此处是将数组中数值添加到滚动的那个显示栏上
 */
- (NSString*)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    if (component == 0) {
        return [self.selectParticle objectAtIndex:row];
    } else {
        return [self.unit objectAtIndex:row];
        
    }
}

#pragma mark - Action
- (void)rightAction{
    if (self.type == 0){
        if (self.delegate && [self.delegate respondsToSelector:@selector(selectDataWithValue:type:)]){
            [self.delegate selectDataWithValue:self.selectValue type:self.type];
        }
    }else if (self.type == 1){
        if (self.delegate && [self.delegate respondsToSelector:@selector(selectDataWithValue:type:)]){
            [self.delegate selectDataWithValue:self.selectValue type:self.type];
        }
    }else if (self.type == 2){
        if (self.delegate && [self.delegate respondsToSelector:@selector(selectDataWithValue:type:)]){
            [self.delegate selectDataWithValue:self.selectValue type:self.type];
        }
    }else if (self.type == 3){
        if (self.delegate && [self.delegate respondsToSelector:@selector(selectDataWithValue:type:)]){
            [self.delegate selectDataWithValue:self.selectValue type:self.type];
        }
    }else if (self.type == 4){
        if (self.delegate && [self.delegate respondsToSelector:@selector(appointmentvalueCallBackWithHours:Minutes:isStart:)]){
            [self.delegate appointmentvalueCallBackWithHours:self.selectValue Minutes:self.minutes isStart:YES];
        }
    }else if (self.type == 5){
        if (self.delegate && [self.delegate respondsToSelector:@selector(appointmentvalueCallBackWithHours:Minutes:isStart:)]){
            [self.delegate appointmentvalueCallBackWithHours:self.selectValue Minutes:self.minutes isStart:NO];
        }
    }else if (self.type == 6){
        if (self.delegate && [self.delegate respondsToSelector:@selector(selectDataWithValue:type:)]) {
            [self.delegate selectDataWithValue:self.selectValue type:self.type];
        }
    }
    
    [self.navigationController popViewControllerAnimated:YES];
}

@end
