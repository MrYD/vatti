//
//  ShareDeviceViewController.m
//  vatti
//
//  Created by 李叶 on 2018/6/9.
//  Copyright © 2018年 xlink. All rights reserved.
//

#import "ShareDeviceViewController.h"
#import "UIImage+XLCategory.h"
#import "ShareMemberCell.h"
#import "ShareViewController.h"
#import "XLUtilTool.h"
#import "NoSharerViewController.h"
#import "XLinkHandleShareDeviceTask.h"
#import "MJRefresh.h"
#import "LoginViewController.h"
#import "BaseNavigationViewController.h"

@interface ShareDeviceViewController ()<UITableViewDelegate,UITableViewDataSource>

//tableView
@property (weak,   nonatomic) IBOutlet UITableView *tabview;

///提示
@property (weak,   nonatomic) IBOutlet UILabel *labTips;

@property (strong, nonatomic) NSMutableArray <MemberModel *> *dataSourceArray;

@end

static NSString *const XLShareMemberCell = @"XLShareMemberCell";

@implementation ShareDeviceViewController

#pragma mark - LifeCycle

- (void)viewDidLoad {
    [super viewDidLoad];
}

- (void)initConfigure
{
    [super initConfigure];
    
    @weakify(self)
    [[[kXLNotificationCenter rac_addObserverForName:XLDeviceMemberListChangeNofication object:nil] deliverOnMainThread] subscribeNext:^(NSNotification * _Nullable x) {
        @strongify(self)
        [self.tabview.mj_header beginRefreshing];
    }];

}

- (void)initSubViews{
    [super initSubViews];
    
    @weakify(self)
    
    //navi
    self.navigationItem.title = XLLocalizeString(@"设备分享");
    
    //tableView
    [self.tabview registerNib:[UINib nibWithNibName:NSStringFromClass([ShareMemberCell class]) bundle:nil] forCellReuseIdentifier:XLShareMemberCell];
    
    self.tabview.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        @strongify(self)
        [self getShareList];
    }];
    
}

- (void)buildData
{
    [super buildData];
    
    [self.tabview.mj_header beginRefreshing];
}

#pragma mark - Action
- (IBAction)shareAction:(UIButton *)sender {
    
    if (XL_DATA_VIRTUAL.isLogin == NO){
        
        LoginViewController *login = [[LoginViewController alloc] init];

        BaseNavigationViewController *nvc = [[BaseNavigationViewController alloc] initWithRootViewController:login];
        [nvc.navigationBar setBackgroundImage:[UIImage xl_imageForColor:[UIColor colorWithRed:255/255. green:255/255. blue:255/255. alpha:1]] forBarMetrics:UIBarMetricsDefault];
        [XLUtilTool setRootViewController:nvc animationOptions:UIViewAnimationOptionTransitionCrossDissolve duration:0.3];
        
        return;
    }
    
    ShareViewController *share = [[ShareViewController alloc] init];
    share.deviceModel = self.deviceModel;
    
    [self.navigationController pushViewController:share animated:YES];
}

#pragma mark - UI

- (void)updateUI
{
    BOOL b1 = self.dataSourceArray.count > 0;
    
    self.labTips.hidden = b1;
    
    [self.tabview reloadData];
}

#pragma mark - Request

/**
 获取分享列表
 */
- (void)getShareList
{
    @weakify(self)
    XLinkSDK *sdk = [XLinkSDK share];
    NSLog(@"token:%@",sdk.userModel.access_token);
    [XLinkHttpRequest getShareListWithAccessToken:[XLinkSDK share].userModel.access_token didLoadData:^(id result, NSError *err) {
        @strongify(self)
        
        [self.tabview.mj_header endRefreshing];
        
        if (err == nil){
            
            //data
            NSArray *dataArray = result;
            NSMutableArray *membersArray = [NSMutableArray array];
            
            for (NSDictionary *dict in dataArray){
                NSNumber *userId   = [dict objectForKey:@"from_id"];
                NSNumber *deviceid = [dict objectForKey:@"device_id"];
                
                NSNumber *currentUserID = XL_DATA_SOURCE.user.userId;
                BOOL b1 = [userId isEqual:currentUserID];
                BOOL b2 = self.deviceModel.device.deviceID == deviceid.integerValue;
                if (b1 && b2){
                    MemberModel *member = [[MemberModel alloc] init];
                    [member mj_setKeyValues:dict];
                    member.device_id = deviceid;
                    [membersArray addObject:member];
                }
            }
            
            //倒序
            self.dataSourceArray = [NSMutableArray arrayWithArray:[[membersArray reverseObjectEnumerator] allObjects]];
            
            
        }
        //UI
        [self updateUI];
    }];
}

/**
 取消分享
 */
- (void)requestShareDeviceActionCancelWithInviteCode:(NSString *)invitecode
{
    KShowLoading;
    XLinkHandleShareDeviceTask *task = [XLinkHandleShareDeviceTask handleShareDeviceTaskWithInviteCode:invitecode handleShareAction:XLinkHandleShareDeviceActionCancel timeout:10 completionHandler:^(id result, NSError *error) {
        
        KHideLoading;
        if (error == nil){
            [self getShareList];
        }else{
            KShowErrorLoadingWithMsg(@"取消失败");
        }
        
    }];
    [task start];
}

#pragma mark - UITableViewDataSource & UITableViewDelegate

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.dataSourceArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    ShareMemberCell *cell = [tableView dequeueReusableCellWithIdentifier:XLShareMemberCell];
    cell.memberModel = self.dataSourceArray[indexPath.row];
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 60.0f;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    NSInteger rowNum = indexPath.row;
    
    if (XL_DATA_VIRTUAL.isLogin == NO){
        return;
    }
    
    MemberModel *memberModel = [self.dataSourceArray objectAtIndex:rowNum];
    NSString *state      = memberModel.state;
    NSString *invitecode = memberModel.invite_code;
    NSString *name       = memberModel.to_name;
    
    [self configureSelectedWithState:state andInvitecode:invitecode andName:name];
}

- (void)configureSelectedWithState:(NSString *)state andInvitecode:(NSString*)invitecode andName:(NSString *)name{
    
    @weakify(self)
    
    NSString *title = @"";
    NSString *message = @"";
    
    if ([state isEqualToString:@"pending"] == YES) {
        title = @"取消分享";
        message = [NSString stringWithFormat:@"是否确定取消分享给%@？",name];
    }else if ([state isEqualToString:@"accept"] == YES){
        title = @"解除分享";
        message = [NSString stringWithFormat:@"是否确定解除%@的设备管理权限？",name];
    }else {
        return;
    }
    
    UIAlertController *sheet = [UIAlertController alertControllerWithTitle:nil message:nil preferredStyle:(UIAlertControllerStyleActionSheet)];
    
    UIAlertAction *cancelShare = [UIAlertAction actionWithTitle:title style:(UIAlertActionStyleDefault) handler:^(UIAlertAction * _Nonnull action) {
        @strongify(self)
        
        [self alertHintWithMessage:message cancelTitle:@"取消" otherTitles:@[@"确定"] handler:^(UIAlertAction *action, NSUInteger tag) {
            if (tag == 1) {
                [self requestShareDeviceActionCancelWithInviteCode:invitecode];
            }
        }];
    }];
    
    UIAlertAction *cancle = [UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:nil];
    
    [sheet addAction:cancelShare];
    [sheet addAction:cancle];
    
    [self presentViewController:sheet animated:YES completion:nil];
}

@end
