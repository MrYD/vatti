//
//  BathSetViewController.m
//  vatti
//
//  Created by 李叶 on 2018/6/11.
//  Copyright © 2018年 xlink. All rights reserved.
//

#import "BathSetViewController.h"
#import "SetHotController.h"
#import "XLUtilTool.h"

#define MinWaterValue 10
#define MaxWaterValue 600
#define DefaultValue 150

@interface BathSetViewController ()<SelectCallbackDelegate>

@property (weak,   nonatomic) IBOutlet UILabel *water;
@property (weak,   nonatomic) IBOutlet UILabel *BathName;

///浴缸水量
@property (assign, nonatomic) int selectValue;

@end

@implementation BathSetViewController

#pragma mark - LifeCycle
- (void)viewDidLoad {
    [super viewDidLoad];
}

- (void)initSubViews{
    [super initSubViews];
    
    //navi
    [self setupNavi];
    
    //lab
    [self setupLabWater];
    
}

#pragma mark - UI
- (void)setupNavi
{
    NSString *name;
    if (self.index == 0) {
        name = @"浴缸1";
    }else if (self.index == 1){
        name = @"浴缸2";
    }
    
    self.navigationItem.title = XLLocalizeString(name);
    
    //完成
    UIBarButtonItem *addItem = [[UIBarButtonItem alloc]initWithTitle:@"完成" style:(UIBarButtonItemStyleDone) target:self action:@selector(complete)];
    [addItem setTitleTextAttributes:@{NSForegroundColorAttributeName:[UIColor blackColor],NSFontAttributeName : [UIFont systemFontOfSize:15]} forState:UIControlStateNormal];
    [addItem setTitleTextAttributes:@{NSForegroundColorAttributeName:[UIColor grayColor],NSFontAttributeName : [UIFont systemFontOfSize:15]} forState:UIControlStateDisabled];
    self.navigationItem.rightBarButtonItem = addItem;
    
    //取消
    UIBarButtonItem *cancleItem = [[UIBarButtonItem alloc]initWithTitle:@"取消" style:(UIBarButtonItemStyleDone) target:self action:@selector(cancle)];
    [cancleItem setTitleTextAttributes:@{NSForegroundColorAttributeName:[UIColor blackColor],NSFontAttributeName : [UIFont systemFontOfSize:15]} forState:UIControlStateNormal];
    [cancleItem setTitleTextAttributes:@{NSForegroundColorAttributeName:[UIColor grayColor],NSFontAttributeName : [UIFont systemFontOfSize:15]} forState:UIControlStateDisabled];
    self.navigationItem.leftBarButtonItem = cancleItem;
    
    //浴缸名称
    self.BathName.text = [NSString stringWithFormat:@"    %@",name];
}

- (void)setupLabWater
{
    int water;
    if (self.index == 0){
        water = self.deviceModel.bath1_water * 10;
    }else if (self.index == 1){
        water = self.deviceModel.bath2_water * 10;
    }
    
    if (water < MinWaterValue) {
        water = DefaultValue;
    }else if (water > MaxWaterValue){
        water = MaxWaterValue;
    }
    
    self.water.text = [NSString stringWithFormat:@"%d L",water];
    self.selectValue = (water / 10);
}

- (void)complete{
    
    //游客模式
    if (XL_DATA_VIRTUAL.isLogin == NO){
        if (self.index == 0){
            self.deviceModel.bath1_water = self.selectValue;
        }else if (self.index == 1){
            self.deviceModel.bath2_water = self.selectValue;
        }
        
        if (self.delegate && [self.delegate respondsToSelector:@selector(completeCallBack)]){
            [self.delegate completeCallBack];
            [self.navigationController popViewControllerAnimated:YES];
        }
    }else{
        int dataPointIndex = 0;
        if (self.index == 0) {
            dataPointIndex = 35;
        }else if (self.index == 1) {
            dataPointIndex = 36;
        }
        
        [self setDataPointWithValue:self.selectValue index:dataPointIndex type:2];
    }
    
}

#pragma mark - Action
- (void)cancle{
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)selectWaterAciton:(UIButton *)sender {
    
    SetHotController *setHot = [[SetHotController alloc]init];
    setHot.type = 3;
    setHot.delegate = self;
    setHot.defaultValue = self.selectValue;
    setHot.deviceModel = self.deviceModel;
    [self.navigationController pushViewController:setHot animated:YES];
}

- (void)setDataPointWithValue:(int)value index:(int)index type:(int)type{
    
    @weakify(self)

    //水量
    XLinkDataPoint *dataPointWater = [XLinkDataPoint dataPointWithType:type withIndex:index withValue:@(value)];

    KShowLoading;
    [[XLServiceManager shareInstance].deviceService setDeviceDataPoints:self.deviceModel.device dataPoints:@[dataPointWater] timeout:TimeOutSetDataPoint handler:^(id result, NSError *error) {
        
        @strongify(self)
        KHideLoading;
        
        if (error == nil) {
            [self.deviceModel addDataPoints:@[dataPointWater]];
            
            if (self.delegate && [self.delegate respondsToSelector:@selector(completeCallBack)]){
                [self.delegate completeCallBack];
            }
            
            [self.navigationController popViewControllerAnimated:YES];
        }else{
            [self showNetworkError];
        }
    }];
}

#pragma mark - SelectCallbackDelegate

- (void)selectDataWithValue:(int)value type:(int)type{
    self.water.text = [NSString stringWithFormat:@"%d L",value];
    self.selectValue = value/10;
}


@end
