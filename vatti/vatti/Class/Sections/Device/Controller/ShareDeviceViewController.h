//
//  ShareDeviceViewController.h
//  vatti
//
//  Created by 李叶 on 2018/6/9.
//  Copyright © 2018年 xlink. All rights reserved.
//

#import "BaseViewController.h"
#import "WaterHeaterDeviceModel.h"

@interface ShareDeviceViewController : BaseViewController

@property (strong, nonatomic) DeviceModel *deviceModel;

@end
