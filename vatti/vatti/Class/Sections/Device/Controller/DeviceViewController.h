//
//  DeviceViewController.h
//  vatti
//
//  Created by MrYD on 2018/12/24.
//  Copyright © 2018年 xlink. All rights reserved.
//

/**
 *  设备列表
 *
 *  @author BZH
 */

#import "BaseDeviceViewController.h"

@interface DeviceViewController : BaseDeviceViewController

@end
