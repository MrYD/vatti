//
//  NoSharerViewController.m
//  vatti
//
//  Created by 李叶 on 2018/6/9.
//  Copyright © 2018年 xlink. All rights reserved.
//

#import "NoSharerViewController.h"
#import "ShareViewController.h"
#import "DeviceSetViewController.h"
//#import <UINavigationController+JKStackManager.h>

@interface NoSharerViewController ()

@end

@implementation NoSharerViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.navigationItem.title = @"分享设备";
}

#pragma mark - Action
- (IBAction)shareDevice:(UIButton *)sender {
    ShareViewController *share = [[ShareViewController alloc]init];
    share.deviceModel = self.deviceModel;
    [self.navigationController pushViewController:share animated:YES];
}

#pragma mark - OverWrite
- (void)leftBarBtnClicked:(UIBarButtonItem *)leftBarBtn
{
    if (self.type == 1){
        NSArray *vcArray = self.navigationController.viewControllers;
        for (id vc in vcArray) {
            BOOL b1 = [[vc class] isEqual:[DeviceSetViewController class]];
            if (b1 == YES) {
                [self.navigationController popToViewController:vc animated:YES];
                break;
            }
        }
    }else{
        [self.navigationController popViewControllerAnimated:YES];
    }
}

@end
