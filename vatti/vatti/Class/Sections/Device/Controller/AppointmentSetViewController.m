//
//  AppointmentSetViewController.m
//  vatti
//
//  Created by 李叶 on 2018/6/12.
//  Copyright © 2018年 xlink. All rights reserved.
//

#import "AppointmentSetViewController.h"
#import "HotCell.h"
#import "SetHotController.h"
#import "XLUtilTool.h"
#import "UIColor+XLCategory.h"

static NSString *const XLAppointmentSetCellID = @"XLAppointmentSetCellID";

@interface AppointmentSetViewController ()<UITableViewDelegate,UITableViewDataSource,SelectCallbackDelegate>

@property (strong, nonatomic) UITableView *tableView;
@property (assign, nonatomic) int startMinutes;
@property (assign, nonatomic) int startHours;
@property (assign, nonatomic) int endMinutes;
@property (assign, nonatomic) int endHours;
@property (assign, nonatomic) int temperature;

///提示文本
@property (strong, nonatomic) UILabel *labTips;

@end

@implementation AppointmentSetViewController

- (void)viewDidLoad {
    [super viewDidLoad];
}

- (void)initSubViews{
    [super initSubViews];
    
    //navi
    self.navigationItem.title = XLLocalizeString(@"预约设置");

    UIBarButtonItem *addItem = [[UIBarButtonItem alloc]initWithTitle:@"完成" style:(UIBarButtonItemStyleDone) target:self action:@selector(complete)];
    [addItem setTitleTextAttributes:@{NSForegroundColorAttributeName:[UIColor blackColor],NSFontAttributeName : [UIFont systemFontOfSize:15]} forState:UIControlStateNormal];
    [addItem setTitleTextAttributes:@{NSForegroundColorAttributeName:[UIColor grayColor],NSFontAttributeName : [UIFont systemFontOfSize:15]} forState:UIControlStateDisabled];
    self.navigationItem.rightBarButtonItem = addItem;
    
    UIBarButtonItem *cancleItem = [[UIBarButtonItem alloc]initWithTitle:@"取消" style:(UIBarButtonItemStyleDone) target:self action:@selector(cancle)];
    [cancleItem setTitleTextAttributes:@{NSForegroundColorAttributeName:[UIColor blackColor],NSFontAttributeName : [UIFont systemFontOfSize:15]} forState:UIControlStateNormal];
    [cancleItem setTitleTextAttributes:@{NSForegroundColorAttributeName:[UIColor grayColor],NSFontAttributeName : [UIFont systemFontOfSize:15]} forState:UIControlStateDisabled];
    self.navigationItem.leftBarButtonItem = cancleItem;

    //tableView
    [self.view addSubview:self.tableView];
}

- (void)buildData
{
    [super buildData];
    
    self.startMinutes = self.appointmentModel.startMinutes;
    self.startHours   = self.appointmentModel.startHours;
    self.endMinutes   = self.appointmentModel.endMinutes;
    self.endHours     = self.appointmentModel.endHours;
    self.temperature  = self.deviceModel.heater_temp;
    
    [self.tableView reloadData];
}

#pragma mark - UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 2;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    HotCell *cell = [tableView dequeueReusableCellWithIdentifier:XLAppointmentSetCellID];
    if (indexPath.section == 0){
        if (indexPath.row == 0){
            cell.name.text = @"开始时间";
            cell.subName.text = [NSString stringWithFormat:@"%02d:%02d",self.startHours,self.startMinutes];
        }else{
            cell.name.text = @"结束时间";
            cell.subName.text = [NSString stringWithFormat:@"%02d:%02d",self.endHours,self.endMinutes];
           
            cell.line.hidden = YES;
        }
    }else if (indexPath.section == 1){
        cell.name.text = [NSString stringWithFormat:@"%d℃",self.temperature] ;
        cell.subName.text = @"";
        cell.line.hidden = YES;
        
    }
    return cell;
}

#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    SetHotController *vc = [[SetHotController alloc]init];
    vc.delegate = self;
    vc.deviceModel = self.deviceModel;

    if (indexPath.row == 0){
        vc.type = 4;
        vc.startHours = self.startHours;
        vc.startMinutes = self.startMinutes;
    }else {
        vc.type = 5;
        vc.startHours = self.endHours;
        vc.startMinutes = self.endMinutes;
    }
    
    [self.navigationController pushViewController:vc animated:YES];

}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 30;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    CGRect frameRect = CGRectMake(26, 0, 100, 30);
    
    UILabel *label = [[UILabel alloc] init];
    label.textAlignment = NSTextAlignmentLeft;
    label.frame = frameRect;
    label.font = [UIFont systemFontOfSize:17];
    
    label.textColor = [UIColor grayColor];
    if (section == 1){
        label.text=@"出水温度";
    }
    UIView *view = [[UIView alloc]initWithFrame:CGRectMake(0, -20, kXLScreenWidth, 40)];
    [view addSubview:label];
    return view;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    UIView *bgView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 24)];

    self.labTips = [[UILabel alloc] init];
    self.labTips.textColor = [UIColor colorWithHexString:@"#818286"];
    self.labTips.font = [UIFont systemFontOfSize:13];
    self.labTips.text = @"*建议预约时间≥30分钟";
    self.labTips.frame = CGRectMake(25, 8, self.view.frame.size.width - 25, 16);
    [bgView addSubview:self.labTips];
    
    return bgView;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 24.0f;
}

#pragma mark - OverWrite

- (UITableView *)tableView{
    if (_tableView == nil){
        _tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, kXLScreenWidth, kXLScreenHeight) style:UITableViewStyleGrouped];
        
        [_tableView registerNib:[UINib nibWithNibName:NSStringFromClass([HotCell class]) bundle:nil] forCellReuseIdentifier:XLAppointmentSetCellID];
        
        _tableView.delegate = self;
        _tableView.dataSource = self;
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        
    }
    return _tableView;
}

#pragma mark - SelectCallbackDelegate
- (void)selectDataWithValue:(int)value type:(int)type{
    self.temperature = value;
    [self.tableView reloadData];
}

- (void)appointmentvalueCallBackWithHours:(int)hours Minutes:(int)minutes isStart:(BOOL)isStart{
    if (isStart){
        self.startHours = hours;
        self.startMinutes = minutes;
    }else{
        self.endMinutes = minutes;
        self.endHours = hours;
    }
    [self.tableView reloadData];
    
}

- (void)complete{
    
    //校验开始结束时间
    int difference = (self.endHours * 60 + self.endMinutes) - (self.startHours * 60 + self.startMinutes);
    if (difference == 0){
        [self alertHintWithMessage:@"结束时间不能与开始时间相同"];
        return;
    }

    int valueTime = ((self.startHours & 0xff) << 24) | ((self.startMinutes & 0xff) << 16) | ((self.endHours & 0xff) << 8) | (self.endMinutes & 0xff);
    
    NSInteger dataPointIndex;
    
    dataPointIndex = self.appointmentModel.index;
    
    [self setDataPointWithValue:valueTime index:dataPointIndex type:XLinkDataTypeUnsignedInt temperature:self.temperature];
    
}

- (void)setDataPointWithValue:(int)value index:(int)index type:(int)type temperature:(int)temperature{
    
    @weakify(self)
    
    int openValue = 0;//是否打开，默认打开
    NSInteger appointmentModelIndex = self.appointmentModel.index - 21;

    switch (appointmentModelIndex) {
        case 1:
            openValue = self.deviceModel.order_flag | 0x01;
            break;
        case 2:
            openValue = self.deviceModel.order_flag | 0x02;
            break;
        case 3:
            openValue = self.deviceModel.order_flag | 0x04;
            break;
        case 4:
            openValue = self.deviceModel.order_flag | 0x08;
            break;
        case 5:
            openValue = self.deviceModel.order_flag | 0x10;
            break;
        case 6:
            openValue = self.deviceModel.order_flag | 0x20;
            break;
        case 7:
            openValue = self.deviceModel.order_flag | 0x40;
            break;
        case 8:
            openValue = self.deviceModel.order_flag | 0x80;
            break;
        case 9:
            openValue = self.deviceModel.order_flag2 | 0x01;
            break;
        case 10:
            openValue = self.deviceModel.order_flag2 | 0x02;
            break;
        case 11:
            openValue = self.deviceModel.order_flag2 | 0x04;
            break;
        case 12:
            openValue = self.deviceModel.order_flag2 | 0x08;
            break;
        default:
            break;
    }
    
    //预约时间
    XLinkDataPoint *dataPointTime = [XLinkDataPoint dataPointWithType:type withIndex:index withValue:@(value)];

    //是否打开
    XLinkDataPoint *dataPointTemp;
    if (appointmentModelIndex > 8){
        dataPointTemp = [XLinkDataPoint dataPointWithType:2 withIndex:21 withValue:@(openValue)];
    }else{
        dataPointTemp = [XLinkDataPoint dataPointWithType:2 withIndex:20 withValue:@(openValue)];
    }

    NSArray *dataPointArray = @[dataPointTime,dataPointTemp];
    
    //游客模式判断
    if (XL_DATA_VIRTUAL.isLogin == NO){
        [self.deviceModel addDataPoints:dataPointArray];
        if (self.delegate && [self.delegate respondsToSelector:@selector(completeCallBack)]) {
            [self.delegate completeCallBack];
            [self.navigationController popViewControllerAnimated:YES];
        }
        return;
    }
    
    KShowLoading;
    [[XLServiceManager shareInstance].deviceService setDeviceDataPoints:self.deviceModel.device dataPoints:dataPointArray timeout:10. handler:^(id result, NSError *error) {
        @strongify(self)
        KHideLoading;

        if (error == nil) {
            [self.deviceModel addDataPoints:dataPointArray];
            
            if (self.delegate && [self.delegate respondsToSelector:@selector(completeCallBack)]){
                [self.delegate completeCallBack];
                [self.navigationController popViewControllerAnimated:YES];
            }
        }else{
            [self showNetworkError];
        }
    }];
}

- (void)cancle{
    [self.navigationController popViewControllerAnimated:YES];
}

@end
