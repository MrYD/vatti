//
//  BathViewController.h
//  vatti
//
//  Created by BZH on 2018/6/10.
//  Copyright © 2018年 xlink. All rights reserved.
//

/**
 *  浴缸设置
 *
 *  @author BZH
 */

#import "BaseWaterHeaterDeviceViewController.h"

@interface BathViewController : BaseWaterHeaterDeviceViewController

@end
