//
//  DeviceControlViewController.m
//  testDeviceControl
//
//  Created by Eric on 2018/7/20.
//  Copyright © 2018年 Eric. All rights reserved.
//

#import "DeviceControlViewController.h"
#import "SliderView.h"
#import "CircleView.h"
#import "FunctionView.h"
#import "BZButton.h"
#import "CurrentDeviceAlarmViewController.h"
#import "DeviceSetViewController.h"
#import "AppointmentViewController.h"
#import "XLinkSetDataPointTask.h"
#import "XLWaterHeaterDeviceDao.h"

@interface DeviceControlViewController ()<CircleViewDelegate,SliderViewDelegate,FunctionViewDelegate,UIAlertViewDelegate>

#pragma mark - IBOutLet

#pragma mark - UIView

///顶部状态 view
@property (weak,   nonatomic) IBOutlet UIView *viewState;
///圆形图 背景 view
@property (weak,   nonatomic) IBOutlet UIView *viewCircle;
///slider 背景 view
@property (weak,   nonatomic) IBOutlet UIView *viewSlider;
///功能按钮 view
@property (weak,   nonatomic) IBOutlet UIView *viewFunction;

#pragma mark - CustomUI
///圆形图 view
@property (strong, nonatomic) CircleView *circleView;
///slider view
@property (strong, nonatomic) SliderView *sliderView;
///Function view
@property (strong, nonatomic) FunctionView *functionView;

#pragma mark - UIButton

///连接状态
@property (weak,   nonatomic) IBOutlet UIButton *btnConnectedState;
///预约状态
@property (weak,   nonatomic) IBOutlet UIButton *btnAppointState;

#pragma mark - Data

///调用layoutSubviews次数
@property (assign, nonatomic) NSInteger layoutCount;

///滑块进度
@property (assign, nonatomic) CGFloat proportion;

///indexArray
@property (strong, nonatomic) NSMutableArray *indexArray;

@end

@implementation DeviceControlViewController

- (void)viewDidLoad {
    [super viewDidLoad];
}

- (void)initConfigure
{
    [super initConfigure];
    
    self.layoutCount = 0;
}

- (void)initSubViews
{
    [super initSubViews];
    
    //navi
    [self setupNavi];
    
    //circleView
    self.circleView = [CircleView view];
    self.circleView.delegate = self;
    [self.viewCircle addSubview:self.circleView];
    
    //slider
    self.sliderView = [SliderView view];
    self.sliderView.delegate = self;
    [self.viewSlider addSubview:self.sliderView];
    
    //functionView
    self.functionView = [FunctionView view];
    self.functionView.delegate = self;
    [self.viewFunction addSubview:self.functionView];
}

- (void)buildData
{
    [super buildData];
    
    [self updateTempWithTempValue:self.deviceModel.heater_temp andIsNeedSetToDevice:YES];
    [self.functionView setBtnSelectedWithFunctionFlag:self.deviceModel.function_flag];
    
    //浴缸开启
    BOOL b1 = [XLDataPointTool isOpenWithValue:self.deviceModel.function_flag andShift:0];
    BOOL b2 = [XLDataPointTool isOpenWithValue:self.deviceModel.function_flag andShift:4];
    BOOL b3 = [self getIsNeedToastFullWater];
    if (b1 && b2 && b3){
        [self showFullWaterAlert];
    }
    
    //add rac
    [self addRACObserve];
}

- (void)viewWillLayoutSubviews
{
    [super viewWillLayoutSubviews];
    
    //circleView
    self.circleView.frame = self.viewCircle.bounds;
    
    //slider
    self.sliderView.frame = self.viewSlider.bounds;
    
    //functionView
    self.functionView.frame = self.viewFunction.bounds;
    
    //根据温度更新UI(因为5s会出现问题)
    if (self.layoutCount < 2) {
        self.layoutCount = self.layoutCount + 1;
        [self updateTempWithTempValue:self.deviceModel.heater_temp andIsNeedSetToDevice:YES];
    }
}

#pragma mark - UI

- (void)setupNavi
{
    [super setupNavi];
    
    if (self.deviceModel.name.length > 0){
        self.navigationItem.title = self.deviceModel.name;
    }else{
        self.navigationItem.title = XLDeviceGasWaterHeaterName;
    }
    
    UIBarButtonItem *rightBarBtn = [[UIBarButtonItem alloc] initWithTitle:@"更多" style:UIBarButtonItemStyleDone target:self action:@selector(rightAction)];
    [rightBarBtn setTitleTextAttributes:@{NSForegroundColorAttributeName:[UIColor blackColor],NSFontAttributeName : [UIFont systemFontOfSize:15]} forState:UIControlStateNormal];
    [rightBarBtn setTitleTextAttributes:@{NSForegroundColorAttributeName:[UIColor grayColor],NSFontAttributeName : [UIFont systemFontOfSize:15]} forState:UIControlStateDisabled];
    rightBarBtn.tintColor = [UIColor blackColor];
    self.navigationItem.rightBarButtonItem = rightBarBtn;
}

/**
 根据温度更新圆环和滑块view
 */
- (void)updateTempWithTempValue:(NSInteger)temp andIsNeedSetToDevice:(BOOL)isNeed
{
    if (isNeed == YES) {
        self.deviceModel.heater_temp = temp;
    }
    
    BOOL isConnected = self.deviceModel.connected;
    BOOL isOpen = [XLDataPointTool isOpenWithValue:self.deviceModel.function_flag andShift:0];
    
    [self.circleView updateUIWithTemp:temp andIsConnected:isConnected andIsOpen:isOpen];
    [self.sliderView updateUIWithTemp:temp andIsConnected:isConnected andIsOpen:isOpen];
}

- (void)resetSliderValue
{
    [self updateTempWithTempValue:self.deviceModel.heater_temp andIsNeedSetToDevice:YES];
}

- (void)updateFunctionViewWithFunctionFlag:(NSInteger)functionFlag
{
    //有错误
    BOOL isNoError = [self getIsNoError];
    //设备联网
    BOOL isConnect = self.deviceModel.connected;
    //设备电源开关状态
    BOOL isOpen;
    isOpen = [XLDataPointTool isOpenWithValue:functionFlag andShift:0];
    
    if (isNoError == YES) {
        if (isConnect == YES) {
            if (isOpen == YES) {
                [self.functionView setAllBtnEnable:YES];
                [self.functionView setBtnSelectedWithFunctionFlag:functionFlag];
            }else{
                [self.functionView setAllBtnSelected:NO];
                [self.functionView setAllBtnEnable:NO];
                self.functionView.btnSwitch.enabled = YES;
            }
        }else{
            [self.functionView setAllBtnSelected:NO];
            [self.functionView setAllBtnEnable:NO];
        }
    }else{
        [self.functionView setAllBtnSelected:NO];
        [self.functionView setAllBtnEnable:NO];
    }
}

#pragma mark - Data

- (NSMutableArray *)indexArray
{
    if (_indexArray == nil) {
        _indexArray = [NSMutableArray array];
        
        //目前只更新的端点index:8、16
        [_indexArray addObject:[NSNumber numberWithInt:8]];
        [_indexArray addObject:[NSNumber numberWithInt:16]];
        
    }
    
    return _indexArray;
    
}

- (void)addRACObserve
{
    @weakify(self)
    
#pragma mark - 全局
    //监控温度
    [[RACObserve(self.deviceModel, heater_temp).distinctUntilChanged deliverOnMainThread] subscribeNext:^(id  _Nullable x) {
        @strongify(self)
        if (self.isResponseChange == YES) {
            [self updateTempWithTempValue:self.deviceModel.heater_temp andIsNeedSetToDevice:NO];
        }
        
    }];
    
    //监控浴缸水满
    [[RACObserve(self.deviceModel, surplus_water).distinctUntilChanged deliverOnMainThread] subscribeNext:^(id  _Nullable x) {
//        if (self.isResponseChange == YES) {
        
            NSLog(@"index17的值为:%ld",[x integerValue]);
            
            BOOL b1 = [XLDataPointTool isOpenWithValue:self.deviceModel.function_flag andShift:0];
            BOOL b2 = [XLDataPointTool isOpenWithValue:self.deviceModel.function_flag andShift:4];
            BOOL b3 = self.deviceModel.connected == YES;
            BOOL b4 = self.deviceModel.surplus_water == 0;
            
            //无故障
            BOOL b5 = [self getIsNoError];
            
            if (b1 && b2 && b3 && b4 && b5){
                NSLog(@"RAC监控到水满并弹窗提示");
                [self showFullWaterAlert];
            }
//        }
    }];
    
    //设备上下线
    [[RACObserve(self.deviceModel, connected).distinctUntilChanged deliverOnMainThread] subscribeNext:^(id  _Nullable x) {
        @strongify(self)
        
        BOOL isConnected = self.deviceModel.connected;
        if (isConnected == YES) {
            [self.btnConnectedState setImage:[UIImage imageNamed:@"ic_online"] forState:UIControlStateNormal];
        }else{
            [self.btnConnectedState setImage:[UIImage imageNamed:@"ic_offline"] forState:UIControlStateNormal];
        }
        
        //温度
        [self updateTempWithTempValue:self.deviceModel.heater_temp andIsNeedSetToDevice:YES];
        
        //控制面板
        [self.functionView setAllBtnEnable:isConnected];
    }];
    
#pragma mark - 设备状态面板
    //预约
    [[RACObserve(self.deviceModel, function_flag).distinctUntilChanged deliverOnMainThread] subscribeNext:^(id  _Nullable x) {
        @strongify(self)
        
        BOOL isOpen = [XLDataPointTool isOpenWithValue:self.deviceModel.function_flag andShift:6];
        if (isOpen == YES){
            self.circleView.labAppState.text = @"预约已开启";
        }else{
            self.circleView.labAppState.text = @"预约已关闭";
        }
        
        self.btnAppointState.hidden = !isOpen;
        
    }];
    
    //告警
    [[RACObserve(self.deviceModel, master_err).distinctUntilChanged deliverOnMainThread] subscribeNext:^(id  _Nullable x) {
        @strongify(self)
        BOOL noWaring = [self getIsNoError];
        
        self.circleView.viewWaring.hidden = noWaring;
        self.circleView.viewNormal.hidden = !noWaring;
    }];
    
    //正在运行
    [[RACObserve(self.deviceModel, dev_status).distinctUntilChanged deliverOnMainThread] subscribeNext:^(id  _Nullable x) {
        @strongify(self)
        BOOL b1 = [XLDataPointTool isOpenWithValue:self.deviceModel.dev_status andShift:2];//设备正在运行,热水器已点火
        self.circleView.labRuning.hidden = !b1;
        
        BOOL b2 = [XLDataPointTool isOpenWithValue:self.deviceModel.dev_status andShift:3];
        BOOL b3 = [XLDataPointTool isOpenWithValue:self.deviceModel.function_flag andShift:3];
        BOOL b4 = self.deviceModel.heater_temp > 50;
        BOOL b5 = [self getIsDeviceCanControlWithIsNeedAlert:NO];
        
        BOOL isBathtub = [XLDataPointTool isOpenWithValue:self.deviceModel.function_flag andShift:4];
        BOOL isHeaterOrAppointment = [self getIsHeatOrAppointment];
        
        NSMutableArray<XLinkDataPoint *> *dataPointArray = [NSMutableArray array];
        if (b2 && b3 && b4 && b5) {
            //把温度改成50
            XLinkDataPoint *dataPointTemp = [XLinkDataPoint dataPointWithType:XLinkDataTypeByte withIndex:8 withValue:@(50)];
            [dataPointArray addObject:dataPointTemp];
        }
        
        if (isBathtub && isHeaterOrAppointment) {
            // 关闭浴缸
            NSInteger funcValue = [XLDataPointTool getDataPointValueWithValue:self.deviceModel.function_flag andShift:4 andIsOpen:NO];
            if (b5) {
                XLinkDataPoint *dataPointFunction = [XLinkDataPoint dataPointWithType:XLinkDataTypeByte withIndex:16 withValue:@(funcValue)];
                [dataPointArray addObject:dataPointFunction];
            }
        }
        if (dataPointArray.count > 0) {
            [self setDataPoints:dataPointArray];
        }
        
    }];
    
#pragma mark - 滑块面板
    //监控设备连接状态和告警状态
    [[[RACObserve(self.deviceModel, connected).distinctUntilChanged merge:RACObserve(self.deviceModel, master_err).distinctUntilChanged] deliverOnMainThread] subscribeNext:^(id  _Nullable x) {
        @strongify(self)
        
        BOOL isConnect = self.deviceModel.connected;
        BOOL isError   = [self getIsNoError] == NO;
        BOOL isOpen    = [XLDataPointTool isOpenWithValue:self.deviceModel.function_flag andShift:0];
        
        [self.navigationItem.rightBarButtonItem setEnabled: isConnect && !isError];
        
        if (isError || !isConnect || !isOpen){
            [self.sliderView updateControlWithEnable:NO];
        }else{
            [self.sliderView updateControlWithEnable:YES];
        }
        
    }];
    
    //监控锁状态和开关状态
    [[RACObserve(self.deviceModel, function_flag).distinctUntilChanged deliverOnMainThread] subscribeNext:^(id  _Nullable x) {
        @strongify(self)
        
        BOOL isLock = [self getIsLock];
        [self.sliderView updateIVLockState:isLock];
        
        BOOL isConnect = self.deviceModel.connected;
        BOOL isError   = [self getIsNoError] == NO;
        BOOL isOpen    = [XLDataPointTool isOpenWithValue:self.deviceModel.function_flag andShift:0];
        
        if (isError || !isConnect || !isOpen){
            [self.sliderView updateControlWithEnable:NO];
        }else{
            [self.sliderView updateControlWithEnable:YES];
        }
        
        if (self.isResponseChange == YES) {
            [self updateTempWithTempValue:self.deviceModel.heater_temp andIsNeedSetToDevice:NO];
        }
    }];
    
    //监控
    //在即热及预约模式，水泵运行时，主机发送水泵状态信号，当设定温度高于50℃时，自动降至50℃。
    [[[[RACObserve(self.deviceModel, dev_status).distinctUntilChanged merge:RACObserve(self.deviceModel, function_flag).distinctUntilChanged] merge:RACObserve(self.deviceModel, heater_temp).distinctUntilChanged] deliverOnMainThread] subscribeNext:^(id  _Nullable x) {
        @strongify(self)
        
        BOOL canNotSetTempOver50 = [self getCanNotSetTempOver50WithTemp:self.deviceModel.heater_temp];
        
        if (canNotSetTempOver50 == YES) {
            int tempValue = 50;
            //发起请求
            //温度
            XLinkDataPoint *dataPointTemp = [XLinkDataPoint dataPointWithType:XLinkDataTypeByte withIndex:8 withValue:@(tempValue)];
            //加锁
            NSInteger valueLock = (self.deviceModel.function_flag & (~0x02));
            XLinkDataPoint *dataPointLock = [XLinkDataPoint dataPointWithType:XLinkDataTypeByte withIndex:16 withValue:@(valueLock)];
            
            [self setDataPoints:@[dataPointTemp,dataPointLock]];
        }
    }];
    
#pragma mark - 控制面板
    //监控设备链接状态，错误码以及功能开关
    [[[[RACObserve(self.deviceModel, connected).distinctUntilChanged merge:RACObserve(self.deviceModel, master_err).distinctUntilChanged] merge:RACObserve(self.deviceModel, function_flag).distinctUntilChanged] deliverOnMainThread] subscribeNext:^(id  _Nullable x) {
        
        @strongify(self)
        
        if (self.isResponseChange == YES) {
            [self updateFunctionViewWithFunctionFlag:self.deviceModel.function_flag];
        }

    }];
    
}

/**
 即热或者预约执行中
 */
- (BOOL)getIsHeatOrAppointment
{
    //开启了即热模式
    BOOL b1 = [XLDataPointTool isOpenWithValue:self.deviceModel.function_flag andShift:3];
    
    //开启了预约模式
    BOOL b2 = [XLDataPointTool isOpenWithValue:self.deviceModel.function_flag andShift:6];
    //已经到达预约时间、设备正在执行预约
    BOOL b3 = [XLDataPointTool isOpenWithValue:self.deviceModel.dev_status andShift:4];
    //开启了即热或预约模式
    BOOL b4 = b2 && b3;
    
    //开启了即热模式或者预约开启并且执行中
    BOOL b5 = b1 || b4;
    
    return b5;
}

- (NSArray <XLinkDataPoint*> *)getDataPointsArrayWithFunctionFlagValue:(NSInteger)functionFlagValue andBtn:(UIButton*)btn
{
    NSArray <XLinkDataPoint*> *dataPointsArray = nil;
    
    //温度
    NSInteger tempValue = '\0';
    
    //当舒适浴开启的时候，要根据环境温度设置热水器温度
    BOOL b1 = [btn isEqual:self.functionView.btnComfortableBath] == YES;
    BOOL b2 = btn.selected == YES;
    
    if (b1 && b2) {
        tempValue = [self getHeaterTemp];
    }
    
    //功能开关端点
    XLinkDataPoint *dataPointFunctionFlag = [XLinkDataPoint dataPointWithType:XLinkDataTypeByte withIndex:16 withValue:@(functionFlagValue)];
    
    //温度端点
    XLinkDataPoint *dataPointTemp;
    
    if (tempValue > 0) {
        dataPointTemp = [XLinkDataPoint dataPointWithType:XLinkDataTypeByte withIndex:8 withValue:@(tempValue)];
        
        //温度低于50度需要加锁
        NSInteger valueLock = [XLDataPointTool getDataPointValueWithValue:[dataPointFunctionFlag.value intValue] andShift:1 andIsOpen:NO];
        dataPointFunctionFlag.value = @(valueLock);
        
        dataPointsArray = @[dataPointFunctionFlag,dataPointTemp];
    }else{
        dataPointsArray = @[dataPointFunctionFlag];
    }
    
    return dataPointsArray;
}

/**
 根据环境温度获得需要设置当前热水器温度
 */
- (NSInteger)getHeaterTemp
{
    int heaterTemp = 0;
    
    BOOL b1 = (self.deviceModel.temp_env >= 0) && (self.deviceModel.temp_env <= 10);
    BOOL b2 = (self.deviceModel.temp_env >= 11) && (self.deviceModel.temp_env <= 15);
    BOOL b3 = (self.deviceModel.temp_env >= 16) && (self.deviceModel.temp_env <= 20);
    BOOL b4 = (self.deviceModel.temp_env >= 21) && (self.deviceModel.temp_env <= 25);
    BOOL b5 = (self.deviceModel.temp_env >= 26) && (self.deviceModel.temp_env <= 28);
    BOOL b6 = (self.deviceModel.temp_env >= 29) && (self.deviceModel.temp_env <= 30);
    BOOL b7 = (self.deviceModel.temp_env >= 31) && (self.deviceModel.temp_env <= 32);
    
    if (b1 == YES) {
        heaterTemp = 46;
    }else if (b2 == YES) {
        heaterTemp = 45;
    }else if (b3 == YES) {
        heaterTemp = 44;
    }else if (b4 == YES) {
        heaterTemp = 42;
    }else if (b5 == YES) {
        heaterTemp = 40;
    }else if (b6 == YES) {
        heaterTemp = 38;
    }else if (b7 == YES) {
        heaterTemp = 36;
    }
    
    return heaterTemp;
}

/**
 不能设置温度超过50度
 
 @return YES:不能 NO:能
 */
- (BOOL)getCanNotSetTempOver50WithTemp:(NSInteger)temp
{
    //温度大于50度
    BOOL b1 = temp > 50;
    
    //即热或者预约执行中
    BOOL b2 = [self getIsHeatOrAppointment];
    
    //水泵运行
    BOOL b3 = ((self.deviceModel.dev_status & 0x08) >> 3) == 1;
    
    BOOL b4 = b1 && b2 && b3;
    return b4;
}

/**
 是否加锁
 
 @return YES:加锁 NO:没加锁
 */
- (BOOL)getIsLock
{
    // == 0 表示锁上 == 1 表示解锁
    BOOL isLock = ![XLDataPointTool isOpenWithValue:self.deviceModel.function_flag andShift:1];
    
    if (isLock == YES) {
        NSLog(@"当前设备加锁");
    }else{
        NSLog(@"当前设备解锁");
    }
    return isLock;
}

/**
 是否需要提示水满
 */
- (BOOL)getIsNeedToastFullWater
{
    //水满
    BOOL b1 = self.deviceModel.surplus_water == 0;
    //在线
    BOOL b2 = self.deviceModel.connected == YES;
    //无故障
    BOOL b3 = [self getIsNoError];
    
    BOOL b4 = b1 && b2 && b3;
    return b4;
}

- (void)configureAddActionLock
{
    // 由于AlertController的block回调有延迟会有问题，此处需要使用AlertView
    UIAlertView *unlockAlert = [[UIAlertView alloc] initWithTitle:@"提示" message:@"目前出水温度已达到50度需解锁设备才能继续提升温度" delegate:self cancelButtonTitle:@"取消" otherButtonTitles:@"解锁", nil];
    [unlockAlert show];
}

- (void)timerFinished:(NSTimer*)timer
{
    [super timerFinished:timer];
    
    @weakify(self)
    
    //游客模式判断
    if (XL_DATA_VIRTUAL.isLogin == NO){
        return;
    }
    
    NSMutableString *mIndexString = [[NSMutableString alloc] init];
    [mIndexString appendString:@"正在获取以下端点:"];
    for (NSNumber *indexNum in self.indexArray) {
        [mIndexString appendFormat:@"%@,",indexNum];
    }
    NSLog(@"%@",mIndexString);
    
    NSLog(@"开始Probe");
    [[XLServiceManager shareInstance].deviceService probeDataPointWithDevice:self.deviceModel.device andIndexArray:self.indexArray andTimeOut:TimeOutSetDataPoint andCallBack:^(id  _Nullable result, NSError * _Nullable error) {
        
        @strongify(self)
        
        if (error == nil) {
            NSLog(@"Probe成功");
            NSArray<XLinkDataPoint *> *dataPoints = result;
            
            if (dataPoints.count > 0) {
                for (XLinkDataPoint *dp in dataPoints) {
                    NSLog(@"端点%d的值为%d",dp.index,[dp.value intValue]);
                }
                
                [self.deviceModel addDataPoints:dataPoints];
                [self updateTempWithTempValue:self.deviceModel.heater_temp andIsNeedSetToDevice:NO];
                [self updateFunctionViewWithFunctionFlag:self.deviceModel.function_flag];
            }else{
                NSLog(@"没有获取到数据端点");
            }
            
        }else {
            NSLog(@"Probe失败, %@", error.userInfo);
            //回滚
            [XLWaterHeaterDeviceDao queryWithXDevice:self.deviceModel.device callBack:^(WaterHeaterDeviceModel * _Nullable xlWaterHeaterDevice) {
                if (xlWaterHeaterDevice == nil) {
                    return;
                }
                
                [xlWaterHeaterDevice addObserver];
                self.deviceModel = xlWaterHeaterDevice;
                
                [self updateTempWithTempValue:self.deviceModel.heater_temp andIsNeedSetToDevice:NO];
                [self updateFunctionViewWithFunctionFlag:self.deviceModel.function_flag];
            }];
        }
    }];
}

#pragma mark - Action

- (void)rightAction
{
    if (self.deviceModel.connected == YES) {
        DeviceSetViewController *deviceSet = [[DeviceSetViewController alloc]init];
        deviceSet.deviceModel = self.deviceModel;
        [self.navigationController pushViewController:deviceSet animated:YES];
    }
}

/**
 浴缸满水提示
 */
- (void)showFullWaterAlert{
    [self alertHintWithMessage:@"水量已注满，请您享受沐浴！" handler:^(UIAlertAction *action, NSUInteger tag) {
        [self closeYuGang];
    }];
}

/**
 关闭浴缸
 */
- (void)closeYuGang
{
    NSLog(@"关闭浴缸");
    
    BOOL isDeviceCanControl = [self getIsDeviceCanControlWithIsNeedAlert:NO];
    if (isDeviceCanControl == YES) {
        NSInteger closeFunctionFlag = [XLDataPointTool getDataPointValueWithValue:self.deviceModel.function_flag andShift:4 andIsOpen:NO];
        XLinkDataPoint *dataPointClose = [XLinkDataPoint dataPointWithType:XLinkDataTypeByte withIndex:16 withValue:@(closeFunctionFlag)];
        [self updateFunctionViewWithFunctionFlag:closeFunctionFlag];
        [self setDataPoints:@[dataPointClose]];
    }
}

#pragma mark - Request

/**
 设置数据端点数据
 */
- (void)setDataPoints:(NSArray <XLinkDataPoint*> *)dataPointsArray {
    @weakify(self)
    
    //1.处理发送端点前的逻辑
    [self startLogicalBefortSet];
    
    //2.设置端点值到数据模型
    [self.deviceModel addDataPoints:dataPointsArray];
    
    //3.更新UI
    [self updateTempWithTempValue:self.deviceModel.heater_temp andIsNeedSetToDevice:YES];
    [self updateFunctionViewWithFunctionFlag:self.deviceModel.function_flag];
    
    //4.判断游客模式
    if (XL_DATA_VIRTUAL.isLogin == NO){
        return;
    }
    
    //获取当前登录用户ID，加入数据端点
    NSMutableArray *mDataPoints = [self addUserIdDataPointWithDataPointsArray:dataPointsArray];
    
    //判断是否设置相同任务
    //如果是，则取消上一个任务
    [self cancelLastSameTaskWithNewDataPointsArray:mDataPoints];
    
    self.setDataPointTask = [[XLServiceManager shareInstance].deviceService setDeviceDataPoints:self.deviceModel.device dataPoints:mDataPoints timeout:TimeOutSetDataPoint handler:^(id result, NSError *error) {
        
        @strongify(self)
        
        if (error == nil) {
            
        }else{
            KShowErrorLoadingWithMsg(XLHint_SetDataPontFailed);
            NSLog(@"设置失败, %@", error.userInfo);
        }
    }];
    
}

#pragma mark - CircleViewDelegate

/**
 点击查看详情
 */
- (void)circleView:(CircleView *)circleView btnShowDetailClicked:(UIButton *)btnShowDetail
{
    CurrentDeviceAlarmViewController *vc = [[CurrentDeviceAlarmViewController alloc]init];
    vc.deviceModel = self.deviceModel;
    [self.navigationController pushViewController:vc animated:YES];
}

#pragma mark - SliderViewDelegate

/**
 slider此时会滑动，但是value还没正改变，在这个时候更新UI
 */
- (void)sliderView:(SliderView *)sliderView sliderDragInside:(UISlider *)slider
{
    //1.判断设备是否可控制
    BOOL isDeviceCanControl = [self getIsDeviceCanControlWithIsNeedAlert:YES];
    if (isDeviceCanControl == NO) {
        [self resetSliderValue];
        return;
    }
    
    //2.如果设备电源关闭
    if ((self.deviceModel.function_flag & 0x01) == 0){
        [self resetSliderValue];
        return;
    }
    
    //3.判断是否加锁
    BOOL isLock = [self getIsLock];
    BOOL isOver50Degree = slider.value >= 50;
    
    //温度
    NSInteger valueTemp = (NSInteger)slider.value;
    
    //滑块超过50度
    if (isOver50Degree == YES){
        //不能设置温度超过50度
        BOOL canNotSetTempOver50 = [self getCanNotSetTempOver50WithTemp:(valueTemp + 5)];
        if (canNotSetTempOver50 == YES) {
            [self updateTempWithTempValue:50 andIsNeedSetToDevice:NO];
            return;
        }
        
        if (isLock == YES){
            
            //取消响应
            [slider cancelTrackingWithEvent:nil];
            
            [self updateTempWithTempValue:50 andIsNeedSetToDevice:YES];
            
            //继续处理加锁代理
            [self configureAddActionLock];
            
            return;
        }
        
        if (slider.value <= 55) {
            [self updateTempWithTempValue:55 andIsNeedSetToDevice:NO];
        }else{
            [self updateTempWithTempValue:60 andIsNeedSetToDevice:NO];
        }
        
    }else{
        [self updateTempWithTempValue:valueTemp andIsNeedSetToDevice:YES];
    }
}

/**
 slider此时value已经改变，在这个时候发送设置数据端点请求
 */
- (void)sliderView:(SliderView *)sliderView sliderValueChanged:(UISlider *)slider
{
    //1.判断设备是否可控制
    BOOL isDeviceCanControl = [self getIsDeviceCanControlWithIsNeedAlert:YES];
    if (isDeviceCanControl == NO) {
        [self resetSliderValue];
        return;
    }
    
    //2.如果设备电源关闭
    if ((self.deviceModel.function_flag & 0x01) == 0){
        [self resetSliderValue];
        return;
    }
    
    //温度
    NSInteger valueTemp = (NSInteger)slider.value;
    BOOL b1 = slider.value >  49;
    BOOL b2 = slider.value <= 55;
    BOOL b3 = b1 && b2;
    BOOL b4 = !b2;
    
    //是否50~55度
    if (b3 == YES) {
        valueTemp = 55;
    }
    //是否>55度
    else if (b4 == YES) {
        valueTemp = 60;
    }
    
    //3.判断是否加锁
    BOOL isLock = [self getIsLock];
    BOOL isOver50Degree = slider.value >= 50;
    //不能设置温度超过50度
    BOOL canNotSetTempOver50 = [self getCanNotSetTempOver50WithTemp:valueTemp];
    if (isOver50Degree && (isLock || canNotSetTempOver50)) {
        valueTemp = 50;
    }
    
    NSInteger functionFlagValue;
    //关闭舒适浴
    functionFlagValue = [XLDataPointTool getDataPointValueWithValue:self.deviceModel.function_flag andShift:2 andIsOpen:NO];
    
    if (valueTemp <= 50){
        //从解锁状态变成加锁
        functionFlagValue = [XLDataPointTool getDataPointValueWithValue:functionFlagValue andShift:1 andIsOpen:NO];
    }
    
    XLinkDataPoint *dataPointTemp = [XLinkDataPoint dataPointWithType:XLinkDataTypeByte withIndex:8 withValue:@(valueTemp)];
    
    XLinkDataPoint *dataPointComftable = [XLinkDataPoint dataPointWithType:XLinkDataTypeByte withIndex:16 withValue:@(functionFlagValue)];
    
    //request
    [self setDataPoints:@[dataPointTemp,dataPointComftable]];
}

/**
 增加按钮点击
 */
- (void)sliderView:(SliderView *)sliderView btnAddClicked:(UIButton *)btnAdd
{
    
    //1.判断设备是否可控制
    BOOL isDeviceCanControl = [self getIsDeviceCanControlWithIsNeedAlert:YES];
    if (isDeviceCanControl == NO) {
        return;
    }
    
    //2.判断是否大于等于60度最高温度
    NSInteger tempValue = (NSInteger)sliderView.slider.value;
    if (tempValue >= 60){
        return;
    }
    
    //3.如果设备电源关闭
    if ((self.deviceModel.function_flag & 0x01) == 0){
        return;
    }
    
    //4.判断是否大于50度
    if (tempValue >= 50) {
        
        BOOL canNotSetTempOver50 = [self getCanNotSetTempOver50WithTemp:(tempValue + 5)];
        if (canNotSetTempOver50 == YES) {
            return;
        }
        
        BOOL isLock = [self getIsLock];
        if (isLock == YES){
            //加锁状态
            [self configureAddActionLock];
            return;
        }else{
            //没加锁状态
            tempValue = (tempValue + 5);
        }
        
    }else{
        //小于50度
        tempValue = (tempValue + 1);
    }
    
    //生成端点
    //温度端点
    XLinkDataPoint *dataPointTemp = [XLinkDataPoint dataPointWithType:XLinkDataTypeByte withIndex:8 withValue:@(tempValue)];
    
    //舒适浴端点
    NSInteger functionFlagValueCloseConf = self.deviceModel.function_flag & (~0x04);
    XLinkDataPoint *dataPointComftable = [XLinkDataPoint dataPointWithType:XLinkDataTypeByte withIndex:16 withValue:@(functionFlagValueCloseConf)];
    
    //发起请求
    [self setDataPoints:@[dataPointTemp,dataPointComftable]];
}

/**
 减少按钮点击
 */
- (void)sliderView:(SliderView *)sliderView btnCutClicked:(UIButton *)btnCut
{
    //1.判断设备是否可控制
    BOOL isDeviceCanControl = [self getIsDeviceCanControlWithIsNeedAlert:YES];
    if (isDeviceCanControl == NO) {
        return;
    }
    
    //2.判断是否低于最低温度35度
    NSInteger tempValue = (NSInteger)sliderView.slider.value;
    if (tempValue <= 35){
        return;
    }
    
    //3.如果设备电源关闭
    if ((self.deviceModel.function_flag & 0x01) == 0){
        return;
    }
    
    //当前温度大于50度
    if (tempValue > 50){
        tempValue = (tempValue - 5);
    }else{
        //当前温度小于50度
        tempValue  = (tempValue - 1);
    }
    
    NSInteger functionFlagValue;
    //生成端点
    //温度
    XLinkDataPoint *dataPointTemp = [XLinkDataPoint dataPointWithType:XLinkDataTypeByte withIndex:8 withValue:@(tempValue)];
    
    //关闭舒适浴
    XLinkDataPoint *dataPointComftable;
    functionFlagValue = [XLDataPointTool getDataPointValueWithValue:self.deviceModel.function_flag andShift:2 andIsOpen:NO];
    
    if (tempValue <= 50){
        //从解锁状态变成加锁
        functionFlagValue = [XLDataPointTool getDataPointValueWithValue:functionFlagValue andShift:1 andIsOpen:NO];
    }
    
    dataPointComftable = [XLinkDataPoint dataPointWithType:XLinkDataTypeByte withIndex:16 withValue:@(functionFlagValue)];
    
    [self setDataPoints:@[dataPointTemp,dataPointComftable]];
}

#pragma mark - FunctionViewDelegate

/**
 所有面板按钮点击
 */
- (void)functionView:(FunctionView *)functionView btnClicked:(BZButton *)button
{
    BOOL isDeviceCanControl = [self getIsDeviceCanControlWithIsNeedAlert:YES];
    if (isDeviceCanControl == NO) {
        return;
    }
    
    //功能开关
    NSInteger functionFlagValue = '\0';
    
    if ([button isEqual:functionView.btnAppoint] == YES) {//预约
        
        BOOL isOpen = button.selected == NO;
        BOOL isOrderFlagClose = (self.deviceModel.order_flag & 0xFF) == 0;
        BOOL isOrderFlag2Close = (self.deviceModel.order_flag2 & 0xFF) == 0;
        BOOL isNeedGoToSetAppointment = isOpen && isOrderFlagClose && isOrderFlag2Close;
        if (isNeedGoToSetAppointment) {
            AppointmentViewController *appointment = [[AppointmentViewController alloc] init];
            appointment.deviceModel = self.deviceModel;
            [self.navigationController pushViewController:appointment animated:YES];
            return;
        }
        
        functionFlagValue = [XLDataPointTool getDataPointValueWithValue:self.deviceModel.function_flag andShift:6 andIsOpen:!button.selected];
        
        BOOL b1 = [XLDataPointTool isOpenWithValue:self.deviceModel.dev_status andShift:4];
        if (b1 == YES) {//到达预约时间需要关闭浴缸
            functionFlagValue = [XLDataPointTool getDataPointValueWithValue:functionFlagValue andShift:4 andIsOpen:NO];
        }
    }else if ([button isEqual:functionView.btnHot] == YES) {//即热
        functionFlagValue = [XLDataPointTool getDataPointValueWithValue:self.deviceModel.function_flag andShift:3 andIsOpen:!button.selected];
        functionFlagValue = [XLDataPointTool getDataPointValueWithValue:functionFlagValue andShift:4 andIsOpen:NO];//点击即热需要关闭浴缸
    }else if ([button isEqual:functionView.btnComfortableBath] == YES) {//舒适浴
        functionFlagValue = [XLDataPointTool getDataPointValueWithValue:self.deviceModel.function_flag andShift:2 andIsOpen:!button.selected];
        //当舒适浴开启的时候，要根据环境温度设置热水器温度
        BOOL b1 = button.selected == NO;
        if (b1 == YES) {
            [self updateTempWithTempValue:[self getHeaterTemp] andIsNeedSetToDevice:YES];
        }
    }else if ([button isEqual:functionView.btnBathtub] == YES) {//浴缸
        BOOL b1 = [self getIsHeatOrAppointment];
        if (b1 == YES) {
            return;
        }
        
        functionFlagValue = [XLDataPointTool getDataPointValueWithValue:self.deviceModel.function_flag andShift:4 andIsOpen:!button.selected];
        
        BOOL b2 = button.selected == NO;
        BOOL b3 = [self getIsNeedToastFullWater];
        
        if (b2 && b3){
            [self showFullWaterAlert];
        }
        
    }else if ([button isEqual:functionView.btnWaterfallBath] == YES) {//瀑布浴
        functionFlagValue = [XLDataPointTool getDataPointValueWithValue:self.deviceModel.function_flag andShift:5 andIsOpen:!button.selected];
    }else if ([button isEqual:functionView.btnSwitch] == YES) {//开关
        functionFlagValue = [XLDataPointTool getDataPointValueWithValue:self.deviceModel.function_flag andShift:0 andIsOpen:!button.selected];
    }
    
    //先更新UI
    [self updateFunctionViewWithFunctionFlag:functionFlagValue];
    
    //获取设置端点数组
    NSArray *dataPointsArray = [self getDataPointsArrayWithFunctionFlagValue:functionFlagValue andBtn:button];
    
    [self setDataPoints:dataPointsArray];
    
}

#pragma mark - UIAlertViewDelegate

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    int valueTemp = 50;

    if (buttonIndex == 0) {
        // 取消
        XLinkDataPoint *dataPointTemp = [XLinkDataPoint dataPointWithType:XLinkDataTypeByte withIndex:8 withValue:@(valueTemp)];
        [self setDataPoints:@[dataPointTemp]];
    }else {
        // 解锁
        NSInteger valueUnLock = [XLDataPointTool getDataPointValueWithValue:self.deviceModel.function_flag andShift:1 andIsOpen:YES];//解锁
        XLinkDataPoint *dataPointUnLock = [XLinkDataPoint dataPointWithType:XLinkDataTypeByte withIndex:16 withValue:@(valueUnLock)];
        XLinkDataPoint *dataPointTemp = [XLinkDataPoint dataPointWithType:XLinkDataTypeByte withIndex:8 withValue:@(valueTemp)];
        
        NSArray *dataPointArray = @[dataPointUnLock,dataPointTemp];
        
        [self.deviceModel addDataPoints:dataPointArray];
        
        [self setDataPoints:dataPointArray];
    }
}

@end
