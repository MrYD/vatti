//
//  AppointmentSetViewController.h
//  vatti
//
//  Created by 李叶 on 2018/6/12.
//  Copyright © 2018年 xlink. All rights reserved.
//

/**
 *  预约设置
 *
 *  @author BZH
 */

#import "BaseViewController.h"
#import "WaterHeaterDeviceModel.h"
#import "AppointmentModel.h"

@protocol AppointmentSetDelegate <NSObject>

@optional

- (void)completeCallBack;

@end

@interface AppointmentSetViewController : BaseViewController

@property (strong, nonatomic) WaterHeaterDeviceModel *deviceModel;

@property (strong, nonatomic) AppointmentModel *appointmentModel;

@property (weak,   nonatomic) id<AppointmentSetDelegate> delegate;

@end

