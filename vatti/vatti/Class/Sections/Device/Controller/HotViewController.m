//
//  HotViewController.m
//  vatti
//
//  Created by BZH on 2018/6/10.
//  Copyright © 2018年 xlink. All rights reserved.
//

#import "HotViewController.h"
#import "HotCell.h"
#import "SetHotController.h"
#import "UIColor+XLCategory.h"
#import "XLUtilTool.h"

@interface HotViewController ()<UITableViewDelegate,UITableViewDataSource,SelectCallbackDelegate>

@property (weak,   nonatomic) IBOutlet UITableView *tabview;

@property (assign, nonatomic) int diff_temp;
@property (assign, nonatomic) int keep_time;

@end

static NSString *const XLHotCell = @"XLHotCell";

@implementation HotViewController

- (void)viewDidLoad {
    [super viewDidLoad];
}

- (void)initSubViews{
    [super initSubViews];
    
    //naiv
    self.navigationItem.title = XLLocalizeString(@"即热");

    //tableView
    [self.tabview registerNib:[UINib nibWithNibName:NSStringFromClass([HotCell class]) bundle:nil] forCellReuseIdentifier:XLHotCell];

}

- (void)buildData
{
    [super buildData];

    self.diff_temp = self.deviceModel.diff_temp == 0 ? 5 : self.deviceModel.diff_temp;
    self.keep_time = self.deviceModel.keep_time == 0 ? 45 : self.deviceModel.keep_time;
    
}

#pragma mark: -- UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 2;
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    HotCell *cell = [tableView dequeueReusableCellWithIdentifier:XLHotCell];
    
    if (indexPath.row == 0){
        cell.name.text = @"保温温差";
        cell.subName.text = [NSString stringWithFormat:@"%d℃",self.diff_temp];
    }else if (indexPath.row == 1){
        cell.name.text = @"保温时长";
        cell.subName.text = [NSString stringWithFormat:@"%d min",self.keep_time];
    }
    
    return cell;
}

#pragma mark: -- UITableViewDelegate

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    return 50.;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    SetHotController *setHot = [[SetHotController alloc]init];
    setHot.type = indexPath.row;
    setHot.delegate = self;
    
    if (indexPath.row == 0){
        setHot.defaultValue = self.diff_temp;
    }else if (indexPath.row == 1){
        setHot.defaultValue = self.keep_time;
    }
    [self.navigationController pushViewController:setHot animated:YES];
}

#pragma mark: -- SelectCallbackDelegate
- (void)selectDataWithValue:(int)value type:(int)type{
    //Data
    if (value == 0){
        return;
    }
    if (type == 0){
        self.diff_temp = value;
    }else if (type == 1){
        self.keep_time = value;
    }
    
    [self setDataPoint];
    
    //UI
    [self.tabview reloadData];
}

/**
 设置端点数据
 */
- (void)setDataPoint{
    @weakify(self)
    
    //游客模式判断
    if (XL_DATA_VIRTUAL.isLogin == NO){
        self.deviceModel.diff_temp = self.diff_temp;
        self.deviceModel.keep_time = self.keep_time;
        return;
    }
    
    XLinkDataPoint *dataPoint1 = [XLinkDataPoint dataPointWithType:2 withIndex:18 withValue:@(self.diff_temp)];
    XLinkDataPoint *dataPoint2 = [XLinkDataPoint dataPointWithType:2 withIndex:19 withValue:@(self.keep_time)];

    KShowLoading;
    [[XLServiceManager shareInstance].deviceService setDeviceDataPoints:self.deviceModel.device dataPoints:@[dataPoint1,dataPoint2] timeout:10. handler:^(id result, NSError *error) {
        @strongify(self)
        KHideLoading;
        
        if (error == nil) {
            [self.deviceModel addDataPoints:@[dataPoint1,dataPoint2]];
        }else{
            KShowErrorLoadingWithMsg(XLHint_SetDataPontFailed);
        }
    }];
}

@end
