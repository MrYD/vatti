//
//  CurrentDeviceAlarmViewController.m
//  vatti
//
//  Created by BZH on 2018/6/19.
//  Copyright © 2018年 xlink. All rights reserved.
//

#import "CurrentDeviceAlarmViewController.h"
#import "MessageCell.h"

@interface CurrentDeviceAlarmViewController ()<UITableViewDelegate,UITableViewDataSource>

@property (weak,   nonatomic) IBOutlet UITableView *tableview;

///数据源
@property (strong, nonatomic) NSMutableArray *dataSourceArray;

@property (strong, nonatomic) DishWasherDeviceModel *dishwasherDeviceModel;

@property (strong, nonatomic) WaterHeaterDeviceModel *waterHeaterDeviceModel;

@end

static NSString *const XLCurrentDeviceAlarmCellID = @"XLCurrentDeviceAlarmCellID";

@implementation CurrentDeviceAlarmViewController

- (void)viewDidLoad {
    [super viewDidLoad];
}
-(void)setDeviceModel:(DeviceModel *)deviceModel
{
    if ([deviceModel isKindOfClass:[DishWasherDeviceModel class]]) {
        self.dishwasherDeviceModel = (DishWasherDeviceModel*)deviceModel;
    }else if ([deviceModel isKindOfClass:[WaterHeaterDeviceModel class]]) {
        self.waterHeaterDeviceModel = (WaterHeaterDeviceModel*)deviceModel;
    }
}
- (void)initConfigure
{
    [super initConfigure];
    
    self.dataSourceArray = [NSMutableArray array];
}

- (void)initSubViews{
    [super initSubViews];
    
    //navi
    self.navigationItem.title = @"告警列表";
    
    //tableView
    [self.tableview registerNib:[UINib nibWithNibName:NSStringFromClass([MessageCell class]) bundle:nil] forCellReuseIdentifier:XLCurrentDeviceAlarmCellID];
}

- (void)buildData
{
    [super buildData];
    
    @weakify(self)
    [[RACObserve(self.dishwasherDeviceModel, err_code) deliverOnMainThread] subscribeNext:^(id  _Nullable x) {
        @strongify(self)
        
        //data
        [self configureErrorNum];

        //ui
        [self.tableview reloadData];
    }];
    [[RACObserve(self.waterHeaterDeviceModel, master_err) deliverOnMainThread] subscribeNext:^(id  _Nullable x) {
        @strongify(self)
        
        //data
        [self configureErrorNum];
        
        //ui
        [self.tableview reloadData];
    }];

}

#pragma mark - Data

- (void)configureErrorNum
{
    [self.dataSourceArray removeAllObjects];
    
    
    if ([self.deviceModel.productId isEqualToString:@"1607d2b688d41f411607d2b688d47a01"]) {
        
        uint8_t value = self.dishwasherDeviceModel.err_code;
        
        
        
        
        if (value == 0) {
        }else if (value == 1){
            [self.dataSourceArray addObject:@"E01 热敏电阻短路"];
        }else if (value == 2){
            [self.dataSourceArray addObject:@"E02 热敏电阻开路"];
        }else if (value == 3){
            [self.dataSourceArray addObject:@"E03 机器不加热"];
        }else if (value == 4){
            [self.dataSourceArray addObject:@"E04 进水阀故障"];
        }else if (value == 5){
            [self.dataSourceArray addObject:@"E05 溢流"];
        }else if (value == 6){
            [self.dataSourceArray addObject:@"E06 机器加热异常"];
        }else if (value == 7){
            //        [self.dataSourceArray addObject:@"洗涤程序运行异常"];
        }else if (value == 8){
            [self.dataSourceArray addObject:@"E08 洗涤程序运行异常"];
        }else if (value == 9){
            [self.dataSourceArray addObject:@"E09 按键出错"];
        }
        
        return;
    }
    
    
    UInt16 value = self.waterHeaterDeviceModel.master_err;
    
    if (value == 0) {
    }else if (value == 1){
        [self.dataSourceArray addObject:@"40min长时间燃烧报警."];
    }else if (value == 2){
        [self.dataSourceArray addObject:@"CO报警."];
    }else if (value == 3){
        [self.dataSourceArray addObject:@"CH4报警."];
    }else if (value == 4){
        [self.dataSourceArray addObject:@"EP故障（循环水泵启动异常）."];
    }else if (value == 5){
        [self.dataSourceArray addObject:@"主控板与显示板通讯异常."];
    }else if (value == 6){
        [self.dataSourceArray addObject:@"环境温度传感器异常."];
    }else if (value == 7){
        [self.dataSourceArray addObject:@"EEPROM数据异常."];
    }else if (value == 8){
        [self.dataSourceArray addObject:@"火焰检测电路异常或意外熄火."];
    }else if (value == 9){
        [self.dataSourceArray addObject:@"出水温度异常."];
    }else if (value == 10){
        [self.dataSourceArray addObject:@"75度连续10S 或80度连续5S."];
    }else if (value == 11){
        [self.dataSourceArray addObject:@"超温E5."];
    }else if (value == 12){
        [self.dataSourceArray addObject:@"电磁阀驱动电路异常."];
    }else if (value == 13){
        [self.dataSourceArray addObject:@"风压开关检测电路异常."];
    }else if (value == 14){
        [self.dataSourceArray addObject:@"调水阀异常."];
    }else if (value == 15){
        [self.dataSourceArray addObject:@"进水温度传感器异常."];
    }else if (value == 16){
        [self.dataSourceArray addObject:@"燃烧室侧边温度异常."];
    }else if (value == 17){
        [self.dataSourceArray addObject:@"火焰溢出异常."];
    }else if (value == 18){
        [self.dataSourceArray addObject:@"风机异常."];
    }
}

#pragma mark - Action

/**
 打电话
 */
- (IBAction)btnCallClicked:(UIButton *)sender {
    NSMutableString *phoneStr = [[NSMutableString alloc] initWithFormat:@"telprompt://%@",@"4008886288"];
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:phoneStr]];
}


#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 1;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return self.dataSourceArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    MessageCell *cell = [tableView dequeueReusableCellWithIdentifier:XLCurrentDeviceAlarmCellID];
    cell.errorNum = [self.dataSourceArray objectAtIndex:indexPath.section];
    return cell;
    
}

@end
