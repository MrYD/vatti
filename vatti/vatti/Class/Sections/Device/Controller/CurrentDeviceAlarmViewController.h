//
//  CurrentDeviceAlarmViewController.h
//  vatti
//
//  Created by BZH on 2018/6/19.
//  Copyright © 2018年 xlink. All rights reserved.
//

/**
 *  告警列表
 *
 *  @author BZH
 */

#import "BaseViewController.h"
#import "WaterHeaterDeviceModel.h"

@interface CurrentDeviceAlarmViewController : BaseViewController

@property (strong, nonatomic) DeviceModel *deviceModel;

@end
