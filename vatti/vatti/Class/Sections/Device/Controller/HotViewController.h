//
//  HotViewController.h
//  vatti
//
//  Created by BZH on 2018/6/10.
//  Copyright © 2018年 xlink. All rights reserved.
//

/**
 *  即热设置
 *
 *  @author BZH
 */

#import "BaseWaterHeaterDeviceViewController.h"

@interface HotViewController : BaseWaterHeaterDeviceViewController

@end
