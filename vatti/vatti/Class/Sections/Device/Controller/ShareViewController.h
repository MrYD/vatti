//
//  ShareViewController.h
//  vatti
//
//  Created by 李叶 on 2018/6/10.
//  Copyright © 2018年 xlink. All rights reserved.
//

#import "BaseViewController.h"
#import "WaterHeaterDeviceModel.h"
#import "MemberModel.h"

@interface ShareViewController : BaseViewController

@property (strong, nonatomic) DeviceModel *deviceModel;
@property (strong, nonatomic) MemberModel *memberModel;
@end
