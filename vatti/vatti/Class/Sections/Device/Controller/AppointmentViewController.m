//
//  AppointmentViewController.m
//  vatti
//
//  Created by BZH on 2018/6/12.
//  Copyright © 2018年 xlink. All rights reserved.
//

#import "AppointmentViewController.h"
#import "AppointmentSetViewController.h"
#import "AppointmentCell.h"
#import "XLUtilTool.h"
#import "AppointmentModel.h"
#import "UIColor+XLCategory.h"
#import "XLinkProbeDataPointTask.h"
#import "XLinkSetDataPointTask.h"
#import "XLWaterHeaterDeviceDao.h"

@interface AppointmentViewController ()<UITableViewDelegate,UITableViewDataSource,AppointmentSetDelegate,AppointmentCellDelegate>

@property (weak,   nonatomic) IBOutlet UITableView *tabview;
@property (weak,   nonatomic) IBOutlet UIView *noAPpointment;

#pragma mark - Data

///预约数据
@property (strong, nonatomic) NSMutableArray<AppointmentModel*> *appoinmentArray;
///没预约数据
@property (strong, nonatomic) NSMutableArray<AppointmentModel*> *noAppoinmentArray;
///indexArray
@property (strong, nonatomic) NSMutableArray *indexArray;
///1-8预约开关缓存
@property (assign, nonatomic) NSInteger cacheValue1;
///9-12预约开关缓存
@property (assign, nonatomic) NSInteger cacheValue2;
@end

static NSString *const XLAppointmentCell = @"XLAppointmentCell";
@implementation AppointmentViewController

- (void)viewDidLoad {
    [super viewDidLoad];
}

- (void)initConfigure
{
    [super initConfigure];
    
    self.cacheValue1 = self.deviceModel.order_flag;
    self.cacheValue2 = self.deviceModel.order_flag2;
}

- (void)initSubViews{
    [super initSubViews];
    
    //naiv
    self.navigationItem.title = XLLocalizeString(@"预约");
    
    //tableView
    [self.tabview registerNib:[UINib nibWithNibName:NSStringFromClass([AppointmentCell class]) bundle:nil] forCellReuseIdentifier:XLAppointmentCell];
    
}

- (void)buildData
{
    [super buildData];
    
    //处理数据并更新UI
    [self refreshDataFromDB:NO];
    
    //RAC
    [self addRAC];
}

#pragma mark - UI

/**
 处理数据并更新UI
 */
- (void)refreshDataFromDB:(BOOL)isFromDB
{
    if (isFromDB == YES) {
        [XLWaterHeaterDeviceDao queryWithXDevice:self.deviceModel.device callBack:^(WaterHeaterDeviceModel * _Nullable xlWaterHeaterDevice) {
            if (xlWaterHeaterDevice == nil) {
                return;
            }
            
            //Data
            [xlWaterHeaterDevice addObserver];
            self.deviceModel = xlWaterHeaterDevice;
            [self getDataSourceArrayWithDeviceModel:self.deviceModel];
            
            //UI
            [self.tabview reloadData];
        }];
    }else{
        //Data
        [self getDataSourceArrayWithDeviceModel:self.deviceModel];
        
        self.cacheValue1 = self.deviceModel.order_flag;
        self.cacheValue2 = self.deviceModel.order_flag2;
        
        //UI
        [self.tabview reloadData];
    }
}

#pragma mark - Data

- (void)addRAC
{
    @weakify(self)
    
    [[RACObserve(self.deviceModel, order_flag).distinctUntilChanged deliverOnMainThread] subscribeNext:^(id  _Nullable x) {
        @strongify(self)
        
        [self configureRACValueChange];
    }];
    
    [[RACObserve(self.deviceModel, order_flag2).distinctUntilChanged deliverOnMainThread] subscribeNext:^(id  _Nullable x) {
        @strongify(self)
        
        [self configureRACValueChange];
    }];
    
    [[RACObserve(self.deviceModel, order2).distinctUntilChanged deliverOnMainThread] subscribeNext:^(id  _Nullable x) {
        @strongify(self)
        
        [self configureRACValueChange];
    }];
    
    [[RACObserve(self.deviceModel, order3).distinctUntilChanged deliverOnMainThread] subscribeNext:^(id  _Nullable x) {
        @strongify(self)
        
        [self configureRACValueChange];
    }];
    
    [[RACObserve(self.deviceModel, order4).distinctUntilChanged deliverOnMainThread] subscribeNext:^(id  _Nullable x) {
        @strongify(self)
        
        [self configureRACValueChange];
    }];
    
    [[RACObserve(self.deviceModel, order5).distinctUntilChanged deliverOnMainThread] subscribeNext:^(id  _Nullable x) {
        @strongify(self)
        
        [self configureRACValueChange];
    }];
    
    [[RACObserve(self.deviceModel, order6).distinctUntilChanged deliverOnMainThread] subscribeNext:^(id  _Nullable x) {
        @strongify(self)
        
        [self configureRACValueChange];
    }];
    
    [[RACObserve(self.deviceModel, order7).distinctUntilChanged deliverOnMainThread] subscribeNext:^(id  _Nullable x) {
        @strongify(self)
        
        [self configureRACValueChange];
    }];
    
    [[RACObserve(self.deviceModel, order8).distinctUntilChanged deliverOnMainThread] subscribeNext:^(id  _Nullable x) {
        @strongify(self)
        
        [self configureRACValueChange];
    }];
    
    [[RACObserve(self.deviceModel, order9).distinctUntilChanged deliverOnMainThread] subscribeNext:^(id  _Nullable x) {
        @strongify(self)
        
        [self configureRACValueChange];
    }];
    
    [[RACObserve(self.deviceModel, order10).distinctUntilChanged deliverOnMainThread] subscribeNext:^(id  _Nullable x) {
        @strongify(self)
        
        [self configureRACValueChange];
    }];
    
    [[RACObserve(self.deviceModel, order11).distinctUntilChanged deliverOnMainThread] subscribeNext:^(id  _Nullable x) {
        @strongify(self)
        
        [self configureRACValueChange];
    }];
    
    [[RACObserve(self.deviceModel, order12).distinctUntilChanged deliverOnMainThread] subscribeNext:^(id  _Nullable x) {
        @strongify(self)
        
        [self configureRACValueChange];
    }];
}

- (void)getDataSourceArrayWithDeviceModel:(WaterHeaterDeviceModel*)deviceModel{
    
    self.appoinmentArray   = [NSMutableArray array];
    self.noAppoinmentArray = [NSMutableArray array];
    
    [self getAppointmentModelWithOrder:deviceModel.order1 orderFlag:deviceModel.order_flag index:1];
    [self getAppointmentModelWithOrder:deviceModel.order2 orderFlag:deviceModel.order_flag index:2];
    [self getAppointmentModelWithOrder:deviceModel.order3 orderFlag:deviceModel.order_flag index:3];
    [self getAppointmentModelWithOrder:deviceModel.order4 orderFlag:deviceModel.order_flag index:4];
    [self getAppointmentModelWithOrder:deviceModel.order5 orderFlag:deviceModel.order_flag index:5];
    [self getAppointmentModelWithOrder:deviceModel.order6 orderFlag:deviceModel.order_flag index:6];
    [self getAppointmentModelWithOrder:deviceModel.order7 orderFlag:deviceModel.order_flag index:7];
    [self getAppointmentModelWithOrder:deviceModel.order8 orderFlag:deviceModel.order_flag index:8];
    [self getAppointmentModelWithOrder:deviceModel.order9 orderFlag:deviceModel.order_flag2 index:9];
    [self getAppointmentModelWithOrder:deviceModel.order10 orderFlag:deviceModel.order_flag2 index:10];
    [self getAppointmentModelWithOrder:deviceModel.order11 orderFlag:deviceModel.order_flag2 index:11];
    [self getAppointmentModelWithOrder:deviceModel.order12 orderFlag:deviceModel.order_flag2 index:12];

    BOOL hadAppointment = self.appoinmentArray.count > 0;
    self.noAPpointment.hidden = hadAppointment;
}

- (void)getAppointmentModelWithOrder:(NSInteger)order orderFlag:(NSInteger)orderFlag index:(NSInteger)index{
    
    AppointmentModel *appointmentMoel = [[AppointmentModel alloc] init];
    
    if (index > 8) {
        appointmentMoel.isOpen = [XLDataPointTool isOpenWithValue:orderFlag andShift:(index - 9)];
    }else{
        appointmentMoel.isOpen = [XLDataPointTool isOpenWithValue:orderFlag andShift:(index - 1)];
    }
    
    appointmentMoel.index        = index + 21;
    appointmentMoel.startHours   = (order >> 24) & 0xff;
    appointmentMoel.startMinutes = (order >> 16) & 0xff;
    appointmentMoel.endHours     = (order >> 8)  & 0xff;
    appointmentMoel.endMinutes   = (order >> 0)  & 0xff;
    
    NSLog(@"order:%lu,index:%lu \n",order,index);
    if (index == 1 || order > 0){
        [self.appoinmentArray   addObject:appointmentMoel];
    }else{
        [self.noAppoinmentArray addObject:appointmentMoel];
    }
    
}

- (NSMutableArray *)indexArray
{
    if (_indexArray == nil) {
        _indexArray = [NSMutableArray array];
        
        for (NSInteger i=0; i<41; i++) {
            NSNumber *numIndex = [NSNumber numberWithInteger:i];
            [_indexArray addObject:numIndex];
        }
    }
    
    return _indexArray;
}

- (void)configureRACValueChange
{
    if (self.isResponseChange == YES) {
        [self refreshDataFromDB:NO];
    }
}

/**
 重写父类定时器方法
 */
- (void)timerFinished:(NSTimer*)timer
{
    [super timerFinished:timer];
    
    //游客模式判断
    if (XL_DATA_VIRTUAL.isLogin == NO){
        return;
    }
    
    NSMutableString *mIndexString = [[NSMutableString alloc] init];
    [mIndexString appendString:@"正在获取以下端点:"];
    for (NSNumber *indexNum in self.indexArray) {
        [mIndexString appendFormat:@"%@,",indexNum];
    }
    NSLog(@"%@",mIndexString);
    
    NSLog(@"开始Probe");
    [[XLServiceManager shareInstance].deviceService probeDataPointWithDevice:self.deviceModel.device andIndexArray:self.indexArray andTimeOut:TimeOutSetDataPoint andCallBack:^(id  _Nullable result, NSError * _Nullable error) {
        if (error == nil) {
            NSLog(@"Probe成功");
            NSArray<XLinkDataPoint *> *dataPoints = result;
            
            if (dataPoints.count > 0) {
                for (XLinkDataPoint *dp in dataPoints) {
                    NSLog(@"端点%d的值为%d",dp.index,[dp.value intValue]);
                }
                
                [self.deviceModel addDataPoints:dataPoints];
                self.cacheValue1 = self.deviceModel.order_flag;
                self.cacheValue2 = self.deviceModel.order_flag2;
                
                [self refreshDataFromDB:NO];
            }else{
                NSLog(@"没有获取到数据端点");
            }
            
        }else {
            NSLog(@"Probe失败, %@", error.userInfo);
        }
    }];
}

#pragma mark - Action

- (void)configureEditActionWithIndex:(NSInteger)index
{
    AppointmentModel *appointmentModel = [self.appoinmentArray objectAtIndex:index];
    
    AppointmentSetViewController *vc = [[AppointmentSetViewController alloc] init];
    
    vc.appointmentModel = appointmentModel;
    
    vc.deviceModel      = self.deviceModel;
    vc.delegate         = self;
    
    [self.navigationController pushViewController:vc animated:YES];
}

- (void)configureDeleteActionWithIndex:(NSInteger)index {
    @weakify(self)
    
    [self alertHintWithTitle:@"提示" message:@"是否删除该预约" cancelTitle:@"取消" otherTitles:@[@"确定"] handler:^(UIAlertAction *action, NSUInteger tag) {
        @strongify(self)
        
        if (tag == 1) {//点击确定
            
            //1.判断设备是否可控制
            BOOL isDeviceCanControl = [self getIsDeviceCanControlWithIsNeedAlert:YES];
            if (isDeviceCanControl == NO) {
                return;
            }
            
            //2.处理发送端点前的逻辑
            [self startLogicalBefortSet];
            
            //3.生成数据端点
            AppointmentModel *appointmentModel = [self.appoinmentArray objectAtIndex:index];
            
            //时间改为0
            XLinkDataPoint *dataPointTime = [XLinkDataPoint dataPointWithType:XLinkDataTypeUnsignedInt withIndex:appointmentModel.index withValue:@(0)];
            
            //开关改为关闭
            XLinkDataPoint *dataPointSwitch;
            NSInteger index = appointmentModel.index - 21;
            BOOL isNeedClosAllAppointment = NO;
            
            if (index > 8) {
                NSInteger switchValue = [appointmentModel getValueWithIsOpen:NO andCacheValue:self.cacheValue2];
                dataPointSwitch = [XLinkDataPoint dataPointWithType:XLinkDataTypeByte withIndex:21 withValue:@(switchValue)];
                isNeedClosAllAppointment = switchValue == 0 && (self.cacheValue1 == 0);
            }else{
                NSInteger switchValue = [appointmentModel getValueWithIsOpen:NO andCacheValue:self.cacheValue1];
                dataPointSwitch = [XLinkDataPoint dataPointWithType:XLinkDataTypeByte withIndex:20 withValue:@(switchValue)];
                isNeedClosAllAppointment = switchValue == 0 && (self.cacheValue2 == 0);
            }
            
            NSArray <XLinkDataPoint*> *dataPointsArray = @[dataPointTime,dataPointSwitch];
            
            if (isNeedClosAllAppointment) {
                NSInteger valueFunction = [XLDataPointTool getDataPointValueWithValue:self.deviceModel.function_flag andShift:6 andIsOpen:NO];
                XLinkDataPoint *dataPointFunction = [XLinkDataPoint dataPointWithType:XLinkDataTypeByte withIndex:16 withValue:@(valueFunction)];
                dataPointsArray = @[dataPointTime,dataPointSwitch, dataPointFunction];
            }
            
            //获取当前登录用户ID，加入数据端点
            NSMutableArray *mDataPoints = [self addUserIdDataPointWithDataPointsArray:dataPointsArray];
            
            //提前加入数据端点到内存
            [self.deviceModel addDataPoints:mDataPoints];
            //刷新数据
            [self refreshDataFromDB:NO];
            
            //游客模式判断
            if (XL_DATA_VIRTUAL.isLogin == NO){
                return;
            }
            
            //判断是否设置相同任务
            //如果是，则取消上一个任务
            [self cancelLastSameTaskWithNewDataPointsArray:mDataPoints];
            
            //发起请求
            [self setDataPoints:mDataPoints];

        }
    }];
}

- (IBAction)appointmentAction:(UIButton *)sender {
    
    if (self.appoinmentArray.count >= 12){
        [self alertHintWithMessage:@"预约最多只可设置12条"];
        return;
    }
    
    AppointmentSetViewController *vc = [[AppointmentSetViewController alloc] init];
    
    AppointmentModel *appointmentModel = [self.noAppoinmentArray firstObject];
    vc.appointmentModel  = appointmentModel;
    
    vc.deviceModel       = self.deviceModel;
    vc.delegate          = self;
    
    [self.navigationController pushViewController:vc animated:YES];
}

#pragma mark - Request

- (void)setDataPoints:(NSArray <XLinkDataPoint*> *)dataPoints
{
    @weakify(self)
    self.setDataPointTask = [[XLServiceManager shareInstance].deviceService setDeviceDataPoints:self.deviceModel.device dataPoints:dataPoints timeout:TimeOutSetDataPoint handler:^(id result, NSError *error) {
        @strongify(self)
        
        if (error == nil) {
            
        }else{
            KShowErrorLoadingWithMsg(XLHint_SetDataPontFailed);
            //回滚
            [self refreshDataFromDB:YES];
        }
    }];
}

#pragma mark - UITableViewDataSource && UITableViewDelegate

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.appoinmentArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    NSInteger rowNum = indexPath.row;
    
    AppointmentModel *appointmentModel = [self.appoinmentArray objectAtIndex:rowNum];
    
    AppointmentCell *cell = [tableView dequeueReusableCellWithIdentifier:XLAppointmentCell];
    cell.delegate = self;
    cell.appointmentModel = appointmentModel;

    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

/**
 左滑cell时按钮Delegate方法
 */
- (NSArray *)tableView:(UITableView *)tableView editActionsForRowAtIndexPath:(NSIndexPath *)indexPath
{
    @weakify(self)
    
    NSInteger rowNum = indexPath.row;
    
    AppointmentModel *appointmentModel = [self.appoinmentArray objectAtIndex:rowNum];
    if (appointmentModel.index == 22) {
        return @[];
    }
    
    UITableViewRowAction *actionEdit = [UITableViewRowAction rowActionWithStyle:UITableViewRowActionStyleNormal title:@"编辑" handler:^(UITableViewRowAction *action, NSIndexPath *indexPath) {
        @strongify(self)
        
        [self configureEditActionWithIndex:rowNum];
        
    }];
    
    actionEdit.backgroundColor = [UIColor colorWithHexString:XLGreenColor];
    
    UITableViewRowAction *actionDelete = [UITableViewRowAction rowActionWithStyle:UITableViewRowActionStyleDefault title:@"删除" handler:^(UITableViewRowAction *action, NSIndexPath *indexPath) {
        @strongify(self)
        [self configureDeleteActionWithIndex:rowNum];
    }];
    
    return @[actionDelete, actionEdit];
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 50;
}

#pragma mark - AppointmentSetDelegate

- (void)completeCallBack {
    [self refreshDataFromDB:NO];
}

#pragma mark - AppointmentCellDelegate

- (void)appointmentCell:(AppointmentCell *)cell dicClickedSwitch:(UISwitch *)sender
{
    //1.判断设备是否可控制
    BOOL isDeviceCanControl = [self getIsDeviceCanControlWithIsNeedAlert:YES];
    if (isDeviceCanControl == NO) {
        [sender setOn:!sender.isOn animated:YES];
        return;
    }
    
    //2.处理发送端点前的逻辑
    [self startLogicalBefortSet];
    
    //3.生成数据
    AppointmentModel *appointmentModel = cell.appointmentModel;
    appointmentModel.isOpen = !appointmentModel.isOpen;
    
    NSInteger index = appointmentModel.index - 21;
    
    NSInteger cacheValue = 0;
    if (index > 8){
        self.cacheValue2 = [appointmentModel getValueWithIsOpen:sender.isOn andCacheValue:self.cacheValue2];
        cacheValue = self.cacheValue2;
    }else{
        self.cacheValue1 = [appointmentModel getValueWithIsOpen:sender.isOn andCacheValue:self.cacheValue1];
        cacheValue = self.cacheValue1;
    }

    BOOL isNeedCloseAllAppointment = cacheValue == 0;
    
    NSMutableArray *dataPoints = [NSMutableArray array];
    XLinkDataPoint *dataPoint;
    
    if (index > 8){
        dataPoint = [XLinkDataPoint dataPointWithType:2 withIndex:21 withValue:@(cacheValue)];
        isNeedCloseAllAppointment = isNeedCloseAllAppointment && (self.cacheValue1 == 0);
    }else{
        dataPoint = [XLinkDataPoint dataPointWithType:2 withIndex:20 withValue:@(cacheValue)];
        isNeedCloseAllAppointment = isNeedCloseAllAppointment && (self.cacheValue2 == 0);
    }
    [dataPoints addObject:dataPoint];
    
    if (isNeedCloseAllAppointment) {
        NSInteger funValue = [XLDataPointTool getDataPointValueWithValue:self.deviceModel.function_flag andShift:6 andIsOpen:NO];
        XLinkDataPoint *dataPointAllAppointment = [XLinkDataPoint dataPointWithType:XLinkDataTypeByte withIndex:16 withValue:@(funValue)];
        [dataPoints addObject:dataPointAllAppointment];
    }
    //游客模式判断
    if (XL_DATA_VIRTUAL.isLogin == NO){
        [self.deviceModel addDataPoints:dataPoints];
        [self refreshDataFromDB:NO];
        return;
    }
    
    //获取当前登录用户ID，加入数据端点
    NSMutableArray *mDataPoints = [self addUserIdDataPointWithDataPointsArray:dataPoints];
    //先把数据端点写入内存
    [self.deviceModel addDataPoints:mDataPoints];
    
    //判断是否设置相同任务
    //如果是，则取消上一个任务
    [self cancelLastSameTaskWithNewDataPointsArray:mDataPoints];
    
    //发起请求
    [self setDataPoints:mDataPoints];
    
}
@end
