//
//  DeviceViewController.m
//  vatti
//
//  Created by MrYD on 2018/12/24.
//  Copyright © 2018年 xlink. All rights reserved.
//

#import "DeviceViewController.h"
#import "DeviceCell.h"
#import "DishWasherCell.h"
#import "XLUtilTool.h"
#import "DeviceGuideViewController.h"
#import "WaterHeaterDeviceModel.h"
#import "DeviceControlViewController.h"
#import "XLNetworkModule.h"
#import "LoginViewController.h"
#import "BaseNavigationViewController.h"
#import "UIColor+XLCategory.h"
#import "MJRefresh.h"
#import "XLScanDeviceHelperTool.h"
#import "UIImage+XLCategory.h"
#import "AddDeviceSelectionViewController.h"
#import "UIScrollView+EmptyDataSet.h"
#import "StrongWashViewController.h"
#import "DishWasherController.h"

@interface DeviceViewController ()<UITableViewDelegate,UITableViewDataSource,UITextFieldDelegate,DZNEmptyDataSetDelegate,DZNEmptyDataSetSource>

#pragma mark - IBOutlet

#pragma mark - UIView
///列表
@property (weak,   nonatomic) IBOutlet UITableView *tableView;

#pragma mark - UIButton
///添加设备 按钮
@property (weak,   nonatomic) IBOutlet UIButton *btnAdd;

#pragma mark - Data
///设备模型数组
@property (strong, nonatomic) NSMutableArray <DeviceModel*> *deviceArray;

@end

static NSString *const XLDeviceCellID = @"XLDeviceCellID";
static NSString *const DishWasherDeviceCellID = @"DishWasherDeviceCellID";

@implementation DeviceViewController

- (void)viewDidLoad {
    [super viewDidLoad];

}

- (void)initConfigure
{
    [super initConfigure];
    
    self.deviceArray        = [NSMutableArray array];
}

- (void)initSubViews{
    [super initSubViews];
    
    //navi
    [self setupNavi];
    
    //tableView
    [self setupTableView];
    
    //add btn
    [self setupAddBtn];
    
}

- (void)buildData
{
    [super buildData];
    
    @weakify(self)
    
    [RACObserve(XL_DATA_SOURCE, user.deviceModels).deliverOnMainThread subscribeNext:^(id  _Nullable x) {
        @strongify(self);
        [self reloadTableViewWithMonitoring];
    }];
    
    if ([XLUtilTool isValidateAutoLogin] == YES) {
        [self.tableView.mj_header beginRefreshing];
    }
}

- (void)viewWillLayoutSubviews
{
    [super viewWillLayoutSubviews];
}

#pragma mark - Action

/**
 添加设备
 */
- (IBAction)btnAddClicked:(UIButton *)sender {
    if ([XLUtilTool isValidateAutoLogin] == NO){
        
        LoginViewController *login = [[LoginViewController alloc] init];

        BaseNavigationViewController *nvc = [[BaseNavigationViewController alloc] initWithRootViewController:login];
        [nvc.navigationBar setBackgroundImage:[UIImage xl_imageForColor:[UIColor colorWithRed:255/255. green:255/255. blue:255/255. alpha:1]] forBarMetrics:UIBarMetricsDefault];
        [XLUtilTool setRootViewController:nvc animationOptions:UIViewAnimationOptionTransitionCrossDissolve duration:0.3];
        return;
        
    }
    
#define RanReOnly

#ifdef RanReOnly
    //只走燃热配网
    DeviceGuideViewController *vc = [[DeviceGuideViewController alloc] init];
    vc.productId = @"1607d2b688d41f411607d2b688d47a01";
    [self.navigationController pushViewController:vc animated:YES];
#else
    //多设备配网
    AddDeviceSelectionViewController *vc = [[AddDeviceSelectionViewController alloc]init];
    [self.navigationController pushViewController:vc animated:YES];
#endif

}

- (void)showAlertWithDeviciModel:(DeviceModel *)deviceModel{

    @weakify(self)
    
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"重命名设备" message:@"" preferredStyle:UIAlertControllerStyleAlert];
    [alert addTextFieldWithConfigurationHandler:^(UITextField *textField){
        
        NSString *deviceName;
        if (deviceModel.name != nil) {
            deviceName = deviceModel.name;
        }else{
            deviceName = XLDeviceGasWaterHeaterName;
        }
        
        textField.text = deviceName;
        
        textField.placeholder = @"";
        textField.clearButtonMode = UITextFieldViewModeWhileEditing;
        textField.delegate = self;
        [textField addTarget:self action:@selector(tfRenameChanged:) forControlEvents:(UIControlEventEditingChanged)];
    }];
    
    UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        UITextField *login = alert.textFields.firstObject;
        if (login.text.length <= 0){
            [self showErrorWithMessage:@"请输入设备名称"];
            return;
        }else if ([XLUtilTool isEmpty:login.text]){
            [self showErrorWithMessage:@"设备名称不可全为空格"];
            return;
        }
        
        KShowLoading;
        NSString *name = login.text;
        [[XLServiceManager shareInstance].deviceService modifyDeviceNameWithDeviceId:deviceModel.deviceId productId:deviceModel.productId deviceName:login.text timeout:10 handler:^(id result, NSError *error) {
            @strongify(self)
            KHideLoading;
            if (error == nil){
                deviceModel.name = name;
            }else{
                KShowErrorLoadingWithMsg(@"重命名失败");
            }
        }];
        
    }];
    
    UIAlertAction *cancle = [UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {
        
    }];
    
    [alert addAction:cancle];
    [alert addAction:okAction];
    [self presentViewController:alert animated:YES completion:nil];
    
}

- (void)showVirtualDeviceAtIndexPath:(NSIndexPath *)indexPath{
    NSInteger rowNum = indexPath.row;
    
    DeviceModel *deviceModel = [self.deviceArray objectAtIndex:rowNum];

    if ([deviceModel.productId isEqualToString:@"1607d2b688d41f411607d2b688d47a01"]) {
        DishWasherController *dishWasherVCtrl = [[DishWasherController alloc] init];
        dishWasherVCtrl.deviceModel = (DishWasherDeviceModel*)deviceModel;
        
        if ([XLUtilTool isValidateAutoLogin]) [deviceModel getDeviceDataPoints];
        XL_DATA_VIRTUAL.isLogin = [XLUtilTool isValidateAutoLogin];
        if ((dishWasherVCtrl.deviceModel.dev_statu == 3) || (dishWasherVCtrl.deviceModel.dev_statu == 4)) {
            
            StrongWashViewController *strongWashViewController = [[StrongWashViewController alloc] init];
            strongWashViewController.deviceModel = dishWasherVCtrl.deviceModel;
//            [self.navigationController pushViewController:strongWashViewController animated:YES];
            NSLog(@"===self.navigationController.visibleViewController = %@", self.navigationController.visibleViewController);
            NSLog(@"===self.navigationController.topViewController = %@", self.navigationController.topViewController);
//            self.navigationController.viewControllers = @[self,dishWasherVCtrl,strongWashViewController];
//            [self.navigationController pushViewController:strongWashViewController animated:YES];
            dishWasherVCtrl.hidesBottomBarWhenPushed = YES;
            [self.navigationController setViewControllers:@[self,dishWasherVCtrl,strongWashViewController] animated:YES];
        }else{
            [self.navigationController pushViewController:dishWasherVCtrl animated:YES];
        }
        return;
    }
    
    DeviceControlViewController *vc = [[DeviceControlViewController alloc]init];
    vc.deviceModel = (WaterHeaterDeviceModel*)deviceModel;
    if ([XLUtilTool isValidateAutoLogin]) {
        [deviceModel getDeviceDataPoints];
    }
    XL_DATA_VIRTUAL.isLogin = [XLUtilTool isValidateAutoLogin];
    
    [self.navigationController pushViewController:vc animated:YES];
    
}

- (void)deleteDeviceWithDeviceModel:(DeviceModel *)deviceModel{
    NSString *name = deviceModel.name;
    if (name == nil || name.length == 0) {
        name = XLDeviceGasWaterHeaterName;
    }
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"删除设备" message:[NSString stringWithFormat:@"是否删除%@",name] preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *sure = [UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        KShowLoading;
        [[XLServiceManager shareInstance].deviceService removeDevice:deviceModel.device timeout:90 completionHandler:^(id result, NSError *error) {
            KHideLoading;
            
            if (error == nil){
                [XL_DATA_SOURCE.user removeDeviceModel:deviceModel];
            }else{
                KShowErrorLoadingWithMsg(@"删除失败");
            }
            
        }];
    }];
    
    UIAlertAction *cancle = [UIAlertAction actionWithTitle:@"取消" style:(UIAlertActionStyleCancel) handler:^(UIAlertAction * _Nonnull action) {
        
    }];
    
    [alert addAction:cancle];
    [alert addAction:sure];
    
    [self presentViewController:alert animated:YES completion:nil];
    
}

/**
 刷新tableView
 */
- (void)reloadTableViewWithMonitoring{
    
    //处理数据
    [self configureData];
    
    [self.tableView reloadData];
}

#pragma mark - UI

- (void)setupNavi
{
    self.navigationItem.title = @"华帝·智尚心居";
}

- (void)setupTableView
{
    [self.tableView registerNib:[UINib nibWithNibName:NSStringFromClass([DeviceCell class]) bundle:nil] forCellReuseIdentifier:XLDeviceCellID];
    [self.tableView registerNib:[UINib nibWithNibName:NSStringFromClass([DishWasherCell class]) bundle:nil] forCellReuseIdentifier:DishWasherDeviceCellID];
    
    
    self.tableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        [self getAccountSubscribeDeviceList];
    }];
    
}

- (void)setupAddBtn
{
    [self.btnAdd setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
    
    self.btnAdd.layer.borderWidth = 1.0;
    self.btnAdd.layer.borderColor = [UIColor grayColor].CGColor;
    self.btnAdd.layer.cornerRadius = 8;
    self.btnAdd.layer.masksToBounds = YES;
}

- (void)configureData
{
    if ([XLUtilTool isValidateAutoLogin] == YES){
        self.deviceArray = [NSMutableArray arrayWithArray:XL_DATA_SOURCE.user.deviceModels];
    }else{
        XL_DATA_VIRTUAL.deviceModel = [[WaterHeaterDeviceModel alloc] init];
        self.deviceArray = [NSMutableArray arrayWithObject:XL_DATA_VIRTUAL.deviceModel];
        XL_DATA_VIRTUAL.dishWasherModel = [[DishWasherDeviceModel alloc] init];
        [self.deviceArray addObject:XL_DATA_VIRTUAL.dishWasherModel];
    }
    
}

#pragma mark - Request

- (void)getAccountSubscribeDeviceList
{
    @weakify(self)
    
    [XL_NETWORK_MODULE getAccountSubscribeDeviceListWithXLUserModel:XL_DATA_SOURCE.user handler:^(BOOL a) {
        @strongify(self)
        [self.tableView.mj_header endRefreshing];
    }];
}

#pragma mark - UITableViewDelegate&UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    if ([XLUtilTool isValidateAutoLogin] == YES){
        return self.deviceArray.count;
    }else{
        return 2;
    }
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 70.f;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    NSInteger rowNum = indexPath.row;
    
    DeviceModel *deviceModel = [self.deviceArray objectAtIndex:rowNum];
    if ([deviceModel.productId isEqualToString:@"1607d2b688d41f411607d2b688d47a01"]) {
        DishWasherCell *cell = [tableView dequeueReusableCellWithIdentifier:DishWasherDeviceCellID];
        cell.deviceModel = (DishWasherDeviceModel*)deviceModel;
        return cell;
    }
    
    DeviceCell *cell = [tableView dequeueReusableCellWithIdentifier:XLDeviceCellID];
    cell.deviceModel = (WaterHeaterDeviceModel*)deviceModel;
    return cell;
    
//    DeviceCell *cell = [tableView dequeueReusableCellWithIdentifier:XLDeviceCellID];
//
//    WaterHeaterDeviceModel *deviceModel = [self.deviceArray objectAtIndex:rowNum];
//    cell.deviceModel = deviceModel;
//
//    return cell;
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    [self showVirtualDeviceAtIndexPath:indexPath];
    
}

/**
 左滑cell时出现按钮代理方法
 */
- (NSArray *)tableView:(UITableView *)tableView editActionsForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSInteger rowNum = indexPath.row;
    
    UITableViewRowAction *actionRename = [UITableViewRowAction rowActionWithStyle:UITableViewRowActionStyleNormal title:@"重命名" handler:^(UITableViewRowAction *action, NSIndexPath *indexPath) {
        [self showAlertWithDeviciModel:[self.deviceArray objectAtIndex:rowNum]];
    }];
    actionRename.backgroundColor = [UIColor colorWithHexString:XLGreenColor];
    
    UITableViewRowAction *actionDelete = [UITableViewRowAction rowActionWithStyle:UITableViewRowActionStyleDefault title:@"删除" handler:^(UITableViewRowAction *action, NSIndexPath *indexPath) {
        [self deleteDeviceWithDeviceModel:[self.deviceArray objectAtIndex:rowNum]];
    }];
    
    if ([XLUtilTool isValidateAutoLogin] == YES){
        return @[actionDelete, actionRename];
    }else{
        return @[];
    }
    
}

#pragma mark - UITextFieldDelegate

- (void)tfRenameChanged:(UITextField *)sender{
    if (sender.text.length > 15) {
        NSRange r = NSMakeRange(0, 15);
        NSString *tempString = [sender.text substringWithRange:r];
        sender.text = tempString;
    }
}

#pragma mark - DZNEmptyDataSetSource

- (NSAttributedString *)titleForEmptyDataSet:(UIScrollView *)scrollView
{
    NSMutableDictionary <NSAttributedStringKey,id> *attrDict = [NSMutableDictionary dictionary];
    [attrDict setObject:[UIColor colorWithHexString:@"818268"] forKey:NSForegroundColorAttributeName];
    [attrDict setObject:[UIFont systemFontOfSize:20]           forKey:NSFontAttributeName];
    
    NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
    paragraphStyle.lineSpacing = 7.5;
    paragraphStyle.alignment = NSTextAlignmentCenter;
    [attrDict setObject:paragraphStyle forKey:NSParagraphStyleAttributeName];
    
    NSAttributedString *title = [[NSAttributedString alloc] initWithString:@"你还没添加设备\n请点击下方按钮添加设备吧！" attributes:attrDict];
    return title;
}

- (BOOL)emptyDataSetShouldAllowScroll:(UIScrollView *)scrollView
{
    return YES;
}

@end
