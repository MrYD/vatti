//
//  StrongWashView.h
//  vatti
//
//  Created by panyaping on 2018/8/3.
//  Copyright © 2018年 xlink. All rights reserved.
//

#import <UIKit/UIKit.h>
typedef NS_ENUM(NSUInteger, XLDishState) {
    XLDishRunningState,
    XLDishPauseState,
    XLDishCompleteState,
    XLDishMachineStoppageState,
    XLDishPlanState,
};
@class StrongWashView;
@protocol StrongWashViewDelegate <NSObject>
-(void)startActionWithStrongWashView:(StrongWashView*)strongWashView;
-(void)pauseActionWithStrongWashView:(StrongWashView*)strongWashView;
-(void)stoppageDetailActionWithStrongWashView:(StrongWashView*)strongWashView;
@end
@interface StrongWashView : UIImageView
@property (nonatomic, weak) id delegate;
@property (nonatomic, assign) uint32_t remainningTime;
@property (nonatomic, assign) uint8_t curremp;
@property (nonatomic, assign) uint32_t orderTime;
@property (nonatomic, assign) XLDishState state;
- (void)startRun;
@end
