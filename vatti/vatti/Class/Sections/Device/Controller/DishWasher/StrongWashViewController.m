//
//  StrongWashViewController.m
//  vatti
//
//  Created by xyz on 2018/8/3.
//  Copyright © 2018年 xlink. All rights reserved.
//

#import "StrongWashViewController.h"
#import "StrongWashView.h"
#import "UIColor+XLCategory.h"
#import "DishWasherConstants.h"
#import "DishWasherSettingController.h"
#import "DeviceViewController.h"
#import "WarningViewController.h"
#import "CurrentDeviceAlarmViewController.h"
#import "DishWasherController.h"
#import "DishWashProgressView.h"
#import "DishWasherTools.h"

@interface StrongWashViewController ()<StrongWashViewDelegate>
{
    CGFloat bottomMargin;
    CGFloat topMargin;
}
@property (nonatomic, strong) UILabel *strongWashLabel;
@property (nonatomic, strong) StrongWashView *strongNewWashView;
@property (nonatomic, strong) UIButton *endButton;
@property (nonatomic, strong) UIButton *pauseButton;
@property (nonatomic, strong) UIButton *planButton;

@property (nonatomic, strong) DishWashProgressView *progeressStatusBar;
@property (nonatomic, assign) XLDishState state;

@end

@implementation StrongWashViewController

- (void)viewDidLoad {
    
    if (iPhone5Screen) {
        bottomMargin = 20;
        topMargin = 10;
    }else if (iPhone6Screen) {
        bottomMargin = 64;
        topMargin = 45;
    }else if (iPhone6PScreen) {
        bottomMargin = 100;
        topMargin = 45;
    }else if (iPhoneXScreen) {
        bottomMargin = 110;
        topMargin = 45;
    }else {
        bottomMargin = 110;
        topMargin = 45;
    }
    [super viewDidLoad];
    
    self.view.backgroundColor = [UIColor whiteColor];
    
    //设置view相关
    [self initViews];
    
    [self buildData];
    
    [self initBottomButtons];

//    [self addRACObserve];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(startRun) name:UIApplicationDidBecomeActiveNotification object:nil];
    [self refreshViewWithDeviceStatue:self.deviceModel.dev_statu];
    
}

- (void)buildData {
    [super buildData];
}


- (void)viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear:animated];
    [self addRACObserve];
    [self  startRun];
    
}

- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
//    DishWasherController *dishWasherVCtrl = [[DishWasherController alloc] init];
//    dishWasherVCtrl.deviceModel = self.deviceModel;
//    [self.navigationController pushViewController:dishWasherVCtrl animated:YES];
}

- (void)viewDidLayoutSubviews {
    [super viewDidLayoutSubviews];
    [self.view layoutSubviews];
}

#pragma mark- observe
- (void)addRACObserve {
    
#if ENV == 1
    self.progeressStatusBar.func_step = 4;
#elif ENV == 2
    @weakify(self)
    [[RACObserve(self.deviceModel, func_step).distinctUntilChanged deliverOnMainThread] subscribeNext:^(id  _Nullable x) {
        @strongify(self)
        self.progeressStatusBar.func_step = self.deviceModel.func_step;
    }];
    
    [[RACObserve(self.deviceModel, Remaining_Time).distinctUntilChanged deliverOnMainThread] subscribeNext:^(id  _Nullable x) {
        @strongify(self)
        NSLog(@"++++++++++++++++%d",self.deviceModel.order_time);
        self.strongNewWashView.remainningTime = self.deviceModel.Remaining_Time;
        
    }];
    
    [[RACObserve(self.deviceModel, curr_temp).distinctUntilChanged deliverOnMainThread] subscribeNext:^(id  _Nullable x) {
        @strongify(self)
        self.strongNewWashView.curremp = self.deviceModel.curr_temp;
        
    }];
    [[RACObserve(self.deviceModel, order_time).distinctUntilChanged deliverOnMainThread] subscribeNext:^(id  _Nullable x) {
        @strongify(self)
        NSLog(@"---------------%d",self.deviceModel.order_time);
        self.strongNewWashView.orderTime = self.deviceModel.order_time;
        
    }];
    [[RACObserve(self.deviceModel, err_code).distinctUntilChanged deliverOnMainThread] subscribeNext:^(id  _Nullable x) {
        @strongify(self)
        if (self.deviceModel.err_code != 0) {
            self.state = XLDishMachineStoppageState;
            UIAlertController *alertViewCtrl = [UIAlertController alertControllerWithTitle:@"提示" message:@"设备发生故障" preferredStyle:UIAlertControllerStyleAlert];
            [alertViewCtrl addAction:[UIAlertAction actionWithTitle:@"查看" style:(UIAlertActionStyleDestructive) handler:^(UIAlertAction * _Nonnull action) {
                
                CurrentDeviceAlarmViewController *currentDeviceAlarmVCtrl = [[CurrentDeviceAlarmViewController alloc] init];
                currentDeviceAlarmVCtrl.deviceModel = self.deviceModel;
                [self.navigationController pushViewController:currentDeviceAlarmVCtrl animated:YES];
                
            }]];
            
            [alertViewCtrl addAction:[UIAlertAction actionWithTitle:@"确认" style:UIAlertActionStyleDefault handler:nil]];
            [self presentViewController:alertViewCtrl animated:YES completion:nil];
        } else {
            [self refreshViewWithDeviceStatue:self.deviceModel.dev_statu];
        }
        
    }];
    
    [[RACObserve(self.deviceModel, dev_statu).distinctUntilChanged deliverOnMainThread] subscribeNext:^(id  _Nullable x) {
        @strongify(self)
        NSLog(@"deviceModelce_act_switch1:__%d",self.deviceModel.act_switch);
        NSLog(@"deviceModelce——dev_statu1:%d",self.deviceModel.dev_statu);
        [self refreshViewWithDeviceStatue:self.deviceModel.dev_statu];
    }];
    
    [[RACObserve(self.deviceModel, switch_func).distinctUntilChanged deliverOnMainThread] subscribeNext:^(id  _Nullable x) {
        @strongify(self)
        [self refreshDishWashTitle];
        self.progeressStatusBar.switch_func = self.deviceModel.switch_func;
        [self refreshViewWithDeviceStatue:self.deviceModel.dev_statu];
    }];
    [[RACObserve(self.deviceModel, act_switch).distinctUntilChanged deliverOnMainThread] subscribeNext:^(id  _Nullable x) {
        @strongify(self)
        NSLog(@"deviceModelce_act_switch2:__%d",self.deviceModel.act_switch);
        NSLog(@"deviceModelce——dev_statu2:%d",self.deviceModel.dev_statu);
        [self refreshViewWithDeviceStatue:self.deviceModel.dev_statu];
    }];
    
    
    
    [[RACObserve(self.deviceModel, running_flag).distinctUntilChanged deliverOnMainThread] subscribeNext:^(id  _Nullable x) {
        @strongify(self)
        if ((self.deviceModel.running_flag & 1)  == 0 && (self.deviceModel.dev_statu != 3)) {
            
            UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"门已打开" message:nil preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction *action1 = [UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:nil];
            
            [alert addAction:action1];
            [self presentViewController:alert animated:YES completion:nil];
            
        } else if ((self.deviceModel.running_flag & 1) == 1) {
            
            
            
        }
        
    }];
    
#endif
    
}

- (void)timerFinished:(NSTimer *)timer {
    
    [super timerFinished:timer];
    [self refreshViewWithDeviceStatue:self.deviceModel.dev_statu];
    
}

#pragma mark - UI

- (void)backHomePage:(UIButton *)button {
    
    
//    NSArray *viewControllers = self.navigationController.viewControllers;
//    UIViewController *vc = [viewControllers firstObject];
//    DeviceViewController *vCtrl = (DeviceViewController *)vc;
//    [self.navigationController popToViewController:vCtrl  animated:YES];
    [self.navigationController popToRootViewControllerAnimated:YES];
    
}

- (void)setupNavi {
    UIBarButtonItem *cancleItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"tab_bar_ic_home_gray"] style:(UIBarButtonItemStyleDone) target:self action:@selector(backHomePage:)];
    cancleItem.tintColor = [UIColor darkGrayColor];
    self.navigationItem.leftBarButtonItem = cancleItem;
    self.navigationItem.title = @"华帝P1";
    
    if (self.deviceModel.name !=  nil && self.deviceModel.name.length != 0) {
        
        self.navigationItem.title = self.deviceModel.name;
        
    } else {
        
        self.navigationItem.title = XLDeviceDishWasherName;
        
    }
}


- (void)initProgressView {
    
    _progeressStatusBar = [[DishWashProgressView alloc] init];
    [self.view addSubview:_progeressStatusBar];
    self.progeressStatusBar.switch_func = 4;
    [_progeressStatusBar mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.strongNewWashView.mas_bottom).offset(40);
        make.left.right.equalTo(self.view);
        make.height.equalTo(@100);
    }];
    
    
    
}



- (void)startRun {
    [self.strongNewWashView startRun];
}

- (void)initViews {
    
    
    [self setupNavi];
    
    //强力洗label
    _strongWashLabel = [[UILabel alloc] init];
    [self.view addSubview:_strongWashLabel];
    [_strongWashLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.centerX.equalTo(self.view);
        make.top.equalTo(@(self->topMargin));
        make.width.equalTo(@145);
        make.height.equalTo(@20);
        
    }];
    _strongWashLabel.text = [NSString stringWithFormat:@"%@洗-洗涤中",[DishWasherTools getStringWithSwitchFunc:self.deviceModel.switch_func]];
    _strongWashLabel.textAlignment = NSTextAlignmentCenter;
    _strongWashLabel.font = [UIFont systemFontOfSize:15];
    _strongWashLabel.textColor = [UIColor colorWithHexString:@"323345"];
    
    
    //洗涤计时View
    _strongNewWashView = [[StrongWashView alloc] init];
    _strongNewWashView.delegate = self;
    [self.view addSubview:_strongNewWashView];
    [_strongNewWashView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.equalTo(@257);
        make.centerX.equalTo(self.view);
        make.top.equalTo(_strongWashLabel.mas_bottom).offset(30);
        make.height.equalTo(_strongNewWashView.mas_width);
    }];
    
    
    [self initProgressView];
    
    [self startRun];
    
    
}

- (BOOL)getDeviceControl{
    
    if ([self getIsDeviceCanControlWithIsNeedAlert:YES] == NO) {
        return NO;
    }
    
    return YES;
}



- (void)initBottomButtons {
    
    if (!_endButton) {
        //结束按钮
        _endButton = [UIButton buttonWithType:(UIButtonTypeCustom)];
        //    [_endButton setBackgroundColor:[UIColor greenColor]];
        _endButton.layer.cornerRadius = 10;
        [_endButton setBackgroundImage:[UIImage imageNamed:@"ic_shortbtn_active"] forState:(UIControlStateNormal)];
        [_endButton setBackgroundImage:[UIImage imageNamed:@"ic_shortbtn_unactive"] forState:(UIControlStateDisabled)];
        [_endButton setTitle:@"结 束" forState:UIControlStateNormal];
        [_endButton setTitleColor:[UIColor colorWithHexString:@"FEFEFE"] forState:(UIControlStateNormal)];
        [_endButton setTitleColor:[UIColor lightGrayColor] forState:(UIControlStateDisabled)];
        [_endButton addTarget:self action:@selector(endAction:) forControlEvents:(UIControlEventTouchUpInside)];
        [self.view addSubview:_endButton];
        [_endButton mas_makeConstraints:^(MASConstraintMaker *make) {
            
            make.left.equalTo(@10);
            //        make.right.equalTo(@-7);
            make.height.equalTo(@45);
            //        make.top.greaterThanOrEqualTo(lable).offset(10);
            //        make.bottom.equalTo(@-25);
            //            make.top.equalTo(_strongWashView.mas_bottom).offset(140);
            make.bottom.equalTo(self.view).offset(-bottomMargin);
            
        }];
        [_endButton layoutIfNeeded];
    }
    
    
    if (!_pauseButton) {
        //暂停按钮
        _pauseButton = [UIButton buttonWithType:(UIButtonTypeCustom)];
        //    [_pauseButton setBackgroundColor:[UIColor greenColor]];
        
        _pauseButton.layer.cornerRadius = 10;
        [_pauseButton setBackgroundImage:[UIImage imageNamed:@"ic_shortbtn_active"] forState:(UIControlStateNormal)];
        [_pauseButton setBackgroundImage:[UIImage imageNamed:@"ic_shortbtn_unactive"] forState:(UIControlStateDisabled)];
        [_pauseButton addTarget:self action:@selector(pauseButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
        [_pauseButton setTitle:@"暂 停" forState:UIControlStateNormal];
        [_pauseButton setTitleColor:[UIColor colorWithHexString:@"FEFEFE"] forState:(UIControlStateNormal)];
        [_pauseButton setTitleColor:[UIColor lightGrayColor] forState:(UIControlStateDisabled)];
        [self.view addSubview:_pauseButton];
        [_pauseButton mas_makeConstraints:^(MASConstraintMaker *make) {
            
            make.left.equalTo(_endButton.mas_right).equalTo(@22);
            make.right.equalTo(@-10);
            //        make.right.equalTo(@-7);
            make.width.equalTo(_endButton);
            make.height.equalTo(@45);
            //        make.top.greaterThanOrEqualTo(lable).offset(20);
            //        make.bottom.equalTo(@-25);
            make.bottom.equalTo(self.view).offset(-bottomMargin);
            
        }];
        [_pauseButton layoutIfNeeded];
    }
    
    if (!_planButton) {
        //结束按钮
        _planButton = [UIButton buttonWithType:(UIButtonTypeCustom)];
        //    [_endButton setBackgroundColor:[UIColor greenColor]];
        _planButton.layer.cornerRadius = 10;
        [_planButton setBackgroundImage:[UIImage imageNamed:@"ic_longbtn_active"] forState:(UIControlStateNormal)];
        [_planButton setBackgroundImage:[UIImage imageNamed:@"ic_longbtn_unactive"] forState:(UIControlStateDisabled)];
        [_planButton setTitle:@"取消预约" forState:UIControlStateNormal];
        [_planButton setTitleColor:[UIColor colorWithHexString:@"FEFEFE"] forState:(UIControlStateNormal)];
        [_planButton setTitleColor:[UIColor lightGrayColor] forState:(UIControlStateDisabled)];
        [_planButton addTarget:self action:@selector(endAction:) forControlEvents:(UIControlEventTouchUpInside)];
        [self.view addSubview:_planButton];
        _planButton.hidden = YES;
        [_planButton mas_makeConstraints:^(MASConstraintMaker *make) {
            
            make.left.equalTo(@10);
            //        make.right.equalTo(@-7);
            make.height.equalTo(@45);
            //        make.top.greaterThanOrEqualTo(lable).offset(10);
            //        make.bottom.equalTo(@-25);
            //            make.top.equalTo(_strongWashView.mas_bottom).offset(140);
            make.bottom.equalTo(self.view).offset(-bottomMargin);
            
        }];
        [_planButton layoutIfNeeded];
    }

    
}
-(void)pauseButtonClicked:(id)sender
{
    if (self.state == XLDishRunningState) {
        [self pauseAction];
    }else if (self.state == XLDishPauseState) {
        [self startAction];
    }else if (self.state == XLDishCompleteState||self.state == XLDishPlanState) {
        [self backAction];
    }
}

- (void)endAction:(UIButton *)button
{
    if (self.state == XLDishRunningState||self.state == XLDishPauseState) {
        [self endAction];
    } else if (self.state == XLDishCompleteState) {
        [self openAction];
    } else if (self.state == XLDishPlanState) {
        [self cancelPlanAction];
    }
}


#pragma mark - UIRefresh
-(void)refreshViewWithDeviceStatue:(UInt8)devStatu
{
    if (self.deviceModel.dev_statu != 1) {
        BOOL isAppointmentOpen = [XLDataPointTool isOpenWithValue:self.deviceModel.act_switch andShift:4];
        if (isAppointmentOpen) {
            self.state = XLDishPlanState;
            return;
        }
    }
    if (self.state == XLDishPlanState&&![DishWasherTools isOpenPlanedWithActSwitch:self.deviceModel.act_switch]) {//关闭洗涤预约状态
        if (self.deviceModel.dev_statu != DishWasherDeviceStatueStart&&self.deviceModel.dev_statu != DishWasherDeviceStatuePlanPlan) {//没有进入洗涤状态，返回上一级
            for (UIViewController *viewCtrl in self.navigationController.viewControllers) {
                if ([viewCtrl isKindOfClass:[DishWasherController class]]) {
                    [self.navigationController popToViewController:viewCtrl animated:YES];
                    return;
                }
            }
        }
    }
    if (self.deviceModel.dev_statu == 1) {
        //提示设备断电
//        UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"设备已经断电" message:nil preferredStyle:UIAlertControllerStyleAlert];
//        UIAlertAction *action1 = [UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            [self.navigationController popViewControllerAnimated:YES];
//        }];
//        [alert addAction:action1];
//        [self presentViewController:alert animated:YES completion:nil];
    } else if (self.deviceModel.dev_statu == 2){
        self.state = XLDishCompleteState;
    } else if (self.deviceModel.dev_statu == 3) {
        if (self.isResponseChange) {
            self.state = XLDishRunningState;
        }
    } else if (self.deviceModel.dev_statu == 4) {
        if (self.isResponseChange) {
            self.state = XLDishPauseState;
        }
    }
}
-(void)setState:(XLDishState)state
{
    if (_state == state) {
        return;
    }
    _state = state;
    switch (state) {
        case XLDishRunningState:
            [self running];
            break;
        case XLDishPauseState:
            [self pause];
            break;
        case XLDishCompleteState:
            [self dishComplete];
            break;
        case XLDishMachineStoppageState:
            [self machineStoppage];
            break;
        case XLDishPlanState:
            [self planWaite];
        default:
            break;
    }
}
- (void)refreshDishWashTitle
{
    NSString *dish_func = [DishWasherTools getStringWithSwitchFunc:self.deviceModel.switch_func];
    NSString *title = @"";
    self.strongWashLabel.hidden = NO;
    switch (self.state) {
        case XLDishRunningState:
            title = [NSString stringWithFormat:@"%@洗-洗涤中",dish_func];
            break;
        case XLDishPauseState:
            title = [NSString stringWithFormat:@"%@洗-暂停中",dish_func];
            break;
        case XLDishCompleteState:
            if (self.deviceModel.Remaining_Time == 0) {
                title = [NSString stringWithFormat:@"%@洗-已完成",dish_func];
            }else {
                title = [NSString stringWithFormat:@"%@洗-已中止",dish_func];
            }
            break;
        case XLDishMachineStoppageState:
            self.strongWashLabel.hidden = YES;
            break;
        case XLDishPlanState:
            title = [NSString stringWithFormat:@"%@洗-预约中",dish_func];
        default:
            break;
    }
    self.strongWashLabel.text = title;
}
- (void)running {
    
    self.progeressStatusBar.hidden = NO;
    [self refreshDishWashTitle];
    self.planButton.hidden = YES;
    
    self.strongNewWashView.state = XLDishRunningState;
    if (self.progeressStatusBar.disabled) {
        self.progeressStatusBar.disabled = NO;
        self.progeressStatusBar.func_step = self.deviceModel.func_step;
        self.progeressStatusBar.switch_func = self.deviceModel.switch_func;
    }
    self.endButton.enabled = YES;
    self.pauseButton.enabled = YES;
    [self.endButton setTitle:@"结 束" forState:(UIControlStateNormal)];
    [self.pauseButton setTitle:@"暂 停" forState:(UIControlStateNormal)];
}

- (void)pause {
    
    self.planButton.hidden = YES;
    self.progeressStatusBar.hidden = NO;
    [self refreshDishWashTitle];
    self.strongNewWashView.state = XLDishPauseState;
    if (self.progeressStatusBar.disabled) {
        self.progeressStatusBar.disabled = NO;
        self.progeressStatusBar.func_step = self.deviceModel.func_step;
        self.progeressStatusBar.switch_func = self.deviceModel.switch_func;
    }
    self.endButton.enabled = YES;
    self.pauseButton.enabled = YES;
    [self.endButton setTitle:@"结 束" forState:(UIControlStateNormal)];
    [self.pauseButton setTitle:@"开 始" forState:(UIControlStateNormal)];
}

- (void)dishComplete {
    
    self.planButton.hidden = YES;
    self.progeressStatusBar.hidden = NO;
    [self refreshDishWashTitle];
    self.strongNewWashView.state = XLDishCompleteState;
    if (self.progeressStatusBar.disabled) {
        self.progeressStatusBar.disabled = NO;
        self.progeressStatusBar.func_step = self.deviceModel.func_step;
        self.progeressStatusBar.switch_func = self.deviceModel.switch_func;
    }
    self.endButton.enabled = YES;
    self.pauseButton.enabled = YES;
    [self.endButton setTitle:@"开 门" forState:(UIControlStateNormal)];
    [self.pauseButton setTitle:@"返 回" forState:(UIControlStateNormal)];
    
}
- (void)machineStoppage {
    
    self.planButton.hidden = YES;
    [self refreshDishWashTitle];
    self.progeressStatusBar.hidden = NO;
    self.progeressStatusBar.disabled = YES;
    self.endButton.enabled = NO;
    self.pauseButton.enabled = NO;
}
- (void)planWaite
{
    self.planButton.hidden = NO;
    [self refreshDishWashTitle];
    self.strongNewWashView.state = XLDishPlanState;
    self.progeressStatusBar.func_step = 0;
    self.progeressStatusBar.disabled = YES;
    self.progeressStatusBar.hidden = YES;
    self.endButton.enabled = YES;
    self.pauseButton.enabled = YES;
    [self.endButton setTitle:@"取消预约" forState:(UIControlStateNormal)];
    [self.pauseButton setTitle:@"返 回" forState:(UIControlStateNormal)];

}
#pragma mark- Action
- (void)pauseAction {
    
    // 虚拟体验
    if (XL_DATA_VIRTUAL.isLogin == NO) {
        self.deviceModel.dev_statu = 4;
        self.state = XLDishPauseState;
        return;
    }
    
#if ENV == 1
    self.state = XLDishPauseState;
#elif ENV == 2
    
    if ([self getDeviceControl] == NO) {
        return;
    }
    self.state = XLDishPauseState;
    
    uint8_t functionFlagValue = '\0';
    functionFlagValue = [XLDataPointTool getDataPointValueWithValue:0 andShift:3 andIsOpen:1];
    XLinkDataPoint *dataPointFunctionFlag = [XLinkDataPoint dataPointWithType:XLinkDataTypeByte withIndex:5 withValue:@(functionFlagValue)];
    NSArray *dataPointsArray = @[dataPointFunctionFlag];
    
    //1.处理发送端点前的逻辑
    [self startLogicalBefortSet];
    
    //4.判断游客模
    if (XL_DATA_VIRTUAL.isLogin == NO){
        return;
    }
    //获取当前登录用户ID，加入数据端点
    NSMutableArray *mDataPoints = [self addUserIdDataPointWithDataPointsArray:dataPointsArray];
    //判断是否设置相同任务
    //如果是，则取消上一个任务
    [self cancelLastSameTaskWithNewDataPointsArray:dataPointsArray];
    
    [[XLServiceManager shareInstance].deviceService setDeviceDataPoints:self.deviceModel.device dataPoints:dataPointsArray timeout:TimeOutSetDataPoint handler:^(id result, NSError *error) {
        
        //            @strongify(self)
        
        
        if (error == nil) {
            [self.deviceModel addDataPoints:dataPointsArray];
        }else{
            KShowErrorLoadingWithMsg(@"设置失败");
        }
    }];
    
#endif
    
}

- (void)startAction {
    
    // 虚拟体验
    if (XL_DATA_VIRTUAL.isLogin == NO) {
        self.state = XLDishRunningState;
        return;
    }
    
#if ENV == 1
    self.state = XLDishRunningState;
    _strongWashLabel.text = @"强力洗·洗涤中";
    
#elif ENV == 2
    
    if ([self getDeviceControl] == NO) {
        return;
    }
    self.state = XLDishRunningState;    
    uint8_t functionFlagValue = '\0';
    functionFlagValue = [XLDataPointTool getDataPointValueWithValue:0 andShift:2 andIsOpen:1];
    XLinkDataPoint *dataPointFunctionFlag = [XLinkDataPoint dataPointWithType:XLinkDataTypeByte withIndex:5 withValue:@(functionFlagValue)];
    NSArray *dataPointsArray = @[dataPointFunctionFlag];
    //原本是注释的
        [self.deviceModel addDataPoints:dataPointsArray];
//        [self setDataPoints:dataPointsArray];
    
    
    //1.处理发送端点前的逻辑
    [self startLogicalBefortSet];
    
    //4.判断游客模
    if (XL_DATA_VIRTUAL.isLogin == NO){
        return;
    }
    //获取当前登录用户ID，加入数据端点
    NSMutableArray *mDataPoints = [self addUserIdDataPointWithDataPointsArray:dataPointsArray];
    //判断是否设置相同任务
    //如果是，则取消上一个任务
    [self cancelLastSameTaskWithNewDataPointsArray:dataPointsArray];
    
    @weakify(self)
    [[XLServiceManager shareInstance].deviceService setDeviceDataPoints:self.deviceModel.device dataPoints:dataPointsArray timeout:TimeOutSetDataPoint handler:^(id result, NSError *error) {
        
        @strongify(self)
        
        if (error == nil) {
            [self.deviceModel addDataPoints:dataPointsArray];
        }else{
            KShowErrorLoadingWithMsg(@"设置失败");
        }
    }];
    
#endif
}

-(void)backAction
{
    if (self.state == XLDishPlanState) {
        [self.navigationController popViewControllerAnimated:YES];
        return;
    }
    UIViewController *viewController = nil;
    for (UIViewController *vc in self.navigationController.viewControllers) {
        if ([vc isKindOfClass:[DishWasherController class]]) {
            viewController = vc;
            break;
        }
    }
    if (viewController) {
        [self.navigationController popToViewController:viewController animated:YES];
    }else {
        [self.navigationController popViewControllerAnimated:YES];
    }
}


- (void)endAction {
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"是否结束本次洗涤" message:nil preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *action1 = [UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
    // 虚拟体验
    if (XL_DATA_VIRTUAL.isLogin == NO) {
        self.deviceModel.dev_statu = 2;
        return ;
    }
#if ENV == 1
        self.state = XLDishCompleteState;
#elif ENV == 2
        [self endRequest];
#endif
    }];
    UIAlertAction *action2 = [UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:nil];
    [alert addAction:action1];
    [alert addAction:action2];
    [self presentViewController:alert animated:YES completion:nil];
}
- (void)cancelPlanAction
{
    if ([self getDeviceControl] == NO) {
        return;
    }
    uint8_t valuePlan = [XLDataPointTool getDataPointValueWithValue:self.deviceModel.act_switch andShift:4 andIsOpen:NO];
    XLinkDataPoint *dataPoint_valuePlan = [XLinkDataPoint dataPointWithType:XLinkDataTypeByte withIndex:10 withValue:@(valuePlan)];
    uint32_t valueTime = 0;
    XLinkDataPoint *dataPoint_devStatu = [XLinkDataPoint dataPointWithType:XLinkDataTypeByte withIndex:12 withValue:@2];
    XLinkDataPoint *dataPoint_planTime = [XLinkDataPoint dataPointWithType:XLinkDataTypeUnsignedInt withIndex:11 withValue:@(valueTime)];
    NSArray *dataPointsArray = @[dataPoint_devStatu,dataPoint_valuePlan, dataPoint_planTime];
    
    [self cancelLastSameTaskWithNewDataPointsArray:dataPointsArray];
    [[XLServiceManager shareInstance].deviceService setDeviceDataPoints:self.deviceModel.device dataPoints:dataPointsArray timeout:TimeOutSetDataPoint handler:^(id result, NSError *error) {
        if (error == nil) {
            [self.deviceModel addDataPoints:dataPointsArray];
//            [self.navigationController popViewControllerAnimated:YES];
        }else{
            KShowErrorLoadingWithMsg(@"设置失败");
        }
    }];
}
-(void)endRequest
{
    if ([self getDeviceControl] == NO) {
        return;
    }
    uint8_t functionFlagValue = '\0';
    
    functionFlagValue = [XLDataPointTool getDataPointValueWithValue:0 andShift:4 andIsOpen:1];
    XLinkDataPoint *dataPointFunctionFlag = [XLinkDataPoint dataPointWithType:XLinkDataTypeByte withIndex:5 withValue:@(functionFlagValue)];
    NSArray *dataPointsArray = @[dataPointFunctionFlag];
    
    [self cancelLastSameTaskWithNewDataPointsArray:dataPointsArray];
    
    [[XLServiceManager shareInstance].deviceService setDeviceDataPoints:self.deviceModel.device dataPoints:dataPointsArray timeout:TimeOutSetDataPoint handler:^(id result, NSError *error) {
        
        //            @strongify(self)
        
        if (error == nil) {
            self.state = XLDishCompleteState;
        }else{
            KShowErrorLoadingWithMsg(@"设置失败");
        }
    }];
}
-(void)openAction
{
    // 虚拟体验
    if (XL_DATA_VIRTUAL.isLogin == NO) {
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"门已打开" message:nil preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *action1 = [UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:nil];
        [alert addAction:action1];
        [self presentViewController:alert animated:YES completion:nil];
        return;
    }
#if ENV == 1
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"门已打开" message:nil preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *action1 = [UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:nil];
    [alert addAction:action1];
    [self presentViewController:alert animated:YES completion:nil];
    
#elif ENV == 2
    [self openRequest];
#endif
}
-(void)openRequest
{
    if ([self getDeviceControl] == NO) {
        return;
    }
    uint8_t functionFlagValue = '\0';
    functionFlagValue = [XLDataPointTool getDataPointValueWithValue:0 andShift:5 andIsOpen:1];
    XLinkDataPoint *dataPointFunctionFlag = [XLinkDataPoint dataPointWithType:XLinkDataTypeByte withIndex:5 withValue:@(functionFlagValue)];
    NSArray *dataPointsArray = @[dataPointFunctionFlag];
    
    [self cancelLastSameTaskWithNewDataPointsArray:dataPointsArray];
    
    
    [[XLServiceManager shareInstance].deviceService setDeviceDataPoints:self.deviceModel.device dataPoints:dataPointsArray timeout:TimeOutSetDataPoint handler:^(id result, NSError *error) {
        if (error == nil) {
            [self.deviceModel addDataPoints:dataPointsArray];
        }else{
            KShowErrorLoadingWithMsg(@"设置失败");
        }
    }];
}

#pragma mark- StrongWashViewDelegate
- (void)startActionWithStrongWashView:(StrongWashView *)strongWashView
{
    [self startAction];
}
- (void)pauseActionWithStrongWashView:(StrongWashView *)strongWashView
{
    [self pauseAction];
}
- (void)stoppageDetailActionWithStrongWashView:(StrongWashView *)strongWashView
{
    CurrentDeviceAlarmViewController *currentDeviceAlarmVCtrl = [[CurrentDeviceAlarmViewController alloc] init];
    currentDeviceAlarmVCtrl.deviceModel = self.deviceModel;
    [self.navigationController pushViewController:currentDeviceAlarmVCtrl animated:YES];

}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
