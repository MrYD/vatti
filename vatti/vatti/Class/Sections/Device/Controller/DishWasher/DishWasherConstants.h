//
//  DishWasherConstants.h
//  vatti
//
//  Created by dongxiao on 2018/8/7.
//  Copyright © 2018年 xlink. All rights reserved.
//
#import "UIColor+XLCategory.h"

#ifndef DishWasherConstants_h
#define DishWasherConstants_h


#define IsPhone ([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPhone)
#define UIScale (IsPhone ? 1 : (768.f / 320.f))

// 适配比例数值
#define UIAdjustNum(float) (((float) / 2.0) * UIScale)

// 适配字体
#define UIFontScale(iPhoneSize, iPadSize) (IsPhone ? [UIFont systemFontOfSize:iPhoneSize] : [UIFont systemFontOfSize:iPadSize])
#define UIFontAdjust(Size) (IsPhone ? [UIFont systemFontOfSize:((Size)/2)] : [UIFont systemFontOfSize:(Size)])

// 颜色转换
//#define VattiColorRGBA(rgbValue, a) \
//([UIColor colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 green:((float)((rgbValue & 0x00FF00) >>  8))/255.0 blue:((float)((rgbValue & 0x0000FF) >>  0))/255.0 alpha:a])
//#define VattiColorRGB(rgbValue) (VattiColorRGBA(rgbValue,1.0))

#ifndef iPhone5Screen
#define iPhone5Screen CGSizeEqualToSize(CGSizeMake(320, 568), [[UIScreen mainScreen] bounds].size)
#endif

#ifndef iPhone6Screen
#define iPhone6Screen CGSizeEqualToSize(CGSizeMake(375, 667), [[UIScreen mainScreen] bounds].size)
#endif

#ifndef iPhone6PScreen
#define iPhone6PScreen CGSizeEqualToSize(CGSizeMake(414, 736), [[UIScreen mainScreen] bounds].size)
#endif

#ifndef iPhoneXScreen
#define iPhoneXScreen CGSizeEqualToSize(CGSizeMake(375, 812), [[UIScreen mainScreen] bounds].size)
#endif

//检测是否是刘海屏
#define IsiPhoneNotchInScreen ([UIScreen instancesRespondToSelector:@selector(currentMode)] ? \
(CGSizeEqualToSize(CGSizeMake(1125, 2436), [[UIScreen mainScreen] currentMode].size) || \
CGSizeEqualToSize(CGSizeMake(828, 1792), [[UIScreen mainScreen] currentMode].size) ||  \
CGSizeEqualToSize(CGSizeMake(1242, 2688), [[UIScreen mainScreen] currentMode].size)) : NO)

#pragma mark - BarHeight
/// 状态栏高度
#define kStatusBarHeight (IsiPhoneNotchInScreen ? 44.0 : 20.0)
/// 导航栏高度
#define kNavigationBarHeight 44.0f
/// 导航栏+状态栏高度
#define kTopBarHeight (kStatusBarHeight + kNavigationBarHeight)
/// TabBar高度
#define kTabBarHeight (IsiPhoneNotchInScreen ? 83.0f : 49.0f)
/// 底部偏移高度，适配
#define kBottomOffset (IsiPhoneNotchInScreen ? 34.0f : 0.f)

#endif /* DishWasherConstants_h */



#ifdef INPUTENV

#if INPUTENV == 1
#define ENV (1) //测试环境
#elif INPUTENV == 2
#define ENV (2) //正式环境
#else
#error Args error!
#endif

#else

//#define ENV (1) //测试环境
#define ENV (2) //正式环境

#endif

