//
//  WarningTableViewCell.m
//  vatti
//
//  Created by panyaping on 2018/8/31.
//  Copyright © 2018年 xlink. All rights reserved.
//

#import "WarningTableViewCell.h"
#import "UIColor+XLCategory.h"

#define cellHeight 45

@interface WarningTableViewCell()

@property (nonatomic, strong) UILabel *titleLabel;

@end

@implementation WarningTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    
//    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        
        [self initView];
        
    }
    
    
    return self;
    
}

- (void)initView {

    self.titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(40, 0, 106, 47)];
    _titleLabel.font = [UIFont systemFontOfSize:14];
//    [_titleLabel setTextColor:[UIColor colorWithHexString:@"3233345"]];
//    _titleLabel.backgroundColor = [UIColor redColor];
    [self.contentView addSubview:_titleLabel];

}


- (void)setErr_str:(NSString *)err_str {
    self.titleLabel.text = err_str;
}


- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
