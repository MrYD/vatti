//
//  WarningTableViewCell.h
//  vatti
//
//  Created by panyaping on 2018/8/31.
//  Copyright © 2018年 xlink. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface WarningTableViewCell : UITableViewCell

@property (assign,nonatomic) NSString* err_str;

@end
