//
//  DishWasherSetLightController.m
//  vatti
//
//  Created by dongxiao on 2018/8/14.
//  Copyright © 2018年 xlink. All rights reserved.
//

#import "DishWasherSetLightController.h"
#import "DishWasherConstants.h"
#import "UIColor+XLCategory.h"

@interface DishWasherSetLightController () <UITableViewDelegate, UITableViewDataSource>

@property (nonatomic, strong) UISwitch *switchBtn;

@end

@implementation DishWasherSetLightController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.view.backgroundColor = [UIColor colorWithHexString:@"EFEEE8"];
}

- (void)initConfigure
{
    [super initConfigure];
}

- (void)initSubViews{
    [super initSubViews];
    
    //navi
    [self setupNavi];
    
    UITableView *tableView = [[UITableView alloc] init];
    tableView.delegate = self;
    tableView.dataSource = self;
    tableView.bounces = NO;
    [self.view addSubview:tableView];
    [tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(UIAdjustNum(84));
        make.width.mas_equalTo(self.view.mas_width);
        make.height.mas_equalTo(UIAdjustNum(109));
    }];
    
}

- (void)buildData
{
    [super buildData];

    [self addRACObserve];
}

- (void)addRACObserve
{
    @weakify(self)
    //#pragma mark 监听全局
    //    //设备上下线
    //    [[RACObserve(self.dishWasherModel, connected).distinctUntilChanged deliverOnMainThread] subscribeNext:^(id  _Nullable x) {
    //        @strongify(self)
    //
    //    }];
    //
    //    //设备状态
    //    [[RACObserve(self.dishWasherModel, dev_statu).distinctUntilChanged deliverOnMainThread] subscribeNext:^(id  _Nullable x) {
    //        @strongify(self)
    //
    //
    //    }];
    
#pragma mark 监听开关
    //开关
    [[RACObserve(self.deviceModel, act_switch).distinctUntilChanged deliverOnMainThread] subscribeNext:^(id  _Nullable x) {
        @strongify(self)
        if (self.isResponseChange) {
            [self.switchBtn setOn:[XLDataPointTool isOpenWithValue:self.deviceModel.act_switch andShift:2]];
        }
    }];
}


#pragma mark - UI

- (void)setupNavi
{
    [super setupNavi];
    self.navigationItem.title = @"氛围灯";
}


#pragma mark - UITableViewDelegate, UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 1;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return UIAdjustNum(109.f);
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"DishWasherSetLightCell"];
    if(!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"DishWasherSetLightCell"];
        [cell.textLabel setFont:UIFontAdjust(28.f)];    cell.selectionStyle = UITableViewCellSelectionStyleNone;
        self.switchBtn = [[UISwitch alloc]init];
        [self.switchBtn addTarget:self action:@selector(switchBtnChanged) forControlEvents:UIControlEventValueChanged];
        cell.accessoryView = self.switchBtn;
        cell.textLabel.text = @"氛围灯--开启";
        [self.switchBtn setOn:[XLDataPointTool isOpenWithValue:self.deviceModel.act_switch andShift:2]];
    }

    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
}

- (void)switchBtnChanged
{
    [self.switchBtn setOn:!self.switchBtn.isOn];

    BOOL isOpen = [XLDataPointTool isOpenWithValue:self.deviceModel.act_switch andShift:2];
    uint8_t autoOpenValue = [XLDataPointTool getDataPointValueWithValue:self.deviceModel.act_switch andShift:2 andIsOpen:!isOpen];
    XLinkDataPoint *dataPoint = [XLinkDataPoint dataPointWithType:XLinkDataTypeByte withIndex:10 withValue:@(autoOpenValue)];
    [self.deviceModel setDataPoint:dataPoint];
    
    __weak __typeof(self) weakself = self;
    [self setDataPoints:@[dataPoint] withSeccussBlock:nil withFailBlock:^{
        [weakself.switchBtn setOn:[XLDataPointTool isOpenWithValue:weakself.deviceModel.act_switch andShift:2]];
    }];
}

#pragma mark - Request

/**
 设置数据端点数据
 */
- (void)setDataPoints:(NSArray <XLinkDataPoint*> *)dataPointsArray withSeccussBlock:(void (^)(void))successBlock withFailBlock:(void (^)(void))failBlock{
    
    if ([self getIsDeviceCanControlWithIsNeedAlert:YES] == NO) {
        return;
    }
    
    @weakify(self)
    
    //1.处理发送端点前的逻辑
    [self startLogicalBefortSet];
    
    //2.设置端点值到数据模型
    //    [self.deviceModel addDataPoints:dataPointsArray];
    
    //3.更新UI
    //    [self updateTempWithTempValue:self.deviceModel.heater_temp andIsNeedSetToDevice:YES];
    //    [self updateFunctionViewWithFunctionFlag:self.deviceModel.function_flag];
    
    //4.判断游客模式
    if (XL_DATA_VIRTUAL.isLogin == NO){
        return;
    }
    
    //获取当前登录用户ID，加入数据端点
    NSMutableArray *mDataPoints = [self addUserIdDataPointWithDataPointsArray:dataPointsArray];
    
    //判断是否设置相同任务
    //如果是，则取消上一个任务
    [self cancelLastSameTaskWithNewDataPointsArray:dataPointsArray];
    
    self.setDataPointTask = [[XLServiceManager shareInstance].deviceService setDeviceDataPoints:self.deviceModel.device dataPoints:dataPointsArray timeout:TimeOutSetDataPoint handler:^(id result, NSError *error) {
        
        @strongify(self)
        
        if (error == nil) {
            [self.deviceModel addDataPoints:dataPointsArray];
            if (successBlock) {
                successBlock();
            }
        }else{
            KShowErrorLoadingWithMsg(@"设置失败");
            NSLog(@"设置失败, %@", error.userInfo);
            if (failBlock) {
                failBlock();
            }
        }
    }];
}

- (void)timerFinished:(NSTimer*)timer
{
    [super timerFinished:timer];
    self.isResponseChange = YES;
    [self.switchBtn setOn:[XLDataPointTool isOpenWithValue:self.deviceModel.act_switch andShift:2]];
}

@end
