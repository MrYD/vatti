//
//  DishWashterController.m
//  vatti
//
//  Created by dongxiao on 2018/8/3.
//  Copyright © 2018年 xlink. All rights reserved.
//

#import "DishWasherController.h"

#import "DishWasherConstants.h"
#import "SDCycleScrollView.h"
#import "StrongWashViewController.h"
#import "DishWasherButton.h"
#import "CycleScrollViewModel.h"
#import "DeviceSetViewController.h"
#import "XLinkSetDataPointTask.h"
#import "DishWasherSettingController.h"
#import "DishWasherPlanView.h"
#import "DishWashCustomController.h"
#import "CurrentDeviceAlarmViewController.h"
#import "DeviceViewController.h"
#import "DishWasherPlanButton.h"
#import "DishWasherTools.h"

@interface DishWasherController () <SDCycleScrollViewDelegate,UIGestureRecognizerDelegate,UINavigationControllerDelegate>
{
    CGFloat topMargin;
    CGFloat flowFunMargin;
    CGFloat funMargin;
    
    CGFloat bottomMargin;
    CGFloat powPlanMargin;
    CGFloat planCustomMargin;
}
@property (assign, nonatomic) BOOL isObserveChangeScroll;

// 更多按钮
@property (strong, nonatomic) UIBarButtonItem *rightBarBtn;

// 滑动图
@property (assign, nonatomic) NSInteger currentPage;
@property (strong, nonatomic) SDCycleScrollView *adCycleScrollView;

// 预约选择器
@property (strong, nonatomic) DishWasherPlanView *dishWasherPlanView;
// 预约时间
@property (assign, nonatomic) NSInteger timeType;
@property (strong, nonatomic) NSDate    *selectedDate;
// 开关按钮
@property (nonatomic, strong) DishWasherButton *powerBtn;

// 预约按钮
@property (nonatomic, strong) DishWasherPlanButton *nPlanBtn;
// 所有按钮集
@property (nonatomic, strong) NSMutableArray *dishWasherBtnArray;

// 所有附属按钮集
@property (nonatomic, strong) NSMutableArray *subTypeBtnArray;

@property (nonatomic, strong) UIView *disableScrollView;

@property (nonatomic, strong) DishWasherButton *lastbutton;

@end

@implementation DishWasherController


- (instancetype)init
{
    self = [super init];
    if (self) {
#if ENV == 1
        _currentPage = 0;
#elif ENV == 2
        _currentPage = MAX(0, self.deviceModel.switch_func - 1);
        _currentPage = (_currentPage > 7) ? 0 : _currentPage;
#endif
        self.isResponseChange = YES;
        self.isObserveChangeScroll = NO;
        if (iPhone5Screen) {
            topMargin = 10;
            flowFunMargin = 25;
            funMargin = 20;
            
            bottomMargin = 20;
            powPlanMargin = 15;
            planCustomMargin = 20;
        }else if (iPhone6Screen) {
            topMargin = 30;
            flowFunMargin = 35;
            funMargin = 20;
            
            bottomMargin = 35;
            powPlanMargin = 30;
            planCustomMargin = 40;
        }else if (iPhone6PScreen) {
            topMargin = 64;
            flowFunMargin = 60;
            funMargin = 20;
            
            bottomMargin = 35;
            powPlanMargin = 30;
            planCustomMargin = 40;
        }else if (iPhoneXScreen) {
            topMargin = 60;
            flowFunMargin = 55;
            funMargin = 20;
            
            bottomMargin = 100;
            powPlanMargin = 30;
            planCustomMargin = 40;
        }else {
            topMargin = 60;
            flowFunMargin = 55;
            funMargin = 20;
            
            bottomMargin = 100;
            powPlanMargin = 30;
            planCustomMargin = 40;
        }

        
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    BOOL isNeedPushCustomController = ([DishWasherTools isCustomFuncWithSwitchFunc:self.deviceModel.switch_func]//是自定义模式
                                       && ![DishWasherTools isOpenPlanedWithActSwitch:self.deviceModel.act_switch]//不是预约状态
                                       && !(self.deviceModel.dev_statu == 3||self.deviceModel.dev_statu == 4));//不是洗涤运行或暂停状态
    if (isNeedPushCustomController) {
        if (![self.navigationController.visibleViewController isKindOfClass:[DishWashCustomController class]]) {
            DishWashCustomController *dishWashCustomController = [[DishWashCustomController alloc] init];
            dishWashCustomController.deviceModel = self.deviceModel;
//            __weak typeof(self) weakSelf = self;
//            [dishWashCustomController setControllerBackBlock:^{
//                [weakSelf.navigationController popViewControllerAnimated:YES];
//            }];
//            [weakSelf.navigationController pushViewController:dishWashCustomController animated:YES];
        }
    }
}


- (void)viewWillAppear:(BOOL)animated
{
    // 添加监听
    [self addRACObserve];
    [self.adCycleScrollView adjustWhenControllerViewWillAppera];
    
}


- (void)viewDidDisappear:(BOOL)animated {
    
    [super viewDidDisappear:animated];
    
    self.isResponseChange = YES;
}


- (void)viewDidAppear:(BOOL)animated
{
    [self responseChangeUI:[self getIsDeviceCanControl]];
}


- (void)initConfigure
{
    [super initConfigure];
}


- (void)initSubViews
{
    [super initSubViews];
    
    //    self.edgesForExtendedLayout = UIRectEdgeTop;
    CGRect frame = self.view.frame;
    frame.size.height -= UIAdjustNum(132.f);
    self.view.frame = frame;
    
    [self setupNavi];
    
    [self setSubViewUI];
}


- (void)setupNavi
{
    [super setupNavi];
    
    if (self.deviceModel.name != nil && self.deviceModel.name.length != 0){
        self.navigationItem.title = self.deviceModel.name;
    }else{
        self.navigationItem.title = XLDeviceDishWasherName;
    }
    
    self.rightBarBtn = [[UIBarButtonItem alloc] initWithTitle:@"更多" style:UIBarButtonItemStylePlain target:self action:@selector(rightAction)];
    [self.rightBarBtn setTintColor:[UIColor blackColor]];
    self.navigationItem.rightBarButtonItem = self.rightBarBtn;
    self.navigationController.delegate = self;
    self.view.backgroundColor = [UIColor whiteColor];
}
- (void)navigationController:(UINavigationController *)navigationController willShowViewController:(UIViewController *)viewController animated:(BOOL)animated
{
    if ([viewController isKindOfClass:[DeviceViewController class]]) {
        [self cancelDevicepriority];
    }
}
- (void)setSubViewUI
{
    self.dishWasherBtnArray = [NSMutableArray array];
    self.subTypeBtnArray = [NSMutableArray array];
    
    [self setSubViewUIDisableScrollView];
    [self setSubViewUIADCycleScrollView];
    [self setSubViewUISubTypeBtn];
    [self setSubViewUIFunctionTypeBtn];
    [self setSubViewUIDishWasherPlanView];
    
    [self responseChangeUI:NO];
}



#pragma mark - Data

- (void)buildData
{
    [super buildData];
    
    _timeType = 0;
    
//    // 添加监听
//    [self addRACObserve];
}



// 添加监听
- (void)addRACObserve
{
    @weakify(self)
    
#pragma mark 监听全局
    //设备上下线
    [[RACObserve(self.deviceModel, connected).distinctUntilChanged deliverOnMainThread] subscribeNext:^(id  _Nullable x) {
        
        @strongify(self)
        NSLog(@"DishWasherController监听-----------设备上下线:   %@", (self.deviceModel.connected) ? @"在线":@"离线");
        [self responseChangeUI:[self getIsDeviceCanControl]];
    }];
    
    //设备状态
    [[RACObserve(self.deviceModel, dev_statu).distinctUntilChanged deliverOnMainThread] subscribeNext:^(id  _Nullable x) {
        
        @strongify(self)
        NSLog(@"deviceModelce%d",self.deviceModel.dev_statu);
        NSLog(@"DishWasherController监听-----------设备状态:   %d", self.deviceModel.dev_statu);
        BOOL isNeedPushStrongWashViewController = (self.deviceModel.dev_statu == 3||self.deviceModel.dev_statu == 4)//是洗涤运行或者暂停状态
        &&(![DishWasherTools isOpenPlanedWithActSwitch:self.deviceModel.act_switch]);//不是预约模式
        if (isNeedPushStrongWashViewController) {
            if(![self.navigationController.topViewController isKindOfClass:[StrongWashViewController class]])
            {
                StrongWashViewController *strongWashViewController = [[StrongWashViewController alloc] init];
                strongWashViewController.deviceModel = self.deviceModel;
                if ((self.deviceModel.running_flag & 1) == 1) {
                
                    [self.navigationController pushViewController:strongWashViewController animated:YES];
                    NSLog(@"===self.navigationController.visibleViewController = %@", self.navigationController.visibleViewController);
                    NSLog(@"===self.navigationController.topViewController = %@", self.navigationController.topViewController);
                }
            }
        }
        if (self.isResponseChange == NO) {
            
            return;
        }
        
        [self responseChangeUI:[self getIsDeviceCanControl]];
    }];
    
#pragma mark  监听设备状态面板
    //开关
    [[RACObserve(self.deviceModel, control_cmd).distinctUntilChanged deliverOnMainThread] subscribeNext:^(id  _Nullable x) {
        
        @strongify(self)
//                [self responseChangeUI:[self getIsDeviceCanControl]];
    }];
    //告警
    
#pragma mark  监听滑块面板
    // 滑动图
    [[RACObserve(self.deviceModel, switch_func).distinctUntilChanged deliverOnMainThread] subscribeNext:^(id  _Nullable x) {
        
        @strongify(self)
        self.nPlanBtn.switchFunc = self.deviceModel.switch_func;
        if (self.isResponseChange == NO) {
            
            return;
        }
        // 网页端修改，滑动图同步
        NSInteger switchValue = MAX(0, self.deviceModel.switch_func - 1);
        if (switchValue != self.currentPage) {
            
            [self.adCycleScrollView adjustControllerViewImage:switchValue];
            self.isObserveChangeScroll = YES;
            for (DishWasherButton *btn in self.subTypeBtnArray) {
                
                btn.selected = NO;
            }
            
            if (switchValue > 7) {
                
                if ([XLDataPointTool isOpenWithValue:self.deviceModel.act_switch andShift:4]) {
                    
                    self.currentPage = 0;
                    [self.adCycleScrollView adjustControllerViewImage:self.currentPage];
                }
            }
            else
            {
                self.currentPage = switchValue;
            }
        }
    }];
    
    // 附属模式
    [[RACObserve(self.deviceModel, func_mode).distinctUntilChanged deliverOnMainThread] subscribeNext:^(id  _Nullable x) {
        @strongify(self)
        if (self.isResponseChange == NO) {
            return;
        } else {
            
            
            
        }
#if ENV == 1
#elif ENV == 2
        NSInteger shiftValue = -1;
        for (NSInteger i = 0; i < 7; i++)
        {
            BOOL isOpen = [XLDataPointTool isOpenWithValue:self.deviceModel.func_mode andShift:i];
            if (isOpen) {
                shiftValue = i;
            }
        }
        
        if (shiftValue < 0) {
            for (DishWasherButton *btn in self.subTypeBtnArray) {
                btn.selected = NO;
            }
        }
        else
        {
            for (DishWasherButton *btn in self.subTypeBtnArray) {
                btn.selected = NO;
                NSNumber *shiftNum = [btn.tagDic valueForKey:@"subTypeBtn"];
                if (shiftValue == shiftNum.integerValue) {
                    btn.selected = YES;
                    
                }
            }
        }
#endif
    }];
    
#pragma mark  监听面板其他
    // 预约
    [[RACObserve(self.deviceModel, act_switch).distinctUntilChanged deliverOnMainThread] subscribeNext:^(id  _Nullable x) {
        @strongify(self)
        [self responseChangeUI:[self getIsDeviceCanControl]];
        BOOL isOpenPlaned = [DishWasherTools isOpenPlanedWithActSwitch:self.deviceModel.act_switch];
        self.nPlanBtn.plantActSwitch = isOpenPlaned;
        if (isOpenPlaned && ![self.navigationController.topViewController isKindOfClass:[StrongWashViewController class]]) {
            StrongWashViewController *strongWashVCtrl  = [[StrongWashViewController alloc] init];
            strongWashVCtrl.deviceModel = self.deviceModel;
//            if (strongWashVCtrl.deviceModel.dev_statu == 3 ||strongWashVCtrl.deviceModel.dev_statu == 4) {
//                NSLog(@"--------");
//            }else{
            if ((self.deviceModel.running_flag & 1) == 1) {
                [self.navigationController pushViewController:strongWashVCtrl animated:YES];
//                }
            }
            
        }
    }];
    
    // 预约时间
    [[RACObserve(self.deviceModel, order_time).distinctUntilChanged deliverOnMainThread] subscribeNext:^(id  _Nullable x) {
        @strongify(self)
        self.nPlanBtn.orderTime = self.deviceModel.order_time;
        [self responseChangeUI:[self getIsDeviceCanControl]];
    }];
    
    
    [[RACObserve(self.deviceModel, err_code).distinctUntilChanged deliverOnMainThread] subscribeNext:^(id  _Nullable x) {
        @strongify(self)
        if (self.deviceModel.err_code != 0) {
            
            UIAlertController *alertViewCtrl = [UIAlertController alertControllerWithTitle:@"提示" message:@"设备发生故障" preferredStyle:UIAlertControllerStyleAlert];
            [alertViewCtrl addAction:[UIAlertAction actionWithTitle:@"查看" style:(UIAlertActionStyleDestructive) handler:^(UIAlertAction * _Nonnull action) {
                
                CurrentDeviceAlarmViewController *currentDeviceAlarmVCtrl = [[CurrentDeviceAlarmViewController alloc] init];
                currentDeviceAlarmVCtrl.deviceModel = self.deviceModel;
                [self.navigationController pushViewController:currentDeviceAlarmVCtrl animated:YES];
                
            }]];
            
            [alertViewCtrl addAction:[UIAlertAction actionWithTitle:@"确认" style:UIAlertActionStyleDefault handler:nil]];
            [self presentViewController:alertViewCtrl animated:YES completion:nil];
        }
        
    }];
    
}
- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRequireFailureOfGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer
{
    return NO;
}
- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldBeRequiredToFailByGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer
{
    return NO;
}
- (BOOL)gestureRecognizerShouldBegin:(UIGestureRecognizer *)gestureRecognizer
{
    if ([self getIsDeviceCanControlWithIsNeedAlert:YES]==NO) {
        return YES;
    }
    return NO;
}
#pragma mark - SDCycleScrollViewDelegate
/** 点击图片回调 */
- (void)cycleScrollView:(SDCycleScrollView *)cycleScrollView didSelectItemAtIndex:(NSInteger)index
{
    [self.adCycleScrollView adjustWhenControllerViewWillAppera];
}


/** 图片滚动回调 */
- (void)cycleScrollView:(SDCycleScrollView *)cycleScrollView didScrollToIndex:(NSInteger)index
{
    if (_currentPage == index) {
        return;
    }
    
    NSArray *disabelBtnArr = [[CycleScrollViewModel shareInstance] getDisableArr];
    NSArray *numArr = disabelBtnArr[index];
    for(NSInteger i = 0; i < self.subTypeBtnArray.count; i++)
    {
        DishWasherButton *btn = (DishWasherButton *)self.subTypeBtnArray[i];
        [btn setDishWasherButtonActive:YES];
        if ([numArr containsObject:[NSNumber numberWithInteger:i]]) {
            [btn setDishWasherButtonActive:NO];
        }
    }
    
    // 虚拟体验
    if (XL_DATA_VIRTUAL.isLogin == NO) {
        self.deviceModel.switch_func = index + 1;
    }
    
    if (!self.isObserveChangeScroll) {
        XLinkDataPoint *dataPoint_switchFunc = [XLinkDataPoint dataPointWithType:XLinkDataTypeByte withIndex:6 withValue:@(index + 1)];
        XLinkDataPoint *dataPoint_funcMode = [XLinkDataPoint dataPointWithType:XLinkDataTypeByte withIndex:8 withValue:@(0)];
        [self setDataPoints:@[dataPoint_switchFunc, dataPoint_funcMode] withSeccussBlock:nil withFailBlock:nil];
    }
    self.isObserveChangeScroll = NO;
}


#pragma mark - BtnAction
- (void)tapGestureRecognizer
{
    
}
- (void)rightAction
{
    DishWasherSettingController *dishWasherSettingController = [[DishWasherSettingController alloc]init];
    dishWasherSettingController.deviceModel = self.deviceModel;
    [self.navigationController pushViewController:dishWasherSettingController animated:YES];
}


- (void)subTypeBtnClick:(DishWasherButton *)btn
{
    if ([self getIsDeviceCanControlWithIsNeedAlert:YES]==NO) {
        return;
    }
    self.lastbutton = btn;
    for (DishWasherButton *button in self.subTypeBtnArray) {
        
        if (![button isEqual:btn]) {
            button.selected = NO;
        }
    }
    
    btn.selected = !btn.selected;
#if ENV == 1
#elif ENV == 2
    NSNumber *shiftNum = [btn.tagDic valueForKey:@"subTypeBtn"];
    uint8_t funcValue = [XLDataPointTool getDataPointValueWithValue:0 andShift:shiftNum.intValue andIsOpen:btn.selected];
    XLinkDataPoint *dataPoint = [XLinkDataPoint dataPointWithType:XLinkDataTypeByte withIndex:8 withValue:@(funcValue)];
    [self setDataPoints:@[dataPoint] withSeccussBlock:nil withFailBlock:nil];
#endif
}


- (void)customBtnClick
{
    BOOL customMode = self.deviceModel.switch_func >= 9 && self.deviceModel.switch_func <= 19;
    BOOL hasPlaned = [XLDataPointTool isOpenWithValue:self.deviceModel.act_switch andShift:4];
    if (hasPlaned && !customMode) {
        
        [self alertHintWithMessage:@"您已开启预约，请先取消预约再进行设置。"];
        //        if (isAppointmentOpen == YES){
        //            [self alertHintWithMessage:@"您已开启预约，请先取消预约再进行设置。"];
        //            return ;
        //        }
        
        return;
        
    }
    
    DishWashCustomController *dishWashCustomController = [[DishWashCustomController alloc] init];
    dishWashCustomController.deviceModel = self.deviceModel;
    __weak typeof(self) weakSelf = self;
    [dishWashCustomController setControllerBackBlock:^{
        [weakSelf.navigationController popViewControllerAnimated:YES];
    }];
    [self.navigationController pushViewController:dishWashCustomController animated:YES];
#if ENV == 1
    
#elif ENV == 2
    
#endif
}


/*
 *预约
 */
- (void)planBtnClick
{
    if ([self getIsDeviceCanControlWithIsNeedAlert:YES]==NO) {
        return;
    }
    if (self.nPlanBtn.plantActSwitch) {
        [self.dishWasherPlanView showEditPlanViewWithTimeType:_timeType selectedDate:self.selectedDate];
    }
    else
    {
        [self.dishWasherPlanView showPlanView];
    }
}

- (void)powerBtnClick:(UIButton *)button
{
    if ([self getIsDeviceCanControlWithIsNeedAlert:YES]==NO) {
        return;
    }
    if (self.deviceModel.connected == NO) {
        
        return;
    }
    //    button.selected = !button.selected;
    
    //    [self responseChangeUI:button.selected];
    //    // 虚拟体验
    //    if (XL_DATA_VIRTUAL.isLogin == NO) {
    //        [self responseChangeUI:!button.selected];
    //    }
    
    NSInteger shiftValue = !button.selected ? 0 : 1;
    uint8_t functionFlagValue = [XLDataPointTool getDataPointValueWithValue:0 andShift:shiftValue andIsOpen:YES];
    XLinkDataPoint *dataPoint = [XLinkDataPoint dataPointWithType:XLinkDataTypeByte withIndex:5 withValue:@(functionFlagValue)];
    [self setDataPoints:@[dataPoint] withSeccussBlock:nil withFailBlock:nil];
    
    [self responseChangeUI:!button.selected];
}


//开始洗涤
- (void)startBtnClick
{

    @weakify(self)
    [[RACObserve(self.deviceModel, running_flag).distinctUntilChanged deliverOnMainThread] subscribeNext:^(id  _Nullable x) {
        @strongify(self)
        if (((self.deviceModel.running_flag & 1) == 0) && (self.deviceModel.dev_statu != 3)) {
            
            UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"提示" message:@"请关好门!" preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction *action1 = [UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                return ;
            }];
            
            [alert addAction:action1];
            [self presentViewController:alert animated:YES completion:nil];
            
        } else if ((self.deviceModel.running_flag & 1) == 1) {
            
        }
    }];
    // 虚拟体验
    if (XL_DATA_VIRTUAL.isLogin == NO) {
        
        self.deviceModel.dev_statu = 3;
        return;
    }
    
    if (self.deviceModel.dev_statu == 3) {
        
        StrongWashViewController *strongWashViewController = [[StrongWashViewController alloc] init];
        strongWashViewController.deviceModel = self.deviceModel;
        [self.navigationController pushViewController:strongWashViewController animated:YES];
    }
    else
    {
        if (self.deviceModel.dev_statu == 2) {
            BOOL isAppointmentOpen = [XLDataPointTool isOpenWithValue:self.deviceModel.act_switch andShift:4];
            if (isAppointmentOpen == YES){
                //        [self alertHintWithMessage:@"您已开启预约，请先取消预约再进行设置。"];
                StrongWashViewController *strongWashVCtrl  = [[StrongWashViewController alloc] init];
                strongWashVCtrl.deviceModel = self.deviceModel;
                [self.navigationController pushViewController:strongWashVCtrl animated:YES];
                return;
            }
        }
        if ([self getIsDeviceCanControlWithIsNeedAlert:YES]==NO) {
            return;
        }

#if ENV == 1
        //        uint8_t functionFlagValue = [XLDataPointTool getDataPointValueWithValue:0 andShift:2 andIsOpen:YES];
        //        XLinkDataPoint *dataPoint = [XLinkDataPoint dataPointWithType:XLinkDataTypeByte withIndex:5 withValue:@(functionFlagValue)];
        //        XLinkDataPoint *dev_statu = [XLinkDataPoint dataPointWithType:XLinkDataTypeByte withIndex:12 withValue:@(3)];
        //        [self.deviceModel setDataPoint:dataPoint, dev_statu];
#elif ENV == 2
        uint8_t functionFlagValue = [XLDataPointTool getDataPointValueWithValue:0 andShift:2 andIsOpen:YES];
        XLinkDataPoint *dataPoint = [XLinkDataPoint dataPointWithType:XLinkDataTypeByte withIndex:5 withValue:@(functionFlagValue)];
        BOOL isCustom = [DishWasherTools isCustomFuncWithSwitchFunc:self.deviceModel.switch_func];
        if (isCustom) {
            XLinkDataPoint *dataPoint_switchFunc = [XLinkDataPoint dataPointWithType:XLinkDataTypeByte withIndex:DishWasherDataPoint_switch_func withValue:@(DishWasherSwitchFuncIntelligence)];
            [self setDataPoints:@[dataPoint_switchFunc, dataPoint] withSeccussBlock:nil withFailBlock:nil];
        }else {
            [self setDataPoints:@[dataPoint] withSeccussBlock:nil withFailBlock:nil];
        }
        
#endif
    }
}



#pragma mark - OtherAction

- (void)responseChangeUI:(BOOL)isActive
{
    // 替换彩色与灰色滑动图
    self.adCycleScrollView.localizationImageNamesGroup = [self getAdCycleScrollViewGroup:isActive];
    _currentPage = MAX(0, self.deviceModel.switch_func - 1);
    _currentPage = (_currentPage > 7) ? 0 : _currentPage;
    [self.adCycleScrollView adjustControllerViewImage:_currentPage];
    
    // 设置附属功能
    NSArray *disabelBtnArr = [[CycleScrollViewModel shareInstance] getDisableArr];
    NSArray *numArr = disabelBtnArr[_currentPage];
    
    //  重置所有按钮的激活状态
    for (DishWasherButton *btn in self.dishWasherBtnArray) {
        
        [btn setDishWasherButtonActive:isActive];
    }
    //  根据当前模式禁用对应的附属功能
    for (NSNumber *num in numArr) {
        
        DishWasherButton *btn = (DishWasherButton *)self.subTypeBtnArray[num.integerValue];
        [btn setDishWasherButtonActive:NO];
    }
    
    if (isActive == NO) {
        
        // 添加阻挡操作滑动图的遮罩
        [self.view addSubview:self.disableScrollView];
        [self.disableScrollView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(self.view.mas_top);
            make.width.mas_equalTo(self.view.mas_width);
            make.height.mas_equalTo(UIAdjustNum(700.f));
        }];
        // 电源开关
        self.powerBtn.enabled = YES;
        self.powerBtn.selected = NO;
    }
    else
    {
        // 移除阻挡操作滑动图的遮罩
        [self.disableScrollView removeFromSuperview];
        self.powerBtn.selected = YES;
    }
    
    
    NSLog(@"%d",self.deviceModel.func_mode);
    
    
    
    NSInteger shiftValue = -1;
    for (NSInteger i = 0; i < 7; i++)
    {
        BOOL isOpen = [XLDataPointTool isOpenWithValue:self.deviceModel.func_mode andShift:i];
        if (isOpen) {
            shiftValue = i;
        }
    }
    
    if (shiftValue < 0) {
        for (DishWasherButton *btn in self.subTypeBtnArray) {
            btn.selected = NO;
        }
    }
    else
    {
        for (DishWasherButton *btn in self.subTypeBtnArray) {
            btn.selected = NO;
            NSNumber *shiftNum = [btn.tagDic valueForKey:@"subTypeBtn"];
            if (shiftValue == shiftNum.integerValue) {
                btn.selected = YES;
                
                if (self.lastbutton != btn) {
                    self.lastbutton = btn;
                    KShowErrorLoadingWithMsg(@"设置失败");
                }
                
            }
        }
    }
    
}


- (NSArray *)getAdCycleScrollViewGroup:(BOOL)isActive
{
    if(isActive)
    {
        return [[CycleScrollViewModel shareInstance] getActiveArr];
    }
    else{
        return [[CycleScrollViewModel shareInstance] getUnactiveArr];
    }
}


/**
 获取设备是否可控
 */
- (BOOL)getIsDeviceCanControl
{
#if ENV == 1
    return self.deviceModel.connected;
#elif ENV == 2
    BOOL isActive = (self.deviceModel.dev_statu > 1);
    return ((self.deviceModel.connected) && isActive);
#endif
}


- (void)timerFinished:(NSTimer*)timer
{
    [super timerFinished:timer];
    self.isResponseChange = YES;
    [self responseChangeUI:[self getIsDeviceCanControl]];
    self.isObserveChangeScroll = NO;
}



#pragma mark - Request

/**
 设置数据端点数据
 */
- (void)setDataPoints:(NSArray <XLinkDataPoint*> *)dataPointsArray withSeccussBlock:(void (^)(void))successBlock withFailBlock:(void (^)(void))failBlock{
    
    
    if ([self getIsDeviceCanControlWithIsNeedAlert:YES] == NO) {
        return;
    }
    
    @weakify(self)
    
    //1.处理发送端点前的逻辑
    [self startLogicalBefortSet];
    
    //2.设置端点值到数据模型
    //    [self.deviceModel addDataPoints:dataPointsArray];
    
    //3.更新UI
    //    [self updateTempWithTempValue:self.deviceModel.heater_temp andIsNeedSetToDevice:YES];
    //    [self updateFunctionViewWithFunctionFlag:self.deviceModel.function_flag];
    
    //4.判断游客模式
    if (XL_DATA_VIRTUAL.isLogin == NO){
        return;
    }
    
    //获取当前登录用户ID，加入数据端点
    NSMutableArray *mDataPoints = [self addUserIdDataPointWithDataPointsArray:dataPointsArray];
    
    //判断是否设置相同任务
    //如果是，则取消上一个任务
    [self cancelLastSameTaskWithNewDataPointsArray:dataPointsArray];
    
    self.setDataPointTask = [[XLServiceManager shareInstance].deviceService setDeviceDataPoints:self.deviceModel.device dataPoints:dataPointsArray timeout:TimeOutSetDataPoint handler:^(id result, NSError *error) {
        
        @strongify(self)
        
        if (error == nil) {
            //            KShowErrorLoadingWithMsg(@"DishWasherController设置成功");
            [self.deviceModel addDataPoints:dataPointsArray];
            if (successBlock) {
                successBlock();
            }
        }else{
            if([[self getCurrentVC] isKindOfClass:[DishWasherController class]]){
                KShowErrorLoadingWithMsg(@"设置失败");
                NSLog(@"设置失败, %@", error.userInfo);
            }

            [self responseChangeUI:[self getIsDeviceCanControl]];
            if (failBlock) {
                failBlock();
            }
        }
    }];
}
///自己写的
//获取当前屏幕显示的viewcontroller
- (UIViewController *)getCurrentVC
{
    UIViewController *rootViewController = [UIApplication sharedApplication].keyWindow.rootViewController;
    
    UIViewController *currentVC = [self getCurrentVCFrom:rootViewController];
    
    return currentVC;
}

- (UIViewController *)getCurrentVCFrom:(UIViewController *)rootVC
{
    UIViewController *currentVC;
    
    if ([rootVC presentedViewController]) {
        // 视图是被presented出来的
        rootVC = [rootVC presentedViewController];
    }
    
    if ([rootVC isKindOfClass:[UITabBarController class]]) {
        // 根视图为UITabBarController
        currentVC = [self getCurrentVCFrom:[(UITabBarController *)rootVC selectedViewController]];
    } else if ([rootVC isKindOfClass:[UINavigationController class]]){
        // 根视图为UINavigationController
        currentVC = [self getCurrentVCFrom:[(UINavigationController *)rootVC visibleViewController]];
    } else {
        // 根视图为非导航类
        currentVC = rootVC;
    }
    
    return currentVC;
}

- (NSInteger)getTimeInterValue {
    
    NSInteger minute;
    switch (self.deviceModel.switch_func) {
        case 1:
            minute = 60;
            break;
        case 2:
            minute = 120;
            break;
        case 3:
            minute = 90;
            break;
        case 4:
            minute = 30;
            break;
        case 5:
            minute = 80;
            break;
        case 6:
            minute = 20;
            break;
        case 7:
            minute = 40;
            break;
        case 8:
            minute = 180;
            break;
        case 9:
            minute = 20;
            break;
        case 10:
            minute = 30;
            break;
        case 11:
            minute = 40;
            break;
        case 12:
            minute = 50;
            break;
        case 13:
            minute = 60;
            break;
        case 14:
            minute = 70;
            break;
        case 15:
            minute = 80;
            break;
        case 16:
            minute = 90;
            break;
        case 17:
            minute = 100;
            break;
        case 18:
            minute = 110;
            break;
        case 19:
            minute = 120;
            break;
        default:
            minute = 0;
            break;
    }
    
    return minute;
    
}



#pragma mark - DateTime
/**
 转换预约时间值
 */

- (uint32_t)timeToPointValue
{
    NSInteger minute = [DishWasherTools getTimeInterValueWithSwitchFunc:self.deviceModel.switch_func];
    if ([DishWasherTools isCustomFuncWithSwitchFunc:self.deviceModel.switch_func]) {
        minute = [DishWasherTools getTimeInterValueWithSwitchFunc:DishWasherSwitchFuncIntelligence];
    }
    NSInteger selectedTime = [self.selectedDate timeIntervalSinceDate:[NSDate date]];;
    NSInteger valueTime = selectedTime;
    NSUInteger valueHour = valueTime / 3600;
    NSUInteger ValueSec = valueTime % 60;
    NSUInteger valueMin = (valueTime - valueHour * 3600) / 60 + ((ValueSec > 0) ? 1 : 0);
    
    NSUInteger returnValue = ((_timeType & 0xff) << 31) | ((valueHour & 0xff) << 8) | (valueMin & 0xff);
    return returnValue;
}



// 获取当天0点时间
- (NSDate *)zeroOfTodayTime
{
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSDateComponents *components = [calendar components:NSUIntegerMax fromDate:[NSDate date]];
    components.hour = 0;
    components.minute = 0;
    components.second = 0;
    
    // components.nanosecond = 0 not available in iOS
    NSTimeInterval ts = [[calendar dateFromComponents:components] timeIntervalSince1970];
    return [NSDate dateWithTimeIntervalSince1970:ts];
}


// 计算今天现在的时间
- (NSInteger)sinceTodayTime
{
    NSTimeInterval timeValue = [[NSDate date] timeIntervalSinceDate:[self zeroOfTodayTime]];
    return timeValue;
}



#pragma mark - SubViewsUI

- (void)setSubViewUIDisableScrollView
{
    // 滑动遮挡层
    self.disableScrollView = [[UIView alloc] init];
    [self.view addSubview:self.disableScrollView];
    [self.disableScrollView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.view.mas_top);
        make.width.mas_equalTo(self.view.mas_width);
        make.height.mas_equalTo(UIAdjustNum(700.f));
    }];
}


- (void)setSubViewUIADCycleScrollView
{
    // 滑动图
    self.adCycleScrollView = [SDCycleScrollView cycleScrollViewWithFrame:CGRectZero delegate:self placeholderImage:[UIImage new] withImageMaxWidth:UIAdjustNum(500.f) withImageMinWidth:UIAdjustNum(500.f - 23.f) withImageMaxHeight:UIAdjustNum(350.f) withImageMinHeight:UIAdjustNum(350.f - 80.f)];
    self.adCycleScrollView.autoScroll = NO;
    self.adCycleScrollView.infiniteLoop = NO;
    self.adCycleScrollView.autoScrollTimeInterval = 10.0;
    self.adCycleScrollView.zoomType = YES;  // 是否使用缩放效果
    self.adCycleScrollView.pageControlStyle = SDCycleScrollViewPageContolStyleNone;
    self.adCycleScrollView.scrollDirection = UICollectionViewScrollDirectionHorizontal;
    self.adCycleScrollView.pageControlDotSize = CGRectZero.size;  // pageControl小点的大小
    UIPanGestureRecognizer *gesture = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(tapGestureRecognizer)];
    [self.adCycleScrollView addGestureRecognizer:gesture];
    gesture.delegate = self;
    [self.view addSubview:self.adCycleScrollView];
    [self.adCycleScrollView.cycGestureRecognizer requireGestureRecognizerToFail:gesture];
    [self.adCycleScrollView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.view).mas_offset(topMargin);
        make.left.right.equalTo(self.view);
        make.height.mas_equalTo(UIAdjustNum(350.f));
    }];
}


- (void)setSubViewUISubTypeBtn
{
    // 附属按钮
    NSMutableArray *tempUIArr1 = [NSMutableArray array];
    NSMutableArray *tempUIArr2 = [NSMutableArray array];
    CGFloat subTypeBtnHeight = UIAdjustNum(60.f);
    NSArray *btnTitleArr = @[@"加强漂洗", @"加强消毒", @"无水干燥", @"少量洗", @"增压", @"加速", @"夜间洗", @""];
    NSArray *btnTagArr = @[@1, @0, @2, @4, @6, @5, @3, @999];
    for (int i = 0; i < 8; i++) {
        DishWasherButton *subTypeBtn = [DishWasherButton buttonWithType:UIButtonTypeCustom];
        [subTypeBtn setActiveImage:[UIImage imageNamed:@"ic_smallbtn_standby"] activeColor:[UIColor colorWithHexString:@"818286"] andUnactiveImage:[UIImage imageNamed:@"ic_smallbtn_unactive"] unactiveColor:[UIColor colorWithHexString:@"818286"]];
        [subTypeBtn setBackgroundImage:[UIImage imageNamed:@"ic_smallbtn_active"] forState:UIControlStateSelected];
        [subTypeBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateSelected];
        [subTypeBtn setTitle:btnTitleArr[i] forState:UIControlStateNormal];
        [subTypeBtn.titleLabel setFont:UIFontAdjust(24.f)];
        [subTypeBtn addTarget:self action:@selector(subTypeBtnClick:) forControlEvents:UIControlEventTouchUpInside];
        subTypeBtn.tagDic = @{@"subTypeBtn" : btnTagArr[i]};
        subTypeBtn.selected = NO;
        [self.view addSubview:subTypeBtn];
        [self.dishWasherBtnArray addObject:subTypeBtn];
        [self.subTypeBtnArray addObject:subTypeBtn];
        if (i < 4) {
            [subTypeBtn mas_makeConstraints:^(MASConstraintMaker *make) {
                make.top.mas_equalTo(self.adCycleScrollView.mas_bottom).offset(flowFunMargin);
                make.height.mas_equalTo(subTypeBtnHeight);
            }];
            [tempUIArr1 addObject:subTypeBtn];
        }
        else{
            [tempUIArr2 addObject:subTypeBtn];
        }
        if (i == 7)
        {
            subTypeBtn.hidden = YES;
        }
    }
    [tempUIArr1 mas_distributeViewsAlongAxis:MASAxisTypeHorizontal withFixedSpacing:UIAdjustNum(34.f) leadSpacing:UIAdjustNum(24.f) tailSpacing:UIAdjustNum(24.f)];
    [tempUIArr2 mas_distributeViewsAlongAxis:MASAxisTypeHorizontal withFixedSpacing:UIAdjustNum(34.f) leadSpacing:UIAdjustNum(24.f) tailSpacing:UIAdjustNum(24.f)];
    [tempUIArr2 mas_makeConstraints:^(MASConstraintMaker *make) {
        UIButton *btn = (UIButton *)tempUIArr1[0];
        make.top.equalTo(btn.mas_bottom).offset(UIAdjustNum(funMargin));
        make.height.mas_equalTo(subTypeBtnHeight);
    }];
}


- (void)setSubViewUIFunctionTypeBtn
{
    // 电源按钮
    self.powerBtn = [DishWasherButton buttonWithType:UIButtonTypeCustom];
    [self.powerBtn setActiveImage:[UIImage imageNamed:@"ic_power11_active"] activeColor:[UIColor whiteColor] andUnactiveImage:[UIImage imageNamed:@"ic_power1_unactive"] unactiveColor:[UIColor colorWithHexString:@"818286"]];
    [self.powerBtn addTarget:self action:@selector(powerBtnClick:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:self.powerBtn];
    [self.powerBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.mas_equalTo(self.view.mas_bottom).offset(-bottomMargin);
        make.left.mas_equalTo(self.view.mas_left).mas_offset(UIAdjustNum(24.f));
        make.right.mas_equalTo(self.view.mas_right).mas_offset(-UIAdjustNum(24.f));
        make.height.mas_equalTo(UIAdjustNum(90.f));
    }];
    [self.dishWasherBtnArray addObject:self.powerBtn];
    self.nPlanBtn = [DishWasherPlanButton buttonWithType:UIButtonTypeCustom];
    [self.nPlanBtn addTarget:self action:@selector(planBtnClick) forControlEvents:UIControlEventTouchUpInside];
    
    [self.view addSubview:self.nPlanBtn];
    [self.nPlanBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(self.powerBtn.mas_top).offset(-powPlanMargin);
        make.height.mas_equalTo(UIAdjustNum(90.f));
    }];
    [self.dishWasherBtnArray addObject:self.nPlanBtn];
    NSMutableArray *btnArray3 = [NSMutableArray array];
    [btnArray3 addObject:self.nPlanBtn];
    DishWasherButton *functionTypeBtn = [DishWasherButton buttonWithType:UIButtonTypeCustom];
    [functionTypeBtn setActiveImage:[UIImage imageNamed:@"ic_shortbtn_active"] activeColor:[UIColor whiteColor] andUnactiveImage:[UIImage imageNamed:@"ic_shortbtn_unactive"] unactiveColor:[UIColor colorWithHexString:@"818286"]];
    [functionTypeBtn setTitle:@"开始洗涤" forState:UIControlStateNormal];
    functionTypeBtn.titleLabel.textAlignment = NSTextAlignmentCenter;
    [functionTypeBtn.titleLabel setFont:UIFontAdjust(36.f)];
    [functionTypeBtn addTarget:self action:@selector(startBtnClick) forControlEvents:UIControlEventTouchUpInside];
    [btnArray3 addObject:functionTypeBtn];
    [self.dishWasherBtnArray addObject:functionTypeBtn];
    [self.view addSubview:functionTypeBtn];
    [functionTypeBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(self.powerBtn.mas_top).offset(-powPlanMargin);
        make.height.mas_equalTo(UIAdjustNum(90.f));
    }];
    [btnArray3 mas_distributeViewsAlongAxis:MASAxisTypeHorizontal withFixedSpacing:UIAdjustNum(22.f) leadSpacing:UIAdjustNum(24.f) tailSpacing:UIAdjustNum(24.f)];
    // 自定义按钮
    DishWasherButton *customBtn = [DishWasherButton buttonWithType:UIButtonTypeCustom];
    [customBtn setActiveImage:[UIImage imageNamed:@"ic_longbtn_active"] activeColor:[UIColor whiteColor] andUnactiveImage:[UIImage imageNamed:@"ic_longbtn_unactive"] unactiveColor:[UIColor colorWithHexString:@"818286"]];
    [customBtn setTitle:@"自定义" forState:UIControlStateNormal];
    [customBtn.titleLabel setFont:UIFontAdjust(36.f)];
    [customBtn addTarget:self action:@selector(customBtnClick) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:customBtn];
    [customBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        //        UIButton *btn = (UIButton *)tempUIArr2[0];
        make.bottom.equalTo(self.nPlanBtn.mas_top).offset(-planCustomMargin);
        make.left.mas_equalTo(self.view.mas_left).mas_offset(UIAdjustNum(24.f));
        make.right.mas_equalTo(self.view.mas_right).mas_offset(-UIAdjustNum(24.f));
        make.height.mas_equalTo(UIAdjustNum(90.f));
    }];
    [self.dishWasherBtnArray addObject:customBtn];
    
    // 功能行按钮
    
}


- (void)setSubViewUIDishWasherPlanView
{
    __weak typeof(self) weakSelf = self;
    self.dishWasherPlanView = [[DishWasherPlanView alloc] initPlanViewWithDeviceModel:self.deviceModel WithEditBlack:^(NSUInteger timeType, NSDate *selectedDate) {
        BOOL isCustom = [DishWasherTools isCustomFuncWithSwitchFunc:weakSelf.deviceModel.switch_func];
        if (isCustom) {
            weakSelf.nPlanBtn.switchFunc = DishWasherSwitchFuncIntelligence;
        } else {
            weakSelf.nPlanBtn.switchFunc = weakSelf.deviceModel.switch_func;
        }
        weakSelf.nPlanBtn.selectedDate = selectedDate;
        
        self.selectedDate = selectedDate;
        NSUInteger valueTime = [weakSelf timeToPointValue];
        XLinkDataPoint *dataPoint_funcMode = [XLinkDataPoint dataPointWithType:XLinkDataTypeByte withIndex:8 withValue:@(0)];
        uint8_t valuePlan = [XLDataPointTool getDataPointValueWithValue:weakSelf.deviceModel.act_switch andShift:4 andIsOpen:YES];
        XLinkDataPoint *dataPoint_valuePlan = [XLinkDataPoint dataPointWithType:XLinkDataTypeByte withIndex:10 withValue:@(valuePlan)];
        XLinkDataPoint *dataPoint_planTime = [XLinkDataPoint dataPointWithType:XLinkDataTypeUnsignedInt withIndex:11 withValue:@(valueTime)];
        XLinkDataPoint *dataPoint_devStatu = [XLinkDataPoint dataPointWithType:XLinkDataTypeByte withIndex:12 withValue:@3];
        NSArray *dataPoints = @[dataPoint_devStatu,dataPoint_valuePlan,dataPoint_planTime, dataPoint_funcMode];
        if (isCustom) {
            XLinkDataPoint *dataPoint_switchFunc = [XLinkDataPoint dataPointWithType:XLinkDataTypeByte withIndex:DishWasherDataPoint_switch_func withValue:@(DishWasherSwitchFuncIntelligence)];
            dataPoints = @[dataPoint_switchFunc,dataPoint_devStatu,dataPoint_valuePlan, dataPoint_planTime, dataPoint_funcMode];
        }
        [weakSelf setDataPoints:dataPoints withSeccussBlock:nil withFailBlock:^{
            weakSelf.selectedDate = nil;
            weakSelf.nPlanBtn.plantActSwitch = [XLDataPointTool isOpenWithValue:weakSelf.deviceModel.act_switch andShift:4];;
            weakSelf.nPlanBtn.timeType = weakSelf.timeType;
            weakSelf.nPlanBtn.switchFunc = weakSelf.deviceModel.switch_func;
            weakSelf.nPlanBtn.orderTime = weakSelf.deviceModel.order_time;
            weakSelf.timeType = weakSelf.nPlanBtn.timeType;
        }];
        
    } withCancleBlock:^{
        if ([weakSelf getIsDeviceCanControlWithIsNeedAlert:YES] == NO) {
            return;
        }
        weakSelf.selectedDate = nil;
        weakSelf.nPlanBtn.plantActSwitch = NO;
        XLinkDataPoint *dataPoint_devStatu = [XLinkDataPoint dataPointWithType:XLinkDataTypeByte withIndex:12 withValue:@2];
        uint8_t valuePlan = [XLDataPointTool getDataPointValueWithValue:weakSelf.deviceModel.act_switch andShift:4 andIsOpen:NO];
        XLinkDataPoint *dataPoint_valuePlan = [XLinkDataPoint dataPointWithType:XLinkDataTypeByte withIndex:10 withValue:@(valuePlan)];
        uint32_t valueTime = 0;
        XLinkDataPoint *dataPoint_planTime = [XLinkDataPoint dataPointWithType:XLinkDataTypeUnsignedInt withIndex:11 withValue:@(valueTime)];
        [weakSelf setDataPoints:@[dataPoint_devStatu,dataPoint_valuePlan, dataPoint_planTime] withSeccussBlock:^{
            
        } withFailBlock:^{
            weakSelf.nPlanBtn.plantActSwitch = [XLDataPointTool isOpenWithValue:weakSelf.deviceModel.act_switch andShift:4];;
            weakSelf.nPlanBtn.timeType = weakSelf.timeType;
            weakSelf.nPlanBtn.switchFunc = weakSelf.deviceModel.switch_func;
            weakSelf.nPlanBtn.orderTime = weakSelf.deviceModel.order_time;
        }];
    }];

    [self.view addSubview:self.dishWasherPlanView];
    [self.dishWasherPlanView loadDishWasherPlanView];
    [self.dishWasherPlanView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(0.f);
        make.width.mas_equalTo(self.view.mas_width);
        make.height.mas_equalTo(self.view.mas_height);
    }];
}
@end
