//
//  WarningViewController.m
//  vatti
//
//  Created by panyaping on 2018/8/31.
//  Copyright © 2018年 xlink. All rights reserved.
//

#import "WarningViewController.h"
#import "WarningTableViewCell.h"
#import "MessageCell.h"
#import "UIColor+XLCategory.h"

@interface WarningViewController ()<UITableViewDelegate,UITableViewDataSource>

@property (nonatomic, strong) UITableView *warningTableView;
@property (nonatomic, strong) NSMutableArray *dataSourceArray;

@end

@implementation WarningViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = @"告警列表";
    self.warningTableView = [[UITableView alloc] initWithFrame:self.view.frame style:(UITableViewStylePlain)];
    self.warningTableView.tableHeaderView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, 82)];
    self.warningTableView.backgroundColor = [UIColor lightGrayColor];
    
    
    self.warningTableView.delegate = self;
    self.warningTableView.dataSource = self;
    
    [self.view addSubview:self.warningTableView];
    
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, 300)];
    view.backgroundColor = [UIColor lightGrayColor];
    
    UILabel *seviceLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 60, [UIScreen mainScreen].bounds.size.width, 40)];
    [view addSubview:seviceLabel];
    seviceLabel.text = @"如有需要，请拨打华帝官方服务热线";
    seviceLabel.textAlignment = NSTextAlignmentCenter;
    seviceLabel.textColor = [UIColor colorWithHexString:@"818286"];
    seviceLabel.font = [UIFont systemFontOfSize:14];;
    
    UIImageView *imageView = [[UIImageView alloc] init];
    [view addSubview:imageView];
    imageView.image = [UIImage imageNamed:@"call"];
    [imageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(seviceLabel.mas_bottom).offset(93);
        make.center.equalTo(self.view);
        make.width.height.equalTo(@58);
    }];
    
    self.warningTableView.tableFooterView = view;
    
    
   
    // Do any additional setup after loading the view.
}

- (void)initConfigure
{
    [super initConfigure];
    
    self.dataSourceArray = [NSMutableArray array];
    
    [self.dataSourceArray addObject:@"热敏电阻短路"];
    [self.dataSourceArray addObject:@"热敏电阻短路"];
    [self.dataSourceArray addObject:@"热敏电阻短路"];
    [self.dataSourceArray addObject:@"热敏电阻短路"];
    [self.dataSourceArray addObject:@"热敏电阻短路"];
}

- (void)buildData
{
    [super buildData];
    
    @weakify(self)
    [[RACObserve(self.deviceModel, err_code) deliverOnMainThread] subscribeNext:^(id  _Nullable x) {
        @strongify(self)
        
        //data
        [self configureErrorNum];
        
        //ui
        [self.warningTableView reloadData];
    }];
    
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    
    return self.dataSourceArray.count;
    
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    
    NSString *identify = @"cellIdentify";
    WarningTableViewCell *cell = [[WarningTableViewCell alloc] initWithStyle:(UITableViewCellStyleDefault) reuseIdentifier:identify];
    cell.err_str = [self.dataSourceArray objectAtIndex:indexPath.row];
    
    
    
    return cell;
    
    
}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 47;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

//
//- (void)viewDidLoad {
//    [super viewDidLoad];
//}
//
//- (void)initConfigure
//{
//    [super initConfigure];
//
//    self.dataSourceArray = [NSMutableArray array];
//}
//
//- (void)initSubViews{
//    [super initSubViews];
//
//    //navi
//    self.navigationItem.title = @"告警列表";
//
//    //tableView
//    [self.tableview registerNib:[UINib nibWithNibName:NSStringFromClass([MessageCell class]) bundle:nil] forCellReuseIdentifier:XLCurrentDeviceAlarmCellID];
//}
//
//- (void)buildData
//{
//    [super buildData];
//
//    @weakify(self)
//    [[RACObserve(self.deviceModel, master_err) deliverOnMainThread] subscribeNext:^(id  _Nullable x) {
//        @strongify(self)
//
//        //data
//        [self configureErrorNum];
//
//        //ui
//        [self.warningTableView reloadData];
//    }];
//
//}

#pragma mark - Data

- (void)configureErrorNum
{
    [self.dataSourceArray removeAllObjects];
    
    UInt16 value = self.deviceModel.err_code;
    
    if (value == 0) {
    }else if (value == 1){
        [self.dataSourceArray addObject:@"E01 热敏电阻短路"];
    }else if (value == 2){
        [self.dataSourceArray addObject:@"E02 热敏电阻开路"];
    }else if (value == 3){
        [self.dataSourceArray addObject:@"E03 机器不加热"];
    }else if (value == 4){
        [self.dataSourceArray addObject:@"E04 进水阀故障"];
    }else if (value == 5){
        [self.dataSourceArray addObject:@"E05 溢流"];
    }else if (value == 6){
        [self.dataSourceArray addObject:@"E06 机器加热异常"];
    }else if (value == 7){
//        [self.dataSourceArray addObject:@"洗涤程序运行异常"];
    }else if (value == 8){
        [self.dataSourceArray addObject:@"E08 洗涤程序运行异常"];
    }else if (value == 9){
        [self.dataSourceArray addObject:@"E09 按键出错"];
    }
}

#pragma mark - Action

/**
 打电话
 */
- (IBAction)btnCallClicked:(UIButton *)sender {
    NSMutableString *phoneStr = [[NSMutableString alloc] initWithFormat:@"telprompt://%@",@"4008886288"];
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:phoneStr]];
}


//#pragma mark - UITableViewDataSource
//
//- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
//    return 1;
//}
//
//- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
//    return self.dataSourceArray.count;
//}
//
//- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
//
//    MessageCell *cell = [tableView dequeueReusableCellWithIdentifier:XLCurrentDeviceAlarmCellID];
//    cell.errorNum = [self.dataSourceArray objectAtIndex:indexPath.section];
//    return cell;
//
//}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
