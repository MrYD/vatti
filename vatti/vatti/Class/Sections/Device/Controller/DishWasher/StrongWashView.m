//
//  StrongWashView.m
//  vatti
//
//  Created by panyaping on 2018/8/3.
//  Copyright © 2018年 xlink. All rights reserved.
//

#import "StrongWashView.h"
#import "DishWasherConstants.h"
#import "DishWasherTools.h"
@interface StrongWashView ()
@property (nonatomic, strong) UIImageView *progressView;
@property (nonatomic, strong) UILabel *timeTipLabel;
@property (nonatomic, strong) UILabel *timeLabel;
@property (nonatomic, strong) UIButton *startWashBtn;
@property (nonatomic, strong) UILabel *temperatureLabel;
@property (nonatomic, strong) CALayer *colorLayer;
@property (nonatomic, strong) UILabel *completeLabel;
@property (nonatomic, strong) UILabel *stoppageLabel;
@property (nonatomic, strong) UIButton *ViewDetailButton;
@property (nonatomic, strong) CAKeyframeAnimation *animation;
@end
@implementation StrongWashView


/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

- (void)initSubViews
{
    
    self.userInteractionEnabled = YES;
    self.image = [UIImage imageNamed:@"wash_bg"];

    _progressView = [[UIImageView alloc] init];
    _progressView.image = [UIImage imageNamed:@"wash_gradientramp_full"];
    [self addSubview:_progressView];
    [_progressView mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.width.height.equalTo(@237);
        make.center.equalTo(self);
        
    }];
    
    _timeTipLabel = [[UILabel alloc] init];
    _timeTipLabel.text = @"距离洗涤结束";
    _timeTipLabel.font = [UIFont systemFontOfSize:14];
    _timeTipLabel.textColor = [UIColor colorWithHexString:@"323345"];
    _timeTipLabel.textAlignment = NSTextAlignmentCenter;
    [self addSubview:_timeTipLabel];
    [_timeTipLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.top.equalTo(@40);
        make.centerX.equalTo(self);
        make.width.equalTo(self.mas_width);
        make.height.equalTo(@30);
        
    }];
    
    _timeLabel = [[UILabel alloc] init];
    _timeLabel.text = @"00:00";
    _timeLabel.font = [UIFont systemFontOfSize:36];
    _timeLabel.textAlignment = NSTextAlignmentCenter;
    _timeLabel.textColor = [UIColor colorWithHexString:@"323345"];
    [self addSubview:_timeLabel];
    [_timeLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.top.equalTo(_timeTipLabel.mas_bottom);
        make.centerX.equalTo(self);
        make.width.equalTo(self);
        make.height.equalTo(@50);
        
    }];
    _startWashBtn = [[UIButton alloc] init];
    [_startWashBtn setBackgroundImage:[UIImage imageNamed:@"ic_pause"] forState:(UIControlStateNormal)];
    [_startWashBtn setBackgroundImage:[UIImage imageNamed:@"ic_start"] forState:(UIControlStateSelected)];
    [_startWashBtn setBackgroundImage:[UIImage imageNamed:@"ic_start"] forState:(UIControlStateDisabled)];
    [_startWashBtn addTarget:self action:@selector(buttonAction:) forControlEvents:(UIControlEventTouchUpInside)];
    [self addSubview:_startWashBtn];
    [_startWashBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.top.equalTo(_timeLabel.mas_bottom).offset(10);
        make.centerX.equalTo(self);
        make.width.equalTo(@28);
        make.height.equalTo(@38);
        
    }];
    
    
    _temperatureLabel = [[UILabel alloc] init];
    _temperatureLabel.text = @"0℃";
    _temperatureLabel.font = [UIFont systemFontOfSize:18];
    _temperatureLabel.textAlignment = NSTextAlignmentCenter;
    _temperatureLabel.textColor = [UIColor colorWithHexString:@"323345"];
    [self addSubview:_temperatureLabel];
    [_temperatureLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.top.equalTo(_startWashBtn.mas_bottom);
        make.centerX.equalTo(self);
        make.width.equalTo(self);
        make.height.equalTo(@50);
        
    }];
    
    _completeLabel = [[UILabel alloc] init];
    _completeLabel.text = @"已完成";
    _completeLabel.textAlignment = NSTextAlignmentCenter;
    _completeLabel.font = [UIFont systemFontOfSize:36];
    _completeLabel.textColor = [UIColor colorWithHexString:@"323345"];
    [self addSubview:_completeLabel];
    [_completeLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.center.width.equalTo(self);
        make.height.equalTo(@60);
        
    }];
    _completeLabel.hidden = YES;
    _stoppageLabel = [[UILabel alloc] init];
    _stoppageLabel.text = @"设备出现故障\n暂时无法使用";
    _stoppageLabel.numberOfLines = 2;
    _stoppageLabel.textAlignment = NSTextAlignmentCenter;
    _stoppageLabel.font = [UIFont systemFontOfSize:21];
    [_stoppageLabel setTextColor:[UIColor colorWithHexString:@"323345"]];
    [self addSubview:_stoppageLabel];
    [self.stoppageLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.top.equalTo(@50);
        make.centerX.equalTo(self);
        make.width.equalTo(@220);
        make.height.equalTo(@100);
        
    }];
    _stoppageLabel.hidden = YES;

    _ViewDetailButton = [UIButton buttonWithType:(UIButtonTypeCustom)];
    [_ViewDetailButton setTitle:@"点击查看详情" forState:(UIControlStateNormal)];
    _ViewDetailButton.titleLabel.font = [UIFont systemFontOfSize:14];
    [_ViewDetailButton setTitleColor:[UIColor colorWithHexString:@"818286"] forState:(UIControlStateNormal)];
    [_ViewDetailButton addTarget:self action:@selector(viewDetailAction:) forControlEvents:(UIControlEventTouchUpInside)];
    [self addSubview:_ViewDetailButton];
    [self.ViewDetailButton mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.bottom.equalTo(self.mas_bottom).offset(-68);
        make.centerX.equalTo(self);
        make.width.equalTo(@150);
        make.height.equalTo(@40);
        
    }];
    _ViewDetailButton.hidden = YES;
    
    _colorLayer = [CALayer layer];
    _colorLayer.frame = CGRectMake(-20, -100, 25, 25);
    _colorLayer.cornerRadius = 25/2;
    _colorLayer.position = CGPointMake(0,237.0/2);
    [_colorLayer setContents:[UIImage imageNamed:@"dishKnob"].CGImage];
    _colorLayer.transform = CATransform3DMakeScale(1.2, 1.2,1);
    [self.progressView.layer addSublayer:_colorLayer];
    
}
- (CAKeyframeAnimation*)animation
{
    if (_animation == nil) {
        //初始化圆点层
        UIBezierPath* path = [[UIBezierPath alloc] init];
        [path addArcWithCenter:CGPointMake(237.0/2,237.0/2)
                        radius:226/2.0
                    startAngle:0
                      endAngle:2 * M_PI
                     clockwise:1];
        
        //创建一个帧动画需要 的圆周 路径，这个路径 与外圆圈一一致
        _animation = [CAKeyframeAnimation animation];
        _animation.keyPath = @"position";
        _animation.path = path.CGPath;
        _animation.duration = 3.0;
        _animation.repeatCount = MAXFLOAT;
        _animation.removedOnCompletion = NO;
        _animation.timingFunction =  [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionLinear];
    }
    return _animation;
}
-(void)mas_updateConstraints
{
    if (self.state == XLDishPlanState) {
        [self updatePlaneStateConstraints];
    }else {
        [self updateNormalConstraints];
    }
}
- (void)updateNormalConstraints
{
    [_timeTipLabel mas_updateConstraints:^(MASConstraintMaker *make) {
        
        make.top.equalTo(@40);
        make.centerX.equalTo(self);
        make.width.equalTo(self.mas_width);
        make.height.equalTo(@30);
        
    }];
    
    [_timeLabel mas_updateConstraints:^(MASConstraintMaker *make) {
        
        make.top.equalTo(_timeTipLabel.mas_bottom);
        make.centerX.equalTo(self);
        make.width.equalTo(self);
        make.height.equalTo(@50);
        
    }];
    [_startWashBtn mas_updateConstraints:^(MASConstraintMaker *make) {
        
        make.top.equalTo(_timeLabel.mas_bottom).offset(10);
        make.centerX.equalTo(self);
        make.width.equalTo(@28);
        make.height.equalTo(@38);
        
    }];
    [_temperatureLabel mas_updateConstraints:^(MASConstraintMaker *make) {
        
        make.top.equalTo(_startWashBtn.mas_bottom);
        make.centerX.equalTo(self);
        make.width.equalTo(self);
        make.height.equalTo(@50);
        
    }];

}
- (void)updatePlaneStateConstraints
{
    [_timeTipLabel mas_updateConstraints:^(MASConstraintMaker *make) {
        
        make.top.equalTo(@60);
        make.centerX.equalTo(self);
        make.width.equalTo(self.mas_width);
        make.height.equalTo(@30);
        
    }];
    
    [_timeLabel mas_updateConstraints:^(MASConstraintMaker *make) {
        
        make.top.equalTo(_timeTipLabel.mas_bottom).offset(10);
        make.centerX.equalTo(self);
        make.width.equalTo(self);
        make.height.equalTo(@50);
        
    }];
    [_startWashBtn mas_updateConstraints:^(MASConstraintMaker *make) {
        
        make.top.equalTo(_timeLabel.mas_bottom).offset(10);
        make.centerX.equalTo(self);
        make.width.equalTo(@28);
        make.height.equalTo(@0);
        
    }];
    [_temperatureLabel mas_updateConstraints:^(MASConstraintMaker *make) {
        
        make.top.equalTo(_startWashBtn.mas_bottom);
        make.centerX.equalTo(self);
        make.width.equalTo(self);
        make.height.equalTo(@50);
        
    }];

}
- (instancetype)initWithFrame:(CGRect)frame
{
    
    if (self = [super initWithFrame:frame]) {
        
        
        [self initSubViews];
        
        
    }
    return self;
}

- (void)setRemainningTime:(uint32_t)remainningTime
{
    if (_remainningTime == remainningTime) {
        return;
    }
    _remainningTime = remainningTime;
    [self refreshShowTime];
}
- (void)setCurremp:(uint8_t)curremp
{
    if (_curremp == curremp) {
        return;
    }
    _curremp = curremp;
    _temperatureLabel.text = [NSString stringWithFormat:@"%d℃",curremp];
}
- (void)setOrderTime:(uint32_t)orderTime
{
    if (_orderTime == orderTime) {
        return;
    }
    _orderTime = orderTime;
    [self refreshShowTime];
}
- (void)refreshShowTime
{
    uint32_t time = 0;
    if (self.state == XLDishPlanState) {
        time = self.orderTime;
        self.timeLabel.text = [DishWasherTools getMMSSFromOrderTime:time];
    }else {
        time = self.remainningTime;
        self.timeLabel.text = [DishWasherTools getMMSSFromSS:time];
    }
}
-(void)setState:(XLDishState)state
{
    if (_state == state) {
        return;
    }
    _state = state;
    switch (state) {
        case XLDishRunningState:
            [self dishRunning];
            
            break;
        case XLDishPauseState:
            [self dishPause];
            break;
        case XLDishCompleteState:
            [self dishComplete];
            break;
        case XLDishMachineStoppageState:
            [self dishMachineStoppage];
            break;
        case XLDishPlanState:
            [self dishPlan];
        default:
            break;
    }
}
- (void)dishRunning
{
    _timeTipLabel.text = @"距离洗涤结束";
    self.image = [UIImage imageNamed:@"wash_bg"];
    _progressView.hidden = NO;
    _timeTipLabel.hidden = NO;
    _timeLabel.hidden = NO;
    _startWashBtn.hidden = NO;
    _startWashBtn.selected = NO;
    _startWashBtn.enabled = YES;
    _temperatureLabel.hidden = NO;
    _completeLabel.hidden = YES;
    _stoppageLabel.hidden = YES;
    _ViewDetailButton.hidden = YES;
    [self continueAnimation];
    [self refreshShowTime];
    [self mas_updateConstraints];
}
- (void)dishPause
{
    _timeTipLabel.text = @"距离洗涤结束";
    self.image = [UIImage imageNamed:@"wash_bg"];
    _progressView.hidden = NO;
    _timeTipLabel.hidden = NO;
    _timeLabel.hidden = NO;
    _startWashBtn.hidden = NO;
    _startWashBtn.selected = YES;
    _startWashBtn.enabled = YES;
    _temperatureLabel.hidden = NO;
    _completeLabel.hidden = YES;
    _stoppageLabel.hidden = YES;
    _ViewDetailButton.hidden = YES;
    [self pauseAnimation];
    [self refreshShowTime];
    [self mas_updateConstraints];
    
}
- (void)dishComplete
{
    if (_remainningTime == 0) {
        _completeLabel.text = @"已完成";
    } else {
        _completeLabel.text = @"已中止";
    }
    self.image = [UIImage imageNamed:@"wash_bg"];
    _progressView.hidden = YES;
    _timeTipLabel.hidden = YES;
    _timeLabel.hidden = YES;
    _startWashBtn.hidden = YES;
    _temperatureLabel.hidden = YES;
    _completeLabel.hidden = NO;
    _stoppageLabel.hidden = YES;
    _ViewDetailButton.hidden = YES;
    [self pauseAnimation];
    [self mas_updateConstraints];
}
- (void)dishMachineStoppage
{
    self.image = [UIImage imageNamed:@"wash_bg_warning"];
    _progressView.hidden = YES;
    _timeTipLabel.hidden = YES;
    _timeLabel.hidden = YES;
    _startWashBtn.hidden = YES;
    _temperatureLabel.hidden = YES;
    _stoppageLabel.hidden = NO;
    _ViewDetailButton.hidden = NO;
    [self pauseAnimation];
    [self mas_updateConstraints];
}
- (void)dishPlan
{
    _timeTipLabel.text = @"距离洗涤开始";
    self.image = [UIImage imageNamed:@"wash_bg"];
    _progressView.hidden = NO;
    _timeTipLabel.hidden = NO;
    _timeLabel.hidden = NO;
    _startWashBtn.hidden = NO;
    _startWashBtn.selected = YES;
    _startWashBtn.enabled = NO;
    _temperatureLabel.hidden = NO;
    _completeLabel.hidden = YES;
    _stoppageLabel.hidden = YES;
    _ViewDetailButton.hidden = YES;
    [self continueAnimation];
    [self refreshShowTime];
    [self mas_updateConstraints];
}
#pragma Animation
- (void)startRun
{
    if (!_animation) {
        [self.colorLayer addAnimation:self.animation forKey:nil];
    }
}
- (void)continueAnimation
{
    CFTimeInterval pausedTime = [_colorLayer timeOffset];
    _colorLayer.speed = 1.0;
    _colorLayer.timeOffset = 0.0;
    _colorLayer.beginTime = 0.0;
    CFTimeInterval timeSincePause = [_colorLayer convertTime:CACurrentMediaTime() fromLayer:nil] - pausedTime;
    _colorLayer.beginTime = timeSincePause;
}
- (void)pauseAnimation
{
    
    CFTimeInterval pausedTime = [_colorLayer convertTime:CACurrentMediaTime() fromLayer:nil];
    _colorLayer.speed = 0.0;
    _colorLayer.timeOffset = pausedTime;
    
}
#pragma Action
- (void)buttonAction:(UIButton *)button
{
    
    if (self.state == XLDishRunningState) {
        if (self.delegate&&[self.delegate respondsToSelector:@selector(pauseActionWithStrongWashView:)]) {
            [self.delegate pauseActionWithStrongWashView:self];
        }
    }else if (self.state == XLDishPauseState) {
        if (self.delegate&&[self.delegate respondsToSelector:@selector(startActionWithStrongWashView:)]) {
            [self.delegate startActionWithStrongWashView:self];
        }
    }
    
}
- (void)viewDetailAction:(UIButton *)button
{
    if (self.delegate&&[self.delegate respondsToSelector:@selector(stoppageDetailActionWithStrongWashView:)]) {
        [self.delegate stoppageDetailActionWithStrongWashView:self];
    }
}
-(void)dealloc
{
    [self.colorLayer removeAllAnimations];
}
@end
