//
//  BaseDishwasherDeviceViewController.m
//  vatti
//
//  Created by panyaping on 2018/9/19.
//  Copyright © 2018年 xlink. All rights reserved.
//

#import "BaseDishwasherDeviceViewController.h"
#import "MainTabViewController.h"
#import "DeviceControlViewController.h"
#import "XLUtilTool.h"

@interface BaseDishwasherDeviceViewController ()

@end

@implementation BaseDishwasherDeviceViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)buildData
{
    [super buildData];
    
    @weakify(self)
    //监控订阅关系取消
    [[[kXLNotificationCenter rac_addObserverForName:XLNotificationDeviceUnSubscribe object:nil] deliverOnMainThread] subscribeNext:^(NSNotification * _Nullable x) {
        @strongify(self)
        [self configureXLNotificationDeviceUnSubscribe:x];
    }];
    
    [[RACObserve(self.deviceModel, connected) deliverOnMainThread] subscribeNext:^(id  _Nullable x){
        @strongify(self)
        [self configureConnectedStateChanged];
    }];
    
//    [[RACObserve(self.deviceModel, master_err).distinctUntilChanged deliverOnMainThread] subscribeNext:^(id  _Nullable x) {
//        @strongify(self)
//        [self configureMasterErrorChage];
//    }];
}

- (void)configureXLNotificationDeviceUnSubscribe:(NSNotification*)no
{
    DishWasherDeviceModel *noDeviceModel = (DishWasherDeviceModel*)no.object;
    
    if ([noDeviceModel.device isEqualToDevice:self.deviceModel.device] == YES) {
        [self alertHintWithMessage:@"您的设备管理权限已被移除" handler:^(UIAlertAction *action, NSUInteger tag) {
            
            DishWasherDeviceModel *noDeviceModel = (DishWasherDeviceModel*)no.object;
            [XL_DATA_SOURCE.user removeDeviceModel:noDeviceModel];
            
            //回到首页
            MainTabViewController *tabbar = [[MainTabViewController alloc] init];
            [XLUtilTool setRootViewController:tabbar animationOptions:UIViewAnimationOptionTransitionCrossDissolve duration:0.3];
        }];
    }
    
}

- (void)configureConnectedStateChanged
{
    Class currentClass = [self class];
    BOOL b1 = currentClass == [DeviceControlViewController class];
    
    if (b1 == YES) {
        return;
    }
    
    BOOL b2 = self.deviceModel.connected == NO;
    if (b2 == YES) {
        [self alertHintWithMessage:@"设备连接已断开，无法操作" handler:^(UIAlertAction *action, NSUInteger tag) {
            //回到设备控制界面
            NSArray *temp = self.navigationController.viewControllers;
            for (id vc in temp) {
                BOOL b2 = [vc isKindOfClass:[DeviceControlViewController class]];
                
                if (b2 == YES) {
                    [self.navigationController popToViewController:vc animated:YES];
                    break;
                }
            }
            
        }];
    }
}

- (void)setUserIdDataPoints:(NSString*)userId {
    
    
//    __weak __typeof(self) weakself = self;
//    XLinkDataPoint *dataPoint = [XLinkDataPoint dataPointWithType:XLinkDataTypeByte withIndex:9 withValue:@(indexPath.row + 1)];
//    [self setDataPoints:@[dataPoint] withSeccussBlock:nil withFailBlock:^{
//        [weakself.tableView reloadData];
//        NSInteger row = [weakself.tableView numberOfRowsInSection:0];
//        if (row > 0) {
//            NSLog(@"已设置软水档位成功%d",weakself.deviceModel.water_gears);
//            NSString *value = [NSString stringWithFormat:@"%d",weakself.deviceModel.water_gears];
//            UITableViewCell *cell = [self.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:([value integerValue] - 1) inSection:0]];
//            UIImageView *imageView = (UIImageView *)cell.accessoryView;
//            [imageView setImage:[UIImage imageNamed:@"ic_cource_active"]];
//        }
//    }];
    

    NSString *functionFlagValue = userId;
    XLinkDataPoint *dataPointFunctionFlag = [XLinkDataPoint dataPointWithType:XLinkDataTypeString withIndex:20 withValue:functionFlagValue];
    XLinkDataPoint *dataPoint_switchFunc = [XLinkDataPoint dataPointWithType:XLinkDataTypeByte withIndex:4 withValue:@(2)];

    NSArray *dataPointsArray = @[dataPoint_switchFunc,dataPointFunctionFlag];
    
    
    //1.处理发送端点前的逻辑
//    [self startLogicalBefortSet];
    
    //4.判断游客模
//    if (XL_DATA_VIRTUAL.isLogin == NO){
//        return;
//    }
    //获取当前登录用户ID，加入数据端点
    NSMutableArray *mDataPoints = [self addUserIdDataPointWithDataPointsArray:dataPointsArray];
    //判断是否设置相同任务
    //如果是，则取消上一个任务
    [self cancelLastSameTaskWithNewDataPointsArray:dataPointsArray];
    
    
    [[XLServiceManager shareInstance].deviceService setDeviceDataPoints:self.deviceModel.device dataPoints:dataPointsArray timeout:TimeOutSetDataPoint handler:^(id result, NSError *error) {
        
        //            @strongify(self)
        
        if (error == nil) {
            [self.deviceModel addDataPoints:dataPointsArray];
            //            [_pauseButton setTitle:@"暂 停" forState:(UIControlStateNormal)];
            //            _strongWashLabel.text = [NSString stringWithFormat:@"%@-洗涤中",_dish_func];
            //            [self running];
            
            //
            //                //            self.lastSuccessTemp = self.deviceModel.heater_temp;
        }else{
            KShowErrorLoadingWithMsg(@"设置失败");
            
            //                if (self.isResponseChange == YES) {
            //                    //回滚温度
            //                    //                [self updateTempWithTempValue:self.lastSuccessTemp andIsNeedSetToDevice:YES];
            //                }
            //
        }
    }];
}


/**
 获取设备是否可控
 */
- (BOOL)getIsDeviceCanControlWithIsNeedAlert:(BOOL)isNeedAlert
{
    NSString *currUserID = [XLDataSource shareInstance].user.userId.stringValue;
    
    BOOL b1 = self.deviceModel.priority == 0;//设备空闲
    BOOL b2 = self.deviceModel.priority == 2;//设备正在被app控制
    
    BOOL b3 = (self.deviceModel.user_id == nil) || (self.deviceModel.user_id.length == 0) || [self.deviceModel.user_id isEqualToString:currUserID];//当前操作设备的用户是登录用户
    BOOL b4 = b2 && b3;//是当前app在操作
    
    BOOL canControl = b1 || b4;
    if (canControl == NO && isNeedAlert == YES) {
        //弹框
        [self alertHintWithMessage:@"无法操作，其他人正在操作设备"];
    } else if (canControl) {
        if (self.deviceModel.user_id.length == 0) {
            [self setUserIdDataPoints:currUserID];
        }
    }

    return canControl;
}
- (void)cancelDevicepriority
{
    NSString *currUserID = [XLDataSource shareInstance].user.userId.stringValue;
    if ([self.deviceModel.user_id isEqualToString:currUserID]) {
        XLinkDataPoint *dataPointFunctionFlag = [XLinkDataPoint dataPointWithType:XLinkDataTypeString withIndex:20 withValue:@""];
        XLinkDataPoint *dataPoint_switchFunc = [XLinkDataPoint dataPointWithType:XLinkDataTypeString withIndex:20 withValue:@""];
        
        NSArray *dataPointsArray = @[dataPoint_switchFunc,dataPointFunctionFlag];
        //判断是否设置相同任务
        //如果是，则取消上一个任务
        [self cancelLastSameTaskWithNewDataPointsArray:dataPointsArray];
        
        @weakify(self)
        [[XLServiceManager shareInstance].deviceService setDeviceDataPoints:self.deviceModel.device dataPoints:dataPointsArray timeout:TimeOutSetDataPoint handler:^(id result, NSError *error) {
            @strongify(self)
            if (error == nil) {
                [self.deviceModel addDataPoints:dataPointsArray];
            }else{
                KShowErrorLoadingWithMsg(@"设置失败");
            }
        }];
    }
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
/**
 生成一个带有用户ID端点的数组
 */
- (NSMutableArray <XLinkDataPoint*>*)addUserIdDataPointWithDataPointsArray:(NSArray <XLinkDataPoint*>*)dataPoints
{
    self.deviceModel.user_id = XL_DATA_SOURCE.user.userId.stringValue;
    XLinkDataPoint *dataPointUserID = [XLinkDataPoint dataPointWithType:XLinkDataTypeString withIndex:40 withValue:self.deviceModel.user_id];
    
    NSMutableArray *mDataPoints = [NSMutableArray array];
    [mDataPoints addObjectsFromArray:dataPoints];
    [mDataPoints addObject:dataPointUserID];
    
    return [mDataPoints mutableCopy];
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
