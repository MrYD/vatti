//
//  BaseDishwasherDeviceViewController.h
//  vatti
//
//  Created by panyaping on 2018/9/19.
//  Copyright © 2018年 xlink. All rights reserved.
//

#import "BaseDeviceViewController.h"
#import "DishWasherDeviceModel.h"

@interface BaseDishwasherDeviceViewController : BaseDeviceViewController <DishWasherDeviceModel *>

/**
 获取当前设备是否可控
 */
- (BOOL)getIsDeviceCanControlWithIsNeedAlert:(BOOL)isNeedAlert;

/**
 注销当前用户控制权限
 */
- (void)cancelDevicepriority;

/**
 生成一个带有用户ID端点的数组
 */
- (NSMutableArray <XLinkDataPoint*>*)addUserIdDataPointWithDataPointsArray:(NSArray <XLinkDataPoint*>*)dataPoints;

@end
