//
//  DishWashCustomController.m
//  vatti
//
//  Created by MrYD on 2018/12/24.
//  Copyright © 2018年 xlink. All rights reserved.
//

#import "DishWashCustomController.h"
#import "DishWasherConstants.h"
#import "StrongWashViewController.h"
#import "DishWasherPlanView.h"
#import "DishWasherPlanButton.h"
#import "DishWasherTools.h"

@interface DishWashCustomController () <UIPickerViewDataSource, UIPickerViewDelegate>

@property (nonatomic, strong) UILabel *explainLabel;

@property (nonatomic, assign) NSInteger selectedValue;

@property (nonatomic, strong) DishWasherPlanButton *nPlanBtn;

// 预约选择器
@property (strong, nonatomic) DishWasherPlanView *dishWasherPlanView;
// 预约时间
@property (assign, nonatomic) NSInteger timeType;
@property (strong, nonatomic) NSDate    *selectedDate;

@property (nonatomic, copy) ControllerBackBlock controllerBackBlock;


@end

@implementation DishWashCustomController

- (void)viewDidLoad {

    [super viewDidLoad];
    
    self.view.backgroundColor = [UIColor whiteColor];
    
//    a & 1 == 1 true // guan
//    a & 1 == 1 false // kai
    
}

- (void)initSubViews{
    [super initSubViews];
    
    CGRect frame = self.view.frame;
    frame.size.height -= UIAdjustNum(132.f);
    self.view.frame = frame;
    
    //naiv
    [self setupNavi];
    
    [self setSubViewsUI];

}

- (void)setupNavi
{
    self.navigationItem.title = XLLocalizeString(@"自定义");
    
    UIBarButtonItem *cancleItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"btn_left1"] style:(UIBarButtonItemStyleDone) target:self action:@selector(leftBarBtnClicked:)];
    cancleItem.tintColor = [UIColor darkGrayColor];
    self.navigationItem.leftBarButtonItem = cancleItem;
}

- (void)buildData
{
    [super buildData];
    
//    [self addRACObserve];
}

- (void)addRACObserve
{
    @weakify(self)
    
#pragma mark 监听全局
    //设备上下线
    [[RACObserve(self.deviceModel, connected).distinctUntilChanged deliverOnMainThread] subscribeNext:^(id  _Nullable x) {
        @strongify(self)
        NSLog(@"DishWasherController监听-----------设备上下线:   %@", (self.deviceModel.connected) ? @"在线":@"离线");
        if (!self.deviceModel.connected) {
            [self.navigationController popViewControllerAnimated:YES];
            BOOL isCustomPlaned = [DishWasherTools isCustomPlanedWithDevice:self.deviceModel];
            if (isCustomPlaned) {
                if (_controllerBackBlock) {
                    _controllerBackBlock();
                }
            }
        }
    }];
    
    //设备状态
    [[RACObserve(self.deviceModel, dev_statu).distinctUntilChanged deliverOnMainThread] subscribeNext:^(id  _Nullable x) {
        @strongify(self)
        NSLog(@"DishWasherController监听-----------设备状态:   %d", self.deviceModel.dev_statu);
        if (self.deviceModel.dev_statu < 1) {
            [self.navigationController popViewControllerAnimated:YES];
            BOOL isCustomPlaned = ([DishWasherTools isCustomPlanedWithDevice:self.deviceModel]);
            if (isCustomPlaned) {
                if (_controllerBackBlock) {
                    _controllerBackBlock();
                }
            }
        }
        else if((self.deviceModel.dev_statu == 3||self.deviceModel.dev_statu == 4)//是洗涤运行或暂停状态
                && !([self.navigationController.visibleViewController isKindOfClass:[StrongWashViewController class]])//未跳转洗涤页
                && !([DishWasherTools isOpenPlanedWithActSwitch:self.deviceModel.act_switch]))//不是预约模式
        {
            StrongWashViewController *strongWashVCtrl  = [[StrongWashViewController alloc] init];
            strongWashVCtrl.deviceModel = self.deviceModel;
            [self.navigationController pushViewController:strongWashVCtrl animated:YES];
        }
    }];
    
#pragma mark  监听面板其他
    // 预约
    [[RACObserve(self.deviceModel, act_switch).distinctUntilChanged deliverOnMainThread] subscribeNext:^(id  _Nullable x) {
        @strongify(self)
        BOOL isOpenPlaned = [DishWasherTools isOpenPlanedWithActSwitch:self.deviceModel.act_switch];
        self.nPlanBtn.plantActSwitch = isOpenPlaned;
        if (isOpenPlaned && ![self.navigationController.visibleViewController isKindOfClass:[StrongWashViewController class]]) {
            StrongWashViewController *strongWashVCtrl  = [[StrongWashViewController alloc] init];
            strongWashVCtrl.deviceModel = self.deviceModel;
            [self.navigationController pushViewController:strongWashVCtrl animated:YES];
        }
    }];
    // 预约时间
    [[RACObserve(self.deviceModel, order_time).distinctUntilChanged deliverOnMainThread] subscribeNext:^(id  _Nullable x) {
        @strongify(self)
        self.nPlanBtn.orderTime = self.deviceModel.order_time;
    }];
    // 洗涤模式
    [[RACObserve(self.deviceModel, switch_func).distinctUntilChanged deliverOnMainThread] subscribeNext:^(id  _Nullable x) {
        @strongify(self)
        self.nPlanBtn.switchFunc = self.deviceModel.switch_func;
    }];
}

- (void)setControllerBackBlock:(ControllerBackBlock)block
{
        _controllerBackBlock = block;
}
- (void)setDataPoints:(NSArray *)array
{
    [self setDataPoints:array handler:nil];
}
- (void)setDataPoints:(NSArray *)array handler:(void (^)(BOOL))handler{
    
    if ([self getIsDeviceCanControlWithIsNeedAlert:YES] == NO) {
        return;
    }
    
    [self.deviceModel setDataPoints:array handler:handler];
}

#pragma mark - SubViewsUI

- (void)setSubViewsUI
{
    // 选择洗涤时长
    UILabel *titleLabel = [[UILabel alloc] init];
    titleLabel.text = @"选择洗涤时长";
    titleLabel.font = UIFontAdjust(36.f);
    titleLabel.textAlignment = NSTextAlignmentCenter;
    [self.view addSubview:titleLabel];
    [titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(UIAdjustNum(95.f));
        make.width.mas_equalTo(self.view.mas_width);
    }];
    
    // 时间选择器
    UIPickerView *timePicker = [[UIPickerView alloc] init];
    timePicker.delegate = self;
    timePicker.dataSource = self;
    timePicker.showsSelectionIndicator = YES;
    [self.view addSubview:timePicker];
    [timePicker mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(titleLabel.mas_bottom);
        make.width.equalTo(self.view.mas_width).multipliedBy(0.75);
        make.height.equalTo(self.view.mas_height).multipliedBy(0.5);
        make.centerX.equalTo(self.view.mas_centerX);
    }];
    
    self.explainLabel = [[UILabel alloc] init];
    self.explainLabel.text = @"";
    self.explainLabel.font = UIFontAdjust(30.f);
    self.explainLabel.textColor = [UIColor colorWithHexString:@"818286"];
    self.explainLabel.textAlignment = NSTextAlignmentCenter;
    [self.view addSubview:self.explainLabel];
    [self.explainLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(timePicker.mas_bottom).mas_offset(UIAdjustNum(40.f));
        make.width.mas_equalTo(self.view.mas_width);
    }];
    self.nPlanBtn = [DishWasherPlanButton buttonWithType:UIButtonTypeCustom];
    self.nPlanBtn.isCustom = YES;
    [self.nPlanBtn addTarget:self action:@selector(planBtnClick:) forControlEvents:UIControlEventTouchUpInside];
    
    [self.view addSubview:self.nPlanBtn];
    [self.nPlanBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.height.mas_equalTo(UIAdjustNum(90.f));
        make.bottom.equalTo(self.view.mas_bottom).mas_offset(UIAdjustNum(-95.f));
    }];

    
    UIButton *startBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [startBtn setTitle:@"开始" forState:UIControlStateNormal];
    [startBtn.titleLabel setFont:UIFontAdjust(36.f)];
    [startBtn setBackgroundImage:[UIImage imageNamed:@"ic_shortbtn_active"] forState:UIControlStateNormal];
    [startBtn addTarget:self action:@selector(startBtnClick) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:startBtn];
    [startBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.height.mas_equalTo(UIAdjustNum(90.f));
        make.bottom.equalTo(self.view.mas_bottom).mas_offset(UIAdjustNum(-95.f));
    }];
    
    [@[self.nPlanBtn, startBtn] mas_distributeViewsAlongAxis:MASAxisTypeHorizontal withFixedSpacing:UIAdjustNum(34.f) leadSpacing:UIAdjustNum(24.f) tailSpacing:UIAdjustNum(24.f)];
    
    __weak typeof(self) weakSelf = self;
    self.dishWasherPlanView = [[DishWasherPlanView alloc] initPlanViewWithDeviceModel:self.deviceModel WithEditBlack:^(NSUInteger timeType, NSDate *selectedDate) {
        if ([weakSelf getIsDeviceCanControlWithIsNeedAlert:YES] == NO) {
            return;
        }
        BOOL isAppointmentOpen = [DishWasherTools isOpenPlanedWithActSwitch:weakSelf.deviceModel.act_switch];
        
        if (isAppointmentOpen == YES){
            [weakSelf alertHintWithMessage:@"您已开启预约，请先取消预约再进行设置。"];
            return;
        }
        weakSelf.timeType = timeType;
        weakSelf.selectedDate = selectedDate;
        weakSelf.nPlanBtn.plantActSwitch = YES;
        weakSelf.nPlanBtn.timeType = timeType;
        weakSelf.nPlanBtn.switchFunc = weakSelf.selectedValue + 9;
        weakSelf.nPlanBtn.selectedDate = selectedDate;
        
        NSUInteger valueTime = [weakSelf timeToPointValue];
        XLinkDataPoint *dataPoint_switchFunc = [XLinkDataPoint dataPointWithType:XLinkDataTypeByte withIndex:DishWasherDataPoint_switch_func withValue:@(weakSelf.selectedValue + 9)];
        XLinkDataPoint *dataPoint_funcMode = [XLinkDataPoint dataPointWithType:XLinkDataTypeByte withIndex:DishWasherDataPoint_func_mode withValue:@(0)];
        uint8_t valuePlan = [XLDataPointTool getDataPointValueWithValue:weakSelf.deviceModel.act_switch andShift:DishWasherActSwitchPlanMode andIsOpen:YES];
        XLinkDataPoint *dataPoint_valuePlan = [XLinkDataPoint dataPointWithType:XLinkDataTypeByte withIndex:DishWasherDataPoint_act_switch withValue:@(valuePlan)];
        XLinkDataPoint *dataPoint_planTime = [XLinkDataPoint dataPointWithType:XLinkDataTypeUnsignedInt withIndex:DishWasherDataPoint_order_time withValue:@(valueTime)];
        XLinkDataPoint *dataPoint_devStatu = [XLinkDataPoint dataPointWithType:XLinkDataTypeByte withIndex:12 withValue:@3];
        [weakSelf setDataPoints:@[dataPoint_devStatu,dataPoint_switchFunc, dataPoint_valuePlan, dataPoint_planTime, dataPoint_funcMode] handler:^(BOOL success) {
            if (!success) {
                [weakSelf showHUDErrorWithMessage:@"设置失败"];
                weakSelf.selectedDate = nil;
                weakSelf.nPlanBtn.plantActSwitch = [XLDataPointTool isOpenWithValue:weakSelf.deviceModel.act_switch andShift:4];;
                weakSelf.nPlanBtn.switchFunc = weakSelf.deviceModel.switch_func;
                weakSelf.nPlanBtn.orderTime = weakSelf.deviceModel.order_time;
                weakSelf.timeType = weakSelf.nPlanBtn.timeType;
            }
        }];
    } withCancleBlock:^{
        if ([weakSelf getIsDeviceCanControlWithIsNeedAlert:YES] == NO) {
            return;
        }
        weakSelf.selectedDate = nil;
        weakSelf.nPlanBtn.plantActSwitch = NO;
        uint8_t valuePlan = [XLDataPointTool getDataPointValueWithValue:weakSelf.deviceModel.act_switch andShift:DishWasherActSwitchPlanMode andIsOpen:NO];
        XLinkDataPoint *dataPoint_valuePlan = [XLinkDataPoint dataPointWithType:XLinkDataTypeByte withIndex:DishWasherDataPoint_act_switch withValue:@(valuePlan)];
        uint32_t valueTime = 0;
        XLinkDataPoint *dataPoint_planTime = [XLinkDataPoint dataPointWithType:XLinkDataTypeUnsignedInt withIndex:DishWasherDataPoint_order_time withValue:@(valueTime)];
        XLinkDataPoint *dataPoint_devStatu = [XLinkDataPoint dataPointWithType:XLinkDataTypeByte withIndex:12 withValue:@2];
        [weakSelf setDataPoints:@[dataPoint_devStatu,dataPoint_valuePlan, dataPoint_planTime] handler:^(BOOL success) {
            if (!success) {
                KShowErrorLoadingWithMsg(@"设置失败");
                weakSelf.nPlanBtn.plantActSwitch = [XLDataPointTool isOpenWithValue:weakSelf.deviceModel.act_switch andShift:4];;
                weakSelf.nPlanBtn.timeType = weakSelf.timeType;
                weakSelf.nPlanBtn.switchFunc = weakSelf.deviceModel.switch_func;
                weakSelf.nPlanBtn.orderTime = weakSelf.deviceModel.order_time;
            }
        }];
    }];
    
    [self.view addSubview:self.dishWasherPlanView];
    [self.dishWasherPlanView loadDishWasherPlanView];
    [self.dishWasherPlanView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(0.f);
        make.width.mas_equalTo(self.view.mas_width);
        make.height.mas_equalTo(self.view.mas_height);
    }];

    
    NSInteger timeNum = 0;
#if ENV == 1
#elif ENV == 2
    
#endif
    //已设定自定义预约
    BOOL isAppointmentOpen = [DishWasherTools isOpenPlanedWithActSwitch:self.deviceModel.act_switch];
    BOOL isCustomFunc = [DishWasherTools isCustomFuncWithSwitchFunc:self.deviceModel.switch_func];
    BOOL canControl = [self getIsDeviceCanControlWithIsNeedAlert:NO];
    self.nPlanBtn.plantActSwitch = isAppointmentOpen;
    self.nPlanBtn.switchFunc = self.deviceModel.switch_func;
    self.nPlanBtn.orderTime = self.deviceModel.order_time;

    if (isCustomFunc) {

        timeNum = self.deviceModel.switch_func - 9;
        self.explainLabel.text = [self getExplainArr][timeNum];
        [timePicker selectRow: timeNum inComponent:1 animated:NO];
        _selectedValue = timeNum;
    }else if (!isCustomFunc&&!isAppointmentOpen&&canControl&&self.deviceModel.dev_statu!=DishWasherDeviceStatuePlanPlan){
        self.explainLabel.text = [self getExplainArr][timeNum];
        [timePicker selectRow:timeNum inComponent:1 animated:NO];
        XLinkDataPoint *dataPoint_switchFunc = [XLinkDataPoint dataPointWithType:XLinkDataTypeByte withIndex:DishWasherDataPoint_switch_func withValue:@(_selectedValue + 9)];
        uint8_t valuePlan = [XLDataPointTool getDataPointValueWithValue:self.deviceModel.act_switch andShift:DishWasherActSwitchPlanMode andIsOpen:NO];
        XLinkDataPoint *dataPoint_valuePlan = [XLinkDataPoint dataPointWithType:XLinkDataTypeByte withIndex:DishWasherDataPoint_act_switch withValue:@(valuePlan)];
        uint32_t valueTime = 0;
        XLinkDataPoint *dataPoint_planTime = [XLinkDataPoint dataPointWithType:XLinkDataTypeUnsignedInt withIndex:DishWasherDataPoint_order_time withValue:@(valueTime)];
//        [self setDataPoints:@[dataPoint_valuePlan, dataPoint_planTime, dataPoint_switchFunc]];

    }else {
        self.explainLabel.text = [self getExplainArr][timeNum];
    }
    
}


#pragma mark - UIPickerViewDataSource, UIPickerViewDelegate
-(NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView{
    return 3;
}

-(NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component{
    NSInteger row = 0;
    switch (component) {
        case 0:
            break;
        case 1:
            row = 11;
            break;
        case 2:
            row = 1;
            break;
        default:
            break;
    }
    return row;
}

- (nullable NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    
    NSString *str = @"";
    switch (component) {
        case 0:
            break;
        case 1:
            str = [NSString stringWithFormat:@"%ld", row * 10 + 20];
            break;
        case 2:
            str = @"分钟";
            break;
        default:
            break;
    }
    return str;
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    
    if (component == 1) {
        if ([self getIsDeviceCanControlWithIsNeedAlert:YES] == NO) {
            [pickerView selectRow:_selectedValue inComponent:1 animated:YES];
            return;
        }
        BOOL isAppointmentOpen = [DishWasherTools isOpenPlanedWithActSwitch:self.deviceModel.act_switch];
        if (isAppointmentOpen == YES){
            [self alertHintWithMessage:@"您已开启预约，请先取消预约再进行设置。"];
            [pickerView selectRow:_selectedValue inComponent:1 animated:YES];
            return;
        }
        self.explainLabel.text = [self getExplainArr][row];
        _selectedValue = row;
//        XLinkDataPoint *dataPoint_switchFunc = [XLinkDataPoint dataPointWithType:XLinkDataTypeByte withIndex:DishWasherDataPoint_switch_func withValue:@(_selectedValue + 9)];
//        [self setDataPoints:@[dataPoint_switchFunc]];
        
        
    }
}

#pragma mark - Action
- (void)planBtnClick:(UIButton *)btn
{
    if (self.nPlanBtn.plantActSwitch) {
        [self.dishWasherPlanView showEditPlanViewWithTimeType:_timeType selectedDate:self.selectedDate];
    }
    else
    {
        [self.dishWasherPlanView showPlanView];
    }
}

- (void)startBtnClick
{
    
//    if (component == 1) {
//        if ([self getIsDeviceCanControlWithIsNeedAlert:YES] == NO) {
//            [pickerView selectRow:_selectedValue inComponent:1 animated:YES];
//            return;
//        }
//        BOOL isAppointmentOpen = [DishWasherTools isOpenPlanedWithActSwitch:self.deviceModel.act_switch];
//        if (isAppointmentOpen == YES){
//            [self alertHintWithMessage:@"您已开启预约，请先取消预约再进行设置。"];
//            [pickerView selectRow:_selectedValue inComponent:1 animated:YES];
//            return;
//        }
    if (((self.deviceModel.running_flag & 1) == 0) && (self.deviceModel.dev_statu != 3)) {
        
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"提示" message:@"请关好门!" preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction *action1 = [UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            return ;
        }];
        
        [alert addAction:action1];
        [[UIApplication sharedApplication].keyWindow.rootViewController presentViewController:alert animated:YES completion:nil];
        return ;
        
    }else if ((self.deviceModel.running_flag & 1) == 1){
        self.explainLabel.text = [self getExplainArr][_selectedValue];
        XLinkDataPoint *dataPoint_switchFunc1 = [XLinkDataPoint dataPointWithType:XLinkDataTypeByte withIndex:DishWasherDataPoint_switch_func withValue:@(_selectedValue + 9)];
        [self setDataPoints:@[dataPoint_switchFunc1]];
    }
    
//        _selectedValue = row;
    
        
        
//    }
    
    if ([self getIsDeviceCanControlWithIsNeedAlert:YES] == NO) {
        return;
    }
    if (self.deviceModel.dev_statu == 3) {
        
        StrongWashViewController *strongWashViewController = [[StrongWashViewController alloc] init];
        strongWashViewController.deviceModel = self.deviceModel;
        [self.navigationController pushViewController:strongWashViewController animated:YES];
    }else if (self.deviceModel.dev_statu == 2) {
        BOOL isAppointmentOpen = [DishWasherTools isOpenPlanedWithActSwitch:self.deviceModel.act_switch];
        if (isAppointmentOpen == YES){
            //        [self alertHintWithMessage:@"您已开启预约，请先取消预约再进行设置。"];
            StrongWashViewController *strongWashVCtrl  = [[StrongWashViewController alloc] init];
            strongWashVCtrl.deviceModel = self.deviceModel;
            [self.navigationController pushViewController:strongWashVCtrl animated:YES];
            return;
        }

    }
    // 虚拟体验
    if (XL_DATA_VIRTUAL.isLogin == NO) {
        self.deviceModel.dev_statu = 3;
        return;
    }
    uint8_t actSwitchValue = [XLDataPointTool getDataPointValueWithValue:self.deviceModel.act_switch andShift:DishWasherActSwitchPlanMode andIsOpen:NO];
    XLinkDataPoint *data_actSwitch = [XLinkDataPoint dataPointWithType:XLinkDataTypeByte withIndex:DishWasherDataPoint_act_switch withValue:@(actSwitchValue)];
    
    XLinkDataPoint *data_orderTime = [XLinkDataPoint dataPointWithType:XLinkDataTypeUnsignedInt withIndex:DishWasherDataPoint_order_time withValue:@(0)];
    
    
    XLinkDataPoint *dataPoint_switchFunc = [XLinkDataPoint dataPointWithType:XLinkDataTypeByte withIndex:DishWasherDataPoint_switch_func withValue:@(_selectedValue+9)];
    
    XLinkDataPoint *dataPoint_funcMode = [XLinkDataPoint dataPointWithType:XLinkDataTypeByte withIndex:DishWasherDataPoint_func_mode withValue:@(0)];
    
    uint8_t controlValue = [XLDataPointTool getDataPointValueWithValue:0 andShift:DishWasherControlCmdBitRun andIsOpen:YES];
    XLinkDataPoint *dataPoint_control = [XLinkDataPoint dataPointWithType:XLinkDataTypeByte withIndex:DishWasherDataPoint_control_cmd withValue:@(controlValue)];
    
    [self setDataPoints:@[data_actSwitch, data_orderTime, dataPoint_switchFunc, dataPoint_funcMode, dataPoint_control]];
}

- (void)leftBarBtnClicked:(UIButton *)button
{
    [self.navigationController popViewControllerAnimated:YES];
    BOOL isCustomPlaned = [DishWasherTools isCustomPlanedWithDevice:self.deviceModel];
    if (isCustomPlaned) {
        if (_controllerBackBlock) {
            _controllerBackBlock();
        }
    }
}



- (NSArray *)getExplainArr
{
    return @[@"适用于无干结、小面积粘稠污渍的餐具",
             @"适用于饭后清洁，清淡无油的餐具",
             @"适用于饭后清洁，清淡少油的餐具",
             @"适用于无干结、轻度脏污的餐具",
             @"适用于无干结、小面积粘稠污渍的餐具",
             @"适用于无干结、大面积粘稠污渍的餐具",
             @"适用于中度脏污少油的餐具",
             @"适用于中度脏污多油的餐具",
             @"适用于重度脏污少油的餐具",
             @"适用于重度脏污多油的餐具",
             @"适用于重度脏污多油的餐具"];
}


#pragma mark - DateTime
/**
 转换预约时间值
 */
- (uint32_t)timeToPointValue
{
    NSInteger minute = [DishWasherTools getTimeInterValueWithSwitchFunc:self.deviceModel.switch_func];
    NSInteger selectedTime = [self.selectedDate timeIntervalSinceDate:[NSDate date]];;
    NSInteger valueTime = selectedTime - ((_timeType==0)?(minute * 60):0);
    NSUInteger valueHour = valueTime / 3600;
    NSUInteger ValueSec = valueTime % 60;
    NSUInteger valueMin = (valueTime - valueHour * 3600) / 60 + ((ValueSec > 0) ? 1 : 0);
    
    NSUInteger returnValue = ((_timeType & 0xff) << 31) | ((valueHour & 0xff) << 8) | (valueMin & 0xff);
    return returnValue;
}

// 获取当天0点时间

@end
