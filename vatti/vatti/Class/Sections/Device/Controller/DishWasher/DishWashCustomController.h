//
//  DishWashCustomController.h
//  vatti
//
//  Created by MrYD on 2018/12/24.
//  Copyright © 2018年 xlink. All rights reserved.
//

#import "BaseDeviceViewController.h"
#import "BaseDishwasherDeviceViewController.h"

typedef void (^ControllerBackBlock)(void);

@interface DishWashCustomController : BaseDishwasherDeviceViewController

- (void)setControllerBackBlock:(ControllerBackBlock)block;

@end
