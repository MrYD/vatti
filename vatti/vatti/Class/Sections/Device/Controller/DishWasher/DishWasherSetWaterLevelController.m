//
//  DishWasherSetWaterLevelController.m
//  vatti
//
//  Created by dongxiao on 2018/8/14.
//  Copyright © 2018年 xlink. All rights reserved.
//

#import "DishWasherSetWaterLevelController.h"
#import "DishWasherConstants.h"
#import "UIColor+XLCategory.h"

@interface DishWasherSetWaterLevelController () <UITableViewDelegate, UITableViewDataSource>

@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, assign) NSUInteger waterGears;

@end

@implementation DishWasherSetWaterLevelController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.view.backgroundColor = [UIColor colorWithHexString:@"EFEEE8"];
    self.waterGears = self.deviceModel.water_gears;
}

- (void)initConfigure
{
    [super initConfigure];
}

- (void)initSubViews{
    [super initSubViews];
    
    //navi
    [self setupNavi];
    
    self.tableView = [[UITableView alloc] init];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.bounces = NO;
    [self.view addSubview:self.tableView];
    [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(UIAdjustNum(84));
        make.width.mas_equalTo(self.view.mas_width);
        make.height.mas_equalTo(UIAdjustNum(109) * 6);
    }];
    
#if ENV == 1
    
#elif ENV == 2

#endif
}

- (void)buildData
{
    [super buildData];
     [self addRACObserve];
}

- (void)addRACObserve
{
    @weakify(self)
//#pragma mark 监听全局
//    //设备上下线
//    [[RACObserve(self.dishWasherModel, connected).distinctUntilChanged deliverOnMainThread] subscribeNext:^(id  _Nullable x) {
//        @strongify(self)
//
//    }];
//
//    //设备状态
//    [[RACObserve(self.dishWasherModel, dev_statu).distinctUntilChanged deliverOnMainThread] subscribeNext:^(id  _Nullable x) {
//        @strongify(self)
//
//
//    }];
    
    #pragma mark 监听点击
        //点击
    [[RACObserve(self.deviceModel, water_gears).distinctUntilChanged deliverOnMainThread] subscribeNext:^(id  _Nullable x) {
        @strongify(self)
        
        if (!self.isResponseChange) {
            return ;
        }
        self.waterGears = self.deviceModel.water_gears;
        [self.tableView reloadData];
        NSLog(@"已设置软水档位更新%d",self.deviceModel.water_gears);
    }];
}

#pragma mark - UI

- (void)setupNavi
{
    [super setupNavi];
    self.navigationItem.title = @"软水档位";
}


#pragma mark - UITableViewDelegate, UITableViewDataSource

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if([indexPath row] == ((NSIndexPath*)[[tableView indexPathsForVisibleRows] lastObject]).row){
//            [self addRACObserve];
        return;
    }
    
    if ([cell respondsToSelector:@selector(setSeparatorInset:)])
    {
        [cell setSeparatorInset:UIEdgeInsetsMake(0, 15, 0, 15)];
    }
    if ([cell respondsToSelector:@selector(setLayoutMargins:)])
    {
        [cell setLayoutMargins:UIEdgeInsetsMake(0, 15, 0, 15)];
    }
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 6;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return UIAdjustNum(109.f);
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"DishWasherSetWaterLevelCell"];
    if(!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"DishWasherSetWaterLevelCell"];
        [cell.textLabel setFont:UIFontAdjust(28.f)];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        UIImageView *imageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"ic_lastwash_unactive"]];
        cell.accessoryView = imageView;
        cell.textLabel.text = [NSString stringWithFormat:@"%ld档", indexPath.row + 1];
    }
    UIImageView *imageView = (UIImageView *)cell.accessoryView;

#if ENV == 1
    if (self.waterGears == 0) {
        if (indexPath.row == 0) {
            [imageView setImage:[UIImage imageNamed:@"ic_cource_active"]];
        }else {
            [imageView setImage:[UIImage imageNamed:@"ic_lastwash_unactive"]];
        }
    }
    else{
        if (self.waterGears == indexPath.row+1) {
            [imageView setImage:[UIImage imageNamed:@"ic_cource_active"]];
        }else {
            [imageView setImage:[UIImage imageNamed:@"ic_lastwash_unactive"]];
        }
        
    }
#elif ENV == 2
    if (indexPath.row == (self.waterGears - 1)) {
        [imageView setImage:[UIImage imageNamed:@"ic_cource_active"]];
    }else {
        [imageView setImage:[UIImage imageNamed:@"ic_lastwash_unactive"]];
    }
#endif

    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
    self.waterGears = indexPath.row + 1;
    [tableView reloadData];
    NSLog(@"已设置软水档位%zd",self.waterGears);
    __weak __typeof(self) weakself = self;
    XLinkDataPoint *dataPoint = [XLinkDataPoint dataPointWithType:XLinkDataTypeByte withIndex:9 withValue:@(indexPath.row + 1)];
    [self setDataPoints:@[dataPoint] withSeccussBlock:nil withFailBlock:^{
        weakself.waterGears = weakself.deviceModel.water_gears;
        [weakself.tableView reloadData];
        NSLog(@"已设置软水档位失败%d",weakself.deviceModel.water_gears);
    }];
}



#pragma mark - Request

/**
 设置数据端点数据
 */
- (void)setDataPoints:(NSArray <XLinkDataPoint*> *)dataPointsArray withSeccussBlock:(void (^)(void))successBlock withFailBlock:(void (^)(void))failBlock{
    
    if ([self getIsDeviceCanControlWithIsNeedAlert:YES] == NO) {
        return;
    }
    
    @weakify(self)
    
    //1.处理发送端点前的逻辑
    [self startLogicalBefortSet];
    
    //2.设置端点值到数据模型
    //    [self.deviceModel addDataPoints:dataPointsArray];
    
    //3.更新UI
    //    [self updateTempWithTempValue:self.deviceModel.heater_temp andIsNeedSetToDevice:YES];
    //    [self updateFunctionViewWithFunctionFlag:self.deviceModel.function_flag];
    
    //4.判断游客模式
    if (XL_DATA_VIRTUAL.isLogin == NO){
        return;
    }
    
    //获取当前登录用户ID，加入数据端点
    NSMutableArray *mDataPoints = [self addUserIdDataPointWithDataPointsArray:dataPointsArray];
    
    //判断是否设置相同任务
    //如果是，则取消上一个任务
    [self cancelLastSameTaskWithNewDataPointsArray:dataPointsArray];
    
    self.setDataPointTask = [[XLServiceManager shareInstance].deviceService setDeviceDataPoints:self.deviceModel.device dataPoints:dataPointsArray timeout:TimeOutSetDataPoint handler:^(id result, NSError *error) {
        
        @strongify(self)
        
        if (error == nil) {
            [self.deviceModel addDataPoints:dataPointsArray];
            if (successBlock) {
                successBlock();
            }
        }else{
            KShowErrorLoadingWithMsg(@"设置失败");
            NSLog(@"设置失败, %@", error.userInfo);
            if (failBlock) {
                failBlock();
            }
        }
    }];
}

- (void)timerFinished:(NSTimer*)timer
{
    [super timerFinished:timer];
    self.isResponseChange = YES;
    self.waterGears = self.deviceModel.water_gears;
    [self.tableView reloadData];
    NSLog(@"已设置软水档位成功%d",self.deviceModel.water_gears);
}

@end
