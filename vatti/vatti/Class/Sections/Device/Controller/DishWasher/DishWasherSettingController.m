//
//  DishWasherSettingController.m
//  vatti
//
//  Created by dongxiao on 2018/8/13.
//  Copyright © 2018年 xlink. All rights reserved.
//

#import "DishWasherSettingController.h"
#import "DishWasherConstants.h"
#import "UIColor+XLCategory.h"
#import "DishWasherSetWaterLevelController.h"
#import "DishWasherSetAutoOpenController.h"
#import "DishWasherSetShutController.h"
#import "DishWasherSetLightController.h"
#import "ShareDeviceViewController.h"


@interface DishWasherSettingController () <UITableViewDelegate, UITableViewDataSource>
@property (nonatomic, strong) UISwitch *switchAutoOpenBtn;
@property (nonatomic, strong) UISwitch *switchSutdownBtn;
@property (nonatomic, strong) UISwitch *switchLightBtn;
@property (nonatomic, strong) UISwitch *switchEndBtn;

@end

@implementation DishWasherSettingController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.view.backgroundColor = [UIColor colorWithHexString:@"EFEEE8"];
}

- (void)initConfigure
{
    [super initConfigure];
}

- (void)initSubViews{
    [super initSubViews];
    
    //navi
    [self setupNavi];
    
    UITableView *tableView = [[UITableView alloc] init];
    tableView.delegate = self;
    tableView.dataSource = self;
    tableView.bounces = NO;
    [self.view addSubview:tableView];
    [tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(UIAdjustNum(84));
        make.width.mas_equalTo(self.view.mas_width);
        make.height.mas_equalTo(UIAdjustNum(109) * 6);
    }];
    
}

- (void)buildData
{
    [super buildData];
}

- (void)viewWillLayoutSubviews
{
    [super viewWillLayoutSubviews];
}
#pragma mark 监听全局

- (void)addRACObserve
{
    @weakify(self)
    [[RACObserve(self.deviceModel, act_switch).distinctUntilChanged deliverOnMainThread] subscribeNext:^(id  _Nullable x) {
        
        @strongify(self)
        NSLog(@"DishWasherSettingController监听-----------设备设置:   %x", self.deviceModel.act_switch);
        [self setupSwitch];
    }];
}
#pragma mark - UI

- (void)setupNavi
{
    [super setupNavi];
    self.navigationItem.title = @"设置";
}

#pragma mark - UITableViewDelegate, UITableViewDataSource

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row == 6) {
        return;
    }
    if ([cell respondsToSelector:@selector(setSeparatorInset:)])
    {
        [cell setSeparatorInset:UIEdgeInsetsMake(0, 15, 0, 15)];
    }
    if ([cell respondsToSelector:@selector(setLayoutMargins:)])
    {
        [cell setLayoutMargins:UIEdgeInsetsMake(0, 15, 0, 15)];
    }
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    // 虚拟体验
    if (XL_DATA_VIRTUAL.isLogin == NO) {
        return 5;
    }
    return 6;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return UIAdjustNum(109.f);
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"DishWasherSettingTableViewCell"];
    if(!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"DishWasherSettingTableViewCell"];
        [cell.textLabel setFont:UIFontAdjust(28.f)];
        cell.accessoryType=UITableViewCellAccessoryDisclosureIndicator;
    }
    cell.accessoryView = nil;
    cell.accessoryType=UITableViewCellAccessoryNone;
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    switch (indexPath.row) {
        case 1:
            cell.accessoryView = self.switchAutoOpenBtn;
            break;
        case 2:
            cell.accessoryView = self.switchSutdownBtn;
            break;
        case 3:
            cell.accessoryView = self.switchLightBtn;
            break;
        case 4:
            cell.accessoryView = self.switchEndBtn;
            break;
            
        default:
            cell.accessoryType=UITableViewCellAccessoryDisclosureIndicator;
            cell.selectionStyle = UITableViewCellSelectionStyleDefault;
            break;
    }
    cell.textLabel.text = [self getCellTextArr][indexPath.row];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:NO];

    switch (indexPath.row) {
        case 0:
        {
            DishWasherSetWaterLevelController *dishWasherSetWaterLevelController = [[DishWasherSetWaterLevelController alloc] init];
            dishWasherSetWaterLevelController.deviceModel = self.deviceModel;
            [self.navigationController pushViewController:dishWasherSetWaterLevelController  animated:YES];
            break;
        }
        case 5:
        {
            ShareDeviceViewController *shareDeviceViewController = [[ShareDeviceViewController alloc] init];
            shareDeviceViewController.deviceModel = self.deviceModel;
            [self.navigationController pushViewController:shareDeviceViewController animated:YES];
            break;
        }
        default:
            break;
    }

}

#pragma mark - Action
- (NSArray *)getCellTextArr
{
    return @[@"软水档位", @"自动开门", @"自动关机", @"氛围灯", @"结束提示音", @"设备分享"];
}
#pragma mark - Switch
-(UISwitch*)switchAutoOpenBtn
{
    if (!_switchAutoOpenBtn) {
        _switchAutoOpenBtn = [[UISwitch alloc]init];
        [_switchAutoOpenBtn addTarget:self action:@selector(switchBtnChanged:) forControlEvents:UIControlEventValueChanged];
        [_switchAutoOpenBtn setOn:[XLDataPointTool isOpenWithValue:self.deviceModel.act_switch andShift:0]];
        _switchAutoOpenBtn.tag = 0;
    }
    return _switchAutoOpenBtn;
}
-(UISwitch*)switchSutdownBtn
{
    if (!_switchSutdownBtn) {
        _switchSutdownBtn = [[UISwitch alloc]init];
        [_switchSutdownBtn addTarget:self action:@selector(switchBtnChanged:) forControlEvents:UIControlEventValueChanged];
        _switchSutdownBtn.tag = 1;
        [_switchSutdownBtn setOn:[XLDataPointTool isOpenWithValue:self.deviceModel.act_switch andShift:1]];
    }
    return _switchSutdownBtn;
}
-(UISwitch*)switchLightBtn
{
    if (!_switchLightBtn) {
        _switchLightBtn = [[UISwitch alloc]init];
        [_switchLightBtn addTarget:self action:@selector(switchBtnChanged:) forControlEvents:UIControlEventValueChanged];
        [_switchLightBtn setOn:[XLDataPointTool isOpenWithValue:self.deviceModel.act_switch andShift:2]];
        _switchLightBtn.tag = 2;
    }
    return _switchLightBtn;
}
-(UISwitch*)switchEndBtn
{
    if (!_switchEndBtn) {
        _switchEndBtn = [[UISwitch alloc]init];
        [_switchEndBtn addTarget:self action:@selector(switchBtnChanged:) forControlEvents:UIControlEventValueChanged];
        [_switchEndBtn setOn:[XLDataPointTool isOpenWithValue:self.deviceModel.act_switch andShift:3]];
        _switchEndBtn.tag = 3;
    }
    return _switchEndBtn;
}
- (void)switchBtnChanged:(id)sender
{
    UISwitch *switchBtn = (UISwitch*)sender;
    if ([self getIsDeviceCanControlWithIsNeedAlert:NO] == NO) {
        [self alertHintWithMessage:@"无法操作，其他人正在操作设备" handler:^(UIAlertAction *action, NSUInteger tag) {
            [switchBtn setOn:[XLDataPointTool isOpenWithValue:self.deviceModel.act_switch andShift:switchBtn.tag]];
        }];
        return;
    }
    BOOL isOpen = [XLDataPointTool isOpenWithValue:self.deviceModel.act_switch andShift:switchBtn.tag];
    uint8_t autoOpenValue = [XLDataPointTool getDataPointValueWithValue:self.deviceModel.act_switch andShift:switchBtn.tag andIsOpen:!isOpen];
    XLinkDataPoint *dataPoint = [XLinkDataPoint dataPointWithType:XLinkDataTypeByte withIndex:10 withValue:@(autoOpenValue)];
    __weak __typeof(self) weakself = self;
    [self setDataPoints:@[dataPoint] withSeccussBlock:nil withFailBlock:^{
        [switchBtn setOn:[XLDataPointTool isOpenWithValue:weakself.deviceModel.act_switch andShift:switchBtn.tag]];
    }];
  
}
-(void)setupSwitch
{
    [self.switchAutoOpenBtn setOn:[XLDataPointTool isOpenWithValue:self.deviceModel.act_switch andShift:0]];
    [self.switchSutdownBtn setOn:[XLDataPointTool isOpenWithValue:self.deviceModel.act_switch andShift:1]];
    [self.switchLightBtn setOn:[XLDataPointTool isOpenWithValue:self.deviceModel.act_switch andShift:2]];
    [self.switchEndBtn setOn:[XLDataPointTool isOpenWithValue:self.deviceModel.act_switch andShift:3]];
}
#pragma mark - Request

/**
 设置数据端点数据
 */
- (void)setDataPoints:(NSArray <XLinkDataPoint*> *)dataPointsArray withSeccussBlock:(void (^)(void))successBlock withFailBlock:(void (^)(void))failBlock{
    
    if ([self getIsDeviceCanControlWithIsNeedAlert:YES] == NO) {
        return;
    }
    
    @weakify(self)
    
    //1.处理发送端点前的逻辑
    [self startLogicalBefortSet];
    
    //2.设置端点值到数据模型
    //    [self.deviceModel addDataPoints:dataPointsArray];
    
    //3.更新UI
    //    [self updateTempWithTempValue:self.deviceModel.heater_temp andIsNeedSetToDevice:YES];
    //    [self updateFunctionViewWithFunctionFlag:self.deviceModel.function_flag];
    
    //4.判断游客模式
    if (XL_DATA_VIRTUAL.isLogin == NO){
        return;
    }
    
    //获取当前登录用户ID，加入数据端点
    NSMutableArray *mDataPoints = [self addUserIdDataPointWithDataPointsArray:dataPointsArray];
    
    //判断是否设置相同任务
    //如果是，则取消上一个任务
    [self cancelLastSameTaskWithNewDataPointsArray:dataPointsArray];
    
    self.setDataPointTask = [[XLServiceManager shareInstance].deviceService setDeviceDataPoints:self.deviceModel.device dataPoints:dataPointsArray timeout:TimeOutSetDataPoint handler:^(id result, NSError *error) {
        
        @strongify(self)
        
        if (error == nil) {
            [self.deviceModel addDataPoints:dataPointsArray];
            if (successBlock) {
                successBlock();
            }
        }else{
            KShowErrorLoadingWithMsg(@"设置失败");
            NSLog(@"设置失败, %@", error.userInfo);
            if (failBlock) {
                failBlock();
            }
        }
    }];
}
- (void)timerFinished:(NSTimer*)timer
{
    [super timerFinished:timer];
    self.isResponseChange = YES;
    [self setupSwitch];
}
@end
