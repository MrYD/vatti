//
//  SetHotController.h
//  vatti
//
//  Created by 李叶 on 2018/6/10.
//  Copyright © 2018年 xlink. All rights reserved.
//

#import "BaseViewController.h"
#import "WaterHeaterDeviceModel.h"

@protocol SelectCallbackDelegate<NSObject>

@optional

- (void)selectDataWithValue:(int)value type:(int)type;

- (void)appointmentvalueCallBackWithHours:(int)hours Minutes:(int)minutes isStart:(BOOL) isStart;

@end

@interface SetHotController : BaseViewController

@property (assign, nonatomic) int type;
@property (assign, nonatomic) int startHours;
@property (assign, nonatomic) int startMinutes;
@property (assign, nonatomic) int defaultValue;

//@property (strong, nonatomic) NSString *name;

@property (strong, nonatomic) WaterHeaterDeviceModel *deviceModel;

@property (weak,   nonatomic) id<SelectCallbackDelegate> delegate;

@end


