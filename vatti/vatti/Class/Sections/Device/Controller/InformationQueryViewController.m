//
//  InformationQueryViewController.m
//  vatti
//
//  Created by BZH on 2018/6/9.
//  Copyright © 2018年 xlink. All rights reserved.
//

#import "InformationQueryViewController.h"

@interface InformationQueryViewController ()

@property (weak, nonatomic) IBOutlet UILabel *dayLab;
@property (weak, nonatomic) IBOutlet UILabel *mothsLab;
@property (weak, nonatomic) IBOutlet UILabel *yearLab;
@property (weak, nonatomic) IBOutlet UILabel *whereWeather;
@property (weak, nonatomic) IBOutlet UILabel *waterConsumption;
@property (weak, nonatomic) IBOutlet UILabel *guysConsumption;
@property (weak, nonatomic) IBOutlet UILabel *cumulativeWater;
@property (weak, nonatomic) IBOutlet UILabel *cumulativeGuys;
@property (weak, nonatomic) IBOutlet UILabel *CO;
@property (weak, nonatomic) IBOutlet UILabel *CH;

@end

@implementation InformationQueryViewController

- (void)viewDidLoad {
    [super viewDidLoad];
}

- (void)initSubViews{
    [super initSubViews];
    
    //naiv
    self.navigationItem.title = XLLocalizeString(@"信息查询");
    
    //setup UI
    [self setupUI];
}

- (void)buildData
{
    [super buildData];
    
    @weakify(self)
    
    //天气
    self.whereWeather.text = self.isConnectNetwork ? @"广州 多云 25℃" : @"_";\
    //日期
    [self updateDate];
    
    //rac
    [[[RACObserve(self.deviceModel, current_water).distinctUntilChanged merge:RACObserve(self, isConnectNetwork).distinctUntilChanged] deliverOnMainThread] subscribeNext:^(id  _Nullable x) {
        @strongify(self)
        //本次用水
        self.waterConsumption.text = self.isConnectNetwork ? [NSString stringWithFormat:@"本次用水:%dL",self.deviceModel.current_water] : @"本次用水:-L";
    }];
    
    [[[RACObserve(self.deviceModel, total_water).distinctUntilChanged merge:RACObserve(self, isConnectNetwork).distinctUntilChanged] deliverOnMainThread] subscribeNext:^(id  _Nullable x) {
        @strongify(self)
        //累计用水
        self.cumulativeWater.text = self.isConnectNetwork ? [NSString stringWithFormat:@"%dL",self.deviceModel.total_water] : @"-L";
    }];
    
    [[[RACObserve(self.deviceModel, current_gas).distinctUntilChanged merge:RACObserve(self, isConnectNetwork).distinctUntilChanged] deliverOnMainThread] subscribeNext:^(id  _Nullable x) {
        @strongify(self)
        //本次用气
        self.guysConsumption.text = self.isConnectNetwork ? [NSString stringWithFormat:@"本次用气:%hum³",self.deviceModel.current_gas] : @"本次用气-m³";
    }];
    
    [[[RACObserve(self.deviceModel, total_gas).distinctUntilChanged merge:RACObserve(self, isConnectNetwork).distinctUntilChanged] deliverOnMainThread] subscribeNext:^(id  _Nullable x) {
        @strongify(self)
        //累计用气
        self.cumulativeGuys.text = self.isConnectNetwork ? [NSString stringWithFormat:@"%hum³",self.deviceModel.total_gas] : @"-m³";
    }];
    
    [[[RACObserve(self.deviceModel, CH4).distinctUntilChanged merge:RACObserve(self, isConnectNetwork).distinctUntilChanged] deliverOnMainThread] subscribeNext:^(id  _Nullable x) {
        @strongify(self)
        //甲烷
        self.CH.text = self.isConnectNetwork ? [NSString stringWithFormat:@"CH4:%dppm",self.deviceModel.CH4] : @"CH4:-ppm";
    }];
    
    [[[RACObserve(self.deviceModel, co).distinctUntilChanged merge:RACObserve(self, isConnectNetwork).distinctUntilChanged] deliverOnMainThread] subscribeNext:^(id  _Nullable x) {
        @strongify(self)
        //一氧化碳
         self.CO.text = self.isConnectNetwork ? [NSString stringWithFormat:@"CO:%dppm",self.deviceModel.co] : @"CO:-ppm";
    }];
    
    
}

#pragma mark - UI

- (void)setupUI{
    self.waterConsumption.adjustsFontSizeToFitWidth = YES;
    self.guysConsumption.adjustsFontSizeToFitWidth = YES;
    self.cumulativeWater.adjustsFontSizeToFitWidth = YES;
    self.cumulativeGuys.adjustsFontSizeToFitWidth = YES;
    self.CO.adjustsFontSizeToFitWidth = YES;
}

/**
 更新日期
 */
- (void)updateDate{
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc]init];
    NSDate *date = [NSDate new];
    [formatter setDateFormat:@"yyyy"];
    
    self.yearLab.text = [NSString stringWithFormat:@"%@年",[formatter stringFromDate:date]];
    [formatter setDateFormat:@"M"];
    self.mothsLab.text = [NSString stringWithFormat:@"%@月",[formatter stringFromDate:date]];
    [formatter setDateFormat:@"d"];
    self.dayLab.text = [formatter stringFromDate:date];

}

@end
