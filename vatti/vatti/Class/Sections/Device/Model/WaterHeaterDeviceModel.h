//
//  WaterHeaterDeviceModel.h
//  vatti
//
//  Created by 李叶 on 2018/6/6.
//  Copyright © 2018年 xlink. All rights reserved.
//

#import "DeviceModel.h"

@interface WaterHeaterDeviceModel : DeviceModel<NSCopying>
/*
 *产品序列号index0
 */
@property (nonatomic, copy) NSString *serial_num;
/*
 *电控板编码index1
 */
@property (nonatomic, copy) NSString *elec_ver;
/*
 *软件版本号index2
 */
@property (nonatomic, copy) NSString *soft_ver;
/*
 *硬件版本号index3
 */
@property (nonatomic, copy) NSString *hard_ver;
/**
 热水器进水温度index4
 */
@property (nonatomic, assign) NSInteger water_in_temp;
/**
 热水器出水温度index5
 */
@property (nonatomic, assign) NSInteger water_out_temp;
/**
 读取环境温度index6
 */
@property (nonatomic, assign) NSInteger temp_env;
/**
 设备上报错误代码index7
 */
@property (nonatomic, assign) NSInteger error_num;
/**
 设置热水器温度index8
 */
@property (nonatomic, assign) NSInteger heater_temp;
/**
 CH4数值显示index9
 */
@property (nonatomic, assign) NSInteger CH4;
/**
 co数值显示index10
 */
@property (nonatomic, assign) NSInteger co;
/**
 本次用水量index11
 */
@property (nonatomic, assign) NSInteger current_water;
/**
 统计热水器总用水量index12
 */
@property (nonatomic, assign) NSInteger total_water;
/**
 本次用气量index13
 */
@property (nonatomic, assign) NSInteger current_gas;
/**
 统计总共用气量index14
 */
@property (nonatomic, assign) NSInteger total_gas;
/**
 警报状态，00：无报警 01：CO报警 02：CH4报警 03：CO/CH4两者报警index15
 */
@property (nonatomic, assign) NSInteger warnning_status;
/**
 功能状态位，BIT0:电源开关状态 BIT1:高温解锁 BIT2:舒适浴 BIT3:即热 BIT4:浴缸 BIT5:瀑布浴 BIT6:预约功能一键开关index16
 */
@property (nonatomic, assign) NSInteger function_flag;
/**
 当前浴缸剩余水量，值为0是表示浴缸已满，需弹框提示index17
 */
@property (nonatomic, assign) NSInteger surplus_water;
/**
 即热回差温度index18
 */
@property (nonatomic, assign) NSInteger diff_temp;
/**
 保温持续时间index19
 */
@property (nonatomic, assign) NSInteger keep_time;
/**
 预约功能开关，bit0-7：第1-8组预约时间的开关值index20
 */
@property (nonatomic, assign) NSInteger order_flag;
/**
 预约状态位，bit0-3：第9-12组预约时间的开关值index21
 */
@property (nonatomic, assign) NSInteger order_flag2;
/**
 预约定时器1index22
 */
@property (nonatomic, assign) NSInteger order1;
/**
 预约定时器2,4个字节分别表示：起始时-起始分-结束时-结束分index23
 */
@property (nonatomic, assign) NSInteger order2;
/**
 预约定时器3index24
 */
@property (nonatomic, assign) NSInteger order3;
/**
 预约定时器4index25
 */
@property (nonatomic, assign) NSInteger order4;
/**
 预约定时器5index26
 */
@property (nonatomic, assign) NSInteger order5;
/**
 预约定时器6index27
 */
@property (nonatomic, assign) NSInteger order6;
/**
 预约定时器7index28
 */
@property (nonatomic, assign) NSInteger order7;
/**
 预约定时器8index29
 */
@property (nonatomic, assign) NSInteger order8;
/**
 预约定时器9index30
 */
@property (nonatomic, assign) NSInteger order9;
/**
 预约定时器10index31
 */
@property (nonatomic, assign) NSInteger order10;
/**
 预约定时器11index32
 */
@property (nonatomic, assign) NSInteger order11;
/**
 预约定时器12index33
 */
@property (nonatomic, assign) NSInteger order12;
/**
 多浴缸模式，浴缸选择，bit0：浴缸1,bit1：浴缸2index34
 */
@property (nonatomic, assign) NSInteger bath_switch;
/**
 浴缸1的设定水量index35
 */
@property (nonatomic, assign) NSInteger bath1_water;
/**
 浴缸2的设定水量index36
 */
@property (nonatomic, assign) NSInteger bath2_water;
/**
 表示设备优先权控制，0表示空闲，1表示主机在操作设备，2表示app在操作设备，app在读到这个值为0或者2时才可以进行写操作index37
 */
@property (nonatomic, assign) NSInteger priority_flag;
/**
 主界面故障显示，01：40min长时间燃烧报警，02：CO报警，03：CH4报警，04：EP故障（循环水泵启动异常），05: 主控板与显示板通讯异常，06:环境温度传感器异常 ，07:EEPROM数据异常。08：火焰检测电路异常或意外熄火。09：出水温度异常。10：75度连续10S 或80度连续5S. 11：超温E5，12:电磁阀驱动电路异常，13：风压开关检测电路异常.14:调水阀异常，15:进水温度传感器异常，16: 燃烧室侧边温度异常 ,17: 火焰溢出异常 ，18:风机异常
 index38
 */
@property (nonatomic, assign) NSInteger master_err;
/**
 BIT0:水流 BIT1:风机 BIT2:火焰（这个值为1，主界面显示正在运行，为0空白） BIT3:水泵（即热模式下需判断这个位，1的时候温度只能是35-50,为0时恢复35-60，预约时间到了也需要判断这个值，为1时温度只能调节35-50,为0时恢复35-60） BIT4:预约状态位，1表示已到达预约时间
 index39
 */
@property (assign, nonatomic) NSInteger dev_status;
/**
 用于判断当前那个用户在操作设备
 index40
 */
@property (copy,   nonatomic) NSString *user_id;

/////设备Id
//@property (nonatomic, copy) NSNumber *deviceId;
//
/////设备ProductId
//@property (nonatomic, copy) NSString *productId;
//
//@property (nonatomic, copy) NSString *name;
//
/////所属网关的homeId
//@property (nonatomic, copy) NSString *homeId;
//
/////是否连接成功
//@property (nonatomic, assign) BOOL connected;
//
/////设备模型
//@property (nonatomic, strong) XDevice *device;
//
//@property (nonatomic, assign) BOOL selected;

/*
 * 以下为洗碗机相关变量
 */

/*
 * 电控板厂家生产编码 ,比如 0000：华帝电子
 */
@property (nonatomic, assign) UInt32 manufacture_id;


/*
 * 软件版本号
 */
@property (nonatomic, assign) uint16_t sw_ver;



/*
 * 硬件版本号
 */
@property (nonatomic, assign) uint16_t hw_ver;


/*
 * 0:VST10.23-0A（带调水阀）,VST10.24-0A（不带调水阀） （历史问题，注：VST10.23-0A 和VST10.24-0A有程序变更后需调整为4和5）1:VST10.23-0B（带调水阀） 2:VST10.24-0B（不带调水阀） 3:VST10.26-0（强鼓机型） 4：VST10.23-0A（带调水阀）5：VST10.24-0A（不带调水阀） 6：VST10.27-0(零冷水平台）2017-7-17调整变更
 */
@property (nonatomic, assign) uint8_t control_id;


/*
 * 00：空闲01：洗碗机操作（APP无法进行操作）02：APP操作（洗碗机无法操作）处理策略：(1)当空闲时,洗碗机操作，此时置为1，app不能操作。5min无操作置为0。（2）当空闲时，APP操作前，先读取到为0才可进行操作(否则提示有人操作设备)，app开始操作时，洗碗机检测到APP有操作，置为2，读取此值0，2则可操作继续操作。（3）另一APP检测此值为0时，才可进行操作。(4)占领控制权的APP完全关闭，APP将此值写入0。（5）洗碗机检测到APP端未操作5min，置0。
 */
@property (nonatomic, assign) uint8_t priority;

/*
 * BIT0:运行命令：上电 BIT1:运行命令：关电 BIT2:运行命令：运行 BIT3:运行命令：暂停 BIT4:运行命令：停止 （程序会恢复至起始时间） BIT5:门动作命令：开门 BIT6:门动作命令：关门
 */
@property (nonatomic, assign) uint8_t control_cmd;


/*
 * 1.智能 2.强力 3.标准 4.快速 5.轻柔 6.冷冲 7.热漂 8.ECO 9.自定义（参考自定义程序）
 */
@property (nonatomic, assign) uint8_t switch_func;

/*
 * 1根据时间选择对应的自定义程序。 1：20分钟程序， 2：30分钟程序， 3：40分钟程序， ... 11:120分钟程序
 */
@property (nonatomic, assign) uint8_t func_time;


/*
 * BIT0:增强消毒 BIT1:加漂洗 BIT2:少量洗 BIT3:无水干燥 BIT4:加速省时 BIT5:夜间洗 BIT6:增压
 */
@property (nonatomic, assign) uint8_t func_mode;

/*
 * 软水档位（1~6）
 */
@property (nonatomic, assign) uint8_t water_gears;


/*
 * BIT0:自动开门功能关闭/开启 值0：关闭 值1：开启 BIT1:自动关机功能关闭/开启 值0：关闭 值1：开启 BIT2:灯光关闭/开启 值0：关闭 值1：开启 BIT3:结束提示音（滴滴声） 值0：关闭 值1：开启 BIT4:预约模式关闭/开启 值0：关闭 值1：开启
 */
@property (nonatomic, assign) uint8_t act_switch;


/*
 * 预约时间，时-分
 */
@property (nonatomic, assign) uint32_t order_time;

/*
 * 值1：休眠(插入电源线，屏幕不亮状态) 值2：待机(按下电源键，屏幕点亮状态) 值3：启动(按下运行键，程序运行状态) 值4：暂停(程序运行时，按下暂停键，程序暂停状态)
 */
@property (nonatomic, assign) uint8_t dev_statu;


/*
 * BIT0:门状态：门开/门关 值0：门开 值1：门关 BIT1:专用盐状态：（备用） 值0：缺失 值1：充足 BIT2:亮碟剂状态：（备用） 值0：缺失 值1：充足
 */
@property (nonatomic, assign) uint8_t running_flag;


/*
 * 程序运行当前步骤 1.预洗 2.主洗 3.漂洗 4.干燥
 */
@property (nonatomic, assign) uint8_t func_step;

/*
 * 程序剩余时间：时-分-秒
 */
@property (nonatomic, assign) uint32_t Remaining_Time;


/*
 * 程序运行当前步骤 1.预洗 2.主洗 3.漂洗 4.干燥
 */
@property (nonatomic, assign) uint8_t curr_temp;

/*
 * 程序运行当前步骤 1.预洗 2.主洗 3.漂洗 4.干燥
 */
@property (nonatomic, assign) uint8_t func_temp;


/*
 * 程序运行当前步骤 1.预洗 2.主洗 3.漂洗 4.干燥
 */
@property (nonatomic, assign) uint8_t err_code;


/*
 * 程序运行当前步骤 1.预洗 2.主洗 3.漂洗 4.干燥
 */
@property (nonatomic, assign) uint8_t last_func;



//- (instancetype)initWithCacheKeyValues:(NSDictionary *)cacheKeyValues;
//
//- (instancetype)initWithXDevice:(XDevice *)device;
//
//- (void)addObserver;
//
//- (void)removeObserver;
//
//- (void)addDataPoint:(XLinkDataPoint *)dataPoint;
//- (void)addDataPoints:(NSArray<XLinkDataPoint *>*)dataPoints;
//
//- (void)setDataPoint:(XLinkDataPoint *)dataPoint;
//- (void)setDataPoints:(NSArray <XLinkDataPoint *>*)dataPoints;
//
//- (void)getDeviceDataPoints;
//
///**
// 存入数据库信息
// */
//- (NSMutableDictionary *)cacheKeyValues;

@end
