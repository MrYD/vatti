//
//  DeviceModel.h
//  vatti
//
//  Created by Chris on 2018/8/9.
//  Copyright © 2018年 xlink. All rights reserved.
//

/**
 *  设备模型父类
 *
 *  @author
 */

#import "XLModel.h"
#import "BGFMDB.h"

@class XLinkDataPointObserver;
@class XDeviceStateObserver;

typedef NS_ENUM(NSInteger, XLDeviceModelType){
    XLDeviceModelTypeUnknown,    //未知设备
    XLDeviceModelTypeWaterHeart, //燃热
    XLDeviceModelTypeElectHeart, //电热
};

@interface DeviceModel : XLModel<NSCopying>

#pragma mark - Property

///设备Id
@property (copy,   nonatomic) NSNumber *deviceId;
///设备ProductId
@property (copy,   nonatomic) NSString *productId;
///设备名称
@property (copy,   nonatomic) NSString *name;
///设备默认名称
@property (copy,   nonatomic) NSString *defaultName;

///是否连接成功
@property (assign, nonatomic) BOOL connected;
///设备模型
@property (strong, nonatomic) XDevice *device;

@property (nonatomic, strong, readonly) NSArray <XLinkDataPoint *>*dataPoints;

#pragma mark - Method

#pragma mrak - 供子类调用方法

/**
 帮助子类处理生成设备模型
 
 @param deviceModel DeviceModel
 @param xDevice xDevice
 @param defaultName 默认名称
 */
- (void)configureInitWithDeviceModel:(DeviceModel*)deviceModel withXDevice:(XDevice*)xDevice andDefaultName:(NSString *)defaultName;

/**
 添加监控
 */
- (void)addObserver;

/**
 移除监控
 */
- (void)removeObserver;

/**
 批量添加数据端点值到模型

 @param dataPoints 数据端点
 */
- (void)addDataPoints:(NSArray<XLinkDataPoint *>*)dataPoints;

/**
 发送网络请求，设置单个数据端点值

 @param dataPoint 数据端点
 */
- (void)setDataPoint:(XLinkDataPoint *)dataPoint;

/**
 发送网络请求，设置单个数据端点值

 @param dataPoint 数据端点
 @param handler 请求回调
 */
- (void)setDataPoint:(XLinkDataPoint *)dataPoint handler:(void(^)(BOOL success))handler;

/**
 发送网络请求，批量设置数据端点值

 @param dataPoints 数据端点
 */
- (void)setDataPoints:(NSArray <XLinkDataPoint *>*)dataPoints;

/**
 发送网络请求，批量设置数据端点值

 @param dataPoints 数据端点
 @param handler 请求回调
 */
- (void)setDataPoints:(NSArray <XLinkDataPoint *>*)dataPoints handler:(void(^)(BOOL success))handler;

/**
 发送网络请求获取数据端点值
 */
- (void)getDeviceDataPoints;

#pragma mark - 供子类重写方法

/**
 根据XDevice初始化方法
 */
- (instancetype)initWithXDevice:(XDevice *)device;

/**
 添加数据端点值到模型
 
 @param dataPoint 数据端点
 */
- (void)addDataPoint:(XLinkDataPoint *)dataPoint;

/**
 处理数据端点和连接状态变更
 */
- (void)configureDataPointChanged;

@end
