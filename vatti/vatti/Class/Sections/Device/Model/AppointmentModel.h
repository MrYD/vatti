//
//  AppointmentModel.h
//  vatti
//
//  Created by 李叶 on 2018/6/12.
//  Copyright © 2018年 xlink. All rights reserved.
//

#import "XLModel.h"

@interface AppointmentModel : XLModel

@property (assign, nonatomic) NSInteger index;
@property (assign, nonatomic) NSInteger startHours;
@property (assign, nonatomic) NSInteger startMinutes;
@property (assign, nonatomic) NSInteger endHours;
@property (assign, nonatomic) NSInteger endMinutes;
@property (assign, nonatomic) BOOL isOpen;
@property (assign, nonatomic) BOOL isQualified;

//- (NSInteger)getValueWithIsOpen:(BOOL)isOpen andDeviceModel:(WaterHeaterDeviceModel*)deviceModel;

- (NSInteger)getValueWithIsOpen:(BOOL)isOpen andCacheValue:(NSInteger)value;

@end
