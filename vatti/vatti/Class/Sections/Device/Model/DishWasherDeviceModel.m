//
//  DishWasherDeviceModel.m
//  vatti
//
//  Created by panyaping on 2018/8/27.
//  Copyright © 2018年 xlink. All rights reserved.
//

#import "DishWasherDeviceModel.h"
#import "XLDishWasherDeviceDao.h"
@implementation DishWasherDeviceModel

/**
 联合主键
 */
+ (NSArray *)bg_unionPrimaryKeys
{
    return @[@"productId",@"deviceId"];
}

+ (NSArray *)bg_ignoreKeys
{
    return @[@"device",@"stateObserver",@"dataPointObserver"];
}
/**
 判断是否相等
 */
- (BOOL)isSame:(DishWasherDeviceModel*)dishWasherDeviceModel
{
    BOOL isSame = NO;
    
    NSDictionary *a = [dishWasherDeviceModel yy_modelToJSONObject];
    NSDictionary *b = [self yy_modelToJSONObject];
    
    NSMutableDictionary *mA = [[NSMutableDictionary alloc] initWithDictionary:a];
    NSMutableDictionary *mB = [[NSMutableDictionary alloc] initWithDictionary:b];
    
    NSArray *maAllKeys = mA.allKeys;
    NSArray *mbAllKeys = mB.allKeys;
    
    NSPredicate * filterPredicate = [NSPredicate predicateWithFormat:@"NOT (SELF IN %@)",maAllKeys];
    
    NSArray *differentKeys = [mbAllKeys filteredArrayUsingPredicate:filterPredicate];
    
    for (NSString *differentKey in differentKeys) {
        [mB removeObjectForKey:differentKey];
    }
    
    if ([mA isEqual:mB] == YES) {
        NSLog(@"数据端点没有发生变化,不需要更新到数据库");
        isSame = YES;
    }else{
        NSLog(@"数据端点发生变化,需要更新到数据库");
    }
    return isSame;
}
#pragma mark - OverWrite

- (BOOL)isEqual:(id)object {
    if (self == object) {
        return YES;
    }else if ([object isKindOfClass:[DishWasherDeviceModel class]]) {
        return [self.device.macAddress isEqualToData:((DishWasherDeviceModel *)object).device.macAddress];
    }else {
        return NO;
    }
}

- (id)copyWithZone:(NSZone *)zone {
    DishWasherDeviceModel *deviceModel = [[[self class] allocWithZone:zone] initWithXDevice:self.device];
    [deviceModel addDataPoints:self.dataPoints];
    deviceModel.connected = self.connected;
    return deviceModel;
}

- (NSArray<XLinkDataPoint *> *)dataPoints {
    return @[[XLinkDataPoint dataPointWithType:XLinkDataTypeUnsignedInt withIndex:0  withValue:@(self.manufacture_id)],
             [XLinkDataPoint dataPointWithType:XLinkDataTypeUnsignedShort withIndex:1  withValue:@(self.sw_ver)],
             [XLinkDataPoint dataPointWithType:XLinkDataTypeUnsignedShort withIndex:2  withValue:@(self.hw_ver)],
             [XLinkDataPoint dataPointWithType:XLinkDataTypeByte withIndex:3  withValue:@(self.control_id)],
             [XLinkDataPoint dataPointWithType:XLinkDataTypeByte withIndex:4  withValue:@(self.priority)],
             [XLinkDataPoint dataPointWithType:XLinkDataTypeByte withIndex:5  withValue:@(self.control_cmd)],
             [XLinkDataPoint dataPointWithType:XLinkDataTypeByte withIndex:6  withValue:@(self.switch_func)],
             [XLinkDataPoint dataPointWithType:XLinkDataTypeByte withIndex:7  withValue:@(self.func_time)],
             [XLinkDataPoint dataPointWithType:XLinkDataTypeByte withIndex:8  withValue:@(self.func_mode)],
             [XLinkDataPoint dataPointWithType:XLinkDataTypeByte withIndex:9  withValue:@(self.water_gears)],
             [XLinkDataPoint dataPointWithType:XLinkDataTypeByte withIndex:10 withValue:@(self.act_switch)],
             [XLinkDataPoint dataPointWithType:XLinkDataTypeUnsignedInt  withIndex:11 withValue:@(self.order_time)],
             [XLinkDataPoint dataPointWithType:XLinkDataTypeByte withIndex:12 withValue:@(self.dev_statu)],
             [XLinkDataPoint dataPointWithType:XLinkDataTypeByte withIndex:13 withValue:@(self.running_flag)],
             [XLinkDataPoint dataPointWithType:XLinkDataTypeByte withIndex:14 withValue:@(self.func_step)],
             [XLinkDataPoint dataPointWithType:XLinkDataTypeUnsignedInt withIndex:15 withValue:@(self.Remaining_Time)],
             [XLinkDataPoint dataPointWithType:XLinkDataTypeByte withIndex:16 withValue:@(self.curr_temp)],
             [XLinkDataPoint dataPointWithType:XLinkDataTypeByte withIndex:17 withValue:@(self.func_temp)],
             [XLinkDataPoint dataPointWithType:XLinkDataTypeByte withIndex:18 withValue:@(self.err_code)],
             [XLinkDataPoint dataPointWithType:XLinkDataTypeByte withIndex:19 withValue:@(self.last_func)],
             [XLinkDataPoint dataPointWithType:XLinkDataTypeString withIndex:20 withValue:self.user_id?self.user_id:@""],];
}

/**
 根据XDevice初始化方法
 */
- (instancetype)initWithXDevice:(XDevice *)device {
    
    BOOL b1 = device == nil;
    BOOL b2 = device.productID.length <= 0;
    if (b1 || b2) {
        return nil;
    }
    
    DishWasherDeviceModel *deviceModel = [[DishWasherDeviceModel alloc] init];
    [self configureInitWithDeviceModel:deviceModel withXDevice:device andDefaultName:XLDeviceDishWasherName];
    
    return deviceModel;
}
- (void)addDataPoint:(XLinkDataPoint *)dataPoint {
    
    
    if (dataPoint.index == 0) {
        self.manufacture_id = [dataPoint.value intValue];
    }else if (dataPoint.index == 1) {
        self.sw_ver = [dataPoint.value intValue];
    }else if (dataPoint.index == 2) {
        self.hw_ver = [dataPoint.value intValue];
    }else if (dataPoint.index == 3) {
        self.control_id = [dataPoint.value intValue];
    }else if (dataPoint.index == 4) {
        self.priority = [dataPoint.value intValue];
    }else if (dataPoint.index == 5) {
        self.control_cmd = [dataPoint.value intValue];
    }else if (dataPoint.index == 6) {
        self.switch_func = [dataPoint.value intValue];
    }else if (dataPoint.index == 7) {
        self.func_time = [dataPoint.value intValue];
    }else if (dataPoint.index == 8) {
        self.func_mode = [dataPoint.value intValue];
    }else if (dataPoint.index == 9) {
        self.water_gears = [dataPoint.value intValue];
    }else if (dataPoint.index == 10) {
        self.act_switch = [dataPoint.value intValue];
    }else if (dataPoint.index == 11) {
        self.order_time = [dataPoint.value intValue];
    }else if (dataPoint.index == 12) {
        self.dev_statu = [dataPoint.value intValue];
    }else if (dataPoint.index == 13) {
        self.running_flag = [dataPoint.value intValue];
    }else if (dataPoint.index == 14) {
        self.func_step = [dataPoint.value intValue];
    }else if (dataPoint.index == 15) {
        self.Remaining_Time = [dataPoint.value intValue];
    }else if (dataPoint.index == 16) {
        self.curr_temp = [dataPoint.value intValue];
    }else if (dataPoint.index == 17) {
        self.func_temp = [dataPoint.value intValue];
    }else if (dataPoint.index == 18) {
        self.err_code = [dataPoint.value intValue];
    }else if (dataPoint.index == 19) {
        self.last_func = [dataPoint.value intValue];
    }else if (dataPoint.index == 20) {
        
        if (dataPoint != nil) {
            self.user_id = [NSString stringWithFormat:@"%@",dataPoint.value];
        }
    }
    
    return;
    
}
/**
 处理数据端点和连接状态变更
 */
- (void)configureDataPointChanged
{
    [XLDishWasherDeviceDao queryWithXDevice:self.device callBack:^(DishWasherDeviceModel * _Nullable xlDishWasherDevice) {
        if (xlDishWasherDevice == nil) {
            return;
        }
        
        if ([self isSame:xlDishWasherDevice] == YES) {
            return;
        }
        
        //数据端点变更时，保存到数据库
        [XLDishWasherDeviceDao saveOrUpdateAsyncWithWaterHeaterDevice:self callBack:^(BOOL isSuccess) {
            if (isSuccess == YES) {
                NSLog(@"插入数据库成功");
            }else{
                NSLog(@"插入数据库失败");
            }
        }];
    }];
}

@end
