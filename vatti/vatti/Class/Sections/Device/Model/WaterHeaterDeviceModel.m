//
//  WaterHeaterDeviceModel.m
//  vatti
//
//  Created by 李叶 on 2018/6/6.
//  Copyright © 2018年 xlink. All rights reserved.
//

#import "WaterHeaterDeviceModel.h"
#import "XLWaterHeaterDeviceDao.h"

@implementation WaterHeaterDeviceModel

/**
 联合主键
 */
+ (NSArray *)bg_unionPrimaryKeys
{
    return @[@"productId",@"deviceId"];
}

+ (NSArray *)bg_ignoreKeys
{
    return @[@"device",@"stateObserver",@"dataPointObserver"];
}

/**
 判断是否相等
 */
- (BOOL)isSame:(WaterHeaterDeviceModel*)waterHeaterDeviceModel
{
    BOOL isSame = NO;
    
    NSDictionary *a = [waterHeaterDeviceModel yy_modelToJSONObject];
    NSDictionary *b = [self yy_modelToJSONObject];
    
    NSMutableDictionary *mA = [[NSMutableDictionary alloc] initWithDictionary:a];
    NSMutableDictionary *mB = [[NSMutableDictionary alloc] initWithDictionary:b];
    
    NSArray *maAllKeys = mA.allKeys;
    NSArray *mbAllKeys = mB.allKeys;
    
    NSPredicate * filterPredicate = [NSPredicate predicateWithFormat:@"NOT (SELF IN %@)",maAllKeys];
    
    NSArray *differentKeys = [mbAllKeys filteredArrayUsingPredicate:filterPredicate];
    
    for (NSString *differentKey in differentKeys) {
        [mB removeObjectForKey:differentKey];
    }
    
    if ([mA isEqual:mB] == YES) {
        NSLog(@"数据端点没有发生变化,不需要更新到数据库");
        isSame = YES;
    }else{
        NSLog(@"数据端点发生变化,需要更新到数据库");
    }
    return isSame;
}

#pragma mark - OverWrite

- (BOOL)isEqual:(id)object {
    if (self == object) {
        return YES;
    }else if ([object isKindOfClass:[WaterHeaterDeviceModel class]]) {
        return [self.device.macAddress isEqualToData:((WaterHeaterDeviceModel *)object).device.macAddress];
    }else {
        return NO;
    }
}

- (id)copyWithZone:(NSZone *)zone {
    WaterHeaterDeviceModel *deviceModel = [[[self class] allocWithZone:zone] initWithXDevice:self.device];
    [deviceModel addDataPoints:self.dataPoints];
    deviceModel.connected = self.connected;
    return deviceModel;
}

- (NSArray<XLinkDataPoint *> *)dataPoints {
    return @[[XLinkDataPoint dataPointWithType:XLinkDataTypeString withIndex:0  withValue:self.serial_num],
             [XLinkDataPoint dataPointWithType:XLinkDataTypeString withIndex:1  withValue:self.elec_ver],
             [XLinkDataPoint dataPointWithType:XLinkDataTypeString withIndex:2  withValue:self.soft_ver],
             [XLinkDataPoint dataPointWithType:XLinkDataTypeString withIndex:3  withValue:self.hard_ver],
             
             [XLinkDataPoint dataPointWithType:XLinkDataTypeByte   withIndex:4  withValue:@(self.water_in_temp)],
             [XLinkDataPoint dataPointWithType:XLinkDataTypeByte   withIndex:5  withValue:@(self.water_out_temp)],
             [XLinkDataPoint dataPointWithType:XLinkDataTypeByte   withIndex:6  withValue:@(self.temp_env)],
             
             [XLinkDataPoint dataPointWithType:XLinkDataTypeShort  withIndex:7  withValue:@(self.error_num)],
             
             [XLinkDataPoint dataPointWithType:XLinkDataTypeByte   withIndex:8  withValue:@(self.heater_temp)],
             
             [XLinkDataPoint dataPointWithType:XLinkDataTypeShort  withIndex:9  withValue:@(self.CH4)],
             [XLinkDataPoint dataPointWithType:XLinkDataTypeShort  withIndex:10 withValue:@(self.co)],
             [XLinkDataPoint dataPointWithType:XLinkDataTypeShort  withIndex:11 withValue:@(self.current_water)],
             [XLinkDataPoint dataPointWithType:XLinkDataTypeShort  withIndex:12 withValue:@(self.total_water)],
             [XLinkDataPoint dataPointWithType:XLinkDataTypeShort  withIndex:13 withValue:@(self.current_gas)],
             [XLinkDataPoint dataPointWithType:XLinkDataTypeShort  withIndex:14 withValue:@(self.total_gas)],
             
             [XLinkDataPoint dataPointWithType:XLinkDataTypeByte   withIndex:15 withValue:@(self.warnning_status)],
             [XLinkDataPoint dataPointWithType:XLinkDataTypeByte   withIndex:16 withValue:@(self.function_flag)],
             [XLinkDataPoint dataPointWithType:XLinkDataTypeByte   withIndex:17 withValue:@(self.surplus_water)],
             [XLinkDataPoint dataPointWithType:XLinkDataTypeByte   withIndex:18 withValue:@(self.diff_temp)],
             [XLinkDataPoint dataPointWithType:XLinkDataTypeByte   withIndex:19 withValue:@(self.keep_time)],
             [XLinkDataPoint dataPointWithType:XLinkDataTypeByte   withIndex:20 withValue:@(self.order_flag)],
             [XLinkDataPoint dataPointWithType:XLinkDataTypeByte   withIndex:21 withValue:@(self.order_flag2)],
             
             [XLinkDataPoint dataPointWithType:XLinkDataTypeUnsignedInt withIndex:22 withValue:@(self.order1)],
             [XLinkDataPoint dataPointWithType:XLinkDataTypeUnsignedInt withIndex:23 withValue:@(self.order2)],
             [XLinkDataPoint dataPointWithType:XLinkDataTypeUnsignedInt withIndex:24 withValue:@(self.order3)],
             [XLinkDataPoint dataPointWithType:XLinkDataTypeUnsignedInt withIndex:25 withValue:@(self.order4)],
             [XLinkDataPoint dataPointWithType:XLinkDataTypeUnsignedInt withIndex:26 withValue:@(self.order5)],
             [XLinkDataPoint dataPointWithType:XLinkDataTypeUnsignedInt withIndex:27 withValue:@(self.order6)],
             [XLinkDataPoint dataPointWithType:XLinkDataTypeUnsignedInt withIndex:28 withValue:@(self.order7)],
             [XLinkDataPoint dataPointWithType:XLinkDataTypeUnsignedInt withIndex:29 withValue:@(self.order8)],
             [XLinkDataPoint dataPointWithType:XLinkDataTypeUnsignedInt withIndex:30 withValue:@(self.order9)],
             [XLinkDataPoint dataPointWithType:XLinkDataTypeUnsignedInt withIndex:31 withValue:@(self.order10)],
             [XLinkDataPoint dataPointWithType:XLinkDataTypeUnsignedInt withIndex:32 withValue:@(self.order11)],
             [XLinkDataPoint dataPointWithType:XLinkDataTypeUnsignedInt withIndex:33 withValue:@(self.order12)],
             
             [XLinkDataPoint dataPointWithType:XLinkDataTypeByte withIndex:34 withValue:@(self.bath_switch)],
             [XLinkDataPoint dataPointWithType:XLinkDataTypeByte withIndex:35 withValue:@(self.bath1_water)],
             [XLinkDataPoint dataPointWithType:XLinkDataTypeByte withIndex:36 withValue:@(self.bath2_water)],
             [XLinkDataPoint dataPointWithType:XLinkDataTypeByte withIndex:37 withValue:@(self.priority_flag)],
             [XLinkDataPoint dataPointWithType:XLinkDataTypeByte withIndex:38 withValue:@(self.master_err)],
             [XLinkDataPoint dataPointWithType:XLinkDataTypeByte withIndex:39 withValue:@(self.dev_status)],
             
             [XLinkDataPoint dataPointWithType:XLinkDataTypeString withIndex:40 withValue:self.user_id]
             ];
}

/**
 根据XDevice初始化方法
 */
- (instancetype)initWithXDevice:(XDevice *)device {
    
    BOOL b1 = device == nil;
    BOOL b2 = device.productID.length <= 0;
    if (b1 || b2) {
        return nil;
    }
    
    WaterHeaterDeviceModel *deviceModel = [[WaterHeaterDeviceModel alloc] init];
    [self configureInitWithDeviceModel:deviceModel withXDevice:device andDefaultName:XLDeviceGasWaterHeaterName];
    
    return deviceModel;
}

//4,5,6,37端点会每隔500ms上报一次
//需要判断有没有改变再设值
- (void)addDataPoint:(XLinkDataPoint *)dataPoint {
    
    
    if (dataPoint.index == 0) {
        self.manufacture_id = [dataPoint.value intValue];
    }else if (dataPoint.index == 1) {
        self.sw_ver = [dataPoint.value intValue];
    }else if (dataPoint.index == 2) {
        self.hw_ver = [dataPoint.value intValue];
    }else if (dataPoint.index == 3) {
        self.control_id = [dataPoint.value intValue];
    }else if (dataPoint.index == 4) {
        self.priority = [dataPoint.value intValue];
    }else if (dataPoint.index == 5) {
        self.control_cmd = [dataPoint.value intValue];
    }else if (dataPoint.index == 6) {
        self.switch_func = [dataPoint.value intValue];
    }else if (dataPoint.index == 7) {
        self.func_time = [dataPoint.value intValue];
    }else if (dataPoint.index == 8) {
        self.func_mode = [dataPoint.value intValue];
    }else if (dataPoint.index == 9) {
        self.water_gears = [dataPoint.value intValue];
    }else if (dataPoint.index == 10) {
        self.act_switch = [dataPoint.value intValue];
    }else if (dataPoint.index == 11) {
        self.order_time = [dataPoint.value intValue];
    }else if (dataPoint.index == 12) {
        self.dev_statu = [dataPoint.value intValue];
    }else if (dataPoint.index == 13) {
        self.running_flag = [dataPoint.value intValue];
    }else if (dataPoint.index == 14) {
        self.func_step = [dataPoint.value intValue];
    }else if (dataPoint.index == 15) {
        self.Remaining_Time = [dataPoint.value intValue];
    }else if (dataPoint.index == 16) {
        self.curr_temp = [dataPoint.value intValue];
    }else if (dataPoint.index == 17) {
        self.func_temp = [dataPoint.value intValue];
    }else if (dataPoint.index == 18) {
        self.err_code = [dataPoint.value intValue];
    }else if (dataPoint.index == 19) {
        self.last_func = [dataPoint.value intValue];
    }else if (dataPoint.index == 20) {
        
        if (dataPoint != nil) {
            self.user_id = [NSString stringWithFormat:@"%@",dataPoint.value];
        } 
    }
    
    return;
    
    
    if (dataPoint.index == 0) {
        self.serial_num = dataPoint.value;
    }else if (dataPoint.index == 1) {
        self.elec_ver = dataPoint.value;
    }else if (dataPoint.index == 2) {
        self.soft_ver = dataPoint.value;
    }else if (dataPoint.index == 3) {
        self.hard_ver = dataPoint.value;
    }else if (dataPoint.index == 4) {
        if (self.water_in_temp != [dataPoint.value intValue]) {
            self.water_in_temp = [dataPoint.value intValue];
        }
    }else if (dataPoint.index == 5) {
        if (self.water_out_temp != [dataPoint.value intValue]) {
            self.water_out_temp = [dataPoint.value intValue];
        }
    }else if (dataPoint.index == 6) {
        if (self.temp_env != [dataPoint.value intValue]) {
            self.temp_env = [dataPoint.value intValue];
        }
    }else if (dataPoint.index == 7) {
        self.error_num = [dataPoint.value intValue];
    }else if (dataPoint.index == 8) {
        self.heater_temp = [dataPoint.value intValue];
    }else if (dataPoint.index == 9) {
        self.CH4 = [dataPoint.value intValue];
    }else if (dataPoint.index == 10) {
        self.co = [dataPoint.value intValue];
    }else if (dataPoint.index == 11) {
        self.current_water = [dataPoint.value intValue];
    }else if (dataPoint.index == 12) {
        self.total_water = [dataPoint.value intValue];
    }else if (dataPoint.index == 13) {
        self.current_gas = [dataPoint.value intValue];
    }else if (dataPoint.index == 14) {
        self.total_gas = [dataPoint.value intValue];
    }else if (dataPoint.index == 15) {
        self.warnning_status = [dataPoint.value intValue];
    }else if (dataPoint.index == 16) {
        self.function_flag = [dataPoint.value intValue];
    }else if (dataPoint.index == 17) {
        self.surplus_water = [dataPoint.value intValue];
    }else if (dataPoint.index == 18) {
        self.diff_temp = [dataPoint.value intValue];
    }else if (dataPoint.index == 19) {
        self.keep_time = [dataPoint.value intValue];
    }else if (dataPoint.index == 20) {
        self.order_flag = [dataPoint.value intValue];
    }else if (dataPoint.index == 21) {
        self.order_flag2 = [dataPoint.value intValue];
    }else if (dataPoint.index == 22) {
        self.order1 = 0x173B;
    }else if (dataPoint.index == 23) {
        self.order2 = [dataPoint.value intValue];
    }else if (dataPoint.index == 24) {
        self.order3 = [dataPoint.value intValue];
    }else if (dataPoint.index == 25) {
        self.order4 = [dataPoint.value intValue];
    }else if (dataPoint.index == 26) {
        self.order5 = [dataPoint.value intValue];
    }else if (dataPoint.index == 27) {
        self.order6 = [dataPoint.value intValue];
    }else if (dataPoint.index == 28) {
        self.order7 = [dataPoint.value intValue];
    }else if (dataPoint.index == 29) {
        self.order8 = [dataPoint.value intValue];
    }else if (dataPoint.index == 30) {
        self.order9 = [dataPoint.value intValue];
    }else if (dataPoint.index == 31) {
        self.order10 = [dataPoint.value intValue];
    }else if (dataPoint.index == 32) {
        self.order11 = [dataPoint.value intValue];
    }else if (dataPoint.index == 33) {
        self.order12 = [dataPoint.value intValue];
    }else if (dataPoint.index == 34) {
        self.bath_switch = [dataPoint.value intValue];
    }else if (dataPoint.index == 35) {
        self.bath1_water = [dataPoint.value intValue];
    }else if (dataPoint.index == 36) {
        self.bath2_water = [dataPoint.value intValue];
    }else if (dataPoint.index == 37) {
        if (self.priority_flag != [dataPoint.value intValue]) {
            self.priority_flag = [dataPoint.value intValue];
        }
    }else if (dataPoint.index == 38) {
        self.master_err = [dataPoint.value intValue];
    }else if (dataPoint.index == 39) {
        self.dev_status = [dataPoint.value intValue];
    }else if (dataPoint.index == 40) {
        self.user_id = dataPoint.value;
    }
}

/**
 处理数据端点和连接状态变更
 */
- (void)configureDataPointChanged
{
    [XLWaterHeaterDeviceDao queryWithXDevice:self.device callBack:^(WaterHeaterDeviceModel * _Nullable xlWaterHeaterDevice) {
        if (xlWaterHeaterDevice == nil) {
            return;
        }
        
        if ([self isSame:xlWaterHeaterDevice] == YES) {
            return;
        }
        
        //数据端点变更时，保存到数据库
        [XLWaterHeaterDeviceDao saveOrUpdateAsyncWithWaterHeaterDevice:self callBack:^(BOOL isSuccess) {
            if (isSuccess == YES) {
                NSLog(@"插入数据库成功");
            }else{
                NSLog(@"插入数据库失败");
            }
        }];
    }];
}

@end
