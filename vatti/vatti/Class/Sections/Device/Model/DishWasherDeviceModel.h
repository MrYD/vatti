//
//  DishWasherDeviceModel.h
//  vatti
//
//  Created by panyaping on 2018/8/27.
//  Copyright © 2018年 xlink. All rights reserved.
//

#import "DeviceModel.h"

@interface DishWasherDeviceModel : DeviceModel<NSCopying>

/*
 * 以下为洗碗机相关变量
 */

/*
 * 电控板厂家生产编码 ,比如 0000：华帝电子
 */
@property (nonatomic, assign) UInt32 manufacture_id;


/*
 * 软件版本号
 */
@property (nonatomic, assign) uint16_t sw_ver;



/*
 * 硬件版本号
 */
@property (nonatomic, assign) uint16_t hw_ver;


/*
 * 0:VST10.23-0A（带调水阀）,VST10.24-0A（不带调水阀） （历史问题，注：VST10.23-0A 和VST10.24-0A有程序变更后需调整为4和5）1:VST10.23-0B（带调水阀） 2:VST10.24-0B（不带调水阀） 3:VST10.26-0（强鼓机型） 4：VST10.23-0A（带调水阀）5：VST10.24-0A（不带调水阀） 6：VST10.27-0(零冷水平台）2017-7-17调整变更
 */
@property (nonatomic, assign) uint8_t control_id;


/*
 * 00：空闲01：洗碗机操作（APP无法进行操作）02：APP操作（洗碗机无法操作）处理策略：(1)当空闲时,洗碗机操作，此时置为1，app不能操作。5min无操作置为0。（2）当空闲时，APP操作前，先读取到为0才可进行操作(否则提示有人操作设备)，app开始操作时，洗碗机检测到APP有操作，置为2，读取此值0，2则可操作继续操作。（3）另一APP检测此值为0时，才可进行操作。(4)占领控制权的APP完全关闭，APP将此值写入0。（5）洗碗机检测到APP端未操作5min，置0。
 */
@property (nonatomic, assign) uint8_t priority;

/*
 * BIT0:运行命令：上电 BIT1:运行命令：关电 BIT2:运行命令：运行 BIT3:运行命令：暂停 BIT4:运行命令：停止 （程序会恢复至起始时间） BIT5:门动作命令：开门 BIT6:门动作命令：关门
 */
@property (nonatomic, assign) uint8_t control_cmd;


/*
 * 1.智能 2.强力 3.标准 4.快速 5.轻柔 6.冷冲 7.热漂 8.ECO 9.自定义（参考自定义程序）
 */
@property (nonatomic, assign) uint8_t switch_func;

/*
 * 1根据时间选择对应的自定义程序。 1：20分钟程序， 2：30分钟程序， 3：40分钟程序， ... 11:120分钟程序
 */
@property (nonatomic, assign) uint8_t func_time;


/*
 * BIT0:增强消毒 BIT1:加漂洗 BIT2:少量洗 BIT3:无水干燥 BIT4:加速省时 BIT5:夜间洗 BIT6:增压
 */
@property (nonatomic, assign) uint8_t func_mode;

/*
 * 软水档位（1~6）
 */
@property (nonatomic, assign) uint8_t water_gears;


/*
 * BIT0:自动开门功能关闭/开启 值0：关闭 值1：开启 BIT1:自动关机功能关闭/开启 值0：关闭 值1：开启 BIT2:灯光关闭/开启 值0：关闭 值1：开启 BIT3:结束提示音（滴滴声） 值0：关闭 值1：开启 BIT4:预约模式关闭/开启 值0：关闭 值1：开启
 */
@property (nonatomic, assign) uint8_t act_switch;


/*
 * 预约时间，时-分
 */
@property (nonatomic, assign) uint32_t order_time;

/*
 * 值1：休眠(插入电源线，屏幕不亮状态) 值2：待机(按下电源键，屏幕点亮状态) 值3：启动(按下运行键，程序运行状态) 值4：暂停(程序运行时，按下暂停键，程序暂停状态)
 */
@property (nonatomic, assign) uint8_t dev_statu;


/*
 * BIT0:门状态：门开/门关 值0：门开 值1：门关 BIT1:专用盐状态：（备用） 值0：缺失 值1：充足 BIT2:亮碟剂状态：（备用） 值0：缺失 值1：充足
 */
@property (nonatomic, assign) uint8_t running_flag;


/*
 * 程序运行当前步骤 1.预洗 2.主洗 3.漂洗 4.干燥
 */
@property (nonatomic, assign) uint8_t func_step;

/*
 * 程序剩余时间：时-分-秒
 */
@property (nonatomic, assign) uint32_t Remaining_Time;


/*
 * 程序运行当前步骤 1.预洗 2.主洗 3.漂洗 4.干燥
 */
@property (nonatomic, assign) uint8_t curr_temp;

/*
 * 程序运行当前步骤 1.预洗 2.主洗 3.漂洗 4.干燥
 */
@property (nonatomic, assign) uint8_t func_temp;


/*
 * 程序运行当前步骤 1.预洗 2.主洗 3.漂洗 4.干燥
 */
@property (nonatomic, assign) uint8_t err_code;


/*
 * 程序运行当前步骤 1.预洗 2.主洗 3.漂洗 4.干燥
 */
@property (nonatomic, assign) uint8_t last_func;

/**
 用于判断当前那个用户在操作设备
 index40
 */
@property (copy,   nonatomic) NSString *user_id;
@end
