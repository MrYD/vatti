//
//  DeviceModel.m
//  vatti
//
//  Created by Chris on 2018/8/9.
//  Copyright © 2018年 xlink. All rights reserved.
//

#import "DeviceModel.h"
#import "XDeviceManager.h"
#import "XLinkDataPointManager.h"
#import "XDeviceStateObserver.h"
#import "XLinkDataPointObserver.h"

@interface DeviceModel()
///连接状态监控
@property (strong, nonatomic) XDeviceStateObserver *stateObserver;
///数据端点监控
@property (strong, nonatomic) XLinkDataPointObserver *dataPointObserver;
@end

@implementation DeviceModel

#pragma mark - 供子类调用方法

/**
 帮助子类处理生成设备模型
 
 @param deviceModel DeviceModel
 @param xDevice xDevice
 @param defaultName 默认名称
 */
- (void)configureInitWithDeviceModel:(DeviceModel*)deviceModel withXDevice:(XDevice*)xDevice andDefaultName:(NSString *)defaultName
{
    //产品ID
    deviceModel.productId = xDevice.productID;
    //设备ID
    deviceModel.deviceId = @(xDevice.deviceID);
    //名称
    deviceModel.name = xDevice.deviceName;
    //默认名称
    deviceModel.defaultName = defaultName;
    //设备
    deviceModel.device = xDevice;
}

/**
 添加监控
 */
- (void)addObserver {
    [self getDeviceDataPoints];
    [self addStateObserver];
    [self addDataPointObserver];
}

/**
 移除监控
 */
- (void)removeObserver {
    [self removeStateObserver];
    [self removeDataPointObserver];
}

/**
 批量添加数据端点值到模型
 
 @param dataPoints 数据端点
 */
- (void)addDataPoints:(NSArray<XLinkDataPoint *>*)dataPoints {
    [dataPoints enumerateObjectsUsingBlock:^(XLinkDataPoint * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        [self addDataPoint:obj];
    }];
}

/**
 发送网络请求，设置单个数据端点值
 
 @param dataPoint 数据端点
 */
- (void)setDataPoint:(XLinkDataPoint *)dataPoint {
    [self setDataPoints:@[dataPoint]];
}

/**
 发送网络请求，设置单个数据端点值
 
 @param dataPoint 数据端点
 @param handler 请求回调
 */
- (void)setDataPoint:(XLinkDataPoint *)dataPoint handler:(void (^)(BOOL))handler {
    [self setDataPoints:@[dataPoint] handler:handler];
}

/**
 发送网络请求，批量设置数据端点值
 
 @param dataPoints 数据端点
 */
- (void)setDataPoints:(NSArray <XLinkDataPoint *> *)dataPoints {
    [self setDataPoints:dataPoints handler:nil];
}

/**
 发送网络请求，批量设置数据端点值
 
 @param dataPoints 数据端点
 @param handler 请求回调
 */
- (void)setDataPoints:(NSArray<XLinkDataPoint *> *)dataPoints handler:(void (^)(BOOL))handler {
    @weakify(self);
    [[XLServiceManager shareInstance].deviceService setDeviceDataPoints:_device dataPoints:dataPoints timeout:10. handler:^(id result, NSError *error) {
        @strongify(self);
        if (!error) {
            [self addDataPoints:dataPoints];
        }
        if (handler) handler(error == nil);
    }];
}

/**
 发送网络请求获取数据端点值
 */
- (void)getDeviceDataPoints {
    @weakify(self);
    [[XLServiceManager shareInstance].deviceService getDeviceDataPoints:self.device timeout:10. handler:^(NSArray<XLinkDataPoint *> *result, XLinkErrorCode code) {
        @strongify(self);
        if (code == 0) {
            [self addDataPoints:result];
        }
    }];
}

#pragma mark - 供子类重写方法

/**
 根据XDevice初始化方法
 */
- (instancetype)initWithXDevice:(XDevice *)device
{
    BOOL b1 = device == nil;
    BOOL b2 = device.productID.length <= 0;
    if (b1 || b2) {
        return nil;
    }
    
    DeviceModel *deviceModel = [[DeviceModel alloc] init];
    [self configureInitWithDeviceModel:deviceModel withXDevice:device andDefaultName:nil];
    
    return deviceModel;
}

/**
 添加数据端点值到模型
 
 @param dataPoint 数据端点
 */
- (void)addDataPoint:(XLinkDataPoint *)dataPoint
{
    
}

/**
 处理数据端点和连接状态变更
 */
- (void)configureDataPointChanged
{
    
}

#pragma mark - OverWrite

- (NSArray<XLinkDataPoint *> *)dataPoints
{
    return nil;
}

- (BOOL)isEqual:(id)object {
    if (self == object) {
        return YES;
    }else if ([object isKindOfClass:[DeviceModel class]]) {
        return [self.device.macAddress isEqualToData:((DeviceModel *)object).device.macAddress];
    }else {
        return NO;
    }
}

- (id)copyWithZone:(NSZone *)zone {
    DeviceModel *deviceModel = [[[self class] allocWithZone:zone] initWithXDevice:self.device];
    deviceModel.connected = self.connected;
    [deviceModel addDataPoints:self.dataPoints];
    return deviceModel;
}

- (void)setConnected:(BOOL)connected
{
    if (_connected != connected) {
        _connected = connected;
        
        if (_connected == YES) {
            [self getDeviceDataPoints];
            NSLog(@"监控 +++ %@ -- %@ --- 设备连接状态已经上线", self.name, self.device.macAddress);
        }else{
            NSLog(@"监控 +++ %@ -- %@ --- 设备连接状态已经离线", self.name, self.device.macAddress);
        }
    }
}

#pragma mark - Privite

/**
 设备连接状态监控
 */
- (void)addStateObserver {
    self.connected = (self.device.connectionState == XDeviceConnectionStateOnline);
    NSLog(@"开始监控 --- %@ -- %@ --- 设备连接状态， 当前状态%@", self.name, self.device.macAddress, self.connected ? @"在线" : @"离线");
    @weakify(self);
    XDeviceStateObserver *observer = [XDeviceStateObserver deviceStateChangedObserverWithDevice:self.device deviceStateChangedBlock:^(XDevice * _Nonnull device, XDeviceConnectionState state) {
        @strongify(self);
        if ([device isEqualToDevice:self.device]) {
            BOOL isConnected = device.connectionState == XDeviceConnectionStateOnline;
            self.connected = isConnected;
        }
        
        [self configureDataPointChanged];
    }];
    [[XLinkSDK share].deviceManager addDeviceStateObserver:observer];
    _stateObserver = observer;
}

/**
 设备数据端点监控
 */
- (void)addDataPointObserver {
    NSLog(@"开始监控 --- %@ -- %@ --- 设备数据端点改变， 当前状态%@", self.name, self.device.macAddress, self.connected ? @"在线" : @"离线");
    @weakify(self);
    XLinkDataPointObserver *observer = [XLinkDataPointObserver dataPointObserverWithDevice:self.device IndexArray:nil withDataPointUpdateBlock:^(XDevice * _Nonnull device, NSArray<XLinkDataPoint *> * _Nonnull dataPoints) {
        @strongify(self);
        if ([device isEqualToDevice:self.device]) {
            NSLog(@"监控 --- %@ -- %@ --- 设备数据端点改变", self.name, self.device.macAddress);
            [self addDataPoints:dataPoints];
        }
        
        [self configureDataPointChanged];
    }];
    [[XLinkSDK share].dataPointManager addDataPointObserver:observer];
    _dataPointObserver = observer;
}

/**
 移除设备状态监控
 */
- (void)removeStateObserver {
    if (self.stateObserver) {
        [[XLinkSDK share].deviceManager removeDeviceStateObserver:self.stateObserver];
    }
}

/**
 移除设备数据端点监控
 */
- (void)removeDataPointObserver {
    if (self.dataPointObserver) {
        [[XLinkSDK share].dataPointManager removeDataPointObserver:self.dataPointObserver];
    }
}

@end
