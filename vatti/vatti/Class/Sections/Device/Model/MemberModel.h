//
//  MemberModel.h
//  vatti
//
//  Created by 李叶 on 2018/6/9.
//  Copyright © 2018年 xlink. All rights reserved.
//

#import "XLModel.h"

@interface MemberModel : XLModel

#pragma mark - Property
/**
 分享邀请码
 */
@property (nonatomic, copy) NSString *invite_code;

/**
分享者ID
 */
@property (nonatomic, copy) NSString *from_id;

/**
 分享者帐号
 */
@property (nonatomic, copy) NSString *from_name;

/**
 被分享者ID
 */
@property (nonatomic, copy) NSString *user_id;

/**
 被分享者昵称
 */
@property (nonatomic, copy) NSString *to_name;

/**
 被分享者帐号
 */
@property (nonatomic, copy) NSString *to_user;

/**
 设备ID
 */
@property (nonatomic, copy) NSNumber *device_id;

/**
 分享状态
 */
@property (nonatomic, copy) NSString *state;

/**
 分享产生时间
 */
@property (nonatomic, copy) NSString *gen_date;

/**
 分享过期时间
 */
@property (nonatomic, copy) NSString *expire_date;

/**
 分享权限
 */
@property (nonatomic, copy) NSString *authority;


#pragma mark - Method
- (instancetype)initWithCacheKeyValues:(NSDictionary *)cacheKeyValues;

/**
 存入数据库信息
 */
- (NSMutableDictionary *)cacheKeyValues;


@end
