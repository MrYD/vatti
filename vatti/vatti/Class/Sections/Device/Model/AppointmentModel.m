//
//  AppointmentModel.m
//  vatti
//
//  Created by 李叶 on 2018/6/12.
//  Copyright © 2018年 xlink. All rights reserved.
//

#import "AppointmentModel.h"

@implementation AppointmentModel

- (BOOL)isQualified{
    
    NSInteger difference = (self.endHours - self.startMinutes)*60 + (self.endMinutes - self.startMinutes);
    if(difference>30){
        return YES;
    }else{
        return NO;
    }
}

- (NSInteger)getValueWithIsOpen:(BOOL)isOpen andDeviceModel:(WaterHeaterDeviceModel*)deviceModel
{
    NSInteger value = 0;
    NSInteger index = self.index - 21;
    
    switch (index) {
        case 1:
            value = isOpen ? deviceModel.order_flag | 0x01 : deviceModel.order_flag & (~0x01);
            break;
        case 2:
            value = isOpen ? deviceModel.order_flag | 0x02 : deviceModel.order_flag & (~0x02);
            break;
        case 3:
            value = isOpen ? deviceModel.order_flag | 0x04 : deviceModel.order_flag & (~0x04);
            break;
        case 4:
            value = isOpen ? deviceModel.order_flag | 0x08 : deviceModel.order_flag & (~0x08);
            break;
        case 5:
            value = isOpen ? deviceModel.order_flag | 0x10 : deviceModel.order_flag & (~0x10);
            break;
        case 6:
            value = isOpen ? deviceModel.order_flag | 0x20 : deviceModel.order_flag & (~0x20);
            break;
        case 7:
            value = isOpen ? deviceModel.order_flag | 0x40 : deviceModel.order_flag & (~0x40);
            break;
        case 8:
            value = isOpen ? deviceModel.order_flag | 0x80 : deviceModel.order_flag & (~0x80);
            break;
        case 9:
            value = isOpen ? deviceModel.order_flag2 | 0x01 : deviceModel.order_flag2 &(~0x01);
            break;
        case 10:
            value = isOpen ? deviceModel.order_flag2 | 0x02 : deviceModel.order_flag2 & (~0x02);
            break;
        case 11:
            value = isOpen ? deviceModel.order_flag2 | 0x04 : deviceModel.order_flag2 & (~0x04);
            break;
        case 12:
            value = isOpen ? deviceModel.order_flag2 | 0x08 : deviceModel.order_flag2 & (~0x08);
            break;
        default:
            break;
    }
    
    return value;
}

- (NSInteger)getValueWithIsOpen:(BOOL)isOpen andCacheValue:(NSInteger)value
{
    NSInteger index = self.index - 21;
    NSInteger shift = index > 8 ? index - 9 : index - 1;
    NSInteger result = isOpen ? value | (0x01 << shift) : value & (~(0x01 << shift));
    return result;
}

@end
