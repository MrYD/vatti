//
//  VirtualDeviceModel.h
//  vatti
//
//  Created by 李叶 on 2018/6/14.
//  Copyright © 2018年 xlink. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "WaterHeaterDeviceModel.h"
#import "DishWasherDeviceModel.h"

#define XL_DATA_VIRTUAL [VirtualDeviceModel shareInstance]

@interface VirtualDeviceModel : NSObject

@property (strong, nonatomic) WaterHeaterDeviceModel *deviceModel;
@property (strong, nonatomic) DishWasherDeviceModel *dishWasherModel;
@property (assign, nonatomic) BOOL isLogin;

+ (instancetype)shareInstance;

@end
