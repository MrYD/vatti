//
//  MemberModel.m
//  vatti
//
//  Created by 李叶 on 2018/6/9.
//  Copyright © 2018年 xlink. All rights reserved.
//

#import "MemberModel.h"
#import <MJExtension/MJExtension.h>

@implementation MemberModel

+ (NSArray *)mj_allowedPropertyNames {
    return @[@"invite_code", @"from_id", @"from_user", @"user_id", @"to_name", @"to_user", @"device_id", @"state",@"create_date",@"expire_date"];
}
- (instancetype)initWithCacheKeyValues:(NSDictionary *)cacheKeyValues {
    self = [super init];
    if (self) {
        [self mj_setKeyValues:cacheKeyValues];
    }
    return self;
}

- (NSMutableDictionary *)cacheKeyValues {
    NSMutableDictionary *cacheKeyValues = self.mj_keyValues;
    return cacheKeyValues;
}
@end
