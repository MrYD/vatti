//
//  VirtualDeviceModel.m
//  vatti
//
//  Created by 李叶 on 2018/6/14.
//  Copyright © 2018年 xlink. All rights reserved.
//

#import "VirtualDeviceModel.h"

@implementation VirtualDeviceModel

+ (instancetype)shareInstance {
    static VirtualDeviceModel *instance;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        instance = [[VirtualDeviceModel alloc] init];
    });
    return instance;
}

- (void)setDeviceModel:(WaterHeaterDeviceModel *)deviceModel{
    deviceModel.temp_env        = 21;
    deviceModel.heater_temp     = 42;
    deviceModel.current_water   = 0;
    deviceModel.total_water     = 0;
    deviceModel.current_gas     = 0;
    deviceModel.total_gas       = 0;
    deviceModel.warnning_status = 0;
    deviceModel.function_flag   = 0;
    deviceModel.surplus_water   = 15;
    deviceModel.diff_temp       = 5;
    deviceModel.keep_time       = 0;
    deviceModel.order_flag      = 0;
    deviceModel.order_flag2     = 0;
    deviceModel.order1          = 0x173B;
    deviceModel.bath1_water     = 15;//默认水量
    deviceModel.priority_flag   = 0;
    deviceModel.bath_switch     = 1;
    deviceModel.connected       = YES;
    
    _deviceModel = deviceModel;
}

- (void)setDishWasherModel:(DishWasherDeviceModel *)dishWasherModel{
    
//    dishWasherModel.manufacture_id = 0;
//    dishWasherModel.sw_ver = 0;
//    dishWasherModel.hw_ver = 0;
//    dishWasherModel.control_id = 0;
//    dishWasherModel.priority = 0;
    dishWasherModel.productId = @"1607d2b688d41f411607d2b688d47a01";
    dishWasherModel.control_cmd = 1;
    dishWasherModel.switch_func = 1;
//    dishWasherModel.func_time = 0;
//    dishWasherModel.func_mode = 0;
//    dishWasherModel.water_gears = 0;
//    dishWasherModel.act_switch = 0;
//    dishWasherModel.order_time = 0;
    dishWasherModel.dev_statu = 2;
    dishWasherModel.running_flag = 1;
//    dishWasherModel.func_step = 0;
//    dishWasherModel.Remaining_Time = 0;
//    dishWasherModel.curr_temp = 0;
//    dishWasherModel.func_temp = 0;
//    dishWasherModel.err_code = 0;
//    dishWasherModel.last_func = 0;
    dishWasherModel.connected = YES;
    
    _dishWasherModel = dishWasherModel;
}

@end
