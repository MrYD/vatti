//
//  XLScanDeviceHelperTool.h
//  vatti
//
//  Created by 李叶 on 2018/6/1.
//  Copyright © 2018年 xlink. All rights reserved.
//

/**
 *  扫描设备工具类
 *
 *  @author BZH
 */

#import <Foundation/Foundation.h>
#import "XLinkScanDeviceTask.h"

@class XDevice;
@class XLScanDeviceHelperTool;

@protocol XLScanDeviceHelperToolDelegate <NSObject>

/**
 扫描到设备
 */
- (void)xlScanDeviceHelperTool:(XLScanDeviceHelperTool*)tool scanDevicesSuccssfully:(NSMutableArray <XDevice *> *)devices;

/**
 扫描不到设备
 */
- (void)xlScanDeviceHelperTool:(XLScanDeviceHelperTool*)tool scanDevicesFailedWithError:(NSError*)error;

/**
 扫描完成
 */
- (void)xlScanDeviceHelperToolScranFinished:(XLScanDeviceHelperTool*)tool;

@end

@interface XLScanDeviceHelperTool : NSObject

@property (strong, nonatomic) NSMutableArray <XDevice *>*devicesArray;

@property (strong, nonatomic) XLinkScanDeviceTask *task;

@property (weak,   nonatomic) id<XLScanDeviceHelperToolDelegate> delegate;

/**
 生成扫描task(任务)
 */
- (void)createScanTaskWithProductIds:(NSArray *)productIds;

/**
 取消扫描task(任务)
 */
- (void)cancelTask;

@end
