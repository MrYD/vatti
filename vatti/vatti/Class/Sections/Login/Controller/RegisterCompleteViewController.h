//
//  RegisterCompleteViewController.h
//  vatti
//
//  Created by 许瑞邦 on 2018/5/11.
//  Copyright © 2018年 xlink. All rights reserved.
//

#import "BaseViewController.h"

@interface RegisterCompleteViewController : BaseViewController

///提示文本
@property (copy,   nonatomic) NSString *successString;

@end
