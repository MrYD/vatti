//
//  LoginViewController.m
//  vatti
//
//  Created by BZH on 2018/4/24.
//  Copyright © 2018年 xlink. All rights reserved.
//

#import <UMShare/UMShare.h>
#import "LoginViewController.h"
#import "RegisterViewController.h"
#import "UITextField+XLCategory.h"
#import "XLUtilTool.h"
#import "XLTextField.h"
#import "MainTabViewController.h"
#import "FindPasswordViewController.h"
#import "XLServiceManager.h"
#import "AppDelegate+ThirdPart.h"
#import "CommonTopSectionView.h"
@interface LoginViewController ()

//@property (weak,   nonatomic) IBOutlet XLTextField *tfAccount;
//@property (weak,   nonatomic) IBOutlet XLTextField *tfPassword;
//
//@property (weak,   nonatomic) IBOutlet UIButton *btnLogin;
//@property (weak,   nonatomic) IBOutlet UIButton *btnForget;
//@property (weak,   nonatomic) IBOutlet UIButton *btnRegist;
//@property (weak, nonatomic) IBOutlet UILabel *descriptionLabel;
@property (strong, nonatomic)UIButton *btnForget;
@property (strong, nonatomic)UIButton *btnRegist;
@property (strong, nonatomic)CommonTopSectionView *topSectionView;
@property (strong, nonatomic)UIView * bottomSectionView;
@property (strong, nonatomic)UIView *otherLoginLeftView;
@property (strong, nonatomic)UIView *otherLoginRightView;
@property (strong, nonatomic)UILabel *otherLoginCenterLabel;
@property (nonatomic,strong) UIButton *WXBtn;
@property (nonatomic,strong) UIButton *SinaBtn;
@property (assign, nonatomic)XLUsingType usingType;


///友盟返回的第三方登录信息对象
@property (strong, nonatomic) UMSocialUserInfoResponse *response;
///调用通过第三方登录信息调用openApi返回的userID
@property (strong, nonatomic) NSNumber *userID;

@end

@implementation LoginViewController

#pragma mark - LifeCycle
- (void)viewDidLoad {
    [super viewDidLoad];
    
}

//视图将要显示时隐藏
-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [self.navigationController.navigationBar setShadowImage:[UIImage new]];
}

//视图将要消失时取消隐藏
-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [self.navigationController.navigationBar setShadowImage:nil];
}

- (void)initSubViews
{
    [super initSubViews];
    self.view.backgroundColor = [UIColor whiteColor];
    //navigation
//    self.navigationItem.title = @"华帝·智尚心居";
    UIView *leftBarView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 130, 44)];
    UIButton *cancleBtn = [[UIButton alloc] initWithFrame:CGRectMake(0, 10, 24, 24)];
    [cancleBtn setImage:[UIImage imageNamed:@"icon_back_tab_black"] forState:UIControlStateNormal];
    [cancleBtn addTarget:self action:@selector(clickCancleBtn:) forControlEvents:UIControlEventTouchUpInside];
    UIButton *logoBtn = [[UIButton alloc] initWithFrame:CGRectMake(25, 10, 105, 21)];
    [logoBtn setImage:[UIImage imageNamed:@"img_logo_01"] forState:UIControlStateNormal];
    [leftBarView addSubview:cancleBtn];
    [leftBarView addSubview:logoBtn];
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:leftBarView];
    
    UIView *rightBarView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 100, 44)];
    UIButton *PWBtn = [[UIButton alloc] initWithFrame:CGRectMake(20, 10, 80, 21)];
    [PWBtn setTitle:@"密码登录" forState:UIControlStateNormal];
    self.usingType = XLUsingTypeDefault;
    [PWBtn setTitleColor:HexRGBA(0x000000, 0.6) forState:UIControlStateNormal];
    [PWBtn.titleLabel setFont:[UIFont fontWithName:@"PingFangSC-Regular" size:15]];
    [PWBtn.titleLabel setTextAlignment:NSTextAlignmentCenter];
    [PWBtn addTarget:self action:@selector(clickPWBtn:) forControlEvents:UIControlEventTouchUpInside];
    [rightBarView addSubview:PWBtn];
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:rightBarView];
    //头部视图
    self.topSectionView = [[CommonTopSectionView alloc] initWithFrame:CGRectZero usingType:XLUsingTypeDefault];
    //默认显示上次登录的用户名
    self.topSectionView.tfAccount.text = [XLUtilTool lastAccount];
    [self.view addSubview:_topSectionView];
    
    //底部视图
    self.bottomSectionView = [[UIView alloc] init];
    
    self.btnRegist = [UIButton buttonWithType:UIButtonTypeCustom];
    [_btnRegist setTitle:@"注册新用户" forState:UIControlStateNormal];
    [_btnRegist addTarget:self action:@selector(clickRegisterText:) forControlEvents:UIControlEventTouchUpInside];
    [_btnRegist.titleLabel setFont:[UIFont fontWithName:@"PingFangSC-Regular" size:15]];
    [self.btnRegist setTitleColor:HexRGBA(0x000000, 0.6) forState:UIControlStateNormal];
    [_bottomSectionView addSubview:_btnRegist];
    
    self.btnForget = [UIButton buttonWithType:UIButtonTypeCustom];
    [_btnForget setTitle:@"忘记密码" forState:UIControlStateNormal];
    [_btnForget.titleLabel setFont:[UIFont fontWithName:@"PingFangSC-Regular" size:15]];
    [self.btnForget setTitleColor:HexRGBA(0x000000, 0.6) forState:UIControlStateNormal];
    [_bottomSectionView addSubview:_btnForget];
    
    self.otherLoginLeftView = [[UIView alloc] init];
    _otherLoginLeftView.backgroundColor = HexRGBA(0x000000, 0.1);
    [_bottomSectionView addSubview:_otherLoginLeftView];
    
    self.otherLoginRightView = [[UIView alloc] init];
    _otherLoginRightView.backgroundColor = HexRGBA(0x000000, 0.1);
    [_bottomSectionView addSubview:_otherLoginRightView];
    
    self.otherLoginCenterLabel = [[UILabel alloc] init];
    _otherLoginCenterLabel.text = @"其他方式登录";
    _otherLoginCenterLabel.textColor = HexRGBA(0x000000, 0.3);
    _otherLoginCenterLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size:14];
    [_bottomSectionView addSubview:_otherLoginCenterLabel];
    
    self.WXBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [_WXBtn setImage:[UIImage imageNamed:@"icon_login_wechat"] forState:UIControlStateNormal];
    [_bottomSectionView addSubview:_WXBtn];
    
    self.SinaBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [_SinaBtn setImage:[UIImage imageNamed:@"icon_login_sina"] forState:UIControlStateNormal];
    [_bottomSectionView addSubview:_SinaBtn];
    
    
    [self.view addSubview:_bottomSectionView];
    [self setSubViewsConstraints];
    
    [self setUpCheckVertityClickEvents];
}
///添加验证数据事件
- (void)setUpCheckVertityClickEvents {
    [self.topSectionView.btnLogin addTarget:self action:@selector(btnLoginClicked:) forControlEvents:UIControlEventTouchUpInside];
    [self.SinaBtn addTarget:self action:@selector(QQAction:) forControlEvents:UIControlEventTouchUpInside];
    [self.WXBtn addTarget:self action:@selector(wechatAction:) forControlEvents:UIControlEventTouchUpInside];
}


///
- (void)clickRegisterText:(UIButton *)button {
//    self.usingType = XLUsingTypeRegister;
    
//    self.navigationItem.rightBarButtonItem.customView.hidden = YES;
//    button.hidden = YES;
//    self.btnForget.hidden = YES;
//    [self.topSectionView changeUIByUsingType:self.usingType];
    RegisterViewController *registerVC = [[RegisterViewController alloc] init];
    registerVC.usingType = XLUsingTypeRegister;
    [self.navigationController pushViewController:registerVC animated:YES];
}

///返回主页面
- (void)clickCancleBtn:(UIButton *)button {
    [self pushToMainTabBarViewController];
}

///
- (void)clickPWBtn:(UIButton *)button {
    switch (self.usingType) {
        case XLUsingTypeDefault:
        {
            [button setTitle:@"验证码登录" forState:UIControlStateNormal];
            self.usingType = XLUsingTypePasswordLogin;
        }
            break;
        case XLUsingTypePasswordLogin:
        {
            [button setTitle:@"密码登录" forState:UIControlStateNormal];
            self.usingType = XLUsingTypeDefault;
        }
            break;
            
        default:
            break;
    }
    [self.topSectionView changeUIByUsingType:self.usingType];
}


///子视图添加约束
- (void)setSubViewsConstraints {

    [self.topSectionView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.view).offset(45);
        make.width.mas_equalTo(self.view);
        make.left.mas_equalTo(self.view);
    }];

    [self.bottomSectionView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.topSectionView.mas_bottom);
        make.width.mas_equalTo(self.view);
        make.left.mas_equalTo(self.view);
        make.bottom.mas_equalTo(self.view.mas_bottom);
    }];
    
    [self.btnRegist mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.bottomSectionView.mas_left).offset(20);
        make.width.mas_equalTo(75);
        make.top.mas_equalTo(self.bottomSectionView.mas_top).offset(6);
        make.height.mas_equalTo(21);
    }];
    
    [self.btnForget mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(self.bottomSectionView.mas_right).offset(-20);
        make.width.mas_equalTo(64);
        make.top.mas_equalTo(self.bottomSectionView.mas_top).offset(6);
        make.height.mas_equalTo(21);
    }];
    
    [self.otherLoginCenterLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(self.bottomSectionView.mas_centerX);
        make.top.mas_equalTo(self.bottomSectionView.mas_top).offset(177.5);
        make.height.mas_equalTo(20);
//        make.width.mas_equalTo(92);
    }];
    
    [self.otherLoginLeftView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.bottomSectionView.mas_left).offset(16);
        make.right.mas_equalTo(self.otherLoginCenterLabel.mas_left).offset(-15.5);
        make.height.mas_equalTo(1);
        make.centerY.mas_equalTo(self.otherLoginCenterLabel);
    }];
    
    [self.otherLoginRightView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(self.bottomSectionView.mas_right).offset(-16);
        make.left.mas_equalTo(self.otherLoginCenterLabel.mas_right).offset(15.5);
        make.height.mas_equalTo(1);
        make.centerY.mas_equalTo(self.otherLoginCenterLabel);
    }];
    
    [self.WXBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.otherLoginCenterLabel.mas_bottom).offset(21.5);
        make.left.mas_equalTo(self.bottomSectionView.mas_left).offset(108);
        make.height.width.mas_equalTo(48);
    }];
    [self.SinaBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.otherLoginCenterLabel.mas_bottom).offset(21.5);
        make.right.mas_equalTo(self.bottomSectionView.mas_right).offset(-108);
        make.height.width.mas_equalTo(48);
    }];

    
}




- (void)buildData
{
    [super buildData];
    
    @weakify(self)
    
    //RAC
    
    //账号
    [[self.topSectionView.tfAccount rac_signalForControlEvents:UIControlEventEditingDidEndOnExit] subscribeNext:^(__kindof UIControl * _Nullable x) {
        @strongify(self)
        [self.topSectionView.tfPassword becomeFirstResponder];
    }];
    
    //密码
    [[self.topSectionView.tfPassword rac_signalForControlEvents:UIControlEventEditingDidEndOnExit] subscribeNext:^(__kindof UIControl * _Nullable x) {
        [x resignFirstResponder];
    }];

//    [self.topSectionView.tfPassword addShowPasswordButton];

    //observeSelectedState
    [self.topSectionView.tfAccount observeSelectedState];
    [self.topSectionView.tfPassword observeSelectedState];
    
}

#pragma mark - Data
- (void)loginSuccess:(XLUserModel *)userModel {
    
    KHideLoading;
    [XLUtilTool setAutoLogin:YES];
    [XLUtilTool setLastAccount:userModel.account];
    [XLUtilTool setlastLoginAuthorizationInfo:[[XLinkSDK share].userModel getAuthorizeDict]];
    
    [userModel updateCache];
    
    XL_DATA_SOURCE.user = userModel;
    XL_DATA_SOURCE.userState = XLUserStateOnline;
    
    [self pushToMainTabBarViewController];
}

/**
 错误处理
 */
- (void)failWithError:(NSError *)error {
    NSInteger errorCode = error.code;
    
    XLLog(@"-- 登录错误码 -- %ld", (long)errorCode);
    
    NSString *message = @"网络连接失败，请检查网络是否正常";
    
    if (errorCode == XLinkErrorCodeApiAccountPasswordError || errorCode == XLinkErrorCodeApiUserNotExists) {
        message = @"账号或密码错误";
    }else if(errorCode == XLinkErrorCodeApiAccountPasswordRetryLimit){
        message = @"密码错误次数过多，请5分钟后再试";
    }
    [self alertHintWithMessage:message];
}

/**
 处理获取到的用户详细信息
 */
- (void)configureUserInfoDict:(NSDictionary*)dict
{
    NSString *phone = [dict objectForKey:@"phone"];//绑定的手机号码
    NSString *passwdInited = [dict objectForKey:@"passwd_inited"];//是否初始化密码
    
    BOOL b1 = phone.length >  0;
    BOOL b2 = passwdInited.boolValue;
    
    if (b1 && b2) {
        //有绑定,继续处理第三方登录操作
        [self configureLoginWithInfoDict:dict];
        
    }else{
        //无绑定,跳转到绑定界面
        [self pushToRegisterViewController];
    }
}

/**
 继续处理第三方登录操作
 */
- (void)configureLoginWithInfoDict:(NSDictionary*)infoDict {
    XLUserModel *userModel = [[XLUserModel alloc] init];
    userModel.account  = [infoDict objectForKey:@"phone"];
    userModel.userId   = [infoDict objectForKey:@"id"];
    userModel.nickname = [infoDict objectForKey:@"nickname"];
    userModel.avatar   = [infoDict objectForKey:@"avatar"];
    [self loginSuccess:userModel];
}

#pragma mark - Action

/**
 清除账号
 */
- (IBAction)clearAction:(UIButton *)sender {
    self.topSectionView.tfAccount.text = @"";
}

/**
 忘记密码
 */
- (IBAction)btnForgetClicked:(UIButton *)sender {
    [self.view endEditing:YES];
    
    FindPasswordViewController * find = [[UIStoryboard storyboardWithName:@"Login" bundle:nil]instantiateViewControllerWithIdentifier:@"FindPasswordViewController"];
    [self.navigationController pushViewController:find animated:YES];
}

- (void)btnLoginClicked:(UIButton *)sender {
    [self.view endEditing:YES];
    
    BOOL b1 = [XLUtilTool validatePhone:self.topSectionView.tfAccount.text];
    BOOL b2 = self.topSectionView.tfPassword.text.length < 6 || self.topSectionView.tfPassword.text.length > 16;
    
    NSString *errMsg = nil;
    if (b1 == NO) {
        errMsg = @"手机号码格式有误,请重试";
        [self alertHintWithMessage:errMsg];
        return;
    }else if (b2 == YES) {
        if (!self.topSectionView.tfPassword.hidden) {
            errMsg = @"请输入6-16位密码";
            [self alertHintWithMessage:errMsg];
            return;
        }
    }
    
    [self loginWithAccount];
}

/**
 注册
 */
- (IBAction)didClickRegisterBtn:(id)sender {
    
    [self.view endEditing:YES];
    
    RegisterViewController* registerVC = [[UIStoryboard storyboardWithName:@"Login" bundle:nil]instantiateViewControllerWithIdentifier:@"RegisterViewController"];
    [self.navigationController pushViewController:registerVC animated:YES];
}

/**
 qq登录
 */
- (void)QQAction:(UIButton *)sender {
    [self.view endEditing:YES];
    
    [self getInfoWithPlatform:UMSocialPlatformType_QQ];
}

/**
 微信登录
 */
- (IBAction)wechatAction:(UIButton *)sender {
    [self.view endEditing:YES];
    
    [self getInfoWithPlatform:UMSocialPlatformType_WechatSession];
}

/**
 跳转到绑定手机页面
 */
- (void)pushToRegisterViewController
{
//    RegisterViewController *vc = [[UIStoryboard storyboardWithName:@"Login" bundle:nil]instantiateViewControllerWithIdentifier:@"RegisterViewController"];
    RegisterViewController *registerVC = [[RegisterViewController alloc] init];
    registerVC.type = RegisterVCTypeBinding;
    registerVC.userID = self.userID;
    registerVC.response = self.response;
    [self.navigationController pushViewController:registerVC animated:YES];
}

- (IBAction)tfEditingChanged:(XLTextField *)sender {

    BOOL b1 = self.topSectionView.tfAccount.text.length > 0;
    BOOL b2 = self.topSectionView.tfPassword.text.length > 0;
    
    if (b1 && b2) {
        self.topSectionView.btnLogin.enabled = YES;
    }else{
        self.topSectionView.btnLogin.enabled = NO;
    }
}

- (void)pushToMainTabBarViewController
{
    MainTabViewController *tabbar = [[MainTabViewController alloc] init];
    [XLUtilTool setRootViewController:tabbar animationOptions:UIViewAnimationOptionTransitionCrossDissolve duration:0.3];
}

#pragma mark - Request

#pragma mark - 账号密码登录相关
- (void)loginWithAccount
{
    @weakify(self)
    
    if ([XLinkSDK share].isStarted == NO) {
        [XL_DATA_SOURCE startSDK];
    }
    
    KShowLoading;
    [[XLServiceManager shareInstance].userService loginWithAccount:self.topSectionView.tfAccount.text password:self.topSectionView.tfPassword.text corpId:XLCorpID timeout:15. handler:^(id result, NSError *error) {
        @strongify(self)
        KHideLoading;
        
        if (error == nil) {
            //userID
            NSNumber *userId = [result objectForKey:@"user_id"];
            
            //创建用户参数模型
            XLUserModel *userModel = [[XLUserModel alloc] init];
            userModel.userId = userId;
            
            //保存用户信息到本地
            NSDictionary *cacheKeyValues = [[XLDatabaseManager instance] cacheWithUserId:userModel.userId];
            if (cacheKeyValues != nil) {
                [userModel setCacheKeyValues:cacheKeyValues];
            }
            
            //设置参数
            userModel.account = self.topSectionView.tfAccount.text;
            
            //获取用户详细信息
            [self getAccountInfomationWithXLUserModel:userModel];
        }else {
            [self failWithError:error];
        }
    }];
}

/**
 获取用户详细信息
 */
- (void)getAccountInfomationWithXLUserModel:(XLUserModel *)userModel{
    @weakify(self)
    
    KShowLoading;
    [[XLServiceManager shareInstance].userService getAccountInfomationWithUserId:userModel.userId timeout:10 handler:^(id result, NSError *error) {
        @strongify(self)
        KHideLoading;
        
        if (error == nil) {
            userModel.nickname = [result objectForKey:@"nickname"];
            userModel.avatar = [result objectForKey:@"avatar"];
            userModel.phone = [result objectForKey:@"phone"];
            userModel.email = [result objectForKey:@"email"];
            
            [self loginSuccess:userModel];
        }else {
            [self failWithError:error];
        }
    }];
}

#pragma mark - 第三方登录相关
/**
 获取第三方平台信息
 */
- (void)getInfoWithPlatform:(UMSocialPlatformType)platform
{
    @weakify(self)

    [[UMSocialManager defaultManager] getUserInfoWithPlatform:platform currentViewController:self completion:^(id result, NSError *error) {
        @strongify(self)
    
        if (error == nil) {
            
            if ([result isKindOfClass:[UMSocialUserInfoResponse class]]) {
                //保存第三方返回的信息
                self.response = result;
                
                //判断sdk开启状态,如果没有开启，则开启
                BOOL isStarted = [XLinkSDK share].isStarted;
                if (isStarted == NO) {
                    AppDelegate *delegate = (AppDelegate*)[UIApplication sharedApplication].delegate;
                    [delegate setupXLSDK];
                }
                
                //根据第三方返回的信息获取用户信息，进而获得UserID
                [self getUserInfoWithUMSocialUserInfoResponse:self.response andPlatform:platform];
            }
            
        }else{
            
        }
    }];
}

/**
 根据第三方返回的信息获取用户信息，进而获得UserID
 */
- (void)getUserInfoWithUMSocialUserInfoResponse:(UMSocialUserInfoResponse*)response andPlatform:(UMSocialPlatformType)platform
{
    @weakify(self)
    
    NSString *resource = @"ios platform";
    
    //登录平台信息
    XLinkUserSourceType sourceType = 0;
    if (platform == UMSocialPlatformType_WechatSession) {
        resource = @"weixin";
        sourceType = XLinkUserSourceTypeWechat;
    }else if (platform == UMSocialPlatformType_QQ) {
        resource = @"qq";
        sourceType = XLinkUserSourceTypeQQ;
    }
    
    KShowLoading;
    [[XLServiceManager shareInstance].userService getUserInfoWithThirdSourceType:sourceType openId:response.openid andAccessToken:response.accessToken andName:response.name andResource:resource andPluginId:nil withCallBack:^(NSDictionary * _Nonnull dic, NSError * _Nullable error) {
        @strongify(self)
        KHideLoading;
        
        if (error == nil) {
            //保存获取到的userID
            self.userID = [dic objectForKey:@"user_id"];
            
            //根据UserID获取用户详细信息
            [self getUserInfoDetailWithUserID:self.userID];
        }
    }];
}

/**
 根据UserID获取用户详细信息
 */
- (void)getUserInfoDetailWithUserID:(NSNumber *)userID
{
    @weakify(self)
    
    KShowLoading;
    [[XLServiceManager shareInstance].userService getUserInfoDetailWithUserID:userID withCallBack:^(NSDictionary * _Nonnull dic, NSError * _Nullable error) {
        @strongify(self)
        KHideLoading;
        
        if (error == nil) {
            
            [self configureUserInfoDict:dic];
        }
    }];
    
}

#pragma mark - OverWrite

- (void)leftBarBtnClicked:(UIBarButtonItem *)leftBarBtn
{
    [self pushToMainTabBarViewController];
}

@end
