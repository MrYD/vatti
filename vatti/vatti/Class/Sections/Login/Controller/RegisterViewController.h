//
//  RegisterViewController.h
//  vatti
//
//  Created by BZH on 2018/5/11.
//  Copyright © 2018年 xlink. All rights reserved.
//

/**
 *  注册账号/绑定手机
 *
 *  @author BZH
 */

#import "BaseViewController.h"
#import <UMShare/UMShare.h>
#import "CommonTopSectionView.h"

typedef NS_ENUM(NSUInteger, RegisterVCType) {
    RegisterVCTypeRegister,
    RegisterVCTypeBinding
};

@interface RegisterViewController : BaseViewController

///控制器类型
@property (assign, nonatomic) RegisterVCType type;

///userID
@property (strong, nonatomic) NSNumber *userID;

@property (strong, nonatomic) UMSocialUserInfoResponse *response;

@property (assign, nonatomic)XLUsingType usingType;
@property (strong, nonatomic) CommonTopSectionView *topSectionView;

@end
