//
//  FindPasswordViewController.m
//  vatti
//
//  Created by 许瑞邦 on 2018/5/11.
//  Copyright © 2018年 xlink. All rights reserved.
//

#import "FindPasswordViewController.h"
#import "XLTextField.h"
#import "UIButton+XLCountTime.h"
#import "UITextField+XLCategory.h"
#import "XLUtilTool.h"

@interface FindPasswordViewController ()

@property (weak, nonatomic) IBOutlet XLTextField *account;
@property (weak, nonatomic) IBOutlet XLTextField *vertify;
@property (weak, nonatomic) IBOutlet XLTextField *password;
@property (weak, nonatomic) IBOutlet XLTextField *passwordAgain;

@property (weak, nonatomic) IBOutlet UIButton *captchaBtn;
@property (weak, nonatomic) IBOutlet UIButton *submitBtn;

@end

@implementation FindPasswordViewController

- (void)viewDidLoad {
    [super viewDidLoad];
}

- (void)initSubViews{
    [super initSubViews];
    
    if (![XLinkSDK share].isStarted) {
        [XL_DATA_SOURCE startSDK];
    }
    
    self.navigationItem.title = XLLocalizeString(@"忘记密码");
    
}

- (void)buildData
{
    [super buildData];
    
    [self addObserveSelectedState];
    
    [self addRAC];
}

#pragma mark - Action
- (void)getVerifyAction {
    [self.view endEditing:YES];
    
    NSString *account = self.account.text;
    if (![XLUtilTool validatePhone:account] ) {
        [self alertHintWithMessage:@"手机号码格式有误，请重试"];
        return;
    }
    
    self.captchaBtn.enabled = NO;
    
    KShowLoading;
    @weakify(self)
    if ([XLUtilTool validatePhone:account]) {
        [[XLServiceManager shareInstance].userService sendResetPasswordVerifyCodeWithPhone:account captcha:nil phoneZone:nil corpId:XLCorpID timeout:10. handler:^(id result, NSError *error) {
            @strongify(self)
            KHideLoading;
            
            if (error == nil) {
                [self.captchaBtn xl_startTime:TimeOutGetVerfyCode title:XLLocalizeString(@"发送验证码") waitTittle:@"已发送"];
            }else {
                self.captchaBtn.enabled = YES;
                
                if(error.code == XLinkErrorCodeApiUserNotExists){
                    [self alertHintWithMessage:@"账号不存在"];
                    return ;
                }
                [self failWithError:error];
            }
        }];
    }
    
}

- (void)nextAction {
    [self.view endEditing:YES];
    
    NSString *account = self.account.text;
    NSString *password = self.password.text;
    NSString *passwordAgain = self.passwordAgain.text;
    NSString *verify = self.vertify.text;
    
    if(![XLUtilTool validatePhone:account]){
        [self alertHintWithMessage:@"手机号码格式有误，请重试"];
        return;
    }else if (verify.length != 6) {
        [self alertHintWithMessage:@"验证码有误，请重试"];
        return;
    }else if (password.length < 6 || password.length > 16 || passwordAgain.length < 6 || passwordAgain.length > 16) {
        [self alertHintWithMessage:@"请输入6-16位密码"];
        return;
    }else if (![passwordAgain isEqualToString:password]){
        [self alertHintWithMessage:@"两次输入密码不一致"];
        return;
    }
    if(![XLUtilTool validateBlankSpaceString:password]){
        [self alertHintWithMessage:@"请输入6-16位密码"];
        return;
    }
    
    KShowLoading;
    @weakify(self)
    if ([XLUtilTool validatePhone:account]) {
        [[XLServiceManager shareInstance].userService resetAccountPasswordWithPhone:account verifyCode:verify password:password corpId:XLCorpID phoneZone:nil timeout:10. handler:^(id result, NSError *error) {
            @strongify(self)
            KHideLoading;
            
            if (error == nil) {
                [self.navigationController popToRootViewControllerAnimated:YES];
            }else {
                [self failWithError:error];
            }
        }];
    }
    
}

- (IBAction)clearAction:(UIButton *)sender {
    self.account.text = @"";
}

#pragma mark - Data
- (void)failWithError:(NSError *)error  {
    NSLog(@" --- 重置密码错误码 --- %ld", error.code);
    NSString *message;
    if (error.code == XLinkErrorCodeApiUserNotExists) {
        NSString *account = self.account.text;
        if ([account containsString:@"@"]) {
            message = @"邮箱未被注册，请注册";
        }else if ([XLUtilTool validateNSNumber:account] && account.length == 11) {
            message = @"手机号码未被注册，请注册";
        }else {
            message = @"账号未被注册，请注册";
        }
    }else if (error.code == 4001031) {
        message = @"手机号码未被注册，请注册";
    }else if (error.code == 4041019 || error.code == 4001032) {
        message = @"邮箱未被注册，请注册";
    }else if (error.code == 4001052) {
        message = @"该手机今日发送短信的次数已达上限";
    }else if (error.code == 4001154) {
        message = @"需要输入图形验证码";
    }else if (error.code == 4001155) {
        message = @"请输入正确的图形验证码";
    }else if (error.code == XLinkErrorCodeApiPhoneVerifycodeError || error.code == XLinkErrorCodeApiPhoneVerifycodeNotExists) {
        message = @"验证码有误，请重试";
    }else {
        message = @"网络连接失败，请检查网络是否正常";
    }
    [self alertHintWithMessage:message];
}

- (void)addObserveSelectedState
{
    [self.account observeSelectedState];
    [self.password addShowPasswordButton];
    [self.password observeSelectedState];
    [self.passwordAgain addShowPasswordButton];
    [self.passwordAgain observeSelectedState];
    [self.vertify observeSelectedState];
}

- (void)addRAC
{
    @weakify(self)

#pragma mark - Enable
    RAC(self.submitBtn, enabled) = [RACSignal combineLatest:@[
                                                              self.account.rac_textSignal,
                                                              self.password.rac_textSignal,
                                                              self.vertify.rac_textSignal
                                                              ]
                                                     reduce:^(NSString *account, NSString *password, NSString *verify){
                                                         return @(account.length && password.length && verify.length);
                                                     }];
    
#pragma mark - UIControlEventEditingChanged
    [[self.account rac_signalForControlEvents:UIControlEventEditingChanged] subscribeNext:^(__kindof UIControl * _Nullable x) {
        BOOL b1 = self.account.text.length > 0;
        BOOL b2 = [self.captchaBtn.titleLabel.text isEqualToString:@"发送验证码"];
        self.captchaBtn.enabled = (b1 && b2);
    }];

#pragma mark - UIControlEventEditingDidEndOnExit
    [[self.account rac_signalForControlEvents:UIControlEventEditingDidEndOnExit] subscribeNext:^(__kindof UIControl * _Nullable x) {
        @strongify(self)
        [self.vertify becomeFirstResponder];
    }];
    
    [[self.vertify rac_signalForControlEvents:UIControlEventEditingDidEndOnExit] subscribeNext:^(__kindof UIControl * _Nullable x) {
        @strongify(self)
        [self.password becomeFirstResponder];
    }];
    
    [[self.password rac_signalForControlEvents:UIControlEventEditingDidEndOnExit] subscribeNext:^(__kindof UIControl * _Nullable x) {
        @strongify(self)
        [self.passwordAgain becomeFirstResponder];
    }];
    
#pragma mark - UIControlEventTouchUpInside
    [[self.captchaBtn rac_signalForControlEvents:UIControlEventTouchUpInside] subscribeNext:^(__kindof UIControl * _Nullable x) {
        @strongify(self)
        [self getVerifyAction];
    }];
    
    [[self.submitBtn rac_signalForControlEvents:UIControlEventTouchUpInside] subscribeNext:^(__kindof UIControl * _Nullable x) {
        @strongify(self)
        [self nextAction];
    }];
}


@end
