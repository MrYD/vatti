//
//  RegisterViewController.m
//  vatti
//
//  Created by BZH on 2018/5/11.
//  Copyright © 2018年 xlink. All rights reserved.
//

#import "RegisterViewController.h"
#import "XLTextField.h"
#import "UITextField+XLCategory.h"
#import "XLUtilTool.h"
#import "UIButton+XLCountTime.h"
#import "RegisterCompleteViewController.h"

@interface RegisterViewController ()

//@property (weak, nonatomic) IBOutlet XLTextField *account;
//@property (weak, nonatomic) IBOutlet XLTextField *verify;
//@property (weak, nonatomic) IBOutlet XLTextField *password;
//@property (weak, nonatomic) IBOutlet XLTextField *passwordAgain;
//
//@property (weak, nonatomic) IBOutlet UIButton *captchaBtn;
//@property (weak, nonatomic) IBOutlet UIButton *submitBtn;
//
@end
//
@implementation RegisterViewController
//
//- (void)viewDidLoad {
//    [super viewDidLoad];
//}
//
//- (void)initSubViews{
//    [super initSubViews];
//    
//    //navi
//    if (self.type == RegisterVCTypeBinding) {
//        self.navigationItem.title = @"绑定手机";
//    }else{
//        self.navigationItem.title = @"注册账号";
//    }
//    
//    //tf
//    [self.account becomeFirstResponder];
//}
//
//- (void)buildData
//{
//    [super buildData];
//    
//    //startSDK
//    if ([XLinkSDK share].isStarted == NO) {
//        [XL_DATA_SOURCE startSDK];
//    }
//    
//    //add observeSelectedState
//    [self addObserveSelectedState];
//    
//    //enable
//    [self addRacObserveEnable];
//    
//    //event
//    [self addRacEnent];
//}
//
//- (void)addObserveSelectedState
//{
//    [self.account observeSelectedState];
//    [self.password addShowPasswordButton];
//    [self.passwordAgain addShowPasswordButton];
//    [self.password observeSelectedState];
//    [self.passwordAgain observeSelectedState];
//    [self.verify observeSelectedState];
//}
//
//- (void)addRacObserveEnable
//{
//    RAC(self.submitBtn, enabled) = [RACSignal combineLatest:@[self.account.rac_textSignal, self.password.rac_textSignal, self.verify.rac_textSignal,self.passwordAgain.rac_textSignal] reduce:^(NSString *account, NSString *password, NSString *verify,NSString *passwordAgain){
//        return @(account.length && password.length && verify.length&&passwordAgain.length);
//    }];
//}
//
//- (void)addRacEnent
//{
//    @weakify(self)
//    
//    [[self.account rac_signalForControlEvents:UIControlEventEditingDidEndOnExit] subscribeNext:^(__kindof UIControl * _Nullable x) {
//        @strongify(self)
//        [self.verify becomeFirstResponder];
//    }];
//    
//    [[self.account rac_signalForControlEvents:UIControlEventEditingChanged] subscribeNext:^(__kindof UIControl * _Nullable x) {
//        @strongify(self)
//        BOOL b1 = self.account.text.length > 0;
//        BOOL b2 = [self.captchaBtn.titleLabel.text isEqualToString:@"发送验证码"];
//        self.captchaBtn.enabled = (b1 && b2);
//    }];
//    
//    [[self.captchaBtn rac_signalForControlEvents:UIControlEventTouchUpInside] subscribeNext:^(__kindof UIControl * _Nullable x) {
//        @strongify(self)
//        [self getVerifyAction];
//    }];
//    
//    [[self.submitBtn rac_signalForControlEvents:UIControlEventTouchUpInside] subscribeNext:^(__kindof UIControl * _Nullable x) {
//        @strongify(self)
//        [self nextAction];
//    }];
//}
//
//#pragma mark - Action
//
//- (IBAction)clearAction:(UIButton *)sender {
//    self.account.text = @"";
//}
//
///**
// 获取验证码
// */
//- (void)getVerifyAction {
//    [self.view endEditing:YES];
//    
//    NSString *account = self.account.text;
//    if ([XLUtilTool validatePhone:account] == NO) {
//        [self alertHintWithMessage:@"手机号码格式有误，请重试"];
//        return;
//    }
//    
//    self.captchaBtn.enabled = NO;
//    
//    if (self.type == RegisterVCTypeBinding) {
//        [self requestBindVerfyCode];
//    }else{
//        [self requestRegisterVerfyCode];
//    }
//    
//}
//
///**
// 提交
// */
//- (void)nextAction {
//    [self.view endEditing:YES];
//    
//    NSString *account = self.account.text;
//    NSString *verify = self.verify.text;
//    NSString *password = self.password.text;
//    NSString *passwordAgain = self.passwordAgain.text;
//    
//    if([XLUtilTool validatePhone:account] == NO){
//        [self alertHintWithMessage:@"手机号码格式有误，请重试"];
//        return;
//    }else if (verify.length != 6) {
//        [self alertHintWithMessage:@"验证码有误，请重试"];
//        return;
//    }else if (password.length < 6 || password.length > 16 || passwordAgain.length < 6 || passwordAgain.length > 16) {
//        [self alertHintWithMessage:@"请输入6-16位密码"];
//        return;
//    }else if([XLUtilTool validateBlankSpaceString:password] == NO){
//        [self alertHintWithMessage:@"请输入6-16位密码"];
//        return;
//    }else if ([passwordAgain isEqualToString:password] == NO){
//        [self alertHintWithMessage:@"两次输入密码不一致"];
//        return;
//    }
//    
//    if (self.type == RegisterVCTypeBinding) {
//        [self requestBindPhone];
//    }else{
//        [self requestRegister];
//    }
//}
//
//- (void)failWithError:(NSError *)error {
//    XLLog(@" --- 注册错误码 --- %ld", error.code);
//    NSString *message = @"网络连接失败，请检查网络是否正常";
//    if (error.code == 4001052) {
//        message = @"该手机今日发送短信的次数已达上限";
//    }else if (error.code == 4001154) {
//        message = @"该手机今日发送短信的次数已达上限";
//    }else if (error.code == 4001155) {
//        message = @"该手机今日发送短信的次数已达上限";
//    }else if (error.code == 4001004 || error.code == 4001003 || error.code == 4001028 || error.code == 4001029) {
//        message = @"验证码有误，请重试";
//    }if (error.code==4001005) {
//        message = @"该手机号码已被注册";
//    }else if (error.code == XLinkErrorCodeApiUserNotExists) {
//        message = @"账号已被注册，请登录";
//    }else if (error.code == XLinkErrorCodeApiRegisterThirdpartyPhoneExists){
//        message = @"该手机号码已被绑定";
//    }
//    
//    [self alertHintWithMessage:message];
//    
//}
//
//- (void)registerSuccessWithAccount: (NSString *)account andPassword:(NSString *)password andNickname: (NSString *)nickname {
//    @weakify(self)
//    
//    [[XLServiceManager shareInstance].userService loginWithAccount:account password:password corpId:XLCorpID timeout:15. handler:^(id result, NSError *error) {
//        
//        @strongify(self)
//        KHideLoading;
//        
//        if (error == nil) {
//            //处理数据
//            NSNumber *userId = [result objectForKey:@"user_id"];
//            XLUserModel *userModel = [[XLUserModel alloc] init];
//            userModel.userId = userId;
//            userModel.account = account;
//            userModel.nickname = nickname;
//            [self loginSuccessWithModel:userModel];
//            
//            //跳转到注册成功页面
//            RegisterCompleteViewController *complete = [[UIStoryboard storyboardWithName:@"Login" bundle:nil]instantiateViewControllerWithIdentifier:@"RegisterCompleteViewController"];
//            [self.navigationController pushViewController:complete animated:YES];
//        }else {
//            [self loginFail];
//        }
//    }];
//}
//
//#pragma mark - Data
//
//- (void)loginSuccessWithModel:(XLUserModel *)userModel{
//    
//    [XLUtilTool setAutoLogin:YES];
//    [XLUtilTool setLastAccount:userModel.account];
//    [XLUtilTool setlastLoginAuthorizationInfo:[[XLinkSDK share].userModel getAuthorizeDict]];
//    
//    [userModel updateCache];
//    XL_DATA_SOURCE.user = userModel;
//    XL_DATA_SOURCE.userState = XLUserStateOnline;
//}
//
//- (void)loginFail {
//    [self.navigationController popToRootViewControllerAnimated:YES];
//}
//
//#pragma mark - Request
//
///**
// 获取注册验证码
// */
//- (void)requestRegisterVerfyCode
//{
//    @weakify(self)
//    
//    KShowLoading;
//    NSString *account = self.account.text;
//    [[XLServiceManager shareInstance].userService sendRegisterVerifyCodeWithPhone:account phoneZone:nil captcha:nil corpId:XLCorpID timeout:10. handler:^(id result, NSError *error) {
//        
//        @strongify(self)
//        
//        KHideLoading;
//        if (error == nil) {
//            [self.captchaBtn xl_startTime:TimeOutGetVerfyCode title:XLLocalizeString(@"发送验证码") waitTittle:XLLocalizeString(@"已发送")];
//        }else {
//            [self failWithError:error];
//            self.captchaBtn.enabled = YES;
//        }
//    }];
//}
//
///**
// 获取绑定手机验证码
// */
//- (void)requestBindVerfyCode
//{
//    @weakify(self)
//    
//    KShowLoading;
//    NSString *account = self.account.text;
//    
//    [[XLServiceManager shareInstance].userService getModifyPhoneSMSVerifycodeWithUserID:self.userID andPhone:account andCaptcha:@"zczaj7" withCallBack:^(NSDictionary * _Nonnull dic, NSError * _Nullable error) {
//        @strongify(self)
//        
//        KHideLoading;
//        
//        if (error == nil) {
//            [self.captchaBtn xl_startTime:TimeOutGetVerfyCode title:@"发送验证码" waitTittle:@"已发送"];
//        }else{
//            [self failWithError:error];
//            self.captchaBtn.enabled = YES;
//            if (error.code == 4001154 || error.code == 4001155) {
//                //获取图片验证码
//                [[XLServiceManager shareInstance].userService getPicVerifyCodeWithUserID:self.userID andPhone:self.account.text withCallBack:^(NSDictionary *dic, NSError *error) {
//                    
//                }];
//            }
//        }
//        
//    }];
//}
//
///**
// 请求绑定手机号码
// */
//- (void)requestBindPhone
//{
//    @weakify(self)
//    
//    NSString *account = self.account.text;
//    NSString *verify = self.verify.text;
//    NSString *password = self.password.text;
//    
//    KShowLoading;
//    [XLinkHttpRequest initPasswordAndBindPhoneWithAccessToken:self.response.accessToken withUserId:self.userID withPhone:account withPhone_Zone:@"" withVerifyCode:verify withPassword:password withNickname:self.response.name didLoadData:^(id result, NSError *err) {
//        
//        @strongify(self)
//        KHideLoading;
//        
//        if (err == nil) {
//            //处理数据
//            NSNumber *userId = self.userID;
//            
//            XLUserModel *userModel = [[XLUserModel alloc] init];
//            userModel.userId = userId;
//            userModel.account = account;
//            [self loginSuccessWithModel:userModel];
//            
//            //跳转到绑定成功页面
//            RegisterCompleteViewController *vc = [[UIStoryboard storyboardWithName:@"Login" bundle:nil]instantiateViewControllerWithIdentifier:@"RegisterCompleteViewController"];
//            vc.successString = @"绑定成功！";
//            [self.navigationController pushViewController:vc animated:YES];
//            
//        }else{
//            [self failWithError:err];
//        }
//    }];
//}
//
///**
// 请求注册账号
// */
//- (void)requestRegister
//{
//    @weakify(self)
//    
//    NSString *account = self.account.text;
//    NSString *verify = self.verify.text;
//    NSString *password = self.password.text;
//    NSString *nickname = [NSString stringWithFormat:@"%@用户", [account stringByReplacingCharactersInRange:NSMakeRange(3, 4) withString:@"****"] ];
//    
//    KShowLoading;
//    [[XLServiceManager shareInstance].userService registerAccountWithPhone:account phoneZone:nil nickname:nickname verifyCode:verify password:password corpId:XLCorpID localLang:nil pluginId:nil timeout:10. handler:^(id result, NSError *error) {
//        
//        @strongify(self)
//        
//        if (error == nil) {
//            [self registerSuccessWithAccount:account andPassword:password andNickname:nickname];
//        }else {
//            KHideLoading;
//            [self failWithError:error];
//        }
//    }];
//}

#pragma mark - LifeCycle
- (void)viewDidLoad {
    [super viewDidLoad];
    
}

////视图将要显示时隐藏
//-(void)viewWillAppear:(BOOL)animated
//{
//    [super viewWillAppear:animated];
//
//    [self.navigationController.navigationBar setShadowImage:[UIImage new]];
//}
//
////视图将要消失时取消隐藏
//-(void)viewWillDisappear:(BOOL)animated
//{
//    [super viewWillDisappear:animated];
//    [self.navigationController.navigationBar setShadowImage:nil];
//}

- (void)initSubViews
{
    [super initSubViews];
    UIView *leftBarView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 130, 44)];
    UIButton *cancleBtn = [[UIButton alloc] initWithFrame:CGRectMake(0, 10, 24, 24)];
    [cancleBtn setImage:[UIImage imageNamed:@"icon_back_tab_black"] forState:UIControlStateNormal];
    [cancleBtn addTarget:self action:@selector(clickCancleBtn:) forControlEvents:UIControlEventTouchUpInside];
    UIButton *logoBtn = [[UIButton alloc] initWithFrame:CGRectMake(25, 10, 105, 21)];
    [logoBtn setImage:[UIImage imageNamed:@"img_logo_01"] forState:UIControlStateNormal];
    [leftBarView addSubview:cancleBtn];
    [leftBarView addSubview:logoBtn];
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:leftBarView];
    
    self.view.backgroundColor = [UIColor whiteColor];
    //头部视图
    self.topSectionView = [[CommonTopSectionView alloc] initWithFrame:CGRectZero usingType:self.usingType];
    [self.view addSubview:self.topSectionView];
    
    [self setSubViewsConstraints];
    [self.topSectionView.btnLogin addTarget:self action:@selector(didClickRegisterBtn:) forControlEvents:UIControlEventTouchUpInside];
    
}

///点击注册按钮
- (void)didClickRegisterBtn:(UIButton *)button {
    if (self.usingType == XLUsingTypeIdentityVerification) {
        self.usingType = XLUsingTypeSettingNewPassword;
        [self.topSectionView changeUIByUsingType:(XLUsingTypeSettingNewPassword)];
    }else if(self.usingType == XLUsingTypeSettingNewPassword){
        //TODO：点击完成注册
        
        
    }else{
        self.usingType = XLUsingTypeIdentityVerification;
        [self.topSectionView changeUIByUsingType:(XLUsingTypeIdentityVerification)];
        [self.topSectionView.vertifyBtn addTarget:self action:@selector(didClickSendVertifyBtn:) forControlEvents:UIControlEventTouchUpInside];
    }
}

///点击发送验证码按钮
- (void)didClickSendVertifyBtn:(UIButton *)button {
    self.topSectionView.descriptionLabel.text = [NSString stringWithFormat:@"验证码已发送至%@",self.topSectionView.tfAccount.text];
}

///返回上一级页面
- (void)clickCancleBtn:(UIButton *)button {
    
    if (self.usingType == XLUsingTypeRegister) {
        [self.navigationController popViewControllerAnimated:YES];
    }else if (self.usingType == XLUsingTypeSettingNewPassword){
        self.usingType = (XLUsingTypeIdentityVerification);
        [self.topSectionView changeUIByUsingType:self.usingType];
    }else{
        self.usingType = (XLUsingTypeRegister);
        [self.topSectionView changeUIByUsingType:self.usingType];
    }
}

///子视图添加约束
- (void)setSubViewsConstraints {
    
    [self.topSectionView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.view).offset(45);
        make.width.mas_equalTo(self.view);
        make.left.mas_equalTo(self.view);
    }];
}




- (void)buildData
{
    [super buildData];
    
    @weakify(self)
    
    //RAC
    
    //账号
    [[self.topSectionView.tfAccount rac_signalForControlEvents:UIControlEventEditingDidEndOnExit] subscribeNext:^(__kindof UIControl * _Nullable x) {
        @strongify(self)
        [self.topSectionView.tfPassword becomeFirstResponder];
    }];
    
    //密码
    [[self.topSectionView.tfPassword rac_signalForControlEvents:UIControlEventEditingDidEndOnExit] subscribeNext:^(__kindof UIControl * _Nullable x) {
        [x resignFirstResponder];
    }];
    
    //    [self.topSectionView.tfPassword addShowPasswordButton];
    
    //observeSelectedState
    [self.topSectionView.tfAccount observeSelectedState];
    [self.topSectionView.tfPassword observeSelectedState];
    
}
















@end
