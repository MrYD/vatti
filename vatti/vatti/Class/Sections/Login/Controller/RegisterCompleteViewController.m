//
//  RegisterCompleteViewController.m
//  vatti
//
//  Created by 许瑞邦 on 2018/5/11.
//  Copyright © 2018年 xlink. All rights reserved.
//

#import "RegisterCompleteViewController.h"
#import "XLUtilTool.h"
#import "MainTabViewController.h"

@interface RegisterCompleteViewController ()

///成功文本
@property (weak, nonatomic) IBOutlet UILabel *labSuccess;

@end

@implementation RegisterCompleteViewController

- (void)viewDidLoad {
    [super viewDidLoad];
}

- (void)initConfigure
{
    [super initConfigure];
    
    self.isNeedBackBtn = NO;
}

- (void)initSubViews
{
    [super initSubViews];
    
    if (self.successString.length > 0) {
        self.labSuccess.text = self.successString;
    }
}

#pragma mark - Action
- (IBAction)complete:(UIButton *)sender {    
    MainTabViewController *tabbar = [[MainTabViewController alloc] init];
    [XLUtilTool setRootViewController:tabbar animationOptions:UIViewAnimationOptionTransitionCrossDissolve duration:0.3];
}
@end
