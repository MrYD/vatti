//
//  CommonTopSectionView.m
//  vatti
//
//  Created by MrYD on 2018/12/24.
//  Copyright © 2018年 xlink. All rights reserved.
//

#import "CommonTopSectionView.h"
#import "UITextField+XLCategory.h"
@interface CommonTopSectionView()
@property (nonatomic, strong)UITapGestureRecognizer *tapG;
@end
@implementation CommonTopSectionView

- (instancetype)initWithFrame:(CGRect)frame usingType:(XLUsingType)usingType {
    self = [super initWithFrame:frame];
    if (self) {
        self.titleLabel = [[UILabel alloc] init];
        _titleLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size:26];
        [self addSubview:_titleLabel];
        
        self.descriptionLabel = [[UILabel alloc] init];
        _descriptionLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size:12];
        _descriptionLabel.textColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.4];
        [self addSubview:_descriptionLabel];

        //用户名
        self.tfAccount = [[XLTextField alloc] init];
        _tfAccount.font = [UIFont fontWithName:@"PingFangSC-Regular" size:15];
//        _tfAccount.textColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.3];
        [_tfAccount addLeftImagePanding:10 withImage:[UIImage imageNamed:@"icon_login_phone"]];
        UIImageView *rightImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"icon_input_box_clear"]];
        _tfAccount.rightView = rightImageView;
        rightImageView.userInteractionEnabled = YES;
        self.tapG = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapClick:)];
        [rightImageView addGestureRecognizer:_tapG];
        _tfAccount.rightViewMode = UITextFieldViewModeWhileEditing;
        
        [self addSubview:_tfAccount];
        //密码
        self.tfPassword = [[XLTextField alloc] init];
        _tfPassword.font = [UIFont fontWithName:@"PingFangSC-Regular" size:15];
        _tfPassword.secureTextEntry = YES;
//        _tfPassword.textColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.3];
        [_tfPassword addLeftImagePanding:10 withImage:[UIImage imageNamed:@"icon_login_password"]];
//        UIImageView *rightImageView2 = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"icon_input_box_eye_01"]];
//        _tfPassword.rightView = rightImageView2;
//        rightImageView2.userInteractionEnabled = YES;
//        UITapGestureRecognizer *tapG2 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapClick2:)];
//        [rightImageView2 addGestureRecognizer:tapG2];
        [_tfPassword addShowPasswordButtonWithShowName:@"icon_input_box_eye_01" hideName:@"icon_login_eye_02"];
        [self addSubview:_tfPassword];
        
        self.btnLogin = [UIButton buttonWithType:UIButtonTypeCustom];
        _btnLogin.layer.cornerRadius = 4;
        _btnLogin.backgroundColor = HexRGBA(0x4A4A4A , 1);
        [self addSubview:_btnLogin];
        [self setSubViewsConstraints];

        [self changeUIByUsingType:usingType];
    }
    return self;
}

- (void)changeUIByUsingType:(XLUsingType)usingType {
    switch (usingType) {
        case XLUsingTypeDefault:
        {
            _titleLabel.text = @"登录";
            _descriptionLabel.text = @"请输入您的手机号码";
            _tfAccount.placeholder = @"请输入手机号码";
            [_btnLogin setTitle:@"登录/注册" forState:UIControlStateNormal];
            _tfPassword.hidden = YES;
            [_btnLogin mas_remakeConstraints:^(MASConstraintMaker *make) {
                make.left.mas_equalTo(self.tfAccount.mas_left);
                make.right.mas_equalTo(self.tfAccount.mas_right);
                make.height.mas_equalTo(45);
                make.top.mas_equalTo(self.tfAccount.mas_bottom).offset(30);
                make.bottom.mas_equalTo(self.mas_bottom).offset(-10);
            }];
        }
            break;
        case XLUsingTypePasswordLogin:
        {
            _tfPassword.hidden = NO;
            _titleLabel.text = @"登录";
            _descriptionLabel.text = @"请输入您的登录账号和密码";
            _tfAccount.placeholder = @"请输入手机号码";
            _tfPassword.placeholder = @"请输入密码";
            [_btnLogin setTitle:@"登录" forState:UIControlStateNormal];
            [_tfPassword removeFromSuperview];
            //密码
            self.tfPassword = [[XLTextField alloc] init];
            _tfPassword.font = [UIFont fontWithName:@"PingFangSC-Regular" size:15];
            _tfPassword.secureTextEntry = YES;
            [_tfPassword addLeftImagePanding:10 withImage:[UIImage imageNamed:@"icon_login_password"]];
            [_tfPassword addShowPasswordButtonWithShowName:@"icon_input_box_eye_01" hideName:@"icon_login_eye_02"];
            [self addSubview:_tfPassword];
            [self.tfPassword mas_makeConstraints:^(MASConstraintMaker *make) {
                make.left.mas_equalTo(self.tfAccount);
                make.right.mas_equalTo(self.tfAccount);
                make.height.mas_equalTo(45);
                make.top.mas_equalTo(self.tfAccount.mas_bottom).offset(20);
                //        make.bottom.mas_equalTo(self).offset(-30);
            }];
            [_btnLogin mas_remakeConstraints:^(MASConstraintMaker *make) {
                make.left.mas_equalTo(self.tfPassword.mas_left);
                make.right.mas_equalTo(self.tfPassword.mas_right);
                make.height.mas_equalTo(45);
                make.top.mas_equalTo(self.tfPassword.mas_bottom).offset(30);
                make.bottom.mas_equalTo(self.mas_bottom).offset(-10);
            }];
        }
            break;
        case XLUsingTypeRegister:
        {
            _titleLabel.text = @"注册";
            _tfAccount.text = @"";
            _tfAccount.placeholder = @"请输入手机号码";
            _descriptionLabel.text = @"请输入您的手机号";
            _tfPassword.hidden = YES;
            [_tfAccount addLeftImagePanding:10 withImage:[UIImage imageNamed:@"icon_login_phone"]];
            UIImageView *rightImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"icon_input_box_clear"]];
            rightImageView.userInteractionEnabled = YES;
            [rightImageView addGestureRecognizer:self.tapG];
            _tfAccount.rightViewMode = UITextFieldViewModeWhileEditing;
            _tfAccount.rightView = rightImageView;
            [_btnLogin mas_remakeConstraints:^(MASConstraintMaker *make) {
                make.left.mas_equalTo(self.tfAccount.mas_left);
                make.right.mas_equalTo(self.tfAccount.mas_right);
                make.height.mas_equalTo(45);
                make.top.mas_equalTo(self.tfAccount.mas_bottom).offset(30);
                make.bottom.mas_equalTo(self.mas_bottom).offset(-10);
            }];
            [_btnLogin setTitle:@"注册" forState:UIControlStateNormal];
            
        }
            break;
        case XLUsingTypeIdentityVerification:
        {
            _titleLabel.text = @"身份验证";
            _tfAccount.placeholder = @"请输入验证码";
            _descriptionLabel.text = @"请输入您收到的验证码";
            [_btnLogin setTitle:@"下一步" forState:UIControlStateNormal];
            _tfPassword.hidden = YES;
            _tfAccount.leftViewMode = UITextFieldViewModeNever;
            UIView * rightView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 86, 45)];
            _tfAccount.rightView = rightView;

            UIView *leftLine = [[UIView alloc] init];
            [rightView addSubview:leftLine];
            leftLine.backgroundColor = HexRGBA(0x000000,0.1);
            [leftLine mas_makeConstraints:^(MASConstraintMaker *make) {
                make.right.mas_equalTo(rightView.mas_left);
                make.centerY.mas_equalTo(rightView.mas_centerY);
                make.width.mas_equalTo(1);
                make.height.mas_equalTo(23);
            }];
        
            self.vertifyBtn = [UIButton buttonWithType:UIButtonTypeCustom];
            [rightView addSubview:_vertifyBtn];
            [_vertifyBtn mas_makeConstraints:^(MASConstraintMaker *make) {
                make.left.mas_equalTo(leftLine.mas_right).offset(9);
                make.centerY.mas_equalTo(leftLine.mas_centerY);
                make.right.mas_equalTo(rightView.mas_right);
                make.height.mas_equalTo(16.5);
            }];
            [_vertifyBtn setTitle:@"获取验证码" forState:UIControlStateNormal];
            [_vertifyBtn setTitleColor:HexRGB(0x4A90E2) forState:UIControlStateNormal];
            [_vertifyBtn.titleLabel setFont:[UIFont fontWithName:@"PingFangSC-Regular" size:12]];
            
            _tfAccount.rightViewMode = UITextFieldViewModeAlways;
            [_btnLogin mas_updateConstraints:^(MASConstraintMaker *make) {
                make.top.mas_equalTo(self.tfAccount.mas_bottom).offset(30);
            }];
        }
            break;
        case XLUsingTypeSettingNewPassword:
        {
            _titleLabel.text = @"设置新密码";
            _tfAccount.placeholder = @"请输入6-18位新密码";
            _descriptionLabel.text = @"请设置至少6位密码，包含数字与字母";
            [_btnLogin setTitle:@"注册" forState:UIControlStateNormal];
            _tfPassword.hidden = YES;
            _tfAccount.leftViewMode = UITextFieldViewModeNever;
            _tfAccount.rightViewMode = UITextFieldViewModeNever;
        }
            break;
        default:
            break;
    }
    
    
}




///
- (void)tapClick:(UITapGestureRecognizer *)gesture {
    _tfAccount.text = @"";
}
///
- (void)tapClick2:(UITapGestureRecognizer *)gesture {
    _tfPassword.secureTextEntry = NO;
}

///子视图添加约束
- (void)setSubViewsConstraints {
    [self.titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.mas_top);
        make.left.mas_equalTo(self).offset(20);
        make.right.mas_equalTo(self).offset(-20);
        make.height.mas_equalTo(36.5);
    }];
    
    [self.descriptionLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.titleLabel);
        make.right.mas_equalTo(self).offset(-20);
        make.height.mas_equalTo(16.5);
        make.top.mas_equalTo(self.titleLabel.mas_bottom).offset(3);
    }];
    
    [self.tfAccount mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.titleLabel);
        make.right.mas_equalTo(self).offset(-20);
        make.height.mas_equalTo(45);
        make.top.mas_equalTo(self.descriptionLabel.mas_bottom).offset(20);
    }];
    
    [self.tfPassword mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.tfAccount);
        make.right.mas_equalTo(self.tfAccount);
        make.height.mas_equalTo(45);
        make.top.mas_equalTo(self.tfAccount.mas_bottom).offset(20);
//        make.bottom.mas_equalTo(self).offset(-30);
    }];
    
    [self.btnLogin mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.tfPassword.mas_left);
        make.right.mas_equalTo(self.tfPassword.mas_right);
        make.height.mas_equalTo(45);
        make.top.mas_equalTo(self.tfPassword.mas_bottom).offset(30);
        make.bottom.mas_equalTo(self.mas_bottom).offset(-10);
    }];
}




@end
