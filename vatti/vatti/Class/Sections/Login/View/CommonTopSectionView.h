//
//  CommonTopSectionView.h
//  vatti
//
//  Created by MrYD on 2018/12/24.
//  Copyright © 2018年 xlink. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "XLTextField.h"
#import "XLUtilTool.h"

typedef NS_ENUM(NSUInteger, XLUsingType) {
    XLUsingTypeDefault,  //验证码登录
    XLUsingTypePasswordLogin,    //密码登录
    XLUsingTypeRegister,    //注册
    XLUsingTypeIdentityVerification,    //身份验证
    XLUsingTypeSettingNewPassword,    //设置新密码
};
@interface CommonTopSectionView : UIView

@property (strong, nonatomic)UILabel *descriptionLabel;
@property (strong, nonatomic)UILabel *titleLabel;
@property (strong, nonatomic)XLTextField *tfAccount;
@property (strong, nonatomic)XLTextField *tfPassword;
@property (strong, nonatomic)UIButton *btnLogin;
@property (strong, nonatomic)UIButton *vertifyBtn;
- (instancetype)initWithFrame:(CGRect)frame usingType:(XLUsingType)usingType;

- (void)changeUIByUsingType:(XLUsingType)usingType;

@end
