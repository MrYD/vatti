//
//  BaseViewController.h
//  vatti
//
//  Created by BZH on 2018/4/23.
//  Copyright © 2018年 xlink. All rights reserved.
//

/**
 *  控制器基类
 *
 *  @author BZH
 */

#import <UIKit/UIKit.h>
#import "UIViewController+UIAlertController.h"
#import "UIViewController+Hud.h"

@interface BaseViewController : UIViewController

#pragma mark - 属性

///是否链接网络
@property (nonatomic, assign) BOOL isConnectNetwork;

///是否需要导航条返回按钮
@property (assign, nonatomic) BOOL isNeedBackBtn;

#pragma mark - 供重写方法

/**
 导航条左按钮点击
 */
- (void)leftBarBtnClicked:(UIBarButtonItem*)leftBarBtn;

/**
 初始化导航条
 */
- (void)setupNavi;

#pragma mark - 供调用方法

/**
 设置view相关
 */
- (void)initSubViews;

/**
 设置对象参数初始化相关
 */
- (void)initConfigure;

/**
 设置数据相关
 */
- (void)buildData;

@end
