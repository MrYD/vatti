//
//  BaseViewController.m
//  vatti
//
//  Created by BZH on 2018/4/23.
//  Copyright © 2018年 xlink. All rights reserved.
//

#import "BaseViewController.h"
#import "LoginViewController.h"
#import "Reachability.h"
#import "XLinkHttpRequest.h"
#import "XLNetworkModule.h"
#import "StartPageViewController.h"
#import "AppDelegate.h"
#import <MBProgressHUD/MBProgressHUD.h>

@interface BaseViewController ()

///hud
@property (nonatomic, strong) MBProgressHUD *hud;

@end

@implementation BaseViewController

#pragma mark - LifeCycle

- (void)viewDidLoad {
    [super viewDidLoad];
    
    //设置对象参数初始化相关
    [self initConfigure];
    
    //设置view相关
    [self initSubViews];
    
    //设置数据相关
    [self buildData];
}

/**
 设置view相关
 */
- (void)initSubViews {
    //让只有view的页面，不自动适配全屏，不会被tabbar阻挡
    self.edgesForExtendedLayout = UIRectEdgeNone;
    
    //让隐藏导航条的页面布局从 (0,0)开始
    //    [self setAutomaticallyAdjustsScrollViewInsets:NO];
    
    //navi
    [self setupNavi];
}

/**
 设置对象参数初始化相关
 */
- (void)initConfigure
{
    self.isNeedBackBtn = YES;
}

/**
 设置数据相关
 */
- (void)buildData {
    
    [self setIsConnectNetwork];
    
    //监控网络
    [kXLNotificationCenter addObserver:self selector:@selector(reachabilityChanged:) name:kReachabilityChangedNotification object: nil];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    NSString *vcName = NSStringFromClass([self class]);
    NSLog(@"当前控制器：%@",vcName);
}

- (void)dealloc {
    XLLog(@"%s", __func__);
    
    [kXLNotificationCenter removeObserver:self];
}

#pragma mark - Data

- (void)reachabilityChanged:(NSNotification *)no{
    
    Reachability* curReach = [no object];
    
    NSParameterAssert([curReach isKindOfClass: [Reachability class]]);
    
    NetworkStatus status = [curReach currentReachabilityStatus];
    [self setIsConnectNetworkWithStatus:status];
    
}

- (void)setIsConnectNetwork
{
    AppDelegate *delegate = (AppDelegate*)[UIApplication sharedApplication].delegate;
    NetworkStatus status = [delegate.hostReachability currentReachabilityStatus];
    [self setIsConnectNetworkWithStatus:status];
}

- (void)setIsConnectNetworkWithStatus:(NetworkStatus)status
{
    switch (status) {
        case NotReachable:
            NSLog(@"====当前网络状态不可达=======");
            self.isConnectNetwork = NO;
            break;
            
        default:
            NSLog(@"====当前网络连接中=======");
            self.isConnectNetwork = YES;
            break;
    }
}

#pragma mark - UI

- (UIStatusBarStyle)preferredStatusBarStyle {
    return UIStatusBarStyleLightContent;
}

- (void)setupNavi{
    
    if (self.isNeedBackBtn == YES) {
        UIBarButtonItem *cancleItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"icon_back_tab_black"] style:(UIBarButtonItemStyleDone) target:self action:@selector(leftBarBtnClicked:)];
        cancleItem.tintColor = [UIColor darkGrayColor];
        self.navigationItem.leftBarButtonItem = cancleItem;
    }else{
        [self.navigationItem setHidesBackButton:YES];
    }
    
}

#pragma mark - Action

- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    [super touchesBegan:touches withEvent:event];
    
    [self.view endEditing:YES];
}

- (void)leftBarBtnClicked:(UIBarButtonItem*)leftBarBtn {
    [self.navigationController popViewControllerAnimated:YES];
}

@end
