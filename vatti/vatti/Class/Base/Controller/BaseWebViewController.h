//
//  BaseWebViewController.h
//  vatti
//
//  Created by Eric on 2018/7/5.
//  Copyright © 2018年 xlink. All rights reserved.
//

/**
 *  WebView基类
 *
 *  @author BZH
 */

#import "BaseViewController.h"

@interface BaseWebViewController : BaseViewController

///导航条title
@property (nonatomic,strong) NSString *titleString;

///url
@property (nonatomic,strong) NSString *urlString;


@end
