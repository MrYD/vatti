//
//  BaseWaterHeaterDeviceViewController.m
//  vatti
//
//  Created by MrYD on 2018/12/240.
//  Copyright © 2018年 xlink. All rights reserved.
//

#import "BaseWaterHeaterDeviceViewController.h"
#import "MainTabViewController.h"
#import "XLUtilTool.h"
#import "DeviceControlViewController.h"

@interface BaseWaterHeaterDeviceViewController ()

@end

@implementation BaseWaterHeaterDeviceViewController

- (void)buildData
{
    [super buildData];
    
    @weakify(self)
    
    [[RACObserve(self.deviceModel, connected) deliverOnMainThread] subscribeNext:^(id  _Nullable x){
        @strongify(self)
        [self configureConnectedStateChanged];
    }];
    
    [[RACObserve(self.deviceModel, master_err).distinctUntilChanged deliverOnMainThread] subscribeNext:^(id  _Nullable x) {
        @strongify(self)
        [self configureMasterErrorChage];
    }];
}

#pragma mark - Data

- (void)configureConnectedStateChanged
{
    Class currentClass = [self class];
    BOOL b1 = currentClass == [DeviceControlViewController class];
    
    if (b1 == YES) {
        return;
    }
    
    BOOL b2 = self.deviceModel.connected == NO;
    if (b2 == YES) {
        [self alertHintWithMessage:@"设备连接已断开，无法操作" handler:^(UIAlertAction *action, NSUInteger tag) {
            //回到设备控制界面
            NSArray *temp = self.navigationController.viewControllers;
            for (id vc in temp) {
                BOOL b2 = [vc isKindOfClass:[DeviceControlViewController class]];
                
                if (b2 == YES) {
                    [self.navigationController popToViewController:vc animated:YES];
                    break;
                }
            }
            
        }];
    }
}

- (void)configureMasterErrorChage {
    Class currentClass = [self class];
    BOOL b1 = currentClass == [DeviceControlViewController class];
    
    if (b1 == YES) {
        return;
    }
    
    BOOL b2 = [self getIsNoError];
    if (b2 == NO) {
        [self alertHintWithMessage:@"设备发生故障，无法操作" handler:^(UIAlertAction *action, NSUInteger tag) {
            //回到设备控制界面
            NSArray *temp = self.navigationController.viewControllers;
            for (id vc in temp) {
                BOOL b2 = [vc isKindOfClass:[DeviceControlViewController class]];
                
                if (b2 == YES) {
                    [self.navigationController popToViewController:vc animated:YES];
                    break;
                }
            }
            
        }];
    }
}

/**
 获取设备是否可控
 */
- (BOOL)getIsDeviceCanControlWithIsNeedAlert:(BOOL)isNeedAlert
{
    NSString *currUserID = [XLDataSource shareInstance].user.userId.stringValue;
    
    BOOL b1 = self.deviceModel.priority_flag == 0;//设备空闲
    BOOL b2 = self.deviceModel.priority_flag == 2;//设备正在被app控制
    BOOL b3 = [self.deviceModel.user_id isEqualToString:currUserID];//当前操作设备的用户是登录用户
    BOOL b4 = b2 && b3;//是当前app在操作
    
    BOOL canControl = b1 || b4;
    if (canControl == NO && isNeedAlert == YES) {
        //弹框
        [self alertHintWithMessage:@"无法操作，其他人正在操作设备"];
    }
    
    return canControl;
}

/**
 是否无故障
 */
- (BOOL)getIsNoError
{
    BOOL b1 = self.deviceModel.master_err < 1;
    BOOL b2 = self.deviceModel.master_err > 18;
    
    BOOL isNoError = b1 || b2;
    return isNoError;
}

/**
 生成一个带有用户ID端点的数组
 */
- (NSMutableArray <XLinkDataPoint*>*)addUserIdDataPointWithDataPointsArray:(NSArray <XLinkDataPoint*>*)dataPoints
{
    self.deviceModel.user_id = XL_DATA_SOURCE.user.userId.stringValue;
    XLinkDataPoint *dataPointUserID = [XLinkDataPoint dataPointWithType:XLinkDataTypeString withIndex:40 withValue:self.deviceModel.user_id];
    
    NSMutableArray *mDataPoints = [NSMutableArray array];
    [mDataPoints addObjectsFromArray:dataPoints];
    [mDataPoints addObject:dataPointUserID];
    
    return [mDataPoints mutableCopy];
}

@end
