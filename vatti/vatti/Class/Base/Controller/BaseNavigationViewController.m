//
//  BaseNavigationViewController.m
//  vatti
//
//  Created by 许瑞邦 on 2018/4/28.
//  Copyright © 2018年 xlink. All rights reserved.
//

#import "BaseNavigationViewController.h"

@interface BaseNavigationViewController ()

@end

@implementation BaseNavigationViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.navigationBar.barStyle = UIBarStyleDefault;
    self.navigationBar.backgroundColor = [UIColor whiteColor];

}

- (BOOL)navigationBar:(UINavigationBar *)navigationBar shouldPushItem:(UINavigationItem *)item {
    UIBarButtonItem *back = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:nil action:nil];
    item.backBarButtonItem = back;
    return YES;
}

- (void)pushViewController:(UIViewController *)viewController animated:(BOOL)animated{
    // 如果viewController不是最早push进来的子控制器 ，隐藏底部的工具条
    if (self.childViewControllers.count > 0) {
        viewController.hidesBottomBarWhenPushed = YES;
    }
    [super pushViewController:viewController animated:animated];

}


@end
