//
//  BaseDeviceViewController.m
//  vatti
//
//  Created by BZH on 2018/4/28.
//  Copyright © 2018年 xlink. All rights reserved.
//

#import "BaseDeviceViewController.h"
#import "DeviceControlViewController.h"
#import "XLinkSetDataPointTask.h"
#import "MainTabViewController.h"
#import "XLUtilTool.h"

@interface BaseDeviceViewController ()

@end

@implementation BaseDeviceViewController

- (void)viewDidLoad {
    [super viewDidLoad];
}

- (void)initConfigure
{
    [super initConfigure];
    
    self.isResponseChange = YES;
}

- (void)initSubViews
{
    [super initSubViews];
}

- (void)buildData
{
    [super buildData];
    
    @weakify(self)
    //监控订阅关系取消
    [[[kXLNotificationCenter rac_addObserverForName:XLNotificationDeviceUnSubscribe object:nil] deliverOnMainThread] subscribeNext:^(NSNotification * _Nullable x) {
        @strongify(self)
        [self configureXLNotificationDeviceUnSubscribe:x];
    }];
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    
    self.isResponseChange = YES;
}

#pragma mark - Data

- (void)configureXLNotificationDeviceUnSubscribe:(NSNotification*)no
{
    DeviceModel *noDeviceModel = (DeviceModel*)no.object;
    
    if ([noDeviceModel.device isEqualToDevice:self.deviceModel.device] == YES) {
        [self alertHintWithMessage:@"您的设备管理权限已被移除" handler:^(UIAlertAction *action, NSUInteger tag) {
            
            [XL_DATA_SOURCE.user removeDeviceModel:noDeviceModel];
            
            //回到首页
            MainTabViewController *tabbar = [[MainTabViewController alloc] init];
            [XLUtilTool setRootViewController:tabbar animationOptions:UIViewAnimationOptionTransitionCrossDissolve duration:0.3];
        }];
    }
    
}

/**
 判断是否设置相同任务
 如果是，则取消上一个任务
 */
- (void)cancelLastSameTaskWithNewDataPointsArray:(NSArray <XLinkDataPoint*> *)mDataPoints
{
    if (self.setDataPointTask != nil) {
        
        NSArray <XLinkDataPoint*> *lastDataPointsArray = [XLServiceManager shareInstance].deviceService.dataPointArray;
        
        NSArray *lastDataPointsIndexArray = [lastDataPointsArray valueForKey:@"index"];
        NSArray *currDataPointsIndexArray = [mDataPoints valueForKey:@"index"];
        
        if ([lastDataPointsIndexArray isEqualToArray:currDataPointsIndexArray]) {
            NSLog(@"已有相同任务，取消上一个");
            [self.setDataPointTask cancel];
            self.setDataPointTask = nil;
        }
        
    }
}

/**
 处理发送端点前的逻辑
 */
- (void)startLogicalBefortSet
{
    //是否响应UI更新
    self.isResponseChange = NO;
    NSLog(@"self.isResponseChange =============================================================== NO;");
    //定时器
    if (self.timer != nil) {
        [self.timer invalidate];
        self.timer = nil;
    }
    
    self.timer = [NSTimer scheduledTimerWithTimeInterval:6 target:self selector:@selector(timerFinished:) userInfo:nil repeats:NO];
}

/**
 定时器到达计时时间
 */
- (void)timerFinished:(NSTimer *)timer
{
    //子类需要继承该方法
    self.isResponseChange = YES;
}

@end
