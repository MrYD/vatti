//
//  BaseDeviceViewController.h
//  vatti
//
//  Created by BZH on 2018/4/28.
//  Copyright © 2018年 xlink. All rights reserved.
//

#import "BaseViewController.h"
#import "WaterHeaterDeviceModel.h"

/**
 *  设备控制基类
 *
 *  @author BZH
 */

@interface BaseDeviceViewController<ObjectType : DeviceModel *> : BaseViewController

///设备
@property (strong, nonatomic) ObjectType deviceModel;
//setDataPointTask
@property (strong, nonatomic) XLinkSetDataPointTask *setDataPointTask;
///是否响应UI更新
@property (assign, nonatomic) BOOL isResponseChange;
///timer
@property (strong, nonatomic) NSTimer *timer;

#pragma mark - 供子类调用的方法

/**
 判断是否设置相同任务
 如果是，则取消上一个任务
 */
- (void)cancelLastSameTaskWithNewDataPointsArray:(NSArray <XLinkDataPoint*> *)mDataPoints;

/**
 处理发送端点前的逻辑
 */
- (void)startLogicalBefortSet;

#pragma mark - 供子类继承的方法
/**
 定时器结束
 */
- (void)timerFinished:(NSTimer*)timer;

@end
