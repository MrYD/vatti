//
//  BaseWaterHeaterDeviceViewController.h
//  vatti
//
//  Created by MrYD on 2018/12/240.
//  Copyright © 2018年 xlink. All rights reserved.
//

/**
 *  燃热设备控制基类
 *
 *  @author BZH
 */

#import "BaseDeviceViewController.h"
#import "WaterHeaterDeviceModel.h"

@interface BaseWaterHeaterDeviceViewController : BaseDeviceViewController<WaterHeaterDeviceModel *>

/**
 获取设备是否可控
 */
- (BOOL)getIsDeviceCanControlWithIsNeedAlert:(BOOL)isNeedAlert;

/**
 是否无故障
 */
- (BOOL)getIsNoError;

/**
 生成一个带有用户ID端点的数组
 */
- (NSMutableArray <XLinkDataPoint*>*)addUserIdDataPointWithDataPointsArray:(NSArray <XLinkDataPoint*>*)dataPoints;
@end
