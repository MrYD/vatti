//
//  XLModel.m
//  vatti
//
//  Created by 李叶 on 2018/5/22.
//  Copyright © 2018年 xlink. All rights reserved.
//

#import "XLModel.h"

@implementation XLModel

- (NSString *)description {
    return [self yy_modelDescription];
}

- (NSString *)debugDescription {
    return [self yy_modelDescription];
}

@end
