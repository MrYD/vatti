//
//  XLModel.h
//  vatti
//
//  Created by 李叶 on 2018/5/22.
//  Copyright © 2018年 xlink. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <MJExtension/MJExtension.h>

/**
 所有模型类都要继承此模型<重写了NSlog方法和po方法>
 */
@interface XLModel : NSObject

@end
